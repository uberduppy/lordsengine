/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.talwood.lords.comparators;

import java.util.Collections;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.talwood.lords.containers.QuestContainer;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.quests.Quest;

/**
 *
 * @author twalker
 */
public class QuestStackComparatorTest {
    
    public QuestStackComparatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCompare() {
        System.out.println("Stacking for Quests");
        QuestContainer qc = new QuestContainer(Expansions.Classic);
        List<Quest> quests = qc.getQuests();
        QuestStackComparator instance = new QuestStackComparator(Quests.PlacateTheWalkingStatue, 
                Quests.BribeTheShipwrights, Quests.RecruitLieutenant, Quests.DefeatUprisingFromUndermountain);
        Collections.shuffle(quests);
        Collections.shuffle(quests);
        Collections.sort(quests, instance);
        
        assertEquals(Quests.PlacateTheWalkingStatue, quests.get(0).getQuestInfo());
        assertEquals(Quests.BribeTheShipwrights, quests.get(1).getQuestInfo());
        assertEquals(Quests.RecruitLieutenant, quests.get(2).getQuestInfo());
        assertEquals(Quests.DefeatUprisingFromUndermountain, quests.get(3).getQuestInfo());
        // TODO review the generated test code and remove the default call to fail.
    }
    
}

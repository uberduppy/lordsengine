package org.talwood.lords.containers;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.quests.Quest;

/**
 *
 * @author twalker
 */
public class QuestContainerTest {
    
    public QuestContainerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    @Test
    public void testAllQuestsDefined() {
        QuestContainer qc = new QuestContainer(Expansions.Classic, Expansions.Skullport, Expansions.Undermountian);
        assertEquals(Quests.values().length, qc.getQuests().size());
        assertEquals(120, qc.getQuests().size());
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.talwood.lords.containers;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.Expansions;

/**
 *
 * @author twalker
 */
public class BuildingContainerTest {
    
    public BuildingContainerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    @Test
    public void testBuildingsWithVariableActivation() {
        BuildingContainer bc = new BuildingContainer(Expansions.Classic, Expansions.Skullport, Expansions.Undermountian);
        for(Building b : bc.getAllBuildings()) {
            if(b.getActivationCostOptions().size() > 0 && !b.getBuildingActions().contains(BuildingActions.RequiresVariableActivation)) {
                fail("Activation options failure for " + b.getBuildingInfo().getName());
            }
            if(b.getActivationCostOptions().size() == 0 && b.getBuildingActions().contains(BuildingActions.RequiresVariableActivation)) {
                fail("Activation options failure for " + b.getBuildingInfo().getName());
            }
        }
    }
    @Test
    public void testBuildingsWithVariableRewards() {
        BuildingContainer bc = new BuildingContainer(Expansions.Classic, Expansions.Skullport, Expansions.Undermountian);
        for(Building b : bc.getAllBuildings()) {
            if(b.getBuildingRewardOptions().size() > 0 && !b.getBuildingActions().contains(BuildingActions.RequiresVariableReward)) {
                fail("Activation options failure for " + b.getBuildingInfo().getName());
            }
            if(b.getBuildingRewardOptions().size() == 0 && b.getBuildingActions().contains(BuildingActions.RequiresVariableReward)) {
                fail("Activation options failure for " + b.getBuildingInfo().getName());
            }
        }
    }
    @Test
    public void testBuildingsWithVariableOwnerRewards() {
        BuildingContainer bc = new BuildingContainer(Expansions.Classic, Expansions.Skullport, Expansions.Undermountian);
        for(Building b : bc.getAllBuildings()) {
            if(b.getOwnerRewardOptions().size() > 0 && !b.getBuildingActions().contains(BuildingActions.RequiresVariableOwnerReward)) {
                fail("Activation options failure for " + b.getBuildingInfo().getName());
            }
            if(b.getOwnerRewardOptions().size() == 0 && b.getBuildingActions().contains(BuildingActions.RequiresVariableOwnerReward)) {
                fail("Activation options failure for " + b.getBuildingInfo().getName());
            }
        }
    }
    @Test
    public void testAllBuildingsDefined() {
        
        BuildingContainer bc = new BuildingContainer(Expansions.Classic, Expansions.Skullport, Expansions.Undermountian);
        assertEquals(Buildings.values().length, bc.getAllBuildings().size());
    }
    
}

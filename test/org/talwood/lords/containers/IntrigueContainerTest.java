/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.talwood.lords.containers;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Intrigues;

/**
 *
 * @author twalker
 */
public class IntrigueContainerTest {
    
    public IntrigueContainerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }



    @Test
    public void testAllBaseIntriguesDefined() {
        IntrigueContainer ic = new IntrigueContainer(Expansions.Classic, Expansions.Skullport, Expansions.Undermountian);
        assertEquals(Intrigues.values().length, ic.getAllBaseIntrigues().size());
    }
    
}

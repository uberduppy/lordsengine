/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.talwood.lords.testobjects;

import java.io.File;
import java.io.FileOutputStream;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.containers.BuildingContainer;
import org.talwood.lords.containers.IntrigueContainer;
import org.talwood.lords.containers.QuestContainer;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Houses;
import org.talwood.lords.helpers.HtmlDisplayHelper;
import org.talwood.lords.helpers.TupleContainer;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.intrigues.Intrigue;
import org.talwood.lords.quests.Quest;
import org.talwood.lords.renderers.BuildingRenderer;
import org.talwood.lords.renderers.H2Renderer;
import org.talwood.lords.renderers.H3Renderer;
import org.talwood.lords.renderers.IntrigueRenderer;
import org.talwood.lords.renderers.QuestRenderer;

/**
 *
 * @author twalker
 */
public class TestGameBoardTest {
    
    public TestGameBoardTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGenerateBuildingList() {
        TupleContainer<HtmlElement, HtmlElement> result = HtmlDisplayHelper.startHtmlToTable(true);
        HtmlElement tra = result.getRightSide().createChildElement(HtmlElementType.TR);
        HtmlElement tda = tra.createChildElement(HtmlElementType.TD);

        HtmlElement table = tda.createChildElement(HtmlElementType.TABLE);
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        BuildingContainer bc = new BuildingContainer(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        int trCount = 0;
        for (Building b : bc.getAllBuildings()) {
            new BuildingRenderer(b, false).render(tr.createChildElement(HtmlElementType.TD));
            trCount++;
            if(trCount % 4 == 0) {
                tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
            }
        }
        try {
            FileOutputStream fos = new FileOutputStream(new File("c:/duelssol/bldgs.html"));
            fos.write(result.getLeftSide().render().getBytes());
            fos.flush();
            fos.close();
        } catch (Exception ex) {
            fail("Oops");
        }
        
    }
    @Test
    public void testGenerateIntrigueList() {
        TupleContainer<HtmlElement, HtmlElement> result = HtmlDisplayHelper.startHtmlToTable(true);
        HtmlElement tra = result.getRightSide().createChildElement(HtmlElementType.TR);
        HtmlElement tda = tra.createChildElement(HtmlElementType.TD);

        HtmlElement table = tda.createChildElement(HtmlElementType.TABLE);
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        IntrigueContainer ic = new IntrigueContainer(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        
        
        int trCount = 0;
        for (Intrigue i : ic.getAllBaseIntrigues()) {
            new IntrigueRenderer(i).render(tr.createChildElement(HtmlElementType.TD));
            trCount++;
            if(trCount % 4 == 0) {
                tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
            }
        }
        try {
            FileOutputStream fos = new FileOutputStream(new File("c:/duelssol/intrigue.html"));
            fos.write(result.getLeftSide().render().getBytes());
            fos.flush();
            fos.close();
        } catch (Exception ex) {
            fail("Oops");
        }
    }
    @Test
    public void testGenerateQuestList() {
        TupleContainer<HtmlElement, HtmlElement> result = HtmlDisplayHelper.startHtmlToTable(true);

        HtmlElement tra = result.getRightSide().createChildElement(HtmlElementType.TR);
        HtmlElement tda = tra.createChildElement(HtmlElementType.TD);

        HtmlElement table = tda.createChildElement(HtmlElementType.TABLE);
        
        
        QuestContainer wqc = new QuestContainer(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        
        for(Expansions e : Expansions.values()) {
            HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
            new H2Renderer("Expansion: " + e.getDescription()).render(tr.createChildElement(HtmlElementType.TD, HtmlAttr.colspan("4")));
            for(Houses h : Houses.values()) {
                tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
                new H3Renderer(h.getDescription()).render(tr.createChildElement(HtmlElementType.TD, HtmlAttr.colspan("4")));
                tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
                int trCount = 0;
                for (Quest quest : wqc.getQuests()) {
                    if(quest.getQuestInfo().getHouse() == h && quest.getQuestInfo().getExpansion() == e) {
                        new QuestRenderer(quest).render(tr.createChildElement(HtmlElementType.TD));
                        trCount++;
                        if(trCount % 4 == 0) {
                            tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
                        }
                    }
                }
            }
            
        }
        
//        int trCount = 0;
//        for (WaterdeepBasicQuest quest : wqc.getQuestList()) {
//            new QuestRenderer(quest).render(tr.createChildElement(HtmlElementType.TD));
//            trCount++;
//            if(trCount % 4 == 0) {
//                tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
//            }
//        }
        
        try {
            FileOutputStream fos = new FileOutputStream(new File("c:/duelssol/quests.html"));
            fos.write(result.getLeftSide().render().getBytes());
            fos.flush();
            fos.close();
        } catch (Exception ex) {
            fail("Oops");
        }
        
    }
    
}

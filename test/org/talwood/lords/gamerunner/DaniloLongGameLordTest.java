/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.talwood.lords.gamerunner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.Lords;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.Res;
import org.talwood.lords.resources.Resources;

/**
 *
 * @author twalker
 */
public class DaniloLongGameLordTest {
    
    public DaniloLongGameLordTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testUseDaniloSecond() {
        GameRunner runner = new GameRunner(true);
        
        GamePlayer dummy = runner.addPlayer(Lords.LarissaNeathal, "Useful Idiot", Long.valueOf(9));
        GamePlayer todd = runner.addPlayer(Lords.DaniloThann, "Todd", Long.valueOf(10));

        runner.setupQuests(Quests.SwindleTheBuildersGuilds,
                Quests.RecruitLieutenant,
                Quests.LureArtisansOfMirabar,
                Quests.DiplomaticMissionToSuzail,
                Quests.ThreatenTheBuildersGuilds,
                Quests.ProtectTheHouseOfWonder,
                Quests.SeizeControlOfTheBloodyHand,
                Quests.ExploreAhghaironsTower,
                Quests.PlaceSleeperAgentInSkullport,
                Quests.RecruitForCityWatch,
                Quests.FenceGoodsForDukeOfDarkness,
                Quests.QuellMercenaryUprising,
                Quests.InstallSpyInCastleWaterdeep,
                Quests.TrainBladesingers,
                Quests.BribeTheShipwrights,
                Quests.StudyInTheLibrarium,
                Quests.PlacateTheWalkingStatue,
                Quests.StudyTheIlluskArch,
                Quests.SafeguardEltorchulMage,
                Quests.EstablishNewMerchantGuild,
                Quests.InvestigateThayanVessel,
                Quests.ResurrectDeadWizards,
                Quests.BreakIntoBlackstaffTower,
                Quests.ExploreTrobriandsGraveyard,
                Quests.ConfrontTheXanathar,
                Quests.ProcureStolenGoods,
                Quests.SealAnEntranceToSkullport,
                Quests.BolsterGriffinCavalry,
                Quests.StealSpellbookFromSilverhand,
                Quests.InfiltrateHalastersCircle,
                Quests.RenewGuardsAndWards,
                Quests.RecruitPaladinsForTyr,
                Quests.CreateShrineToOghma,
                Quests.BuryTheBodies,
                Quests.AmbushArtorMorlin,
                Quests.BattleInMuiralsGauntlet,
                Quests.RaidOnUndermountain,
                Quests.ThinTheCityWatch,
                Quests.WakeTheSixSleepers,
                Quests.RescueClericsOfTymora,
                Quests.HealFallenGrayHandSoldiers,
                Quests.RansackWhitehelmsTomb,
                Quests.AllyWithHouseThann,
                Quests.SendAidToTheHarpers,
                Quests.InvestigateAberrantInfestation,
                Quests.AssassinateRivals,
                Quests.ShelterZhentarimAgents,
                Quests.ConvertANobleToLathander,
                Quests.PatrolDockWard,
                Quests.EstablishShadowThievesGuild,
                Quests.RecruitAcademyCastoffs,
                Quests.RecruitForBlackstaffAcademy,
                Quests.EstablishCultCell,
                Quests.FundAlchemicalResearch,
                Quests.HostFestivalForSune,
                Quests.BanishEvilSpirits,
                Quests.DonateToTheCity,
                Quests.ImprovePrisonSecurity,
                Quests.PayFines,
                Quests.PlunderTheIslandTemple,
                Quests.FundPilgrimageOfWaukeen,
                Quests.ExposeRedWizardsSpies,
                Quests.FormAllianceWithTheRashemi,
                Quests.InstituteReforms,
                Quests.ResearchChronomacy,
                Quests.DefeatUprisingFromUndermountain,
                Quests.SurviveArcturiasTransformarion,
                Quests.EliminateVampireCoven,
                Quests.SponsorBountyHunters,
                Quests.SanctifyTempleToOghma,
                Quests.DomesticateOwlbears,
                Quests.BolsterCityGuard,
                Quests.DefendTheLanceboardRoom,
                Quests.RaidOrcStronghold,
                Quests.SaveKidnappedNobles,
                Quests.TakeOverRivalOrganization,
                Quests.SealGateToCyricsRealm,
                Quests.DefendTheYawningPortal,
                Quests.UnleashCrimeSpree,
                Quests.ImpersonateAdarBrentNoble,
                Quests.ObtainBuildersPlans,
                Quests.SpyOnTheHouseOfLight,
                Quests.RepelSeawraiths,
                Quests.UncoverDrowPlot,
                Quests.RetrieveAncientArtifacts,
                Quests.UncoverForbiddenLore,
                Quests.SanctifyDesecratedTemple,
                
                
                
                Quests.DiscoverHiddenTempleOfLolth,
                Quests.ExposeCultCorruption,
                Quests.ExpandGuildActivities,
                Quests.GiveHonorToMask,
                Quests.DeliverAnUltimatum,
                Quests.EstablishTempleToIbrandul,
                Quests.RescueVictimFromTheSkulls,
                Quests.BuildReputationInSkullport,
                Quests.RecoverTheFlameOfTheNorth,
                Quests.StealGemsFromTheBoneThrone,
                Quests.TrainCastleguards,
                Quests.EstablishHarpersSafeHouse
                );

        runner.setupBuildings(Buildings.SmugglersDock, 
                Buildings.DragonTower, 
                Buildings.RoomOfWisdom, 
                Buildings.Skulkway,
                Buildings.ShadowduskHold,
                Buildings.HighDukesTomb,
                Buildings.CitadelOfTheBloodyHand,
                Buildings.Librarium,
                Buildings.PalaceOfWaterdeep,
                Buildings.MonstersMadeToOrder,
                Buildings.PoisonedQuill,
                Buildings.HouseOfGoodSpirits,
                Buildings.ThreePearls,
                Buildings.HellHoundsMuzzle,
                Buildings.HallOfManyPillars,
                Buildings.NewOlamn,
                Buildings.HouseOfTheMoon,
                Buildings.TowerOfLuck,
                Buildings.ThrownGauntlet
                );
        
        runner.setupIntrigues(
                Intrigues.BlackMarketMoney, 
                Intrigues.IronRingSlaves, 
                Intrigues.ArchitecturalInnovation, 
                Intrigues.InformationBroker1, 
                Intrigues.GraduationDay1,
                Intrigues.GraduationDay2,
                Intrigues.DarkDaggerAssassination,
                Intrigues.SpreadTheWealth1,
                Intrigues.Conscription1,
                Intrigues.SpreadTheWealth2,
                Intrigues.CrimeWave1,
                Intrigues.InformationBroker2,
                Intrigues.GoodFaith1,
                Intrigues.CrimeWave2,
                Intrigues.CallForAssistance1,
                Intrigues.Repent1,
                Intrigues.PlacateAngryMerchants,
                Intrigues.OrganizedCrime,
                Intrigues.RecruitmentDrive,
                Intrigues.QuellRiots,
                Intrigues.Conscription2,
                Intrigues.MindFlayerMercenaries,
                Intrigues.GoodFaith2,
                Intrigues.RecruitSpies,
                Intrigues.FreeDrinks2,
                Intrigues.RecallAgent1,
                Intrigues.Repent2,
                Intrigues.StampOutCultists,
                Intrigues.Repent3,
                Intrigues.CallInAFavor1,
                Intrigues.SponsorApprentices,
                
                Intrigues.CallForAssistance2,
                Intrigues.ClearRustMonsterNest,
                Intrigues.RecallAgent2,
                Intrigues.FriendlyLoan,
                
                Intrigues.RepelDrowInvaders,
                Intrigues.Scapegoat1,
                Intrigues.MercenaryContract,
                Intrigues.Scapegoat2,
                Intrigues.AlliedFaiths,
                Intrigues.UnexpectedSuccess1,
                Intrigues.UnexpectedSuccess2,
                Intrigues.UnlikelyAssistance,
                Intrigues.FreeDrinks1,
                Intrigues.CallForAdventurers1,
                Intrigues.CallForAdventurers2,
                Intrigues.ChangeOfPlans,
                Intrigues.FoilTheZhentarim,
                Intrigues.CallInAFavor2,
                Intrigues.HonorAmongThieves,
                
                
                Intrigues.ResearchAgreement,
                Intrigues.DonationsForCyric,
                
                Intrigues.EvadeAssassins,
                Intrigues.FendOffBandits,
                Intrigues.UnveilAbyssalAgent,
                Intrigues.SubdueIllithidMenace,
                
                
                Intrigues.TaxCollection1,
                Intrigues.TaxCollection2,
                
                Intrigues.BribeTheWatch,
                Intrigues.Proselytize,
                Intrigues.RequestAssistance,
                Intrigues.SummonTheFaithful,
                Intrigues.TaxRevolt,
                
                
                
                Intrigues.ReleaseTheHounds1,
                Intrigues.ReleaseTheHounds2,
                Intrigues.Doppelganger,
                
                Intrigues.HuntHiddenGhoul,
                Intrigues.CoverUpScandal,
                
                
                Intrigues.HonorableExample,
                Intrigues.BiddingWar1,
                Intrigues.BiddingWar2,
                Intrigues.BiddingWar3,
                Intrigues.ExposeCorruption1,
                Intrigues.ExposeCorruption2,
                Intrigues.BribeAgent1,
                Intrigues.BribeAgent2,
                
                
                Intrigues.Blackmail,
                
                
                
                Intrigues.ArcaneMishap1,
                Intrigues.LackOfFaith1,
                Intrigues.Ambush1,
                Intrigues.LackOfFaith2,
                Intrigues.Ambush2,
                
                Intrigues.ArcaneMishap2,
                Intrigues.Assassination1,
                Intrigues.Assassination2,
                Intrigues.SpecialAssignment1,
                Intrigues.SpecialAssignment2
                
                
                
                );
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        
        // TURN 1.
        doTurnOne(runner, dummy, todd);
        doTurnTwo(runner, dummy, todd);
        doTurnThree(runner, dummy, todd);
        doTurnFour(runner, dummy, todd);
        doTurnFive(runner, dummy, todd);
        doTurnSix(runner, dummy, todd);
        doTurnSeven(runner, dummy, todd);
        doTurnEight(runner, dummy, todd);
        
        runner.finishGame();
        
//        runner.listCompletableQuests(todd, 1);
        //runner.debugShowQuests();
        
        runner.renderToFile("c:/duelssol/lordslong.html");
    }

    
    public void doTurnOne(GameRunner runner, GamePlayer dummy, GamePlayer todd) {
        runner.startTurn();
        
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.SmugglersDock)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.ThreeFightersOneRogue)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.EntryWell)
                .andTakeQuest(Quests.ThreatenTheBuildersGuilds)
                .andPlayIntrigue(Intrigues.BlackMarketMoney)
                .andChooseOpponentForReward(todd)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.IronRingSlaves)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.ArchitecturalInnovation)
                .andBuyBuilding(Buildings.RoomOfWisdom)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.SkullIsland)
                .andTakeVariableReward(ResourceChoices.OneClericOneWizardOneCorruption)
                .andCompleteQuest(Quests.RecruitLieutenant)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BlackstaffTower)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.InformationBroker1)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.FieldOfTriumph)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.FieldOfTriumph)
                .andCompleteQuest(Quests.DiplomaticMissionToSuzail)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.SlaversMarket)
                .andEndTurn();
        dummy = dummy.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.AurorasRealm)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.HallOfTheVoice)
                .andTakeQuest(Quests.SeizeControlOfTheBloodyHand)
                .andEndTurn();

    }    
    
    public void doTurnTwo(GameRunner runner, GamePlayer dummy, GamePlayer todd) {
        runner.startTurn();
        // Study in the librarium???
//        
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.Skulkway)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.GraduationDay1)
                .andChooseOpponentForReward(dummy)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.SmugglersDock)
                .andCompleteQuestInCliffwatchInn(Quests.ProtectTheHouseOfWonder)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.FourFighters)
                .andCompleteQuest(Quests.SeizeControlOfTheBloodyHand)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph)
                .andCompleteQuest(Quests.ThreatenTheBuildersGuilds)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.DarkDaggerAssassination)
                .andCompleteQuestInCliffwatchInn(Quests.FenceGoodsForDukeOfDarkness)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.GraduationDay2)
                .andChooseOpponentForReward(dummy)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.DragonTower)
                .andCompleteQuestInCliffwatchInn(Quests.PlaceSleeperAgentInSkullport)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
//        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.Skulkway, Buildings.DragonTower)
//                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.InstallSpyInCastleWaterdeep)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.Skulkway)
                .andTakeVariableOwnerReward(ResourceChoices.OneFighter)
                .andCompleteQuestInCliffwatchInn(Quests.ExploreAhghaironsTower)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.DragonTower)
                .andCompleteQuestInCliffwatchInn(Quests.BribeTheShipwrights)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
    }

    public void doTurnThree(GameRunner runner, GamePlayer dummy, GamePlayer todd) {
        runner.startTurn();
//        // Start turn 3
        dummy = dummy.placeAgent(Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo)
                .andCompleteQuest(Quests.LureArtisansOfMirabar)
                .andTakeBuildingFromBuildersHallAsQuestReward(Buildings.Librarium)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.SlaversMarket)
                .andCompleteQuest(Quests.SwindleTheBuildersGuilds)
                .andTakeBuildingsFromBuildersHallAsQuestReward(Buildings.CitadelOfTheBloodyHand, Buildings.PalaceOfWaterdeep)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.CrimeWave1)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.StudyInTheLibrarium)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.SkullIsland)
                .andTakeVariableReward(ResourceChoices.OneClericOneFighterOneCorruption)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.Conscription1)
                .andChooseOpponentForReward(dummy)
                .andPlayIntrigueDrawnAsAction()
                .andChooseOpponentForReward(dummy)
                .andEndTurn();

        dummy = dummy.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.PlacateTheWalkingStatue)
                .andCompleteQuest(Quests.PlacateTheWalkingStatue)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.GoodFaith1)
                .andChooseOpponentForReward(dummy)
                .andPlayIntrigueDrawnAsAction()
                .andStealResources(dummy, ResourceChoices.TwoFighters)
                .andCompleteQuestInCliffwatchInn(Quests.QuellMercenaryUprising)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.ThreePearls)
                .andPayVariableActivation(ResourceChoices.TwoRogues)
                .andTakeVariableReward(ResourceChoices.OneRogueTwoWizards)
                .andTakeVariableOwnerReward(ResourceChoices.OneCleric)
                .andCompleteQuestInCliffwatchInn(Quests.StudyTheIlluskArch)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();

        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.EstablishNewMerchantGuild)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();

        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.ShadowduskHold)
                .andCompleteQuestInCliffwatchInn(Quests.InvestigateThayanVessel)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andReplaceGenericResources(new Resources(Res.cleric(1), Res.wizard(1)))
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.Librarium, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.RecruitForCityWatch)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.ResurrectDeadWizards)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
    }

    public void doTurnFour(GameRunner runner, GamePlayer dummy, GamePlayer todd) {
        runner.startTurn();
        todd = todd.placeAmbassador(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo)
                .andCompleteQuestInCliffwatchInn(Quests.BreakIntoBlackstaffTower)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo)
                .andEndTurn();
//        runner.debugNotifyNextIntrigue();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.PlacateAngryMerchants)
                .andChooseOpponentForMandatoryQuest(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.ExploreTrobriandsGraveyard)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph)
                .andCompleteMandatoryQuest(Quests.PlacateAngryMerchants)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.RecruitmentDrive)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborThree, Buildings.Librarium)
                .andPlayIntrigueDrawnAsAction()
                .andChooseOpponentForMandatoryQuest(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.ProcureStolenGoods)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.MonstersMadeToOrder)
                .andCompleteMandatoryQuest(Quests.QuellRiots)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.OrganizedCrime)
                .andSelectTargetBuildings(Buildings.CitadelOfTheBloodyHand, Buildings.ThreePearls)
                .andPlayIntrigueDrawnAsAction()
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.ConfrontTheXanathar)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.SealAnEntranceToSkullport)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.BolsterGriffinCavalry)
                .andReturnOneQuestResourceUsed(Res.fighter(1))
                .andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.ThreePearls)
                .andCompleteQuestInCliffwatchInn(Quests.SafeguardEltorchulMage)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.Skulkway)
                .andTakeVariableOwnerReward(ResourceChoices.OneFighter)
                .andCompleteQuestInCliffwatchInn(Quests.StealSpellbookFromSilverhand)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.ThreePearls)
                .andPayVariableActivation(ResourceChoices.TwoRogues)
                .andTakeVariableReward(ResourceChoices.OneClericTwoWizards)
                .andTakeVariableOwnerReward(ResourceChoices.OneWizard)
                .andCompleteQuestInCliffwatchInn(Quests.InfiltrateHalastersCircle)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.RenewGuardsAndWards)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
    }

    public void doTurnFive(GameRunner runner, GamePlayer dummy, GamePlayer todd) {
        runner.startTurn();
        todd = todd.placeAmbassador(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo)
                .andCompleteQuestInCliffwatchInn(Quests.RecruitPaladinsForTyr)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.PoisonedQuill)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.StampOutCultists)
                .andChooseOpponentForMandatoryQuest(dummy)
                .andPlayIntrigueDrawnAsAction()
                .andCompleteQuestInCliffwatchInn(Quests.AmbushArtorMorlin)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth)
                .andCompleteMandatoryQuest(Quests.StampOutCultists)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.RecruitSpies)
                .andChooseOpponentForReward(dummy)
                .andPlayIntrigueDrawnAsAction()
                .andTakeVariableIntrigueReward(ResourceChoices.OneWizard)
//                .andSelectTargetBuildings(Buildings.WaterdeepHarborThree, Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.CreateShrineToOghma)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BlackstaffTower)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.SponsorApprentices)
                .andSelectTargetBuildings(Buildings.ShadowduskHold)
                .andPlayIntrigueDrawnAsAction()
                .andStealResources(dummy, ResourceChoices.OneFighterOneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.BuryTheBodies)
                .andReturnOneQuestResourceUsed(Res.fighter(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.BattleInMuiralsGauntlet)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.ThreeFightersOneRogue)
//                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.RaidOnUndermountain)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.HallOfMirrors)
                .andTakeVariableReward(ResourceChoices.OneCleric)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.PoisonedQuill)
                .andPlayIntrigue(Intrigues.ClearRustMonsterNest)
                .andChooseOpponentForMandatoryQuest(dummy)
                .andPlayIntrigueDrawnAsAction()
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.ThinTheCityWatch)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();

        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.ThreePearls)
                .andCompleteQuestInCliffwatchInn(Quests.WakeTheSixSleepers)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();

        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.RescueClericsOfTymora)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.WaterdeepHarborOne)
                .andCompleteQuestInCliffwatchInn(Quests.HealFallenGrayHandSoldiers)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.RansackWhitehelmsTomb)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
//
    }

    public void doTurnSix(GameRunner runner, GamePlayer dummy, GamePlayer todd) {
        runner.startTurn();
        todd = todd.placeAmbassador(Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo)
                .andCompleteQuestInCliffwatchInn(Quests.AllyWithHouseThann)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.RepelDrowInvaders)
                .andChooseOpponentForMandatoryQuest(dummy)
                .andPlayIntrigueDrawnAsAction()
                .andChooseOpponentForReward(dummy)
//                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.SendAidToTheHarpers)
                .andChooseOpponentForReward(dummy)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.HallOfManyPillars)
                .andCompleteMandatoryQuest(Quests.RepelDrowInvaders)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.MindFlayerMercenaries)
                .andCompleteQuestInCliffwatchInn(Quests.InvestigateAberrantInfestation)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.Conscription2)
                .andChooseOpponentForReward(dummy)
                .andPlayIntrigueDrawnAsAction()
                .andCompleteQuestInCliffwatchInn(Quests.TrainBladesingers)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.Librarium, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.AssassinateRivals)
                .andReturnOneQuestResourceUsed(Res.fighter(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.ShelterZhentarimAgents)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
//                .andReplaceGenericResources(new Resources(Res.wizard(1)))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.HallOfMirrors)
                .andTakeVariableReward(ResourceChoices.OneFighterOneRogue)
                .andCompleteMandatoryQuest(Quests.ClearRustMonsterNest)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.ConvertANobleToLathander)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andTakeQuestAsQuestReward(Quests.RecruitForBlackstaffAcademy)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.ThreePearls)
                .andCompleteQuestInCliffwatchInn(Quests.PatrolDockWard)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.Skulkway)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.EstablishShadowThievesGuild)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.ThreePearls)
                .andPayVariableActivation(ResourceChoices.OneFighterOneWizard)
                .andTakeVariableReward(ResourceChoices.OneFighterOneRogueOneWizard)
                .andTakeVariableOwnerReward(ResourceChoices.OneWizard)
                .andCompleteQuest(Quests.RecruitForBlackstaffAcademy)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.EstablishCultCell)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
    }
    
    public void doTurnSeven(GameRunner runner, GamePlayer dummy, GamePlayer todd) {
        runner.startTurn();
        todd = todd.placeAmbassador(Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo)
                .andCompleteQuestInCliffwatchInn(Quests.FundAlchemicalResearch)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.HellHoundsMuzzle)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.FoilTheZhentarim)
                .andChooseOpponentForMandatoryQuest(dummy)
                .andPlayIntrigueDrawnAsAction()
                .andTakeVariableIntrigueReward(ResourceChoices.TwoRogues)
                .andCompleteQuestInCliffwatchInn(Quests.HostFestivalForSune)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth)
                .andCompleteMandatoryQuest(Quests.FoilTheZhentarim)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.CallForAdventurers1)
                .andTakeVariableIntrigueReward(ResourceChoices.OneFighterOneWizard)
                .andPlayIntrigueDrawnAsAction()
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.BanishEvilSpirits)
                .andReplaceGenericResources(new Resources(Res.cleric(1)))
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        // Big Quests left
        // Bolster City Guard 25
        // plunder island temple 40
        // Defend the lanceboard 12 + 8 wildcard
        // institute reforms, return 3 corr
        // uncover drow plot return 2
        // improve prison security return 3
        // donate to the city return 3
        // save kidnapped nobles, return 3
        
        
        
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.InformationBroker2)
//                .andChooseOpponentForReward(dummy)
                .andPlayIntrigueDrawnAsAction()
                .andCompleteQuestInCliffwatchInn(Quests.DonateToTheCity)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BlackstaffTower)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.ImprovePrisonSecurity)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.Librarium, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.PayFines)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
////                .andReplaceGenericResources(new Resources(Res.wizard(1)))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.HallOfMirrors)
                .andTakeVariableReward(ResourceChoices.OneWizard)
//                .andCompleteMandatoryQuest(Quests.ClearRustMonsterNest)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.RecruitAcademyCastoffs)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.PlunderTheIslandTemple)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.HallOfManyPillars)
                .andPlayMultiIntrigue(Intrigues.EvadeAssassins)
                .andChooseOpponentForMandatoryQuest(dummy)
                .andPlayIntrigueDrawnAsAction()
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.FendOffBandits)
                .andChooseOpponentForMandatoryQuest(dummy)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.UnveilAbyssalAgent)
                .andChooseOpponentForMandatoryQuest(dummy)
                .andPlayIntrigueDrawnAsAction()
                .andSelectTargetBuildings(Buildings.Skulkway)
                .andFinishMultiIntrigue()
                .andCompleteQuestInCliffwatchInn(Quests.FundPilgrimageOfWaukeen)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.Skulkway)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.ExposeRedWizardsSpies)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.FormAllianceWithTheRashemi)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andTakeQuestAsQuestReward(Quests.SurviveArcturiasTransformarion)
                .andEndTurn();
    }

    public void doTurnEight(GameRunner runner, GamePlayer dummy, GamePlayer todd) {
        runner.startTurn();
        todd = todd.placeAmbassador(Buildings.HellHoundsMuzzle)
                .andTakeVariableOwnerReward(ResourceChoices.OneWizard)
                .andCompleteQuestInCliffwatchInn(Quests.InstituteReforms)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        // Dummy 1 of 6
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph)
                .andCompleteMandatoryQuest(Quests.FendOffBandits)
                .andEndTurn();
        //Agent 1 of 7
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.CitadelOfTheBloodyHand, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.ResearchChronomacy)
                .andRecallAgent(Buildings.ShadowduskHold)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        //Dummy 2 of 6
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andCompleteMandatoryQuest(Quests.EvadeAssassins)
                .andEndTurn();
        //Agent 1 of 7 (Returned one from Research Chronomancy
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.ShadowduskHold)
                .andCompleteQuestInCliffwatchInn(Quests.SanctifyTempleToOghma)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        //Dummy 3 of 6
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andCompleteMandatoryQuest(Quests.UnveilAbyssalAgent)
                .andEndTurn();

        // Agent 2 of 7
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.DomesticateOwlbears)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        //Dummy 4 of 6
        dummy = dummy.placeAgent(Buildings.ThePlinth)
                .andEndTurn();
        
        // Agent 3 of 7
        todd = todd.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.ShadowduskHold)
                .andCompleteQuestInCliffwatchInn(Quests.BolsterCityGuard)
                .andReturnOneQuestResourceUsed(Res.fighter(1))
                .andEndTurn();
        
        //Dummy 5 of 6
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.NewOlamn)
                .andEndTurn();
        
        // Agent 4 of 7
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.HallOfManyPillars, Buildings.MonstersMadeToOrder)
                .andCompleteQuestInCliffwatchInn(Quests.DefendTheLanceboardRoom)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andReplaceGenericResources(new Resources(Res.cleric(2), Res.fighter(2), Res.rogue(2), Res.wizard(2)))
                .andEndTurn();

        // Last dunmmy move
        dummy = dummy.placeAgent(Buildings.BlackstaffTower)
                .andEndTurn();
        
        // Agent 5 of 7
        todd = todd.placeAgent(Buildings.HallOfManyPillars)
                .andPlayMultiIntrigue(Intrigues.HuntHiddenGhoul)
                .andChooseOpponentForMandatoryQuest(dummy)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.TaxRevolt)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.ThreePearls)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.RequestAssistance)
                .andFinishMultiIntrigue()
                .andCompleteQuestInCliffwatchInn(Quests.EliminateVampireCoven)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        // Agent 6 of 7
        todd = todd.placeAgent(Buildings.ThreePearls)
                .andPayVariableActivation(ResourceChoices.OneFighterOneRogue)
                .andTakeVariableReward(ResourceChoices.OneClericOneRogueOneWizard)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.RaidOrcStronghold)
                .andReturnOneQuestResourceUsed(Res.fighter(1))
                .andEndTurn();
        
        
        // Agent 7 of 7
        todd = todd.placeAgent(Buildings.MonstersMadeToOrder)
                .andRecallAgents(Buildings.RoomOfWisdom, Buildings.ShadowduskHold, Buildings.CitadelOfTheBloodyHand, Buildings.HallOfManyPillars, Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.SaveKidnappedNobles)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();

        // Agent 3 of 7
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.CitadelOfTheBloodyHand, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.TakeOverRivalOrganization)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        
        // Agent 4 of 7
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.Librarium, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.DefeatUprisingFromUndermountain)
                .andReturnOneQuestResourceUsed(Res.fighter(1))
                .andEndTurn();
                
        // Agent 5 of 7
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.HallOfManyPillars)
                .andCompleteQuestInCliffwatchInn(Quests.SealGateToCyricsRealm)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        // Agent 6 of 7
        todd = todd.placeAgent(Buildings.HallOfManyPillars)
                .andPlayMultiIntrigue(Intrigues.AlliedFaiths)
                .andChooseOpponentForReward(dummy)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.UnlikelyAssistance)
                .andChooseOpponentForReward(dummy)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.Scapegoat1)
                .andReturnResourcesForIntrigue(ResourceChoices.OneRogueTwoCorruption)
                .andFinishMultiIntrigue()
                .andCompleteQuestInCliffwatchInn(Quests.DefendTheYawningPortal)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andReplaceGenericResources(new Resources(Res.cleric(1), Res.fighter(1)))
                .andRecallAgents(Buildings.ShadowduskHold, Buildings.CitadelOfTheBloodyHand, Buildings.RoomOfWisdom)
                .andEndTurn();
        
        // Agent 4 of 7
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.HonorableExample)
                .andCompleteQuestInCliffwatchInn(Quests.UnleashCrimeSpree)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        // Agent 5 of 7
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborTwo, Buildings.WaterdeepHarborThree)
                .andCompleteQuestInCliffwatchInn(Quests.ImpersonateAdarBrentNoble)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        // Agent 6 of 7
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.RecallAgent1)
                .andRecallAgent(Buildings.ShadowduskHold)
                .andPlayIntrigueDrawnAsAction()
                .andCompleteQuestInCliffwatchInn(Quests.SpyOnTheHouseOfLight)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();

        // Agent 6 of 7
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.WaterdeepHarborThree)
                .andCompleteQuestInCliffwatchInn(Quests.ObtainBuildersPlans)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();

        
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.RecallAgent2)
                .andRecallAgent(Buildings.CitadelOfTheBloodyHand)
                .andPlayIntrigueDrawnAsAction()
                .andCompleteQuestInCliffwatchInn(Quests.RepelSeawraiths)
                .andReturnOneQuestResourceUsed(Res.fighter(1))
                .andEndTurn();
        // 7 of 7
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.WaterdeepHarborThree)
                .andCompleteQuestInCliffwatchInn(Quests.UncoverDrowPlot)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.ShadowduskHold)
                .andCompleteQuestInCliffwatchInn(Quests.RetrieveAncientArtifacts)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.RoomOfWisdom, Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.UncoverForbiddenLore)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();

        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.ShadowduskHold)
                .andCompleteQuestInCliffwatchInn(Quests.SanctifyDesecratedTemple)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
    }
    
}

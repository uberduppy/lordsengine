/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.talwood.lords.gamerunner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Houses;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.Lords;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.Res;
import org.talwood.lords.resources.Resources;

/**
 *
 * @author twalker
 */
public class TrobriandLordTest {
    
    public TrobriandLordTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testUseSingleLord() {
        GameRunner runner = new GameRunner();
        
        GamePlayer dummy = runner.addPlayer(Lords.LarissaNeathal, "Useful Idiot", Long.valueOf(9));
        GamePlayer todd = runner.addPlayer(Lords.Trobriand, Houses.Skullduggery, "Todd", Long.valueOf(10));

        runner.setupQuests(Quests.StudyTheIlluskArch,
                Quests.RecruitLieutenant,
                Quests.LureArtisansOfMirabar,
                Quests.DiplomaticMissionToSuzail,
                Quests.ThreatenTheBuildersGuilds,
                Quests.DefeatUprisingFromUndermountain,
                Quests.DeliverAnUltimatum,
                Quests.BribeTheShipwrights,
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.ExploreAhghaironsTower,
                Quests.DestroyTempleOfSelvetarm,
                Quests.PlaceSleeperAgentInSkullport,
                Quests.DiscoverHiddenTempleOfLolth,
                Quests.StealFromHouseAdarbrent,
                Quests.RetrieveAncientArtifacts,
                Quests.BuryTheBodies,
                Quests.FundPilgrimageOfWaukeen,
                Quests.UnleashCrimeSpree,
                Quests.ExtortAurora,
                Quests.RecruitAcademyCastoffs,
                Quests.SendAidToTheHarpers,
                Quests.FundAlchemicalResearch,
                Quests.UncoverDrowPlot,
                Quests.SealGateToCyricsRealm,
                Quests.TakeOverRivalOrganization,
                Quests.CreateShrineToOghma,
                Quests.BuildReputationInSkullport,
                Quests.AssassinateRivals,
                Quests.RansackWhitehelmsTomb,
                Quests.BreakIntoBlackstaffTower,
                Quests.FormAllianceWithTheRashemi,
                Quests.EstablishShadowThievesGuild,
                Quests.UncoverForbiddenLore,
                Quests.GiveHonorToMask,
                Quests.RecoverTheFlameOfTheNorth,
                Quests.EstablishCultCell,
                Quests.SealAnEntranceToSkullport,
                Quests.DefendTheYawningPortal,
                Quests.ResearchChronomacy,
                Quests.InfiltrateHalastersCircle,
                Quests.BattleInMuiralsGauntlet,
                Quests.ImprovePrisonSecurity,
                Quests.EliminateVampireCoven,
                Quests.DomesticateOwlbears,
                Quests.InfiltrateBuildersHall,
                Quests.AllyWithHouseThann,
                Quests.PlunderTheIslandTemple,
                Quests.TrainCastleguards,
                Quests.ExploreTrobriandsGraveyard,
                Quests.RaidOnUndermountain,
//                Quests.ExploreTrobriandsGraveyard,
//                Quests.InfiltrateHalastersCircle,
//                Quests.ProcureStolenGoods,
//                Quests.BreakIntoBlackstaffTower,
//                
//                Quests.AmbushArtorMorlin,
//                Quests.SaveKidnappedNobles,
//                Quests.BuryTheBodies,
//                Quests.EstablishTempleToIbrandul,
//                Quests.ExposeCultCorruption,
//                Quests.EliminateVampireCoven,
//                Quests.StealSpellbookFromSilverhand,
//                Quests.SafeguardEltorchulMage,
//                Quests.BattleInMuiralsGauntlet,
//                Quests.BuildReputationInSkullport,
//                Quests.SealAnEntranceToSkullport,
//                Quests.StealFromHouseAdarbrent,
//                Quests.DefendTheYawningPortal,
//                Quests.UnleashCrimeSpree,
//                Quests.RecruitForBlackstaffAcademy,
//                Quests.EstablishHarpersSafeHouse,
//                Quests.ExpandGuildActivities,
//                Quests.ImpersonateTaxCollector,
//                Quests.DefeatUprisingFromUndermountain,
//                Quests.ResearchChronomacy,
//                Quests.PlunderTheIslandTemple,
//                Quests.DefendTheLanceboardRoom,
//                Quests.CreateShrineToOghma,
//                Quests.ConfrontTheXanathar,
//                Quests.DiscoverHiddenTempleOfLolth,
//                Quests.ImprovePrisonSecurity,
//                Quests.UncoverDrowPlot,
//                Quests.DiplomaticMissionToSuzail,
//                Quests.RansackWhitehelmsTomb,
                Quests.BolsterCityGuard
                );

        runner.setupBuildings(Buildings.SmugglersDock, 
                Buildings.DragonTower, 
                Buildings.RoomOfWisdom, 
                Buildings.Skulkway,
                Buildings.ShadowduskHold,
                Buildings.HighDukesTomb,
                Buildings.CitadelOfTheBloodyHand,
                Buildings.Librarium,
                
                Buildings.PoisonedQuill,
                Buildings.ThreePearls,
                Buildings.HouseOfGoodSpirits,
                Buildings.HellHoundsMuzzle,
                Buildings.NewOlamn,
                Buildings.HouseOfTheMoon,
                Buildings.TowerOfLuck,
                Buildings.ThrownGauntlet
                );
        
        runner.setupIntrigues(
                Intrigues.BlackMarketMoney, 
                Intrigues.IronRingSlaves, 
                Intrigues.ArchitecturalInnovation, 
                Intrigues.InformationBroker1, 
                Intrigues.GraduationDay1,
                Intrigues.SpreadTheWealth1,
                Intrigues.DarkDaggerAssassination,
                Intrigues.SpreadTheWealth2,
                Intrigues.Conscription1,
                Intrigues.CrimeWave1,
                Intrigues.GraduationDay2,
                Intrigues.InformationBroker2,
                Intrigues.GoodFaith1,
                Intrigues.CallForAssistance1,
                Intrigues.Repent1,
                Intrigues.Conscription2,
                Intrigues.CrimeWave2,
                Intrigues.QuellRiots,
                Intrigues.PlacateAngryMerchants,
                Intrigues.RecallAgent1,
                Intrigues.GoodFaith2,
                Intrigues.RecruitSpies,
                Intrigues.FreeDrinks1,
                Intrigues.FreeDrinks2,
                
//                Intrigues.Conscription2,
//                Intrigues.GoodFaith1,
//                Intrigues.ResearchAgreement,
                Intrigues.MindFlayerMercenaries,
                Intrigues.Repent2,
//                Intrigues.RecallAgent1,
//                Intrigues.RecallAgent2,
//                Intrigues.Repent1,
//                Intrigues.CallForAssistance1,
//                Intrigues.CallForAssistance2,
                Intrigues.Repent3,
                Intrigues.CallInAFavor1,
                Intrigues.CallForAssistance2,
                Intrigues.CallInAFavor2,
                Intrigues.AlliedFaiths,
                Intrigues.HonorAmongThieves,
                Intrigues.FriendlyLoan,
                Intrigues.UnlikelyAssistance,
                Intrigues.MercenaryContract,
//                Intrigues.HonorAmongThieves,
//                Intrigues.RepelDrowInvaders,
//                Intrigues.UnveilAbyssalAgent,
//                Intrigues.MercenaryContract,
//                Intrigues.ForgeDeed,
//                Intrigues.UnexpectedSuccess1,
//                Intrigues.UnexpectedSuccess2,
//                Intrigues.CallInAFavor1,
//                Intrigues.SpecialAssignment1,
//                Intrigues.SubdueIllithidMenace,
//                Intrigues.CallInAFavor2,
//                Intrigues.SpecialAssignment2,
//                Intrigues.OrganizedCrime,
                Intrigues.ChangeOfPlans
                );
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        
        // TURN 1.
        runner.startTurn();
        
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.SmugglersDock).andEndTurn();
        todd = todd.placeAgent(Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.ThreeFightersOneRogue).andEndTurn();
        dummy = dummy.placeAgent(Buildings.EntryWell)
                .andTakeQuest(Quests.ThreatenTheBuildersGuilds)
                .andPlayIntrigue(Intrigues.BlackMarketMoney).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.IronRingSlaves)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.ArchitecturalInnovation)
                .andBuyBuilding(Buildings.RoomOfWisdom).andEndTurn();
        todd = todd.placeAgent(Buildings.SkullIsland)
                .andTakeVariableReward(ResourceChoices.OneClericOneWizardOneCorruption)
                .andCompleteQuest(Quests.RecruitLieutenant)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BlackstaffTower).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.InformationBroker1).andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.FieldOfTriumph)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.FieldOfTriumph)
                .andCompleteQuest(Quests.DiplomaticMissionToSuzail).andEndTurn();
        dummy = dummy.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.AurorasRealm).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.HallOfMirrors)
                .andTakeVariableReward(ResourceChoices.OneFighterOneRogue)
                .andEndTurn();
//                
        // Turn 2
        runner.startTurn();
//        
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.Skulkway).andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.SmugglersDock)
//                .andCompleteQuestInCliffwatchInn(Quests.EliminateVampireCoven)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern).andEndTurn();
        
        todd = todd.placeAgent(Buildings.CliffwatchInnTwoGold).andTakeQuest(Quests.FenceGoodsForDukeOfDarkness).andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm).andEndTurn();

        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.DarkDaggerAssassination)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.SpreadTheWealth1)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuest(Quests.FenceGoodsForDukeOfDarkness)
                .andEndTurn();
        
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.GraduationDay1)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.ExploreAhghaironsTower)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.Skulkway)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.TwoFightersTwoRogues)
                .andCompleteQuestInCliffwatchInn(Quests.DestroyTempleOfSelvetarm)
                .andEndTurn();

        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.TheGrimStatue)
                .andEndTurn();

        // Turn 3
        runner.startTurn();
        dummy = dummy.placeAgent(Buildings.HallOfMirrors)
                .andTakeVariableReward(ResourceChoices.OneFighterOneRogue).andEndTurn();
        todd = todd.placeAgent(Buildings.BlackstaffTower)
                .andCompleteQuestInCliffwatchInn(Quests.PlaceSleeperAgentInSkullport).andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.Conscription1)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.DiscoverHiddenTempleOfLolth)
                .andTakeQuestAsQuestReward(Quests.DeliverAnUltimatum).andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.ShadowduskHold)
                .andCompleteQuest(Quests.ThreatenTheBuildersGuilds)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.CrimeWave1)
                .andChooseOpponentForReward(dummy)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.GraduationDay2)
                .andChooseOpponentForReward(dummy)
                .andEndTurn();
        
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.DragonTower, Buildings.ShadowduskHold)
                .andCompleteQuestInCliffwatchInn(Quests.RetrieveAncientArtifacts)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.DragonTower, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.BuryTheBodies)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.DragonTower)
                .andCompleteQuestInCliffwatchInn(Quests.FundPilgrimageOfWaukeen)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.DragonTower)
//                .andCompleteQuestInCliffwatchInn(Quests.FundPilgrimageOfWaukeen)
                .andEndTurn();

        // Turn 4
        runner.startTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andCompleteQuest(Quests.LureArtisansOfMirabar)
                .andTakeBuildingFromBuildersHallAsQuestReward(Buildings.Librarium)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.InformationBroker2)
                .andCompleteQuestInCliffwatchInn(Quests.UnleashCrimeSpree)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.ExtortAurora)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.GoodFaith1)
                .andChooseOpponentForReward(dummy)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.HallOfTheVoice)
                .andTakeQuest(Quests.RecruitAcademyCastoffs)
//                .andBuyBuilding(Buildings.PoisonedQuill)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.CallForAssistance1)
                .andStealResources(dummy, ResourceChoices.OneFighterOneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.SendAidToTheHarpers)
                .andAddResourcesToPlayer(dummy, Res.gold(4))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.PoisonedQuill)
                .andEndTurn();
        
        todd = todd.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.PoisonedQuill)
                .andCompleteQuestInCliffwatchInn(Quests.FundAlchemicalResearch)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.RoomOfWisdom)
//                .andCompleteQuestInCliffwatchInn(Quests.FundAlchemicalResearch)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.PoisonedQuill, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.UncoverDrowPlot)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.PoisonedQuill)
                .andCompleteQuestInCliffwatchInn(Quests.SealGateToCyricsRealm)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PoisonedQuill)
                .andPlayIntrigue(Intrigues.Repent1)
                .andEndTurn();
        
        // turn 5
        runner.startTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.ThreePearls)
                .andCompleteQuest(Quests.ExtortAurora)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.CrimeWave2)
                .andCompleteQuestInCliffwatchInn(Quests.TakeOverRivalOrganization)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.CreateShrineToOghma)
                .andCompleteQuest(Quests.CreateShrineToOghma)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.Conscription2)
                .andCompleteQuestInCliffwatchInn(Quests.BuildReputationInSkullport)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.CitadelOfTheBloodyHand)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.Librarium, Buildings.CitadelOfTheBloodyHand)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.RecallAgent1)
                .andRecallAgent(Buildings.ShadowduskHold)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.RoomOfWisdom, Buildings.ShadowduskHold)
                .andCompleteQuestInCliffwatchInn(Quests.AssassinateRivals)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.RoomOfWisdom, Buildings.Librarium)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.RansackWhitehelmsTomb)
                .andEndTurn();

        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.PoisonedQuill)
                .andCompleteQuestInCliffwatchInn(Quests.BreakIntoBlackstaffTower)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.PoisonedQuill)
                .andPlayIntrigue(Intrigues.GoodFaith2)
                .andCompleteQuestInCliffwatchInn(Quests.FormAllianceWithTheRashemi)
                .andTakeQuestAsQuestReward(Quests.EstablishShadowThievesGuild)
                .andEndTurn();

        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.ThreePearls)
                .andPayVariableActivation(ResourceChoices.TwoRogues)
                .andTakeVariableReward(ResourceChoices.ThreeWizards)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andEndTurn();
        
        // Turn 6!
        runner.startTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.NewOlamn)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.RecruitSpies)
                .andCompleteQuestInCliffwatchInn(Quests.UncoverForbiddenLore)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andCompleteQuest(Quests.StudyTheIlluskArch)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.FreeDrinks1)
                .andStealResources(dummy, ResourceChoices.OneWizard)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.FreeDrinks2)
                .andStealResources(dummy, ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.GiveHonorToMask)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.CitadelOfTheBloodyHand, Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.BribeTheShipwrights)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.RecoverTheFlameOfTheNorth)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.RoomOfWisdom, Buildings.Librarium)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.EstablishCultCell)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.PoisonedQuill)
                .andPlayIntrigue(Intrigues.CallInAFavor1)
                .andTakeVariableIntrigueReward(ResourceChoices.TwoRogues)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.ThreePearls)
                .andPayVariableActivation(ResourceChoices.TwoFighters)
                .andTakeVariableReward(ResourceChoices.TwoClericsOneWizard)
                .andTakeVariableOwnerReward(ResourceChoices.OneWizard)
                .andCompleteQuestInCliffwatchInn(Quests.SealAnEntranceToSkullport)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.TwoFightersTwoRogues)
                .andCompleteQuestInCliffwatchInn(Quests.DefeatUprisingFromUndermountain)
                .andEndTurn();
        
        // Turn 7
//        runner.startTurn();
//        dummy = dummy.placeAgent(Buildings.BuildersHall)
//                .andBuyBuilding(Buildings.HellHoundsMuzzle)
//                .andEndTurn();
//        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
//                .andPlayIntrigue(Intrigues.MindFlayerMercenaries)
//                .andEndTurn();
//
//        dummy = dummy.placeAgent(Buildings.AurorasRealm)
//                .andCompleteQuest(Quests.RecoverTheFlameOfTheNorth)
//                .andEndTurn();
//        
//        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
//                .andPlayIntrigue(Intrigues.CallForAssistance2)
//                .andStealResources(dummy, ResourceChoices.TwoFighters)
//                .andEndTurn();
//        
//        dummy = dummy.placeAgent(Buildings.ThePlinth)
//                .andEndTurn();
//        
//        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
//                .andPlayIntrigue(Intrigues.Repent2)
//                .andEndTurn();
//
//        dummy = dummy.placeAgent(Buildings.FieldOfTriumph)
//                .andEndTurn();
//
//        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
//                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.RoomOfWisdom)
//                .andEndTurn();
//
//        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
//                .andEndTurn();
//
//        todd = todd.placeAgent(Buildings.ShadowduskHold)
//                .andSelectTargetBuildings(Buildings.Librarium, Buildings.RoomOfWisdom)
//                .andEndTurn();
//        todd = todd.placeAgent(Buildings.RoomOfWisdom)
//                .andSelectTargetBuildings(Buildings.Librarium)
//                .andCompleteQuestInCliffwatchInn(Quests.DefendTheYawningPortal)
//                .andRecallAgents(Buildings.CitadelOfTheBloodyHand, Buildings.ShadowduskHold, Buildings.RoomOfWisdom)
//                .andReplaceGenericResources(new Resources(Res.cleric(1), Res.wizard(1)))
//                .andEndTurn();
//        
//        todd = todd.placeAgent(Buildings.Librarium)
//                .andSelectTargetBuildings(Buildings.ShadowduskHold)
//                .andCompleteQuestInCliffwatchInn(Quests.ResearchChronomacy)
//                .andRecallAgent(Buildings.Librarium)
//                .andEndTurn();
//        
//        todd = todd.placeAgent(Buildings.Librarium)
//                .andSelectTargetBuildings(Buildings.CitadelOfTheBloodyHand)
//                .andCompleteQuestInCliffwatchInn(Quests.InfiltrateHalastersCircle)
//                .andEndTurn();
//        
//        todd = todd.placeAgent(Buildings.ShadowduskHold)
//                .andSelectTargetBuildings(Buildings.CitadelOfTheBloodyHand, Buildings.RoomOfWisdom)
//                .andCompleteQuestInCliffwatchInn(Quests.BattleInMuiralsGauntlet)
//                .andEndTurn();
//        
//        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
//                .andSelectTargetBuildings(Buildings.ThreePearls, Buildings.RoomOfWisdom)
//                .andEndTurn();
//        
//        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.RoomOfWisdom)
//                .andSelectTargetBuildings(Buildings.ThreePearls)
//                .andCompleteQuest(Quests.EstablishShadowThievesGuild)
//                .andEndTurn();
//        
//        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.PoisonedQuill)
//                .andPlayIntrigue(Intrigues.Repent3)
//                .andCompleteQuestInCliffwatchInn(Quests.ImprovePrisonSecurity)
//                .andEndTurn();
//        
//        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.ThreePearls)
//                .andPayVariableActivation(ResourceChoices.TwoRogues)
//                .andTakeVariableReward(ResourceChoices.OneFighterOneRogueOneWizard)
//                .andTakeVariableOwnerReward(ResourceChoices.OneWizard)
//                .andCompleteQuestInCliffwatchInn(Quests.EliminateVampireCoven)
//                .andEndTurn();
//
//        // Turn 8
//        runner.startTurn();
//        dummy = dummy.placeAgent(Buildings.AurorasRealm)
//                .andCompleteQuest(Quests.RecruitAcademyCastoffs)
//                .andEndTurn();
//        todd = todd.placeAgent(Buildings.HellHoundsMuzzle)
//                .andTakeVariableOwnerReward(ResourceChoices.OneCleric)
//                .andEndTurn();
//        dummy = dummy.placeAgent(Buildings.CliffwatchInnTwoGold)
//                .andTakeQuest(Quests.DomesticateOwlbears)
//                .andCompleteQuest(Quests.DomesticateOwlbears)
//                .andEndTurn();
//        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
//                .andPlayIntrigue(Intrigues.UnlikelyAssistance)
//                .andChooseOpponentForReward(dummy)
//                .andEndTurn();
//        dummy = dummy.placeAgent(Buildings.CliffwatchInnIntrigue)
//                .andTakeQuest(Quests.InfiltrateBuildersHall)
//                .andCompleteQuest(Quests.InfiltrateBuildersHall)
//                .andEndTurn();
//        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
//                .andPlayIntrigue(Intrigues.FriendlyLoan)
//                .andChooseOpponentForReward(dummy)
//                .andEndTurn();
//        dummy = dummy.placeAgent(Buildings.EntryWell)
//                .andTakeQuest(Quests.AllyWithHouseThann)
//                .andPlayIntrigue(Intrigues.SpreadTheWealth2)
//                .andCompleteQuest(Quests.AllyWithHouseThann)
//                .andEndTurn();
//        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
//                .andPlayIntrigue(Intrigues.AlliedFaiths)
//                .andChooseOpponentForReward(dummy)
//                .andEndTurn();
//        dummy = dummy.placeAgent(Buildings.HallOfTheVoice)
//                .andTakeQuest(Quests.PlunderTheIslandTemple)
//                .andCompleteQuest(Quests.PlunderTheIslandTemple)
//                .andEndTurn();
//        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
//                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.PoisonedQuill)
//                .andEndTurn();
//        todd = todd.placeAgent(Buildings.ShadowduskHold)
//                .andSelectTargetBuildings(Buildings.Librarium, Buildings.PoisonedQuill)
//                .andCompleteQuestInCliffwatchInn(Quests.StealFromHouseAdarbrent)
//                .andEndTurn();
//        
//        todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.PoisonedQuill)
//                .andPlayIntrigue(Intrigues.MercenaryContract)
//                .andChooseOpponentForReward(dummy)
//                .andEndTurn();
//        todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.Librarium)
//                .andSelectTargetBuildings(Buildings.ThreePearls)
//                .andCompleteQuestInCliffwatchInn(Quests.TrainCastleguards)
//                .andAddResourcesToPlayer(todd, Res.wizard(1))
//                .andEndTurn();
//        todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.ThreePearls)
//                .andPayVariableActivation(ResourceChoices.OneFighterOneRogue)
//                .andTakeVariableReward(ResourceChoices.OneFighterOneRogueOneWizard)
//                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
//                .andCompleteQuestInCliffwatchInn(Quests.ExploreTrobriandsGraveyard)
//                .andEndTurn();
        runner.finishGame();
//        runner.listQuestsLessThanPoints(todd, 10);
        
        runner.renderToFile("c:/duelssol/trobriand.html");
    }
    
}

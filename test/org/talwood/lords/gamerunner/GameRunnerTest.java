/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.talwood.lords.gamerunner;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.Lords;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.enums.TurnActions;

/**
 *
 * @author twalker
 */
public class GameRunnerTest {
    
    public GameRunnerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testLandingOnBasicBuilding() {
        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));

        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        runner.startTurn();
        
        todd = todd.placeAgent(Buildings.ThePlinth).andEndTurn();
        assertEquals(1, todd.getResources().getCleric());
        
        
    }
    
    @Test
    public void testLandingOnCliffwatchFailsWithNoQuestSelected() {
        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));

        runner.setupQuests(Quests.ExploreAhghaironsTower, // Todd's 1st
                Quests.DeliverAnUltimatum, // dummy
                Quests.InstallSpyInCastleWaterdeep, // Todd
                Quests.AmbushArtorMorlin, // Dummy
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.BolsterGriffinCavalry, 
                Quests.BribeTheShipwrights);
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        runner.startTurn();
        
        assertEquals(4, todd.getResources().getGold());

        try {
            todd = todd.placeAgent(Buildings.CliffwatchInnTwoGold).andEndTurn();
            fail("This should fail");
        } catch (UnsupportedOperationException ex) {
            // Good
        }
        
        // It should still add the gold, even though a quest wasn't selected
        assertEquals(6, todd.getResources().getGold());
    }
    
    @Test
    public void testLandingOnCliffwatchFailsWithInvalidQuest() {
        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));

        runner.setupQuests(Quests.ExploreAhghaironsTower, // Todd's 1st
                Quests.DeliverAnUltimatum, // dummy
                Quests.InstallSpyInCastleWaterdeep, // Todd
                Quests.AmbushArtorMorlin, // Dummy
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.BolsterGriffinCavalry, 
                Quests.BribeTheShipwrights);
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        runner.startTurn();
        
        assertEquals(4, todd.getResources().getGold());
        try {
            todd = todd.placeAgent(Buildings.CliffwatchInnTwoGold).andTakeQuest(Quests.BattleInMuiralsGauntlet).andEndTurn();
        } catch (UnsupportedOperationException ex) {
            // Good
        }
        
        assertEquals(1, todd.getTurnState().getBuildingActions().size());
        
        // It should still add the gold, even though a quest wasn't selected
        assertEquals(6, todd.getResources().getGold());
        
    }
    
    @Test
    public void testLandingOnCliffwatchSucceedsWithValidQuest() {
        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));

        runner.setupQuests(Quests.ExploreAhghaironsTower, // Todd's 1st
                Quests.DeliverAnUltimatum, // dummy
                Quests.InstallSpyInCastleWaterdeep, // Todd
                Quests.AmbushArtorMorlin, // Dummy
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.BolsterGriffinCavalry, 
                Quests.BribeTheShipwrights);
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        runner.startTurn();
        
        assertEquals(4, todd.getResources().getGold());
        
        todd = todd.placeAgent(Buildings.CliffwatchInnTwoGold).andTakeQuest(Quests.AmbushArtorMorlin).andEndTurn();
        assertNull(todd.getTurnState());
        
        // It should still add the gold, even though a quest wasn't selected
        assertEquals(6, todd.getResources().getGold());
        
        assertEquals(3, todd.getCurrentQuests().size());
        
        runner.renderToFile("c:/duelssol/onequest.html");
        
    }
    
    @Test
    public void testLandingOnCliffwatchSucceedsWithValidQuestAndDrawsIntrigue() {
        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));

        runner.setupQuests(Quests.ExploreAhghaironsTower, // Todd's 1st
                Quests.DeliverAnUltimatum, // dummy
                Quests.InstallSpyInCastleWaterdeep, // Todd
                Quests.AmbushArtorMorlin, // Dummy
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.BolsterGriffinCavalry, 
                Quests.BribeTheShipwrights);

        runner.setupIntrigues(Intrigues.Ambush1, Intrigues.ArcaneMishap1, Intrigues.BiddingWar1);
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        runner.startTurn();
        
        assertEquals(4, todd.getResources().getGold());
        
        todd = todd.placeAgent(Buildings.CliffwatchInnIntrigue).andTakeQuest(Quests.AmbushArtorMorlin).andEndTurn();
        assertNull(todd.getTurnState());
        
        // It should still add the gold, even though a quest wasn't selected
        assertEquals(3, todd.getCurrentQuests().size());
        assertEquals(3, todd.getIntrigueCards().size());
        
        runner.renderToFile("c:/duelssol/onequestint.html");
        
    }
    
    @Test
    public void testLandingOnBuildersHallGetsbuilding() {
        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));

        runner.setupQuests(Quests.ExploreAhghaironsTower, // Todd's 1st
                Quests.DeliverAnUltimatum, // dummy
                Quests.InstallSpyInCastleWaterdeep, // Todd
                Quests.AmbushArtorMorlin, // Dummy
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.BolsterGriffinCavalry, 
                Quests.BribeTheShipwrights);

        runner.setupIntrigues(Intrigues.Ambush1, Intrigues.ArcaneMishap1, Intrigues.BiddingWar1);
        runner.setupBuildings(Buildings.SmugglersDock, Buildings.JestersCourt, Buildings.MonstersMadeToOrder, Buildings.Zoarstar);
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        runner.startTurn();
        
        assertEquals(4, todd.getResources().getGold());
        
        todd = todd.placeAgent(Buildings.BuildersHall).andBuyBuilding(Buildings.SmugglersDock).andEndTurn();
        assertNull(todd.getTurnState());
        
        // It should still add the gold, even though a quest wasn't selected
        assertEquals(1, todd.getOwnedBuildings().size());
        assertEquals(0, todd.getResources().getGold());
        assertEquals(1, todd.getVictoryPoints());
        
        assertEquals(3, runner.getBuildingsInBuildersHall().size());
        List<Building> lb = runner.getBuildingsInBuildersHall();
        assertEquals(Buildings.JestersCourt, lb.get(0).getBuildingInfo());
        assertEquals(Buildings.MonstersMadeToOrder, lb.get(1).getBuildingInfo());
        assertEquals(Buildings.Zoarstar, lb.get(2).getBuildingInfo());
        
        runner.renderToFile("c:/duelssol/onebldg.html");
        
    }
    
    
    @Test
    public void testLandingOnBuildersHallGetsBuildingAndItsUseable() {
        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));
        GamePlayer fred = runner.addPlayer(Lords.CaladornCassalanter, "Fred", Long.valueOf(11));

        runner.setupQuests(Quests.ExploreAhghaironsTower, // Todd's 1st
                Quests.DeliverAnUltimatum, // dummy
                Quests.InstallSpyInCastleWaterdeep, // Todd
                Quests.AmbushArtorMorlin, // Dummy
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.BolsterGriffinCavalry, 
                Quests.BribeTheShipwrights);

        runner.setupIntrigues(Intrigues.Ambush1, Intrigues.ArcaneMishap1, Intrigues.BiddingWar1);
        runner.setupBuildings(Buildings.HelmstarWarehouse, Buildings.JestersCourt, Buildings.MonstersMadeToOrder, Buildings.Zoarstar);
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        runner.startTurn();
        
        assertEquals(4, todd.getResources().getGold());
        
        todd = todd.placeAgent(Buildings.BuildersHall).andBuyBuilding(Buildings.HelmstarWarehouse).andEndTurn();
        assertNull(todd.getTurnState());
        
        // It should still add the gold, even though a quest wasn't selected
        assertEquals(1, todd.getOwnedBuildings().size());
        assertEquals(1, todd.getResources().getGold());
        assertEquals(1, todd.getVictoryPoints());
        
        assertEquals(3, runner.getBuildingsInBuildersHall().size());
        List<Building> lb = runner.getBuildingsInBuildersHall();
        assertEquals(Buildings.JestersCourt, lb.get(0).getBuildingInfo());
        assertEquals(Buildings.MonstersMadeToOrder, lb.get(1).getBuildingInfo());
        assertEquals(Buildings.Zoarstar, lb.get(2).getBuildingInfo());
        
        fred = fred.placeAgent(Buildings.HelmstarWarehouse).andEndTurn();
        assertEquals(2, fred.getResources().getRogue());
        assertEquals(7, fred.getResources().getGold());
        assertEquals(1, todd.getResources().getRogue());
        
        
        runner.renderToFile("c:/duelssol/onebldgtwoplayer.html");
        
    }
    
    @Test
    public void testLandingOnBuildersHallGetsThreePearlsNoActivation() {
        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));
        GamePlayer fred = runner.addPlayer(Lords.BrianneByndraeth, "Fred", Long.valueOf(11));

        runner.setupQuests(Quests.ExploreAhghaironsTower, // Todd's 1st
                Quests.DeliverAnUltimatum, // dummy
                Quests.InstallSpyInCastleWaterdeep, // Todd
                Quests.AmbushArtorMorlin, // Dummy
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.BolsterGriffinCavalry, 
                Quests.BribeTheShipwrights);

        runner.setupIntrigues(Intrigues.Ambush1, Intrigues.ArcaneMishap1, Intrigues.BiddingWar1);
        runner.setupBuildings(Buildings.ThreePearls, Buildings.JestersCourt, Buildings.MonstersMadeToOrder, Buildings.Zoarstar);
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        runner.startTurn();
        
        assertEquals(4, todd.getResources().getGold());
        
        todd = todd.placeAgent(Buildings.BuildersHall).andBuyBuilding(Buildings.ThreePearls).andEndTurn();
        assertNull(todd.getTurnState());
        
        // It should still add the gold, even though a quest wasn't selected
        assertEquals(1, todd.getOwnedBuildings().size());
        assertEquals(0, todd.getResources().getGold());
        assertEquals(1, todd.getVictoryPoints());
        
        assertEquals(3, runner.getBuildingsInBuildersHall().size());
        List<Building> lb = runner.getBuildingsInBuildersHall();
        assertEquals(Buildings.JestersCourt, lb.get(0).getBuildingInfo());
        assertEquals(Buildings.MonstersMadeToOrder, lb.get(1).getBuildingInfo());
        assertEquals(Buildings.Zoarstar, lb.get(2).getBuildingInfo());
        
        fred.getResources().addFighter(2);
        try {
        fred = fred.placeAgent(Buildings.ThreePearls).andEndTurn();
        } catch (UnsupportedOperationException ex) {
            // Good
        }
        assertTrue(fred.getTurnState().hasBuildingAction(BuildingActions.RequiresVariableActivation));
        
        runner.renderToFile("c:/duelssol/threepearls.html");
        
    }
    
    @Test
    public void testLandingOnBuildersHallGetsThreePearlsInvalidActivation() {
        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));
        GamePlayer fred = runner.addPlayer(Lords.BrianneByndraeth, "Fred", Long.valueOf(11));

        runner.setupQuests(Quests.ExploreAhghaironsTower, // Todd's 1st
                Quests.DeliverAnUltimatum, // dummy
                Quests.InstallSpyInCastleWaterdeep, // Todd
                Quests.AmbushArtorMorlin, // Dummy
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.BolsterGriffinCavalry, 
                Quests.BribeTheShipwrights);

        runner.setupIntrigues(Intrigues.Ambush1, Intrigues.ArcaneMishap1, Intrigues.BiddingWar1);
        runner.setupBuildings(Buildings.ThreePearls, Buildings.JestersCourt, Buildings.MonstersMadeToOrder, Buildings.Zoarstar);
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        runner.startTurn();
        
        assertEquals(4, todd.getResources().getGold());
        
        todd = todd.placeAgent(Buildings.BuildersHall).andBuyBuilding(Buildings.ThreePearls).andEndTurn();
        assertNull(todd.getTurnState());
        
        // It should still add the gold, even though a quest wasn't selected
        assertEquals(1, todd.getOwnedBuildings().size());
        assertEquals(0, todd.getResources().getGold());
        assertEquals(1, todd.getVictoryPoints());
        
        assertEquals(3, runner.getBuildingsInBuildersHall().size());
        List<Building> lb = runner.getBuildingsInBuildersHall();
        assertEquals(Buildings.JestersCourt, lb.get(0).getBuildingInfo());
        assertEquals(Buildings.MonstersMadeToOrder, lb.get(1).getBuildingInfo());
        assertEquals(Buildings.Zoarstar, lb.get(2).getBuildingInfo());
        
        fred.getResources().addFighter(2);
        try {
        fred = fred.placeAgent(Buildings.ThreePearls).andPayVariableActivation(ResourceChoices.FourFighters).andEndTurn();
        } catch (UnsupportedOperationException ex) {
            // Good
        }
        assertTrue(fred.getTurnState().hasBuildingAction(BuildingActions.RequiresVariableActivation));
        
        runner.renderToFile("c:/duelssol/ut3.html");
        
    }
    
    @Test
    public void testLandingOnBuildersHallGetsThreePearlsValidActivationNoCost() {
        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));
        GamePlayer fred = runner.addPlayer(Lords.BrianneByndraeth, "Fred", Long.valueOf(11));

        runner.setupQuests(Quests.ExploreAhghaironsTower, // Todd's 1st
                Quests.DeliverAnUltimatum, // dummy
                Quests.InstallSpyInCastleWaterdeep, // Todd
                Quests.AmbushArtorMorlin, // Dummy
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.BolsterGriffinCavalry, 
                Quests.BribeTheShipwrights);

        runner.setupIntrigues(Intrigues.Ambush1, Intrigues.ArcaneMishap1, Intrigues.BiddingWar1);
        runner.setupBuildings(Buildings.ThreePearls, Buildings.JestersCourt, Buildings.MonstersMadeToOrder, Buildings.Zoarstar);
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        fred.getResources().addFighter(2);
        runner.startTurn();
        
        assertEquals(4, todd.getResources().getGold());
        
        todd = todd.placeAgent(Buildings.BuildersHall).andBuyBuilding(Buildings.ThreePearls).andEndTurn();
        
        // It should still add the gold, even though a quest wasn't selected
        assertEquals(1, todd.getOwnedBuildings().size());
        assertEquals(0, todd.getResources().getGold());
        assertEquals(1, todd.getVictoryPoints());
        
        assertEquals(3, runner.getBuildingsInBuildersHall().size());
        List<Building> lb = runner.getBuildingsInBuildersHall();
        assertEquals(Buildings.JestersCourt, lb.get(0).getBuildingInfo());
        assertEquals(Buildings.MonstersMadeToOrder, lb.get(1).getBuildingInfo());
        assertEquals(Buildings.Zoarstar, lb.get(2).getBuildingInfo());

        try {
        fred = fred.placeAgent(Buildings.ThreePearls).andPayVariableActivation(ResourceChoices.TwoFighters).andEndTurn();
        } catch (UnsupportedOperationException ex) {
            // Good
        }
        assertTrue(fred.getTurnState().hasBuildingAction(BuildingActions.RequiresVariableActivation));
        assertTrue(fred.getTurnState().hasBuildingAction(BuildingActions.RequiresVariableReward));
        assertTrue(fred.getTurnState().hasTurnAction(TurnActions.VariableActivationHandled));
        
        runner.renderToFile("c:/duelssol/ut4.html");
        
    }
    
    @Test
    public void testLandingOnBuildersHallGetsThreePearlsValidActivationInvalidCost() {
        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));
        GamePlayer fred = runner.addPlayer(Lords.BrianneByndraeth, "Fred", Long.valueOf(11));

        runner.setupQuests(Quests.ExploreAhghaironsTower, // Todd's 1st
                Quests.DeliverAnUltimatum, // dummy
                Quests.InstallSpyInCastleWaterdeep, // Todd
                Quests.AmbushArtorMorlin, // Dummy
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.BolsterGriffinCavalry, 
                Quests.BribeTheShipwrights);

        runner.setupIntrigues(Intrigues.Ambush1, Intrigues.ArcaneMishap1, Intrigues.BiddingWar1);
        runner.setupBuildings(Buildings.ThreePearls, Buildings.JestersCourt, Buildings.MonstersMadeToOrder, Buildings.Zoarstar);
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        fred.getResources().addFighter(2);
        runner.startTurn();
        
        assertEquals(4, todd.getResources().getGold());
        
        todd = todd.placeAgent(Buildings.BuildersHall).andBuyBuilding(Buildings.ThreePearls).andEndTurn();
        
        // It should still add the gold, even though a quest wasn't selected
        assertEquals(1, todd.getOwnedBuildings().size());
        assertEquals(0, todd.getResources().getGold());
        assertEquals(1, todd.getVictoryPoints());
        
        assertEquals(3, runner.getBuildingsInBuildersHall().size());
        List<Building> lb = runner.getBuildingsInBuildersHall();
        assertEquals(Buildings.JestersCourt, lb.get(0).getBuildingInfo());
        assertEquals(Buildings.MonstersMadeToOrder, lb.get(1).getBuildingInfo());
        assertEquals(Buildings.Zoarstar, lb.get(2).getBuildingInfo());

        try {
            fred = fred.placeAgent(Buildings.ThreePearls).andPayVariableActivation(ResourceChoices.TwoFighters).andTakeVariableReward(ResourceChoices.OneRogue).andEndTurn();
        } catch (UnsupportedOperationException ex) {
            // Good
        }
        assertTrue(fred.getTurnState().hasBuildingAction(BuildingActions.RequiresVariableActivation));
        assertTrue(fred.getTurnState().hasBuildingAction(BuildingActions.RequiresVariableReward));
        assertTrue(fred.getTurnState().hasTurnAction(TurnActions.VariableActivationHandled));
        assertFalse(fred.getTurnState().hasTurnAction(TurnActions.VariableRewardHandled));
        
        runner.renderToFile("c:/duelssol/ut5.html");
        
    }
    
    @Test
    public void testLandingOnBuildersHallGetsThreePearlsValidActivationValidRewardNoOwner() {
        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));
        GamePlayer fred = runner.addPlayer(Lords.BrianneByndraeth, "Fred", Long.valueOf(11));

        runner.setupQuests(Quests.ExploreAhghaironsTower, // Todd's 1st
                Quests.DeliverAnUltimatum, // dummy
                Quests.InstallSpyInCastleWaterdeep, // Todd
                Quests.AmbushArtorMorlin, // Dummy
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.BolsterGriffinCavalry, 
                Quests.BribeTheShipwrights);

        runner.setupIntrigues(Intrigues.Ambush1, Intrigues.ArcaneMishap1, Intrigues.BiddingWar1);
        runner.setupBuildings(Buildings.ThreePearls, Buildings.JestersCourt, Buildings.MonstersMadeToOrder, Buildings.Zoarstar);
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        fred.getResources().addFighter(2);
        runner.startTurn();
        
        assertEquals(4, todd.getResources().getGold());
        
        todd = todd.placeAgent(Buildings.BuildersHall).andBuyBuilding(Buildings.ThreePearls).andEndTurn();
        
        // It should still add the gold, even though a quest wasn't selected
        assertEquals(1, todd.getOwnedBuildings().size());
        assertEquals(0, todd.getResources().getGold());
        assertEquals(1, todd.getVictoryPoints());
        
        assertEquals(3, runner.getBuildingsInBuildersHall().size());
        List<Building> lb = runner.getBuildingsInBuildersHall();
        assertEquals(Buildings.JestersCourt, lb.get(0).getBuildingInfo());
        assertEquals(Buildings.MonstersMadeToOrder, lb.get(1).getBuildingInfo());
        assertEquals(Buildings.Zoarstar, lb.get(2).getBuildingInfo());

        try {
            fred = fred.placeAgent(Buildings.ThreePearls).andPayVariableActivation(ResourceChoices.TwoFighters).andTakeVariableReward(ResourceChoices.ThreeWizards).andEndTurn();
        } catch (UnsupportedOperationException ex) {
            // Good
        }
        assertTrue(fred.getTurnState().hasBuildingAction(BuildingActions.RequiresVariableActivation));
        assertTrue(fred.getTurnState().hasBuildingAction(BuildingActions.RequiresVariableReward));
        assertTrue(fred.getTurnState().hasBuildingAction(BuildingActions.RequiresVariableOwnerReward));
        assertTrue(fred.getTurnState().hasTurnAction(TurnActions.VariableRewardHandled));
        assertTrue(fred.getTurnState().hasTurnAction(TurnActions.VariableActivationHandled));
        
        runner.renderToFile("c:/duelssol/ut6.html");
        
    }
    
    @Test
    public void testLandingOnBuildersHallGetsThreePearlsValidActivationValidRewardValidOwner() {
        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));
        GamePlayer fred = runner.addPlayer(Lords.BrianneByndraeth, "Fred", Long.valueOf(11));

        runner.setupQuests(Quests.ExploreAhghaironsTower, // Todd's 1st
                Quests.DeliverAnUltimatum, // dummy
                Quests.InstallSpyInCastleWaterdeep, // Todd
                Quests.AmbushArtorMorlin, // Dummy
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.BolsterGriffinCavalry, 
                Quests.BribeTheShipwrights);

        runner.setupIntrigues(Intrigues.Ambush1, Intrigues.ArcaneMishap1, Intrigues.BiddingWar1);
        runner.setupBuildings(Buildings.ThreePearls, Buildings.JestersCourt, Buildings.MonstersMadeToOrder, Buildings.Zoarstar);
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        fred.getResources().addFighter(2);
        runner.startTurn();
        
        assertEquals(4, todd.getResources().getGold());
        
        todd = todd.placeAgent(Buildings.BuildersHall).andBuyBuilding(Buildings.ThreePearls).andEndTurn();
        
        // It should still add the gold, even though a quest wasn't selected
        assertEquals(1, todd.getOwnedBuildings().size());
        assertEquals(0, todd.getResources().getGold());
        assertEquals(1, todd.getVictoryPoints());
        
        assertEquals(3, runner.getBuildingsInBuildersHall().size());
        List<Building> lb = runner.getBuildingsInBuildersHall();
        assertEquals(Buildings.JestersCourt, lb.get(0).getBuildingInfo());
        assertEquals(Buildings.MonstersMadeToOrder, lb.get(1).getBuildingInfo());
        assertEquals(Buildings.Zoarstar, lb.get(2).getBuildingInfo());
        
        fred = fred.placeAgent(Buildings.ThreePearls).andPayVariableActivation(ResourceChoices.TwoFighters).andTakeVariableReward(ResourceChoices.ThreeWizards).andTakeVariableOwnerReward(ResourceChoices.OneRogue).andEndTurn();
        
        runner.renderToFile("c:/duelssol/ut7.html");
        
    }
    
    @Test
    public void testTwoPlayerAttemptOne() {

        GameRunner runner = new GameRunner();
        
        GamePlayer todd = runner.addPlayer(Lords.BrianneByndraeth, "Todd", Long.valueOf(10));
        GamePlayer dummy = runner.addPlayer(Lords.BrianneByndraeth, "Idiot", Long.valueOf(11));
        
        // Steps for setting up.
        // Stack the Quests
        runner.setupQuests(Quests.ExploreAhghaironsTower, // Todd's 1st
                Quests.DeliverAnUltimatum, // dummy
                Quests.InstallSpyInCastleWaterdeep, // Todd
                Quests.AmbushArtorMorlin, // Dummy
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.BolsterGriffinCavalry, 
                Quests.BribeTheShipwrights);
        // Stack the intrigues
        // Stack the buildings
        runner.setupBuildings(Buildings.FetlockCourt, Buildings.YawningPortal, Buildings.Waymoot);

        // Start the game
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        runner.startTurn();

        runner.renderToFile("c:/duelssol/firstgame.html");
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.talwood.lords.gamerunner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.Lords;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.Res;
import org.talwood.lords.resources.Resources;

/**
 *
 * @author twalker
 */
public class DaniloLordTest {
    
    public DaniloLordTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testUseDaniloSecond() {
        GameRunner runner = new GameRunner();
        
        GamePlayer dummy = runner.addPlayer(Lords.LarissaNeathal, "Useful Idiot", Long.valueOf(9));
        GamePlayer todd = runner.addPlayer(Lords.DaniloThann, "Todd", Long.valueOf(10));

        runner.setupQuests(Quests.SwindleTheBuildersGuilds,
                Quests.RecruitLieutenant,
                Quests.LureArtisansOfMirabar,
                Quests.DiplomaticMissionToSuzail,
                Quests.ThreatenTheBuildersGuilds,
                Quests.ProtectTheHouseOfWonder,
                Quests.ExploreAhghaironsTower,
                Quests.PlaceSleeperAgentInSkullport,
                Quests.FenceGoodsForDukeOfDarkness,
                Quests.QuellMercenaryUprising,
                Quests.InstallSpyInCastleWaterdeep,
                Quests.TrainBladesingers,
                Quests.BribeTheShipwrights,
                Quests.StudyTheIlluskArch,
                Quests.AmbushArtorMorlin,
                Quests.DefeatUprisingFromUndermountain,
                Quests.SafeguardEltorchulMage,
                Quests.RecruitForBlackstaffAcademy,
                Quests.FormAllianceWithTheRashemi,
                Quests.RescueVictimFromTheSkulls,
                Quests.BolsterCityGuard,
                Quests.HealFallenGrayHandSoldiers,
                Quests.HostFestivalForSune,
                Quests.SponsorBountyHunters,
                Quests.UncoverDrowPlot,
                Quests.StudyInTheLibrarium,
                Quests.ExposeCultCorruption,
                Quests.BuildReputationInSkullport,
                Quests.SanctifyTempleToOghma,
                Quests.DiscoverHiddenTempleOfLolth,
                Quests.CreateShrineToOghma,
                Quests.EstablishCultCell,
                Quests.BuryTheBodies,
                Quests.PayFines,
                Quests.InfiltrateHalastersCircle,
                Quests.ProcureStolenGoods,
                Quests.DomesticateOwlbears,
                Quests.BolsterGriffinCavalry,
                Quests.ThinTheCityWatch,
                Quests.ImprovePrisonSecurity,
                Quests.EstablishShadowThievesGuild,
                Quests.WakeTheSixSleepers,
                Quests.SurviveArcturiasTransformarion,
                Quests.DefendTheLanceboardRoom,
                Quests.StealSpellbookFromSilverhand,
                Quests.ExpandGuildActivities,
                Quests.RetrieveAncientArtifacts,
                Quests.ResurrectDeadWizards,
                Quests.DeliverWeaponsToSelunesTemple,
                Quests.InvestigateAberrantInfestation,
                Quests.FundPilgrimageOfWaukeen,
                Quests.UncoverForbiddenLore,
                Quests.UnleashCrimeSpree,
                Quests.FixTheChampionsGames,
                Quests.PerformThePenanceOfDuty,
                Quests.PlunderTheIslandTemple,
                Quests.RansackWhitehelmsTomb,
                Quests.RaidOnUndermountain,
                Quests.SealAnEntranceToSkullport,
                Quests.DestroyTempleOfSelvetarm,
                Quests.ExtortAurora,
                Quests.TakeOverRivalOrganization,
                Quests.EstablishHarpersSafeHouse,
                Quests.AllyWithHouseThann,
                Quests.DefendTheYawningPortal,
                Quests.DeliverAnUltimatum,
                Quests.ResearchChronomacy,
                Quests.ConfrontTheXanathar,
                Quests.RecruitAcademyCastoffs,
                Quests.BattleInMuiralsGauntlet,
                Quests.DonateToTheCity,
                Quests.ExploreTrobriandsGraveyard
                );

        runner.setupBuildings(Buildings.SmugglersDock, 
                Buildings.DragonTower, 
                Buildings.RoomOfWisdom, 
                Buildings.Skulkway,
                Buildings.ShadowduskHold,
                Buildings.HighDukesTomb,
                Buildings.CitadelOfTheBloodyHand,
                Buildings.Librarium,
                Buildings.PalaceOfWaterdeep,
                Buildings.ThreePearls,
                Buildings.MonstersMadeToOrder,
                Buildings.PoisonedQuill,
                Buildings.HouseOfGoodSpirits,
                Buildings.HellHoundsMuzzle,
                Buildings.HallOfManyPillars,
                Buildings.NewOlamn,
                Buildings.HouseOfTheMoon,
                Buildings.TowerOfLuck,
                Buildings.ThrownGauntlet
                );
        
        runner.setupIntrigues(
                Intrigues.BlackMarketMoney, 
                Intrigues.IronRingSlaves, 
                Intrigues.ArchitecturalInnovation, 
                Intrigues.InformationBroker1, 
                Intrigues.GraduationDay1,
                Intrigues.SpreadTheWealth1,
                Intrigues.DarkDaggerAssassination,
                Intrigues.SpreadTheWealth2,
                Intrigues.Conscription1,
                Intrigues.CrimeWave1,
                Intrigues.GraduationDay2,
                Intrigues.InformationBroker2,
                Intrigues.GoodFaith1,
                Intrigues.CallForAssistance1,
                Intrigues.Repent1,
                Intrigues.PlacateAngryMerchants,
                Intrigues.CrimeWave2,
                Intrigues.FreeDrinks1,
                Intrigues.RecruitmentDrive,
                Intrigues.OrganizedCrime,
                Intrigues.QuellRiots,
                Intrigues.Conscription2,
                Intrigues.RecallAgent1,
                Intrigues.GoodFaith2,
                Intrigues.RecruitSpies,
                Intrigues.FreeDrinks2,
                Intrigues.MindFlayerMercenaries,
                Intrigues.Repent2,
                Intrigues.Repent3,
                Intrigues.CallInAFavor1,
                Intrigues.CallForAssistance2,
                Intrigues.CallInAFavor2,
                Intrigues.AlliedFaiths,
                Intrigues.HonorAmongThieves,
                Intrigues.FriendlyLoan,
                Intrigues.UnlikelyAssistance,
                Intrigues.MercenaryContract,
                Intrigues.Scapegoat1,
                Intrigues.Scapegoat2,
                Intrigues.ChangeOfPlans,
                Intrigues.RecallAgent2,
                Intrigues.SponsorApprentices
                
                );
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        
        // TURN 1.
        runner.startTurn();
        
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.SmugglersDock)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.ThreeFightersOneRogue)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.EntryWell)
                .andTakeQuest(Quests.ThreatenTheBuildersGuilds)
                .andPlayIntrigue(Intrigues.BlackMarketMoney)
                .andChooseOpponentForReward(todd)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.IronRingSlaves)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.ArchitecturalInnovation)
                .andBuyBuilding(Buildings.RoomOfWisdom).andEndTurn();
        todd = todd.placeAgent(Buildings.SkullIsland)
                .andTakeVariableReward(ResourceChoices.OneClericOneWizardOneCorruption)
                .andCompleteQuest(Quests.RecruitLieutenant)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BlackstaffTower).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.InformationBroker1).andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.FieldOfTriumph)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.FieldOfTriumph)
                .andCompleteQuest(Quests.DiplomaticMissionToSuzail).andEndTurn();
        dummy = dummy.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.AurorasRealm).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.HallOfMirrors)
                .andTakeVariableReward(ResourceChoices.OneFighterOneRogue)
                .andEndTurn();
                
        // Turn 2
        runner.startTurn();
//        
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.Skulkway).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.SpreadTheWealth1)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern).andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.SmugglersDock)
                .andCompleteQuestInCliffwatchInn(Quests.ProtectTheHouseOfWonder)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.DarkDaggerAssassination)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.GraduationDay1)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.Skulkway)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.ExploreAhghaironsTower)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.BlackstaffTower)
                .andCompleteQuestInCliffwatchInn(Quests.PlaceSleeperAgentInSkullport)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.ThreeFightersOneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.FenceGoodsForDukeOfDarkness)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.SlaversMarket)
                .andCompleteQuestInCliffwatchInn(Quests.QuellMercenaryUprising)
                .andEndTurn();
        
        // Start turn 3
        runner.startTurn();
        dummy = dummy.placeAgent(Buildings.SlaversMarket)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.Conscription1)
                .andChooseOpponentForReward(dummy)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BlackstaffTower)
                .andCompleteQuest(Quests.ThreatenTheBuildersGuilds)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.DragonTower)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.SpreadTheWealth2)
                .andChooseOpponentForReward(todd)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.GraduationDay2)
                .andChooseOpponentForReward(dummy)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.TwoFightersTwoRogues)
                .andCompleteQuestInCliffwatchInn(Quests.InstallSpyInCastleWaterdeep)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.HighDukesTomb, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.TrainBladesingers)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.BribeTheShipwrights)
                .andEndTurn();
        dummy = dummy.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.BuildersHall)
                .andBuyBuilding(Buildings.Librarium)
                .andCompleteQuest(Quests.LureArtisansOfMirabar)
                .andTakeBuildingFromBuildersHallAsQuestReward(Buildings.CitadelOfTheBloodyHand)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne)
                .andCompleteQuestInCliffwatchInn(Quests.StudyTheIlluskArch)
                .andEndTurn();

        // Turn 4
        runner.startTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo)
                .andCompleteQuestInCliffwatchInn(Quests.AmbushArtorMorlin)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andCompleteQuest(Quests.SwindleTheBuildersGuilds)
                .andTakeBuildingsFromBuildersHallAsQuestReward(Buildings.MonstersMadeToOrder, Buildings.PalaceOfWaterdeep)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.InformationBroker2)
                .andCompleteQuestInCliffwatchInn(Quests.DefeatUprisingFromUndermountain)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.DragonTower)
                .andCompleteQuestInCliffwatchInn(Quests.SafeguardEltorchulMage)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.PoisonedQuill)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.FreeDrinks1)
                .andStealResources(dummy, ResourceChoices.OneCleric)
                .andCompleteQuestInCliffwatchInn(Quests.RecruitForBlackstaffAcademy)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.GoodFaith1)
                .andCompleteQuestInCliffwatchInn(Quests.FormAllianceWithTheRashemi)
                .andTakeQuestAsQuestReward(Quests.BolsterCityGuard)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.RescueVictimFromTheSkulls)
                .andReplaceGenericResources(new Resources(Res.cleric(1)))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.HealFallenGrayHandSoldiers)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PalaceOfWaterdeep)
                .andEndTurn();
        
        // Turn 5 - Unleash the kraken!
        runner.startTurn();
        todd = todd.placeAmbassador(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo)
                .andCompleteQuest(Quests.BolsterCityGuard)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.RecruitmentDrive)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborThree, Buildings.WaterdeepHarborTwo)
                .andCompleteQuestInCliffwatchInn(Quests.HostFestivalForSune)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.SponsorBountyHunters)
                .andCompleteQuest(Quests.SponsorBountyHunters)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.OrganizedCrime)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborThree, Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.UncoverDrowPlot)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.HallOfTheVoice)
                .andTakeQuest(Quests.StudyInTheLibrarium)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.CrimeWave1)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.ExposeCultCorruption)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.ThreePearls)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.BuildReputationInSkullport)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.SanctifyTempleToOghma)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.PoisonedQuill)
                .andPlayIntrigue(Intrigues.CallForAssistance1)
                .andStealResources(dummy, ResourceChoices.TwoRogues)
                .andCompleteQuestInCliffwatchInn(Quests.DiscoverHiddenTempleOfLolth)
                .andTakeQuestAsQuestReward(Quests.CreateShrineToOghma)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.EstablishCultCell)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.WaterdeepHarborOne)
                .andCompleteQuestInCliffwatchInn(Quests.BuryTheBodies)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.PayFines)
                .andEndTurn();

        runner.startTurn();
        todd = todd.placeAmbassador(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.CrimeWave2)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.InfiltrateHalastersCircle)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.HellHoundsMuzzle)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.MindFlayerMercenaries)
                .andCompleteQuestInCliffwatchInn(Quests.ProcureStolenGoods)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.GoodFaith2)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.DomesticateOwlbears)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.BolsterGriffinCavalry)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.HighDukesTomb)
                .andCompleteQuestInCliffwatchInn(Quests.ThinTheCityWatch)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.RoomOfWisdom)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.HellHoundsMuzzle)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.ImprovePrisonSecurity)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.EstablishShadowThievesGuild)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.WakeTheSixSleepers)
                .andEndTurn();

        // Turn 7!
        runner.startTurn();
        todd = todd.placeAmbassador(Buildings.HellHoundsMuzzle)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.SurviveArcturiasTransformarion)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BlackstaffTower)
                .andCompleteQuest(Quests.StudyInTheLibrarium)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.Conscription2)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.DefendTheLanceboardRoom)
                .andReplaceGenericResources(new Resources(Res.cleric(4), Res.wizard(4)))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.Repent1)
                .andCompleteQuest(Quests.CreateShrineToOghma)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.CallForAssistance2)
                .andStealResources(dummy, ResourceChoices.TwoRogues)
                .andCompleteQuestInCliffwatchInn(Quests.StealSpellbookFromSilverhand)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.ExpandGuildActivities)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.RetrieveAncientArtifacts)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.Librarium, Buildings.PalaceOfWaterdeep)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.CitadelOfTheBloodyHand)
                .andCompleteQuestInCliffwatchInn(Quests.ResurrectDeadWizards)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.DeliverWeaponsToSelunesTemple)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.InvestigateAberrantInfestation)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.PoisonedQuill)
                .andPlayIntrigue(Intrigues.Repent2)
                .andCompleteQuestInCliffwatchInn(Quests.FundPilgrimageOfWaukeen)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.UncoverForbiddenLore)
                .andEndTurn();

        // Turn 8!
        runner.startTurn();
        todd = todd.placeAmbassador(Buildings.HellHoundsMuzzle)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.UnleashCrimeSpree)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.CitadelOfTheBloodyHand, Buildings.Librarium)
                .andEndTurn();
        // Agent 1
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.FixTheChampionsGames)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.HallOfManyPillars)
                .andEndTurn();
        // Agent 2
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.ShadowduskHold)
                .andCompleteQuestInCliffwatchInn(Quests.PerformThePenanceOfDuty)
                .andEndTurn();
        
        dummy = dummy.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.PlunderTheIslandTemple)
                .andCompleteQuest(Quests.PlunderTheIslandTemple)
                .andEndTurn();
        // Agent 3
        todd = todd.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.ShadowduskHold)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.CliffwatchInnIntrigue)
                .andTakeQuest(Quests.RansackWhitehelmsTomb)
                .andCompleteQuest(Quests.RansackWhitehelmsTomb)
                .andEndTurn();
        // Agent 4
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.MonstersMadeToOrder, Buildings.ThreePearls)
                .andCompleteQuestInCliffwatchInn(Quests.RaidOnUndermountain)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        // Agent 5.
        todd = todd.placeAgent(Buildings.ThreePearls)
                .andPayVariableActivation(ResourceChoices.OneFighterOneRogue)
                .andTakeVariableReward(ResourceChoices.TwoClericsOneWizard)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.SealAnEntranceToSkullport)
                .andEndTurn();

                // Agent 6, and return 5
        todd = todd.placeAgent(Buildings.MonstersMadeToOrder)
                .andRecallAgents(Buildings.RoomOfWisdom, Buildings.ShadowduskHold, Buildings.CitadelOfTheBloodyHand, Buildings.ThreePearls, Buildings.Librarium)
//                .andCompleteQuestInCliffwatchInn(Quests.DonateToTheCity)
                .andEndTurn();
        
        // Agent 2
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.ThreePearls)
                .andCompleteQuestInCliffwatchInn(Quests.DestroyTempleOfSelvetarm)
                .andEndTurn();
        // Agent 3
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.HallOfManyPillars, Buildings.ThreePearls)
                .andEndTurn();
        
        // Agent 4
        todd = todd.placeAgent(Buildings.HallOfManyPillars)
                .andPlayMultiIntrigue(Intrigues.Scapegoat1)
                .andReturnResourcesForIntrigue(ResourceChoices.OneRogueTwoCorruption)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.Scapegoat2)
                .andReturnResourcesForIntrigue(ResourceChoices.OneFighterTwoCorruption)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.Repent3)
                .andFinishMultiIntrigue()
                .andCompleteQuestInCliffwatchInn(Quests.ExtortAurora)
                .andEndTurn();
        

        // Agent 5.
        todd = todd.placeAgent(Buildings.ThreePearls)
                .andPayVariableActivation(ResourceChoices.OneFighterOneRogue)
                .andTakeVariableReward(ResourceChoices.TwoClericsOneWizard)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.DefendTheYawningPortal)
                .andRecallAgents(Buildings.ThreePearls, Buildings.CitadelOfTheBloodyHand, Buildings.HallOfManyPillars)
                .andReplaceGenericResources(new Resources(Res.cleric(1), Res.wizard(1)))
                .andEndTurn();

        // Agent 3
        todd = todd.placeAgent(Buildings.HallOfManyPillars)
                .andPlayMultiIntrigue(Intrigues.UnlikelyAssistance)
                .andChooseOpponentForReward(dummy)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.HonorAmongThieves)
                .andChooseOpponentForReward(dummy)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.FriendlyLoan)
                .andChooseOpponentForReward(dummy)
                .andFinishMultiIntrigue()
                .andEndTurn();
        
        
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.RecallAgent1)
                .andRecallAgent(Buildings.HallOfManyPillars)
                .andCompleteQuestInCliffwatchInn(Quests.TakeOverRivalOrganization)
                .andEndTurn();
        
        // Agent 3
        todd = todd.placeAgent(Buildings.HallOfManyPillars)
                .andPlayMultiIntrigue(Intrigues.CallInAFavor1)
                .andTakeVariableIntrigueReward(ResourceChoices.TwoFighters)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.MercenaryContract)
                .andChooseOpponentForReward(dummy)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.AlliedFaiths)
                .andChooseOpponentForReward(dummy)
                .andFinishMultiIntrigue()
                .andEndTurn();
        
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborTwo, Buildings.WaterdeepHarborThree)
                .andCompleteQuestInCliffwatchInn(Quests.EstablishHarpersSafeHouse)
                .andEndTurn();
        
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.RecallAgent2)
                .andRecallAgent(Buildings.HallOfManyPillars)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.SponsorApprentices)
                .andSelectTargetBuildings(Buildings.HallOfManyPillars)
                .andCompleteQuestInCliffwatchInn(Quests.DeliverAnUltimatum)
                .andEndTurn();
        

        // Three reallocations...
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.HallOfManyPillars)
                .andPlayMultiIntrigue(Intrigues.CallInAFavor2)
                .andTakeVariableIntrigueReward(ResourceChoices.OneWizard)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.FreeDrinks2)
                .andStealResources(dummy, ResourceChoices.OneCleric)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.RecruitSpies)
                .andChooseOpponentForReward(dummy)
                .andFinishMultiIntrigue()
                .andCompleteQuestInCliffwatchInn(Quests.ResearchChronomacy)
                .andRecallAgent(Buildings.CitadelOfTheBloodyHand)
                .andEndTurn();
        
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.Librarium, Buildings.ThreePearls)
//                .andTakeVariableReward(ResourceChoices.TwoClericsOneWizard)
//                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.ConfrontTheXanathar)
                .andEndTurn();

        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.ThreePearls)
                .andCompleteQuestInCliffwatchInn(Quests.RecruitAcademyCastoffs)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.ThreePearls)
                .andPayVariableActivation(ResourceChoices.OneFighterOneRogue)
                .andTakeVariableReward(ResourceChoices.TwoClericsOneWizard)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.BattleInMuiralsGauntlet)
                .andEndTurn();
        
        runner.finishGame();
        
        runner.renderToFile("c:/duelssol/danilobig.html");
    }
    
}

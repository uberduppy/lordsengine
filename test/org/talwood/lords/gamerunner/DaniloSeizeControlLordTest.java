/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.talwood.lords.gamerunner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.Lords;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.Res;
import org.talwood.lords.resources.Resources;

/**
 *
 * @author twalker
 */
public class DaniloSeizeControlLordTest {
    
    public DaniloSeizeControlLordTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testUseDaniloSecond() {
        GameRunner runner = new GameRunner();
        
        GamePlayer dummy = runner.addPlayer(Lords.LarissaNeathal, "Useful Idiot", Long.valueOf(9));
        GamePlayer todd = runner.addPlayer(Lords.DaniloThann, "Todd", Long.valueOf(10));

        runner.setupQuests(Quests.SwindleTheBuildersGuilds,
                Quests.RecruitLieutenant,
                Quests.LureArtisansOfMirabar,
                Quests.DiplomaticMissionToSuzail,
                Quests.ThreatenTheBuildersGuilds,
                Quests.ProtectTheHouseOfWonder,
                Quests.SeizeControlOfTheBloodyHand,
                Quests.ExploreAhghaironsTower,
                Quests.PlaceSleeperAgentInSkullport,
                Quests.RecruitForCityWatch,
                Quests.FenceGoodsForDukeOfDarkness,
                Quests.QuellMercenaryUprising,
                Quests.InstallSpyInCastleWaterdeep,
                Quests.TrainBladesingers,
                Quests.BribeTheShipwrights,
                Quests.StudyTheIlluskArch,
                Quests.SafeguardEltorchulMage,
                Quests.ProcureStolenGoods,
                Quests.BolsterGriffinCavalry,
                Quests.EstablishNewMerchantGuild,
                Quests.AmbushArtorMorlin,
                Quests.RecruitAcademyCastoffs,
                Quests.RecruitForBlackstaffAcademy,
                Quests.DefeatUprisingFromUndermountain,
                Quests.SealAnEntranceToSkullport,
                Quests.SurviveArcturiasTransformarion,
                Quests.EliminateVampireCoven,
                Quests.SponsorBountyHunters,
                Quests.FormAllianceWithTheRashemi,
                Quests.BolsterCityGuard,
                Quests.StudyInTheLibrarium,
                Quests.RaidOnUndermountain,
                Quests.BreakIntoBlackstaffTower,
                Quests.SanctifyTempleToOghma,
                Quests.DiscoverHiddenTempleOfLolth,
                Quests.CreateShrineToOghma,
                Quests.DomesticateOwlbears,
                Quests.DefendTheLanceboardRoom,
                Quests.ExploreTrobriandsGraveyard,
                Quests.PayFines,
                Quests.PlunderTheIslandTemple,
                Quests.BuryTheBodies,
                Quests.HealFallenGrayHandSoldiers,
                Quests.BattleInMuiralsGauntlet,
                Quests.BanishEvilSpirits,
                Quests.ConfrontTheXanathar,
                Quests.HostFestivalForSune,
                Quests.RenewGuardsAndWards,
                Quests.AllyWithHouseThann,
                Quests.ResurrectDeadWizards,
                Quests.InfiltrateHalastersCircle,
                Quests.PatrolDockWard,
                Quests.ExposeCultCorruption,
                Quests.ExpandGuildActivities,
                Quests.EstablishShadowThievesGuild,
                Quests.GiveHonorToMask,
                Quests.DeliverAnUltimatum,
                Quests.InstituteReforms,
                Quests.FundAlchemicalResearch,
                Quests.EstablishTempleToIbrandul,
                Quests.RescueVictimFromTheSkulls,
                Quests.UncoverDrowPlot,
                Quests.BuildReputationInSkullport,
                Quests.EstablishCultCell,
                Quests.ThinTheCityWatch,
                Quests.ExposeRedWizardsSpies,
                Quests.RecruitPaladinsForTyr,
                Quests.RecoverTheFlameOfTheNorth,
                Quests.RescueClericsOfTymora,
                Quests.RansackWhitehelmsTomb,
                Quests.StealGemsFromTheBoneThrone,
                Quests.WakeTheSixSleepers,
                Quests.ImprovePrisonSecurity,
                Quests.StealSpellbookFromSilverhand,
                Quests.RetrieveAncientArtifacts,
                Quests.DefendTheYawningPortal,
                Quests.AssassinateRivals,
                Quests.DonateToTheCity,
                Quests.ResearchChronomacy,
                Quests.RaidOrcStronghold,
                Quests.RepelSeawraiths,
                Quests.SendAidToTheHarpers,
                Quests.SealGateToCyricsRealm,
                Quests.TrainCastleguards,
                Quests.EstablishHarpersSafeHouse
                );

        runner.setupBuildings(Buildings.SmugglersDock, 
                Buildings.DragonTower, 
                Buildings.RoomOfWisdom, 
                Buildings.Skulkway,
                Buildings.ShadowduskHold,
                Buildings.HighDukesTomb,
                Buildings.CitadelOfTheBloodyHand,
                Buildings.Librarium,
                Buildings.PalaceOfWaterdeep,
                Buildings.ThreePearls,
                Buildings.MonstersMadeToOrder,
                Buildings.PoisonedQuill,
                Buildings.HouseOfGoodSpirits,
                Buildings.HellHoundsMuzzle,
                Buildings.HallOfManyPillars,
                Buildings.NewOlamn,
                Buildings.HouseOfTheMoon,
                Buildings.TowerOfLuck,
                Buildings.ThrownGauntlet
                );
        
        runner.setupIntrigues(
                Intrigues.BlackMarketMoney, 
                Intrigues.IronRingSlaves, 
                Intrigues.ArchitecturalInnovation, 
                Intrigues.InformationBroker1, 
                Intrigues.GraduationDay1,
                Intrigues.GraduationDay2,
                Intrigues.DarkDaggerAssassination,
                Intrigues.SpreadTheWealth1,
                Intrigues.Conscription1,
                Intrigues.SpreadTheWealth2,
                Intrigues.CrimeWave1,
                Intrigues.InformationBroker2,
                Intrigues.GoodFaith1,
                Intrigues.CallForAssistance1,
                Intrigues.Repent1,
                Intrigues.CrimeWave2,
                Intrigues.PlacateAngryMerchants,
                Intrigues.OrganizedCrime,
                Intrigues.RecruitmentDrive,
                Intrigues.QuellRiots,
                Intrigues.Conscription2,
                Intrigues.MindFlayerMercenaries,
                Intrigues.GoodFaith2,
                Intrigues.RecruitSpies,
                Intrigues.FreeDrinks2,
                Intrigues.RecallAgent1,
                Intrigues.Repent2,
                Intrigues.Repent3,
                Intrigues.CallInAFavor1,
                Intrigues.RecallAgent2,
                Intrigues.CallForAssistance2,
                Intrigues.SponsorApprentices,
                Intrigues.Scapegoat1,
                Intrigues.StampOutCultists,
                Intrigues.Scapegoat2,
                Intrigues.AlliedFaiths,
                Intrigues.UnexpectedSuccess1,
                Intrigues.UnexpectedSuccess2,
                Intrigues.FriendlyLoan,
                Intrigues.UnlikelyAssistance,
                Intrigues.MercenaryContract,
                Intrigues.FreeDrinks1,
                Intrigues.CallForAdventurers1,
                Intrigues.CallForAdventurers2,
                Intrigues.HonorAmongThieves,
                Intrigues.ChangeOfPlans,
                Intrigues.CallInAFavor2
                
                );
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        
        // TURN 1.
        runner.startTurn();
        
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.SmugglersDock)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.ThreeFightersOneRogue)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.EntryWell)
                .andTakeQuest(Quests.ThreatenTheBuildersGuilds)
                .andPlayIntrigue(Intrigues.BlackMarketMoney)
                .andChooseOpponentForReward(todd)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.IronRingSlaves)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.ArchitecturalInnovation)
                .andBuyBuilding(Buildings.RoomOfWisdom).andEndTurn();
        todd = todd.placeAgent(Buildings.SkullIsland)
                .andTakeVariableReward(ResourceChoices.OneClericOneWizardOneCorruption)
                .andCompleteQuest(Quests.RecruitLieutenant)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BlackstaffTower).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.InformationBroker1).andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.FieldOfTriumph)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.FieldOfTriumph)
                .andCompleteQuest(Quests.DiplomaticMissionToSuzail).andEndTurn();
        dummy = dummy.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.AurorasRealm).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.HallOfMirrors)
                .andTakeVariableReward(ResourceChoices.OneCleric)
                .andEndTurn();
                
        // Turn 2
        runner.startTurn();
        
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.Skulkway).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.GraduationDay1)
                .andChooseOpponentForReward(dummy)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern).andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.SmugglersDock)
                .andCompleteQuestInCliffwatchInn(Quests.ProtectTheHouseOfWonder)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm).andEndTurn();
        todd = todd.placeAgent(Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.FourFighters)
                .andCompleteQuestInCliffwatchInn(Quests.SeizeControlOfTheBloodyHand)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth).andEndTurn();
        
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.DarkDaggerAssassination)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.GraduationDay2)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.Skulkway)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.ExploreAhghaironsTower)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.FieldOfTriumph)
                .andCompleteQuestInCliffwatchInn(Quests.PlaceSleeperAgentInSkullport)
                .andReturnOneQuestResourceUsed(Res.fighter(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.SlaversMarket)
                .andCompleteQuestInCliffwatchInn(Quests.QuellMercenaryUprising)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        // Start turn 3
        runner.startTurn();
        dummy = dummy.placeAgent(Buildings.SlaversMarket)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.SkullIsland)
                .andTakeVariableReward(ResourceChoices.OneClericOneWizardOneCorruption)
                .andCompleteQuestInCliffwatchInn(Quests.RecruitForCityWatch)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();

        dummy = dummy.placeAgent(Buildings.BlackstaffTower)
                .andCompleteQuest(Quests.ThreatenTheBuildersGuilds)
                .andEndTurn();

        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.Conscription1)
                .andChooseOpponentForReward(dummy)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.SpreadTheWealth1)
                .andChooseOpponentForReward(todd)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.AurorasRealm)
                .andCompleteQuestInCliffwatchInn(Quests.FenceGoodsForDukeOfDarkness)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        
        todd = todd.placeAgent(Buildings.DragonTower)
                .andCompleteQuestInCliffwatchInn(Quests.StudyTheIlluskArch)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.CrimeWave1)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.SafeguardEltorchulMage)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.ProcureStolenGoods)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        dummy = dummy.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.BuildersHall)
                .andBuyBuilding(Buildings.Librarium)
                .andCompleteQuest(Quests.LureArtisansOfMirabar)
                .andTakeBuildingFromBuildersHallAsQuestReward(Buildings.CitadelOfTheBloodyHand)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo)
                .andEndTurn();

        // Turn 4
        runner.startTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo)
                .andCompleteQuestInCliffwatchInn(Quests.BolsterGriffinCavalry)
                .andReturnOneQuestResourceUsed(Res.fighter(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andCompleteQuest(Quests.SwindleTheBuildersGuilds)
                .andTakeBuildingsFromBuildersHallAsQuestReward(Buildings.MonstersMadeToOrder, Buildings.PalaceOfWaterdeep)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.GoodFaith1)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.EstablishNewMerchantGuild)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.SpreadTheWealth2)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.TrainBladesingers)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.PoisonedQuill)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.InformationBroker2)
                .andCompleteQuestInCliffwatchInn(Quests.RecruitAcademyCastoffs)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.RecruitForBlackstaffAcademy)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.BribeTheShipwrights)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.SealAnEntranceToSkullport)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.SurviveArcturiasTransformarion)
                .andReturnOneQuestResourceUsed(Res.fighter(1))
                .andEndTurn();
        
        // Turn 5 - Unleash the kraken!
        runner.startTurn();
        todd = todd.placeAmbassador(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo)
                .andCompleteQuestInCliffwatchInn(Quests.EliminateVampireCoven)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.RecruitmentDrive)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborThree, Buildings.WaterdeepHarborTwo)
                .andCompleteQuestInCliffwatchInn(Quests.InstallSpyInCastleWaterdeep)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.SponsorBountyHunters)
                .andCompleteQuest(Quests.SponsorBountyHunters)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.OrganizedCrime)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborThree, Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.BolsterCityGuard)
                .andReturnOneQuestResourceUsed(Res.fighter(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.HallOfTheVoice)
                .andTakeQuest(Quests.StudyInTheLibrarium)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.CrimeWave2)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.RaidOnUndermountain)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.ThreePearls)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.BreakIntoBlackstaffTower)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.SanctifyTempleToOghma)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        todd = todd.placeAgent(Buildings.PoisonedQuill)
                .andPlayIntrigue(Intrigues.CallForAssistance1)
                .andStealResources(dummy, ResourceChoices.TwoRogues)
                .andCompleteQuestInCliffwatchInn(Quests.DiscoverHiddenTempleOfLolth)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andTakeQuestAsQuestReward(Quests.CreateShrineToOghma)
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.DomesticateOwlbears)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.WaterdeepHarborOne)
                .andCompleteQuestInCliffwatchInn(Quests.DefendTheLanceboardRoom)
                .andReplaceGenericResources(new Resources(Res.cleric(4), Res.wizard(4)))
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.ExploreTrobriandsGraveyard)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();

        // Turn 6?
        runner.startTurn();
        todd = todd.placeAmbassador(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo)
                .andCompleteQuestInCliffwatchInn(Quests.PayFines)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.Conscription2)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.PlunderTheIslandTemple)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.HellHoundsMuzzle)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.MindFlayerMercenaries)
                .andCompleteQuestInCliffwatchInn(Quests.BuryTheBodies)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.GoodFaith2)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.HealFallenGrayHandSoldiers)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.Librarium, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.BattleInMuiralsGauntlet)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.BanishEvilSpirits)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andReplaceGenericResources(new Resources(Res.wizard(1)))
                .andEndTurn();
        todd = todd.placeAgent(Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.ConfrontTheXanathar)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.HellHoundsMuzzle)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.HostFestivalForSune)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.RenewGuardsAndWards)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.AllyWithHouseThann)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
//
        // Turn 7!
        runner.startTurn();
        todd = todd.placeAmbassador(Buildings.HellHoundsMuzzle)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.ResurrectDeadWizards)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BlackstaffTower)
                .andCompleteQuest(Quests.StudyInTheLibrarium)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.RecruitSpies)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.InfiltrateHalastersCircle)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.Repent1)
                .andCompleteQuestInCliffwatchInn(Quests.PatrolDockWard)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.FreeDrinks2)
                .andStealResources(dummy, ResourceChoices.OneCleric)
                .andCompleteQuestInCliffwatchInn(Quests.ExposeCultCorruption)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        dummy = dummy.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.ExpandGuildActivities)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.Librarium)
                .andCompleteQuestInCliffwatchInn(Quests.FormAllianceWithTheRashemi)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andTakeQuestAsQuestReward(Quests.EstablishShadowThievesGuild)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.Librarium, Buildings.PalaceOfWaterdeep)
                .andEndTurn();
        todd = todd.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.CitadelOfTheBloodyHand)
                .andCompleteQuestInCliffwatchInn(Quests.GiveHonorToMask)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.ThreePearls)
                .andCompleteQuestInCliffwatchInn(Quests.DeliverAnUltimatum)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.PalaceOfWaterdeep, Buildings.ThreePearls)
                .andCompleteQuestInCliffwatchInn(Quests.InstituteReforms)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.ThreePearls)
                .andPayVariableActivation(ResourceChoices.OneRogueOneWizard)
                .andTakeVariableReward(ResourceChoices.OneClericOneFighterOneWizard)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.FundAlchemicalResearch)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PalaceOfWaterdeep)
                .andCompleteQuestInCliffwatchInn(Quests.EstablishTempleToIbrandul)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();

        // Turn 8!
        runner.startTurn();
        todd = todd.placeAmbassador(Buildings.HellHoundsMuzzle)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.ExposeRedWizardsSpies)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.HighDukesTomb)
                .andSelectTargetBuildings(Buildings.CitadelOfTheBloodyHand, Buildings.Librarium)
                .andCompleteQuest(Quests.ExpandGuildActivities)
                .andEndTurn();
        // Agent 1
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.RoomOfWisdom)
                .andCompleteQuestInCliffwatchInn(Quests.RecruitPaladinsForTyr)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.HallOfManyPillars)
                .andEndTurn();
        // Agent 2
        todd = todd.placeAgent(Buildings.RoomOfWisdom)
                .andSelectTargetBuildings(Buildings.ShadowduskHold)
                .andCompleteQuestInCliffwatchInn(Quests.ThinTheCityWatch)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        dummy = dummy.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.RecoverTheFlameOfTheNorth)
                .andCompleteQuest(Quests.RecoverTheFlameOfTheNorth)
                .andEndTurn();
        
        // Agent 3
        todd = todd.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.ShadowduskHold)
                .andCompleteQuestInCliffwatchInn(Quests.RescueClericsOfTymora)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.CliffwatchInnIntrigue)
                .andTakeQuest(Quests.RansackWhitehelmsTomb)
                .andCompleteQuest(Quests.RansackWhitehelmsTomb)
                .andEndTurn();
        // Agent 4
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.MonstersMadeToOrder, Buildings.HallOfManyPillars)
                .andCompleteQuestInCliffwatchInn(Quests.StealGemsFromTheBoneThrone)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andEndTurn();
        // Agent 5.
        todd = todd.placeAgent(Buildings.HallOfManyPillars)
                .andPlayMultiIntrigue(Intrigues.Scapegoat1)
                .andReturnResourcesForIntrigue(ResourceChoices.OneFighterTwoCorruption)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.Repent2)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.Repent3)
                .andFinishMultiIntrigue()
                .andCompleteQuestInCliffwatchInn(Quests.WakeTheSixSleepers)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        
//                // Agent 6, and return 5
        todd = todd.placeAgent(Buildings.MonstersMadeToOrder)
                .andRecallAgents(Buildings.RoomOfWisdom, Buildings.ShadowduskHold, Buildings.CitadelOfTheBloodyHand, Buildings.HallOfManyPillars, Buildings.Librarium)
                .andCompleteQuest(Quests.CreateShrineToOghma)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        // Agent 2
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.ShadowduskHold, Buildings.ThreePearls)
                .andCompleteQuestInCliffwatchInn(Quests.EstablishCultCell)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        // Agent 3
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.HallOfManyPillars, Buildings.ThreePearls)
                .andCompleteQuestInCliffwatchInn(Quests.ImprovePrisonSecurity)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        
        // Agent 4
        todd = todd.placeAgent(Buildings.HallOfManyPillars)
                .andPlayMultiIntrigue(Intrigues.Scapegoat2)
                .andReturnResourcesForIntrigue(ResourceChoices.OneFighterTwoCorruption)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.CallForAssistance2)
                .andStealResources(dummy, ResourceChoices.TwoRogues)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.AlliedFaiths)
                .andChooseOpponentForReward(dummy)
                .andFinishMultiIntrigue()
                .andCompleteQuest(Quests.UncoverDrowPlot)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();

        // Agent 5.
        todd = todd.placeAgent(Buildings.ThreePearls)
                .andPayVariableActivation(ResourceChoices.OneFighterOneRogue)
                .andTakeVariableReward(ResourceChoices.TwoClericsOneWizard)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.DefendTheYawningPortal)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andRecallAgents(Buildings.ThreePearls, Buildings.CitadelOfTheBloodyHand, Buildings.HallOfManyPillars)
                .andReplaceGenericResources(new Resources(Res.cleric(1), Res.wizard(1)))
                .andEndTurn();

        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo)
                .andCompleteQuestInCliffwatchInn(Quests.AssassinateRivals)
                .andReturnOneQuestResourceUsed(Res.fighter(1))
                .andEndTurn();
        
        // Agent 3
        todd = todd.placeAgent(Buildings.HallOfManyPillars)
                .andPlayMultiIntrigue(Intrigues.UnexpectedSuccess1)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.SponsorApprentices)
                .andSelectTargetBuildings(Buildings.HallOfManyPillars)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.RecallAgent1)
                .andRecallAgent(Buildings.HallOfManyPillars)
                .andFinishMultiIntrigue()
                .andCompleteQuestInCliffwatchInn(Quests.DonateToTheCity)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
//        
        todd = todd.placeAgent(Buildings.HallOfManyPillars)
                .andPlayMultiIntrigue(Intrigues.UnexpectedSuccess2)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.FriendlyLoan)
                .andChooseOpponentForReward(dummy)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.RecallAgent2)
                .andRecallAgent(Buildings.HallOfManyPillars)
                .andFinishMultiIntrigue()
                .andCompleteQuestInCliffwatchInn(Quests.ResearchChronomacy)
                .andReturnOneQuestResourceUsed(Res.wizard(1))
                .andRecallAgent(Buildings.CitadelOfTheBloodyHand)
                .andEndTurn();
//        
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.CallForAdventurers1)
                .andTakeVariableIntrigueReward(ResourceChoices.OneFighterOneWizard)
                .andTakeVariableIntrigueOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.RaidOrcStronghold)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.CallForAdventurers2)
                .andTakeVariableIntrigueReward(ResourceChoices.OneClericOneWizard)
                .andTakeVariableIntrigueOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.RepelSeawraiths)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.MercenaryContract)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuestInCliffwatchInn(Quests.SendAidToTheHarpers)
                .andChooseOpponentForReward(dummy)
                .andReturnOneQuestResourceUsed(Res.cleric(1))
                .andEndTurn();

        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.Skulkway, Buildings.SmugglersDock)
                .andCompleteQuestInCliffwatchInn(Quests.SealGateToCyricsRealm)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.Skulkway)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuestInCliffwatchInn(Quests.RetrieveAncientArtifacts)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();

        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.FourRogues)
                .andCompleteQuest(Quests.EstablishShadowThievesGuild)
                .andReturnOneQuestResourceUsed(Res.rogue(1))
                .andEndTurn();

        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.HallOfManyPillars)
                .andPlayMultiIntrigue(Intrigues.FreeDrinks1)
                .andStealResources(dummy, ResourceChoices.OneRogue)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.HonorAmongThieves)
                .andChooseOpponentForReward(dummy)
                .andFinishMultiIntrigue()
                .andPlayMultiIntrigue(Intrigues.UnlikelyAssistance)
                .andChooseOpponentForReward(dummy)
                .andFinishMultiIntrigue()
                .andCompleteQuestInCliffwatchInn(Quests.TrainCastleguards)
                .andReturnOneQuestResourceUsed(Res.fighter(1))
                .andEndTurn();

        runner.finishGame();
        
//        runner.listCompletableQuests(todd, 1);
//        runner.debugShowQuests();
        
        runner.renderToFile("c:/duelssol/lords1345.html");
    }
    
}

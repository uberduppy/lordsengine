/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.talwood.lords.gamerunner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Houses;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.Lords;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.Res;
import org.talwood.lords.resources.Resources;

/**
 *
 * @author twalker
 */
public class SkullduggeryLordTest {
    
    public SkullduggeryLordTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testUseSingleLord() {
        GameRunner runner = new GameRunner();
        
        GamePlayer dummy = runner.addPlayer(Lords.LarissaNeathal, "Useful Idiot", Long.valueOf(9));
        GamePlayer todd = runner.addPlayer(Lords.IrusylEraneth, Houses.Skullduggery, "Todd", Long.valueOf(10));

        runner.setupQuests(Quests.StudyTheIlluskArch,
                Quests.RecruitLieutenant,
                Quests.DeliverAnUltimatum,
                Quests.PlaceSleeperAgentInSkullport,
                Quests.AllyWithTheXanatharsGuild,
                Quests.ThreatenTheBuildersGuilds,
                Quests.HealFallenGrayHandSoldiers,
                Quests.FenceGoodsForDukeOfDarkness, 
                Quests.ExploreAhghaironsTower, 
                Quests.InstallSpyInCastleWaterdeep,
                Quests.EstablishShadowThievesGuild,
                Quests.RaidOnUndermountain,
                Quests.DomesticateOwlbears,
                Quests.ExploreTrobriandsGraveyard,
                Quests.InfiltrateHalastersCircle,
                Quests.ProcureStolenGoods,
                Quests.BreakIntoBlackstaffTower,
                
                Quests.AmbushArtorMorlin,
                Quests.SaveKidnappedNobles,
                Quests.BuryTheBodies,
                Quests.EstablishTempleToIbrandul,
                Quests.ExposeCultCorruption,
                Quests.EliminateVampireCoven,
                Quests.StealSpellbookFromSilverhand,
                Quests.SafeguardEltorchulMage,
                Quests.BattleInMuiralsGauntlet,
                Quests.BuildReputationInSkullport,
                Quests.SealAnEntranceToSkullport,
                Quests.StealFromHouseAdarbrent,
                Quests.DefendTheYawningPortal,
                Quests.UnleashCrimeSpree,
                Quests.RecruitForBlackstaffAcademy,
                Quests.EstablishHarpersSafeHouse,
                Quests.ExpandGuildActivities,
                Quests.ImpersonateTaxCollector,
                Quests.DefeatUprisingFromUndermountain,
                Quests.ResearchChronomacy,
                Quests.PlunderTheIslandTemple,
                Quests.DefendTheLanceboardRoom,
                Quests.CreateShrineToOghma,
                Quests.ConfrontTheXanathar,
                Quests.DiscoverHiddenTempleOfLolth,
                Quests.ImprovePrisonSecurity,
                Quests.UncoverDrowPlot,
                Quests.DiplomaticMissionToSuzail,
                Quests.RansackWhitehelmsTomb,
                Quests.BolsterCityGuard,
                Quests.PatrolDockWard
                );

        runner.setupBuildings(Buildings.SmugglersDock, 
                Buildings.DragonTower, 
                Buildings.HellHoundsMuzzle, 
                Buildings.ShadowduskHold,
                Buildings.PoisonedQuill,
                Buildings.Librarium,
                Buildings.ThreePearls,
                Buildings.Skulkway,
                Buildings.HouseOfGoodSpirits,
                Buildings.TowerOfLuck,
                Buildings.NewOlamn,
                Buildings.HouseOfTheMoon,
                Buildings.ThrownGauntlet,
                Buildings.CitadelOfTheBloodyHand
                );
        
        runner.setupIntrigues(
                Intrigues.BlackMarketMoney, 
                Intrigues.DarkDaggerAssassination, 
                Intrigues.ArchitecturalInnovation, 
                Intrigues.InformationBroker1, 
                Intrigues.AcceleratedPlans,
                Intrigues.CallForAdventurers1,
                Intrigues.GraduationDay1,
                Intrigues.SpreadTheWealth1,
                Intrigues.CrimeWave1,
                Intrigues.InformationBroker2,
                Intrigues.CallForAdventurers2,
                Intrigues.Conscription1,
                Intrigues.CrimeWave2,
                Intrigues.QuellRiots,
                Intrigues.PlacateAngryMerchants,
                Intrigues.GraduationDay2,
                Intrigues.Conscription2,
                Intrigues.GoodFaith1,
                Intrigues.GoodFaith2,
                Intrigues.ResearchAgreement,
                Intrigues.MindFlayerMercenaries,
                Intrigues.Repent2,
                Intrigues.RecallAgent1,
                Intrigues.RecallAgent2,
                Intrigues.Repent1,
                Intrigues.CallForAssistance1,
                Intrigues.CallForAssistance2,
                Intrigues.Repent3,
                Intrigues.AlliedFaiths,
                Intrigues.UnlikelyAssistance,
                Intrigues.FriendlyLoan,
                Intrigues.HonorAmongThieves,
                Intrigues.RepelDrowInvaders,
                Intrigues.UnveilAbyssalAgent,
                Intrigues.MercenaryContract,
                Intrigues.ForgeDeed,
                Intrigues.UnexpectedSuccess1,
                Intrigues.UnexpectedSuccess2,
                Intrigues.CallInAFavor1,
                Intrigues.SpecialAssignment1,
                Intrigues.SubdueIllithidMenace,
                Intrigues.CallInAFavor2,
                Intrigues.SpecialAssignment2,
                Intrigues.OrganizedCrime,
                Intrigues.ChangeOfPlans
                );
        
        runner.startGame(Expansions.Classic, Expansions.Undermountian, Expansions.Skullport);
        
        // TURN 1.
        runner.startTurn();
        
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.SmugglersDock).andEndTurn();
        todd = todd.placeAgent(Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.FourFighters).andEndTurn();
        dummy = dummy.placeAgent(Buildings.EntryWell)
                .andTakeQuest(Quests.ThreatenTheBuildersGuilds)
                .andPlayIntrigue(Intrigues.BlackMarketMoney).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.DarkDaggerAssassination)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.ArchitecturalInnovation)
                .andBuyBuilding(Buildings.HellHoundsMuzzle).andEndTurn();
        todd = todd.placeAgent(Buildings.HellHoundsMuzzle)
                .andTakeVariableOwnerReward(ResourceChoices.OneWizard)
                .andCompleteQuest(Quests.RecruitLieutenant).andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth).andEndTurn();
        todd = todd.placeAgent(Buildings.SkullIsland)
                .andTakeVariableReward(ResourceChoices.OneFighterOneWizardOneCorruption)
                .andCompleteQuest(Quests.PlaceSleeperAgentInSkullport).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.InformationBroker1).andEndTurn();

        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.CliffwatchInnIntrigue)
                .andTakeQuest(Quests.ExploreAhghaironsTower).andEndTurn();
        dummy = dummy.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.AurorasRealm).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.BlackstaffTower).andEndTurn();
                
        // Turn 2
        runner.startTurn();
        // Turn Two
        
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.DragonTower).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.CallForAdventurers1)
                .andTakeVariableIntrigueReward(ResourceChoices.OneFighterOneWizard)
                .andTakeVariableIntrigueOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuest(Quests.ExploreAhghaironsTower)
                .andEndTurn();
        dummy = dummy.placeAgent(Buildings.HellHoundsMuzzle).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.CrimeWave1)
                .andChooseOpponentForReward(dummy).andEndTurn();
        dummy = dummy.placeAgent(Buildings.BlackstaffTower)
                .andCompleteQuest(Quests.StudyTheIlluskArch).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.GraduationDay1)
                .andChooseOpponentForReward(dummy).andEndTurn();
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph).andEndTurn();
        todd = todd.placeAgent(Buildings.EntryWell)
                .andPlayIntrigue(Intrigues.InformationBroker2)
                .andTakeQuest(Quests.FenceGoodsForDukeOfDarkness).andEndTurn();
        todd = todd.placeAgent(Buildings.AurorasRealm).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.HallOfMirrors)
                .andTakeVariableReward(ResourceChoices.OneFighterOneRogue)
                .andCompleteQuest(Quests.FenceGoodsForDukeOfDarkness)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.InstallSpyInCastleWaterdeep)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.FourRogues)
                .andEndTurn();
//        
//        // Turn 3
        runner.startTurn();
//
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.ShadowduskHold).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.AcceleratedPlans).andEndTurn();
        todd = todd.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.AllyWithTheXanatharsGuild)
                .andCompleteQuest(Quests.InstallSpyInCastleWaterdeep).andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo).andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.Conscription1)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuest(Quests.AllyWithTheXanatharsGuild).andEndTurn();
        dummy = dummy.placeAgent(Buildings.EntryWell)
                .andTakeQuest(Quests.HealFallenGrayHandSoldiers)
                .andPlayIntrigue(Intrigues.SpreadTheWealth1).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.CrimeWave2)
                .andChooseOpponentForReward(dummy).andEndTurn();
        dummy = dummy.placeAgent(Buildings.HallOfTheVoice)
                .andTakeQuest(Quests.DomesticateOwlbears)
                .andCompleteQuest(Quests.ThreatenTheBuildersGuilds).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.CallForAdventurers2)
                .andTakeVariableIntrigueReward(ResourceChoices.TwoClerics)
                .andTakeVariableIntrigueOwnerReward(ResourceChoices.OneCleric)
                .andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.SmugglersDock).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.FourRogues).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.DragonTower).andEndTurn();

//        // Turn 4
        runner.startTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.HouseOfGoodSpirits).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.GraduationDay2)
                .andChooseOpponentForReward(dummy).andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andCompleteQuest(Quests.DomesticateOwlbears).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.ResearchAgreement).andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.MindFlayerMercenaries).andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern).andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.CliffwatchInnIntrigue, Buildings.CliffwatchInnTwoGold).andEndTurn();
        todd = todd.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.CliffwatchInnIntrigue).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.CliffwatchInnIntrigue)
                .andTakeQuest(Quests.InfiltrateHalastersCircle)
                .andCompleteQuest(Quests.InfiltrateHalastersCircle).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.ProcureStolenGoods)
                .andCompleteQuest(Quests.ProcureStolenGoods).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.EntryWell)
                .andTakeQuest(Quests.BreakIntoBlackstaffTower)
                .andPlayIntrigue(Intrigues.Conscription2)
                .andCompleteQuest(Quests.BreakIntoBlackstaffTower)
                .andChooseOpponentForReward(dummy).andEndTurn();

//        // Turn 5 - Unleash the Kraken!
        runner.startTurn();
        
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.NewOlamn).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.CallForAssistance1)
                .andStealResources(dummy, ResourceChoices.OneFighterOneRogue).andEndTurn();
        dummy = dummy.placeAgent(Buildings.AurorasRealm)
                .andCompleteQuest(Quests.HealFallenGrayHandSoldiers).andEndTurn();
        todd = todd.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborTwo).andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth).andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborTwo, Buildings.WaterdeepHarborThree).andEndTurn();
        dummy = dummy.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.AmbushArtorMorlin)
                .andCompleteQuest(Quests.AmbushArtorMorlin).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.RecallAgent1)
                .andRecallAgent(Buildings.ShadowduskHold).andEndTurn();
        dummy = dummy.placeAgent(Buildings.BlackstaffTower)
                .andCompleteQuest(Quests.DeliverAnUltimatum).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.Repent1).andEndTurn();
        todd = todd.placeAgent(Buildings.PoisonedQuill)
                .andPlayIntrigue(Intrigues.GoodFaith1)
                .andChooseOpponentForReward(dummy).andEndTurn();
        todd = todd.placeAgent(Buildings.CliffwatchInnIntrigue)
                .andTakeQuest(Quests.EstablishShadowThievesGuild)
                .andCompleteQuest(Quests.EstablishShadowThievesGuild).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.EntryWell)
                .andTakeQuest(Quests.ExploreTrobriandsGraveyard)
                .andPlayIntrigue(Intrigues.CallForAssistance2)
                .andStealResources(dummy, ResourceChoices.TwoRogues)
                .andCompleteQuest(Quests.ExploreTrobriandsGraveyard).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.HallOfTheVoice)
                .andTakeQuest(Quests.EstablishTempleToIbrandul)
                .andCompleteQuest(Quests.EstablishTempleToIbrandul).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.NewOlamn)
                .andTakeVariableOwnerReward(ResourceChoices.OneWizard)
                .andCompleteQuest(Quests.SaveKidnappedNobles).andEndTurn();

        // Turn 6 
        runner.startTurn();
        
        dummy = dummy.placeAgent(Buildings.CliffwatchInnIntrigue)
                .andTakeQuest(Quests.SafeguardEltorchulMage)
                .andCompleteQuest(Quests.SafeguardEltorchulMage).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.HonorAmongThieves)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuest(Quests.RaidOnUndermountain).andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.HouseOfTheMoon).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.GoodFaith2)
                .andChooseOpponentForReward(dummy).andEndTurn();
        dummy = dummy.placeAgent(Buildings.GrinningLionTavern).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.FriendlyLoan)
                .andChooseOpponentForReward(dummy).andEndTurn();
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph).andEndTurn();
        todd = todd.placeAgent(Buildings.HouseOfTheMoon)
                .andTakeQuest(Quests.BuildReputationInSkullport).andEndTurn();
        dummy = dummy.placeAgent(Buildings.HallOfTheVoice)
                .andTakeQuest(Quests.EliminateVampireCoven)
                .andCompleteQuest(Quests.EliminateVampireCoven).andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.EntryWell, Buildings.CliffwatchInnTwoGold)
                .andCompleteQuest(Quests.BuildReputationInSkullport).andEndTurn();
        todd = todd.placeAgent(Buildings.EntryWell)
                .andTakeQuest(Quests.StealFromHouseAdarbrent)
                .andPlayIntrigue(Intrigues.UnlikelyAssistance)
                .andChooseOpponentForReward(dummy)
                .andCompleteQuest(Quests.ExposeCultCorruption).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.DefendTheYawningPortal).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.ThreeFightersOneRogue)
                .andCompleteQuest(Quests.BuryTheBodies).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.PoisonedQuill)
                .andPlayIntrigue(Intrigues.AlliedFaiths)
                .andChooseOpponentForReward(dummy).andEndTurn();
        
        // Turn 7
        runner.startTurn();
        dummy = dummy.placeAgent(Buildings.FieldOfTriumph).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.MercenaryContract)
                .andChooseOpponentForReward(dummy).andEndTurn();
        dummy = dummy.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.SealAnEntranceToSkullport)
                .andCompleteQuest(Quests.SealAnEntranceToSkullport).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.Repent2).andEndTurn();
        dummy = dummy.placeAgent(Buildings.HallOfTheVoice)
                .andTakeQuest(Quests.RecruitForBlackstaffAcademy)
                .andCompleteQuest(Quests.RecruitForBlackstaffAcademy).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.Repent3).andEndTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.ThrownGauntlet).andEndTurn();
        todd = todd.placeAgent(Buildings.ThrownGauntlet)
                .andCompleteQuest(Quests.StealFromHouseAdarbrent).andEndTurn();
        dummy = dummy.placeAgent(Buildings.ThePlinth).andEndTurn();
        todd = todd.placeAgent(Buildings.CliffwatchInnIntrigue)
                .andTakeQuest(Quests.UnleashCrimeSpree)
                .andCompleteQuest(Quests.UnleashCrimeSpree).andEndTurn();
        todd = todd.placeAgent(Buildings.EntryWell)
                .andTakeQuest(Quests.ExpandGuildActivities)
                .andCompleteQuest(Quests.ExpandGuildActivities)
                .andPlayIntrigue(Intrigues.UnexpectedSuccess1).andEndTurn();

        // Reallocate
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.HouseOfTheMoon)
                .andTakeQuest(Quests.ImpersonateTaxCollector).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.NewOlamn)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuest(Quests.ImpersonateTaxCollector).andEndTurn();
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo).andEndTurn();

        // Turn 8
        runner.startTurn();
        dummy = dummy.placeAgent(Buildings.BuildersHall)
                .andBuyBuilding(Buildings.CitadelOfTheBloodyHand).andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.CliffwatchInnIntrigue, Buildings.ShadowduskHold).andEndTurn();
        dummy = dummy.placeAgent(Buildings.HallOfTheVoice)
                .andTakeQuest(Quests.DefeatUprisingFromUndermountain)
                .andCompleteQuest(Quests.DefeatUprisingFromUndermountain).andEndTurn();
        todd = todd.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.CliffwatchInnIntrigue, Buildings.ThreePearls).andEndTurn();
        dummy = dummy.placeAgent(Buildings.Librarium)
                .andSelectTargetBuildings(Buildings.CliffwatchInnIntrigue).andEndTurn();
        todd = todd.placeAgent(Buildings.CliffwatchInnIntrigue)
                .andTakeQuest(Quests.ResearchChronomacy)
                .andCompleteQuest(Quests.DefendTheYawningPortal)
                .andRecallAgents(Buildings.CitadelOfTheBloodyHand, Buildings.ShadowduskHold, Buildings.CliffwatchInnIntrigue)
                .andReplaceGenericResources(new Resources(Res.cleric(1), Res.wizard(1))).andEndTurn();
        dummy = dummy.placeAgent(Buildings.ShadowduskHold)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborOne, Buildings.WaterdeepHarborTwo).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborOne)
                .andPlayIntrigue(Intrigues.CallInAFavor1)
                .andTakeVariableIntrigueReward(ResourceChoices.OneCleric).andEndTurn();
        dummy = dummy.placeAgent(Buildings.BlackstaffTower).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborTwo)
                .andPlayIntrigue(Intrigues.SpecialAssignment1)
                .andSelectHouse(Houses.Skullduggery).andEndTurn();
        todd = todd.placeAgent(Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.WaterdeepHarborThree, Buildings.ThreePearls)
                .andCompleteQuest(Quests.DefendTheLanceboardRoom)
                .andReplaceGenericResources(new Resources(Res.cleric(3), Res.fighter(1), Res.wizard(4))).andEndTurn();
        todd = todd.placeAgent(Buildings.ThreePearls)
                .andPayVariableActivation(ResourceChoices.TwoRogues)
                .andTakeVariableReward(ResourceChoices.ThreeClerics)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuest(Quests.ResearchChronomacy)
                .andRecallAgent(Buildings.CitadelOfTheBloodyHand).andEndTurn();
        todd = todd.placeAgent(Buildings.CliffwatchInnTwoGold)
                .andTakeQuest(Quests.PlunderTheIslandTemple)
                .andCompleteQuest(Quests.PlunderTheIslandTemple).andEndTurn();
        todd = todd.placeAgent(Buildings.WaterdeepHarborThree)
                .andPlayIntrigue(Intrigues.RecallAgent2)
                .andRecallAgent(Buildings.ThreePearls).andEndTurn();
        
        todd = todd.placeAgent(Buildings.EntryWell)
                .andTakeQuest(Quests.BattleInMuiralsGauntlet)
                .andPlayIntrigue(Intrigues.SpecialAssignment2)
                .andSelectHouse(Houses.Piety).andEndTurn();
        
        todd = todd.placeAgent(Buildings.PoisonedQuill)
                .andPlayIntrigue(Intrigues.CallInAFavor2)
                .andTakeVariableIntrigueReward(ResourceChoices.TwoFighters)
                .andCompleteQuest(Quests.DiscoverHiddenTempleOfLolth)
                .andTakeQuestAsQuestReward(Quests.ConfrontTheXanathar).andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborOne, Buildings.CitadelOfTheBloodyHand)
                .andSelectTargetBuildings(Buildings.SmugglersDock, Buildings.ThreePearls)
                .andCompleteQuest(Quests.ConfrontTheXanathar).andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborTwo, Buildings.SmugglersDock)
                .andTakeVariableReward(ResourceChoices.FourFighters).andEndTurn();
        
        todd = todd.reallocateAgent(Buildings.WaterdeepHarborThree, Buildings.ThreePearls)
                .andPayVariableActivation(ResourceChoices.TwoRogues)
                .andTakeVariableReward(ResourceChoices.TwoClericsOneWizard)
                .andTakeVariableOwnerReward(ResourceChoices.OneRogue)
                .andCompleteQuest(Quests.BattleInMuiralsGauntlet).andEndTurn();
        
        runner.finishGame();
        
        runner.renderToFile("c:/duelssol/skullduggery.html");
    }
    
}

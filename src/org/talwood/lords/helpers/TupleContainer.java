package org.talwood.lords.helpers;

import java.io.Serializable;

public class TupleContainer<K extends Serializable, T extends Serializable> implements Comparable, Serializable {
    private static final long serialVersionUID = 1L;
    
    private K leftSide;
    private T rightSide;
    
    public TupleContainer(K leftSide, T rightSide) {
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }
    
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Left: ").append(leftSide.toString()).append(" Right:").append(rightSide.toString());
        return sb.toString();
        
    }
    
    @Override
    public int hashCode() {
        return leftSide.hashCode() + (37 * rightSide.hashCode());
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TupleContainer<K, T> other = (TupleContainer<K, T>) obj;
        if (this.leftSide != other.getLeftSide() && (this.leftSide == null || !this.leftSide.equals(other.getLeftSide()))) {
            return false;
        }
        if (this.rightSide != other.getRightSide() && (this.rightSide == null || !this.rightSide.equals(other.getRightSide()))) {
            return false;
        }
        return true;
    }
    

    public K getLeftSide() {
        return leftSide;
    }

    public void setLeftSide(K leftSide) {
        this.leftSide = leftSide;
    }

    public T getRightSide() {
        return rightSide;
    }

    public void setRightSide(T rightSide) {
        this.rightSide = rightSide;
    }


    @Override
    public int compareTo(Object obj) {
        int result = 0;
        // I will only return a value if I'm actually comparing two containers of the same type.
        if(obj != null) {
            if(this.getClass() == obj.getClass()) {
                @SuppressWarnings("unchecked") TupleContainer<K, T> rhs = (TupleContainer<K, T>) obj;
                result = ObjectHelper.compareObjects(getLeftSide(), rhs.getLeftSide());
                if(result == 0) {
                    result = ObjectHelper.compareObjects(getRightSide(), rhs.getRightSide());
                }
            } else {
                throw new ClassCastException("Can only compare two TupleContainers with same sub types");
            }
        } else {
            throw new ClassCastException("Can only compare two TupleContainers, not null");
        }
        
        return result;
    }
}
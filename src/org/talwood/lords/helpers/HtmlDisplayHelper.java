package org.talwood.lords.helpers;

import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;


public class HtmlDisplayHelper {
    public static TupleContainer<HtmlElement, HtmlElement> startHtmlToTableWithStyle(boolean fullWidth, String styleData) {
        HtmlElement html = new HtmlElement(HtmlElementType.HTML);
        HtmlElement head = html.createChildElement(HtmlElementType.HEAD, styleData);
        HtmlElement body = html.createChildElement(HtmlElementType.BODY);
        HtmlElement table = fullWidth ? body.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%")) : body.createChildElement(HtmlElementType.TABLE);
        TupleContainer<HtmlElement, HtmlElement> result = new TupleContainer<HtmlElement, HtmlElement>(html, table);
        
        return result;
    }
    
    public static TupleContainer<HtmlElement, HtmlElement> startHtmlToTable(boolean fullWidth) {
        HtmlElement html = new HtmlElement(HtmlElementType.HTML);
        HtmlElement head = html.createChildElement(HtmlElementType.HEAD);
        HtmlElement body = html.createChildElement(HtmlElementType.BODY);
        HtmlElement table = fullWidth ? body.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%")) : body.createChildElement(HtmlElementType.TABLE);
        return new TupleContainer<HtmlElement, HtmlElement>(html, table);
    }
    
}

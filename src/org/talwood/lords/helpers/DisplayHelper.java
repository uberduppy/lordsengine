package org.talwood.lords.helpers;

import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;


public class DisplayHelper {
    public static String generateMessagePage(String message) {
        TupleContainer<HtmlElement, HtmlElement> html = HtmlDisplayHelper.startHtmlToTable(true);
        HtmlElement table = html.getRightSide();
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        tr.createChildElement(HtmlElementType.TD, message);
        return html.getLeftSide().render();
    }
    
    
}

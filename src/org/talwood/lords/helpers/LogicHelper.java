package org.talwood.lords.helpers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.containers.BuildingContainer;
import org.talwood.lords.containers.IntrigueContainer;
import org.talwood.lords.containers.QuestContainer;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.QuestActions;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.gamerunner.GamePlayer;
import org.talwood.lords.intrigues.Intrigue;
import org.talwood.lords.quests.Quest;

public class LogicHelper {
    
    private static QuestContainer allQuests;
    private static BuildingContainer allBuildings;
    private static IntrigueContainer allIntrigues;
    private static  Map<Intrigues, Quests> mandatoryQuestLookup;
    
    private static QuestContainer getQuestInstance() {
        if(allQuests == null) {
            allQuests = new QuestContainer(Expansions.Classic, Expansions.Skullport, Expansions.Undermountian);
        }
        return allQuests;
    }
    
    private static BuildingContainer getBuildingInstance() {
        if(allBuildings == null) {
            allBuildings = new BuildingContainer(Expansions.Classic, Expansions.Skullport, Expansions.Undermountian);
        }
        return allBuildings;
    }
    
    private static Map<Intrigues, Quests> getMandatoryQuestLookup() {
        if(mandatoryQuestLookup == null) {
            mandatoryQuestLookup = new HashMap<Intrigues, Quests>();
            mandatoryQuestLookup.put(Intrigues.FendOffBandits, Quests.FendOffBandits);
            mandatoryQuestLookup.put(Intrigues.FoilTheZhentarim, Quests.FoilTheZhentarim);
            mandatoryQuestLookup.put(Intrigues.PlacateAngryMerchants, Quests.PlacateAngryMerchants);
            mandatoryQuestLookup.put(Intrigues.QuellRiots, Quests.QuellRiots);
            mandatoryQuestLookup.put(Intrigues.RepelDrowInvaders, Quests.RepelDrowInvaders);
            mandatoryQuestLookup.put(Intrigues.StampOutCultists, Quests.StampOutCultists);
            mandatoryQuestLookup.put(Intrigues.EvadeAssassins, Quests.EvadeAssassins);
            mandatoryQuestLookup.put(Intrigues.SubdueIllithidMenace, Quests.SubdueIllithidMenace);
            mandatoryQuestLookup.put(Intrigues.UnveilAbyssalAgent, Quests.UnveilAbyssalAgent);
            mandatoryQuestLookup.put(Intrigues.ClearRustMonsterNest, Quests.ClearRustMonsterNest);
            mandatoryQuestLookup.put(Intrigues.CoverUpScandal, Quests.CoverUpScandal);
            mandatoryQuestLookup.put(Intrigues.HuntHiddenGhoul, Quests.HuntHiddenGhoul);
        }
        return mandatoryQuestLookup;
    }
    private static IntrigueContainer getIntrigueInstance() {
        if(allIntrigues == null) {
            allIntrigues = new IntrigueContainer(Expansions.Classic, Expansions.Skullport, Expansions.Undermountian);
        }
        return allIntrigues;
    }
    
    public static Quest findQuest(Quests questToFind) {
        return findQuestInCollection(getQuestInstance().getQuests(), questToFind);
    }
    
    public static Quest findMandatoryQuest(Intrigues questToFind) {
        return findQuestInCollection(getQuestInstance().getMandatoryQuests(), getMandatoryQuestLookup().get(questToFind));
    }
    
    public static Building findBuilding(Buildings buildingToFind) {
        return findBuildingInCollection(getBuildingInstance().getAllBuildings(), buildingToFind);
    }
    
    public static Intrigue findIntrigue(Intrigues intrigueToFind) {
        return findIntrigueInCollection(getIntrigueInstance().getAllBaseIntrigues(), intrigueToFind);
    }
    
    public static boolean hasQuestInCollection(List<Quest> quests, Quests questToDraw) {
        boolean hasQuest = false;
        for(Quest q : quests) {
            if(q.getQuestInfo() == questToDraw) {
                hasQuest = true;
                break;
            }
        }
        return hasQuest;
    }
    
    public static boolean hasMandatoryQuestInCollection(List<Quest> quests, Intrigues intrigue) {
        boolean hasQuest = false;
        Quest mQuest = findMandatoryQuest(intrigue);
        for(Quest q : quests) {
            if(q.getQuestInfo() == mQuest.getQuestInfo()) {
                hasQuest = true;
                break;
            }
        }
        return hasQuest;
    }
    
    public static boolean hasQuestInCollectionWithAction(List<Quest> quests, QuestActions questAction) {
        boolean hasQuest = false;
        for(Quest q : quests) {
            if(q.getQuestInfo().getQuestAction() == questAction) {
                hasQuest = true;
                break;
            }
        }
        return hasQuest;
    }
    
    public static Quest findQuestInCollection(List<Quest> quests, Quests questToDraw) {
        Quest quest = null;
        for(Quest q : quests) {
            if(q.getQuestInfo() == questToDraw) {
                quest = q;
                break;
            }
        }
        return quest;
    }
    
    public static Quest findMandatoryQuestInCollection(List<Quest> quests, Intrigues intrigue) {
        Quest quest = null;
        Quest mQuest = findMandatoryQuest(intrigue);
        for(Quest q : quests) {
            if(q.getQuestInfo() == mQuest.getQuestInfo()) {
                quest = q;
                break;
            }
        }
        return quest;
    }
    
    public static boolean hasQuestWithQuestAction(List<Quest> quests, QuestActions action) {
        boolean result = false;
        for(Quest q : quests) {
            if (q.getQuestInfo().getQuestAction() == action) {
                result = true;
                break;
            }
        }
        return result;
    }
    
    public static boolean hasIntrigueInCollection(List<Intrigue> intrigues, Intrigues intrigueToDraw) {
        boolean hasIntrigue = false;
        for(Intrigue q : intrigues) {
            if(q.getIntrigueInfo() == intrigueToDraw) {
                hasIntrigue = true;
                break;
            }
        }
        return hasIntrigue;
    }
    
    public static Intrigue findIntrigueInCollection(List<Intrigue> intrigues, Intrigues intrigueToDraw) {
        Intrigue intrigue = null;
        for(Intrigue q : intrigues) {
            if(q.getIntrigueInfo() == intrigueToDraw) {
                intrigue = q;
                break;
            }
        }
        return intrigue;
    }
    
    public static Building findBuildingInCollection(List<Building> buildings, Buildings buildingToDraw) {
        Building building = null;
        for(Building q : buildings) {
            if(q.getBuildingInfo() == buildingToDraw) {
                building = q;
                break;
            }
        }
        return building;
        
    }
    
    public static boolean hasBuildingInCollection(List<Building> buildings, Buildings buildingToDraw) {
        boolean hasBuilding = false;
        for(Building q : buildings) {
            if(q.getBuildingInfo() == buildingToDraw) {
                hasBuilding = true;
                break;
            }
        }
        return hasBuilding;
    }

    public static boolean isSamePlayer(GamePlayer p1, GamePlayer p2) {
        boolean result = false;
        if(p1 != null && p2 != null) {
            result = p1.getName().equals(p2.getName());
        }
        return result;
    }
}

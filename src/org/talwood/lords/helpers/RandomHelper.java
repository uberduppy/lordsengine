package org.talwood.lords.helpers;

public class RandomHelper {
    
    public static boolean flipACoin() {
        int flip =(int) (Math.random()*2);
        return flip == 1;
    }
    
    public static int getNumberInRange(int starting, int ending) {
        int range = ending - starting;
        int num =(int) (Math.random()*(range + 1));
        return num + starting;
    }
}

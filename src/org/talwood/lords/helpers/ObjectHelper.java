package org.talwood.lords.helpers;

import java.util.StringTokenizer;

public class ObjectHelper {
    public static boolean equals(Object lhs, Object rhs) {
        if (lhs != null) {
            return lhs.equals(rhs);
        }
        else {
            return (rhs == null);
        }
    }
    
    public static int getHashCode(Object o){
        int hashCode = 0;
        if(o != null) {
            hashCode = o.hashCode();
        }
        return hashCode;
    }
    
    public static boolean intInArrayString(String array, int value) {
        boolean result = false;
        StringTokenizer st = new StringTokenizer(array, ",");
        while(st.hasMoreTokens()) {
            String tkn = st.nextToken();
            try {
                int v = Integer.parseInt(tkn.trim());
                if(v == value) {
                    result = true;
                }
            } catch (NumberFormatException ex) {
                // Skip this entry.
            }
        }
        return result;
    }
    
    public static boolean intInArray(int[] array, int value) {
        boolean result = false;
        for (int i = 0; i < array.length; i++) {
            if(array[i] == value) {
                result = true;
                break;
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public static <T> T cast(Object x) {
        return (T) x;
    }
    
    public static int compareObjects(Object lhs, Object rhs) {
        int result = 0;
        // First, only compare two non-null entries
        if(lhs != null && rhs != null) {
            // If the types are different, we're doing a string comparator
            if(lhs.getClass().equals(rhs.getClass())) {
                // They the same type, are they comparable?
                if(lhs instanceof Comparable) {
                    result = ((Comparable)lhs).compareTo(rhs);
                    
                } else {
                    result = lhs.toString().compareTo(rhs.toString());
                }
            } else {
                result = lhs.toString().compareTo(rhs.toString());
            }
        }
        
        return result;
    }

}

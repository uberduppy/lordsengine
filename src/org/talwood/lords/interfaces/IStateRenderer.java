package org.talwood.lords.interfaces;

import org.talwood.lords.html.HtmlElement;


public interface IStateRenderer {
    // All renders are given an HtmlElement startign with a TD.
    public void render(HtmlElement outerTD);
}

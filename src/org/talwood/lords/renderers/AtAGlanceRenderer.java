package org.talwood.lords.renderers;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.gamerunner.GamePlayer;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;

public class AtAGlanceRenderer extends RendererHelpers implements IStateRenderer {

    private List<GamePlayer> players = new ArrayList<GamePlayer>();
    public AtAGlanceRenderer(List<GamePlayer> players) {
        for(GamePlayer gp : players) {
            this.players.add(gp.makeCopyForRenderer());
        }
    }

    @Override
    public void render(HtmlElement outerTD) {
        new HRRenderer().render(outerTD);
        new H3Renderer("Players at a glance").render(outerTD);
        for(GamePlayer gp : players) {
            HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);
            HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
            StringBuilder sb = new StringBuilder();
            sb.append("<b>Player: ").append(gp.getName()).append("</b>&nbsp;");
            sb.append(" (Victory points: ").append(gp.getVictoryPoints()).append(")<br>");
            sb.append(buildImagesForCostWithBreaks(gp.getResources()));
            HtmlElement td = tr.createChildElement(HtmlElementType.TD, sb.toString());
        }
    }
}

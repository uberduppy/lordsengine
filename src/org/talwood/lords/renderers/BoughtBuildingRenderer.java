package org.talwood.lords.renderers;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;

public class BoughtBuildingRenderer extends RendererHelpers implements IStateRenderer {

    private Building building;
    private Building replacedBuilding;
 
    public BoughtBuildingRenderer(Building building, Building replacedBuilding) {
        this.building = building;
        this.replacedBuilding = replacedBuilding;
    }

    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        td.createChildElement(HtmlElementType.B, "Building Purchased:&nbsp;");

        new BuildingRenderer(building, true).render(tr.createChildElement(HtmlElementType.TD));
        td = tr.createChildElement(HtmlElementType.TD);
        td.createChildElement(HtmlElementType.B, "Added to Builder's Hall:&nbsp;");
        new BuildingRenderer(replacedBuilding, false).render(tr.createChildElement(HtmlElementType.TD));
        
        tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        td = tr.createChildElement(HtmlElementType.TD, HtmlAttr.colspan("4"));
        td.createChildElement(HtmlElementType.B, "Gained " + building.getStoredVictoryPoints() + " victory points.");
        
    }
    
}

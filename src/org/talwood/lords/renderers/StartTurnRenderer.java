package org.talwood.lords.renderers;

import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;

public class StartTurnRenderer implements IStateRenderer {

    private int turnNumber;
    
    public StartTurnRenderer(int turnNumber) {
        this.turnNumber = turnNumber;
    }

    @Override
    public void render(HtmlElement outerTD) {
        
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        new HRRenderer().render(td);
        new H2Renderer("Start of turn " + turnNumber).render(td);
        
    }
}

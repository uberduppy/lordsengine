package org.talwood.lords.renderers;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.gamerunner.GamePlayer;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.intrigues.Intrigue;
import org.talwood.lords.quests.Quest;
import org.talwood.lords.resources.Resources;

public class GamePlayerResourceRenderer extends RendererHelpers implements IStateRenderer {

    private GamePlayer player;
    private List<Quest> quests = new ArrayList<>();
    private List<Quest> completedQuests = new ArrayList<>();
    private List<Building> ownedBuildings = new ArrayList<>();
    private List<Intrigue> cards = new ArrayList<>();
    private Resources resources;
    
    public GamePlayerResourceRenderer(GamePlayer player) {
        this.player = player;
        quests.addAll(player.getCurrentQuests());
        completedQuests.addAll(player.getCompletedQuests());
        ownedBuildings.addAll(player.getOwnedBuildings());
        cards.addAll(player.getIntrigueCards());
        resources = new Resources(player.getResources());
    }
    
    public void renderResources(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        tr.createChildElement(HtmlElementType.TD, "<b>Resources:</b> " + buildImagesForCostWithBreaks(resources));
        
    }
    
    public void renderOwnedBuildings(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        HtmlElement fontset = td.createChildElement(HtmlElementType.B, "Owned Buildings");
        tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        td = tr.createChildElement(HtmlElementType.TD);
        new HRRenderer().render(td);
        if(ownedBuildings.isEmpty()) {
            tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
            td = tr.createChildElement(HtmlElementType.TD, "You have no<br>Buildings");
        } else {
            for(Building b : ownedBuildings) {
                tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
                td = tr.createChildElement(HtmlElementType.TD);
                new BuildingRenderer(b, false).render(td);
            }
        }
    }
    
    public void renderIntrigues(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        HtmlElement fontset = td.createChildElement(HtmlElementType.B, "Current Intrigues");
        tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        td = tr.createChildElement(HtmlElementType.TD);
        new HRRenderer().render(td);
        if(cards.isEmpty()) {
            tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
            td = tr.createChildElement(HtmlElementType.TD, "You have no<br>current Intrigue cards");
        } else {
            for(Intrigue i : cards) {
                tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
                td = tr.createChildElement(HtmlElementType.TD);
                new IntrigueRenderer(i).render(td);
            }
        }
    }
    
    public void renderCurrentQuests(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        HtmlElement fontset = td.createChildElement(HtmlElementType.B, "Current Quests");
        tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        td = tr.createChildElement(HtmlElementType.TD);
        new HRRenderer().render(td);
        if(quests.isEmpty()) {
            tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
            td = tr.createChildElement(HtmlElementType.TD, "You have no<br>current Quests");
        } else {
            for(Quest q : quests) {
                tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
                td = tr.createChildElement(HtmlElementType.TD);
                new QuestRenderer(q).render(td);
            }
        }
    }
    
    public void renderCompletedQuests(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        HtmlElement fontset = td.createChildElement(HtmlElementType.B, "Completed Quests");
        tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        td = tr.createChildElement(HtmlElementType.TD);
        new HRRenderer().render(td);
        if(completedQuests.isEmpty()) {
            tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
            td = tr.createChildElement(HtmlElementType.TD, "You have no<br>completed Quests");
        } else {
            for(Quest q : completedQuests) {
                tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
                td = tr.createChildElement(HtmlElementType.TD);
                new QuestRenderer(q).renderTitle(td);
            }
        }
    }

    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        new HRRenderer().render(td);
        new H3Renderer("Tavern for " + player.getName()).render(td);
        tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        td = tr.createChildElement(HtmlElementType.TD);
        renderResources(td);
        new HRRenderer().render(td);
        tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        td = tr.createChildElement(HtmlElementType.TD);
        table = td.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        // Current quests
        renderCurrentQuests(tr.createChildElement(HtmlElementType.TD));
        renderIntrigues(tr.createChildElement(HtmlElementType.TD));
        renderCompletedQuests(tr.createChildElement(HtmlElementType.TD));
        renderOwnedBuildings(tr.createChildElement(HtmlElementType.TD));
        // Completed quests (short list)
        // Intrigues
        // Buildings owned (short list)
//        td = tr.createChildElement(HtmlElementType.TD);
//        HtmlElement questtable = td.createChildElement(HtmlElementType.TABLE);
//        HtmlElement tr1 = questtable.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
//        HtmlElement td1 = tr1.createChildElement(HtmlElementType.TD);
//        HtmlElement fontset = td1.createChildElement(HtmlElementType.B, "Current Quests:&nbsp;");
//        
//        for(Quest quest : quests) {
//            HtmlElement tdx = tr1.createChildElement(HtmlElementType.TD);
//            new QuestRenderer(quest).render(tdx);
//            
//        }
//        tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
//        td = tr.createChildElement(HtmlElementType.TD);
//        new HRRenderer().render(td);
//        HtmlElement intriguetable = td.createChildElement(HtmlElementType.TABLE);
//        HtmlElement tri = intriguetable.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
//        HtmlElement td0 = tri.createChildElement(HtmlElementType.TD);
//        fontset = td0.createChildElement(HtmlElementType.B, "Current Intrigue Cards:&nbsp;");
//        for(Intrigue intCard : cards) {
//            HtmlElement tdi = tri.createChildElement(HtmlElementType.TD);
//            new IntrigueRenderer(intCard).render(tdi);
//            
//        }
//        new HRRenderer().render(td);
    }
}

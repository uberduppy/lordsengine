package org.talwood.lords.renderers;

import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;


public class H3Renderer implements IStateRenderer {
    
    private String headingText;
    
    public H3Renderer(String headingText) {
        this.headingText = headingText;
    }

    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        HtmlElement tr = table.createChildElement(HtmlElementType.TR);
        HtmlElement td = table.createChildElement(HtmlElementType.TD);
        table.createChildElement(HtmlElementType.H3, headingText);
    }
}

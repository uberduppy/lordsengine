package org.talwood.lords.renderers;

import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;

public class VariableOwnerRewardRenderer extends RendererHelpers implements IStateRenderer {

    private ResourceChoices choice;
    private Buildings building;
    
    public VariableOwnerRewardRenderer(Buildings building, ResourceChoices choice) {
        this.choice = choice;
        this.building = building;
    }

    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        td.createChildElement(HtmlElementType.B, "Resources used for owner reward for " + building.getName() + ":&nbsp;");
        tr.createChildElement(HtmlElementType.TD, buildImagesForResources(choice.getResources()));
    }
}

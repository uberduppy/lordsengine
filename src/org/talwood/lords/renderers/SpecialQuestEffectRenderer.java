package org.talwood.lords.renderers;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.enums.ResourceTypes;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.intrigues.Intrigue;
import org.talwood.lords.quests.Quest;
import org.talwood.lords.resources.Res;

public class SpecialQuestEffectRenderer extends RendererHelpers implements IStateRenderer {

    private List<Res> resources = new ArrayList<Res>();
    private Intrigue card;
    private Quest quest;
    
    public SpecialQuestEffectRenderer(Quest quest, Intrigue card) {
        this.quest = quest;
        this.card = card;
                
    }
    public SpecialQuestEffectRenderer(Quest quest, Res... res) {
        this.quest = quest;
        for(Res r : res) {
            resources.add(r);
        }
    }
    
    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        HtmlElement fontset = td.createChildElement(HtmlElementType.B, "Plot quest effect for:&nbsp;");
        
        td = tr.createChildElement(HtmlElementType.TD);
        new QuestRenderer(quest).renderTitle(td);
        
        if(resources.size() > 0) {
            td = tr.createChildElement(HtmlElementType.TD);
            fontset = td.createChildElement(HtmlElementType.B, "Resources received:&nbsp;");
            td = tr.createChildElement(HtmlElementType.TD, buildImagesFromResources(resources));
        }
        
        for(Res r : resources) {
            if(r.getResources() == ResourceTypes.VictoryPoints) {
                td = tr.createChildElement(HtmlElementType.TD, "Victory points: " + r.getCount());
            } else if(r.getResources() == ResourceTypes.Intrigue) {
                td = tr.createChildElement(HtmlElementType.TD, "Intrigue cards drawn: " + r.getCount());
            }
        }
        
        if(card != null) {
            td = tr.createChildElement(HtmlElementType.TD, "Intrigue card drawn: ");
            td = tr.createChildElement(HtmlElementType.TD);
            new IntrigueRenderer(card).render(td);
        }
    }
    
}

package org.talwood.lords.renderers;

import org.talwood.lords.gamerunner.GamePlayer;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.intrigues.Intrigue;
import static org.talwood.lords.renderers.RendererHelpers.buildImagesForResources;
import org.talwood.lords.resources.Resources;

public class PlayerReceivedRenderer extends RendererHelpers implements IStateRenderer {

    private GamePlayer player;
    private Resources reward;
    private Intrigue card;
    
    public PlayerReceivedRenderer(GamePlayer player, Intrigue card, Resources reward) {
        this.player = player;
        this.reward = new Resources(reward);
        this.card = card;
    }

    @Override
    public void render(HtmlElement outerTD) {
        if(reward.isNotEmpty()) {
            HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);

            HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
            HtmlElement td = tr.createChildElement(HtmlElementType.TD);
            td.createChildElement(HtmlElementType.B, "Player " + player.getName() + " received &nbsp;" + buildImagesForResources(reward) + " from: ");

            new IntrigueRenderer(card).renderTitle(tr.createChildElement(HtmlElementType.TD));
        }
    }
}

package org.talwood.lords.renderers;

import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.quests.Quest;

public class DrawQuestRenderer implements IStateRenderer {

    private Quest taken;
    private Quest drawn;
    private String verb;
    
    public DrawQuestRenderer(Quest taken, Quest drawn) {
        this(taken, drawn, "taken");
    }
    public DrawQuestRenderer(Quest taken, Quest drawn, String verb) {
        this.taken = taken;
        this.drawn = drawn;
        this.verb = verb;
    }
    
    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        HtmlElement fontset = td.createChildElement(HtmlElementType.B, "Quest " + verb + ":&nbsp;");
        
        td = tr.createChildElement(HtmlElementType.TD);
        new QuestRenderer(taken).render(td);
        if(drawn != null) {
            td = tr.createChildElement(HtmlElementType.TD);
            fontset = td.createChildElement(HtmlElementType.B, "Quest added to CliffWatch Inn:&nbsp;");

            td = tr.createChildElement(HtmlElementType.TD);
            new QuestRenderer(drawn).render(td);
        }
        
    }
    
}

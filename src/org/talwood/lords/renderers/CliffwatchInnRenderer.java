package org.talwood.lords.renderers;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.quests.Quest;

public class CliffwatchInnRenderer extends RendererHelpers implements IStateRenderer {

    private List<Quest> quests = new ArrayList<Quest>();
    
    public CliffwatchInnRenderer(List<Quest> quests) {
        this.quests.addAll(quests);
    }
    
    @Override
    public void render(HtmlElement outerTD) {
        if(quests.size() != 4) {
            throw new UnsupportedOperationException("Must be four quests in Cliffwatch in to render!");
        }
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        
        HtmlElement questTR1 = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement questTD1 = questTR1.createChildElement(HtmlElementType.TD);
        if(quests.size() > 0) {
            new QuestRenderer(quests.get(0)).render(questTD1);
        } else {
            questTD1.setValue("&nbsp;");
        }
        HtmlElement questTD2 = questTR1.createChildElement(HtmlElementType.TD);
        if(quests.size() > 1) {
            new QuestRenderer(quests.get(1)).render(questTD2);
        } else {
            questTD2.setValue("&nbsp;");
        }
        
//        HtmlElement questTR2 = table.createChildElement(HtmlElementType.TR);
        HtmlElement questTD3 = questTR1.createChildElement(HtmlElementType.TD);
        if(quests.size() > 2) {
            new QuestRenderer(quests.get(2)).render(questTD3);
        } else {
            questTD3.setValue("&nbsp;");
        }
        HtmlElement questTD4 = questTR1.createChildElement(HtmlElementType.TD);
        if(quests.size() > 3) {
            new QuestRenderer(quests.get(3)).render(questTD4);
        } else {
            questTD4.setValue("&nbsp;");
        }
    }
    
    
}

package org.talwood.lords.renderers;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Houses;
import org.talwood.lords.enums.Lords;
import org.talwood.lords.gamerunner.GamePlayer;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.quests.Quest;

public class EndOfGameRenderer extends RendererHelpers implements IStateRenderer {

    private GamePlayer player;
    private int corruptionPenalty;
    
    public EndOfGameRenderer(GamePlayer player, int corruptionPenalty) {
        this.player = player;
//        this.player = player.makeCopyForRenderer();
        this.corruptionPenalty = corruptionPenalty;
    }

    @Override
    public void render(HtmlElement outerTD) {
        new HRRenderer().render(outerTD);
        new H3Renderer("Scoring for " + player.getName()).render(outerTD);
        int vic = player.getVictoryPoints();
        new H4Renderer("Basic points: " + vic).render(outerTD);
        new H4Renderer("Intrigue cards played: " + player.getIntriguesPlayed()).render(outerTD);
        int goldMod = player.getResources().getGold() / 2;
        vic += goldMod;
        new H4Renderer("Points for " + player.getResources().getGold() + " gold: " + goldMod).render(outerTD);
        int troopMod = player.getResources().getCritterCount();
        vic += troopMod;
        new H4Renderer("Points for " + buildTroopImagesForResources(player.getResources()) + ": " + troopMod).render(outerTD);
        // Remove corruption
        if(player.getResources().getCorruption() > 0) {
            int corMod = (player.getResources().getCorruption() * corruptionPenalty);
            new H4Renderer("Penalty for " + player.getResources().getCorruption() + " corruption: " + corMod).render(outerTD);
            vic -= corMod;
        }
        Lords l = player.getLord();
        int lordPoints = 0;
        if(l.isBuildingLord()) {
            lordPoints = player.getOwnedBuildings().size() * 6;
        } else if (l.isAllQuestLord()) {
            lordPoints = player.getCompletedQuests().size() * 3;
        } else if (l.isCorruptionLord()) {
        } else if (l.isSingleHouseLord()) {
            // Determine the single house
            Houses bestHouse = player.getDefinedBestHouse();
            for(Quest q : player.getCompletedQuests()) {
                if(q.getQuestInfo().getHouse() == bestHouse) {
                    lordPoints += 6;
                }
            }
        } else if (l.isSkullportLord()) {
            for(Quest q : player.getCompletedQuests()) {
                if(q.getQuestInfo().getExpansion() == Expansions.Skullport) {
                    lordPoints += 4;
                }
            }
            for(Building b : player.getOwnedBuildings()) {
                if(b.getBuildingInfo().getExpansion() == Expansions.Skullport) {
                    lordPoints += 4;
                }
            }
        } else if (l.isTenPointLord()) {
            for(Quest q : player.getCompletedQuests()) {
                if(q.getQuestInfo().getVictoryPoints() >= 10) {
                    lordPoints += 5;
                }
            }
        } else if (l.isUndermountainLord()) {
            for(Quest q : player.getCompletedQuests()) {
                if(q.getQuestInfo().getExpansion() == Expansions.Undermountian) {
                    lordPoints += 4;
                }
            }
            for(Building b : player.getOwnedBuildings()) {
                if(b.getBuildingInfo().getExpansion() == Expansions.Undermountian) {
                    lordPoints += 4;
                }
            }
            
        } else if(l.getHouse1() != null && l.getHouse2() != null) {
            for(Quest q : player.getCompletedQuests()) {
                if(q.getQuestInfo().getHouse() == l.getHouse1() || q.getQuestInfo().getHouse() == l.getHouse2()) {
                    lordPoints += 4;
                }
            }
        }
        vic += lordPoints;
        new H4Renderer("Extra for Lord: " + l.getLordName() + " (" + l.getDescription() + "): " + lordPoints).render(outerTD);
        new H4Renderer("Total points for " + player.getName() + ": " + vic).render(outerTD);
    }
    
}

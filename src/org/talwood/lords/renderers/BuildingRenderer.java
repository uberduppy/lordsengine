package org.talwood.lords.renderers;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.helpers.StringHelper;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;

public class BuildingRenderer extends RendererHelpers implements IStateRenderer {

    private Building building;
    private Buildings buildingInfo;
    private boolean showVictoryPoints;
    
    public BuildingRenderer(Building building, boolean showVictoryPoints) {
        this.building = building;
        this.buildingInfo = building.getBuildingInfo();
        this.showVictoryPoints = showVictoryPoints;
    }
    
    public void renderTitle(HtmlElement outerTD) {
        HtmlElement tablea = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"), HtmlAttr.border("1"));
        HtmlElement tra = tablea.createChildElement(HtmlElementType.TR);
        HtmlElement tda = tra.createChildElement(HtmlElementType.TD);
        
        HtmlElement table = tda.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"), HtmlAttr.border("0"));

        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR);
        HtmlElement td = tr.createChildElement(HtmlElementType.TD, HtmlAttr.bgcolor("#909090"));
        HtmlElement centered = td.createChildElement(HtmlElementType.CENTER);
        HtmlElement fontset = centered.createChildElement(HtmlElementType.FONT, 
                buildingInfo.getName(), 
                HtmlAttr.face("Papyrus"), HtmlAttr.color("#ffffff"), HtmlAttr.size("+1"));
    }
    
    
    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement tablea = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"), HtmlAttr.border("1"));
        HtmlElement tra = tablea.createChildElement(HtmlElementType.TR);
        HtmlElement tda = tra.createChildElement(HtmlElementType.TD);
        
        HtmlElement table = tda.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"), HtmlAttr.border("0"));

        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR);
        HtmlElement td = tr.createChildElement(HtmlElementType.TD, HtmlAttr.bgcolor("#909090"));
        HtmlElement centered = td.createChildElement(HtmlElementType.CENTER);
        HtmlElement fontset = centered.createChildElement(HtmlElementType.FONT, 
                buildingInfo.getName() 
                 + (showVictoryPoints  ? "&nbsp;&nbsp;[" + building.getStoredVictoryPoints() + " VP]" : "")
                 + (buildingInfo.getCost() == 0 ? "" : "&nbsp;&nbsp;&nbsp;Costs " + buildingInfo.getCost()+ " gold"), 
                HtmlAttr.face("Papyrus"), HtmlAttr.color("#ffffff"), HtmlAttr.size("+1"));
        
        // Is there a descriptive header?
        if(StringHelper.isNotEmpty(building.getDescriptiveHeader())) {
            table.makeEmptyTR().createChildElement(HtmlElementType.TD, buildImagesFromMask(building.getDescriptiveHeader()));
        }
        
        // Is there a general description?
        if(StringHelper.isNotEmpty(building.getGeneralDescription())) {
            table.makeEmptyTR().createChildElement(HtmlElementType.TD, building.getGeneralDescription());
        }
        
        if(StringHelper.isNotEmpty(building.getBestCostMask())) {
            table.makeEmptyTR().createChildElement(HtmlElementType.TD, buildImagesFromMask(building.getBestCostMask()));
        } else if(building.getActivationCost().isNotEmpty()) {
            table.makeEmptyTR().createChildElement(HtmlElementType.TD, "Costs: " + buildImagesForCost(building.getActivationCost()));
        }
        
        String rewardMask = building.getTotalActionRewardMask();
        if(StringHelper.isNotEmpty(building.getDescriptiveText())) {
            table.makeEmptyTR().createChildElement(HtmlElementType.TD, buildImagesFromMask(building.getDescriptiveText()));
            
        } else if(StringHelper.isNotEmpty(rewardMask)) {
            table.makeEmptyTR().createChildElement(HtmlElementType.TD, "Provides: " + buildImagesFromMask(rewardMask));
        } else  if (building.getBuildingReward().isNotEmpty()) {
            table.makeEmptyTR().createChildElement(HtmlElementType.TD, "Provides: " + buildImagesForCost(building.getBuildingReward()));
            if(building.isHasExtraRewards()) {
                table.makeEmptyTR().createChildElement(HtmlElementType.TD, "Extra: " + buildImagesForCost(building.retrieveExtraRewardForDisplay()));
            }
        }

        if(StringHelper.isNotEmpty(building.getOwnerRewardDisplayMask())) {
            table.makeEmptyTR().createChildElement(HtmlElementType.TD, "Owner Reward: " + buildImagesFromMask(building.getOwnerRewardDisplayMask()));
        } else if (building.getOwnerReward().isNotEmpty()) {
            table.makeEmptyTR().createChildElement(HtmlElementType.TD, "Owner Reward: " + buildImagesForResources(building.getOwnerReward()));
        }
    }
}

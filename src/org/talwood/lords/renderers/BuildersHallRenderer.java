package org.talwood.lords.renderers;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.containers.BuildingContainer;
import org.talwood.lords.containers.BuildingPointContainer;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;

public class BuildersHallRenderer extends RendererHelpers implements IStateRenderer {

    private List<BuildingPointContainer> points = new ArrayList<BuildingPointContainer>();
    private List<Building> bldgs = new ArrayList<Building>();
    
    public BuildersHallRenderer(List<BuildingPointContainer> points) {
        BuildingContainer allBuildingsConstants = new BuildingContainer(Expansions.Classic, Expansions.Skullport, Expansions.Undermountian);
        
        for(BuildingPointContainer p : points) {
            Building ab = allBuildingsConstants.findByInfo(p.getBuildingInfo());
            ab.setStoredVictoryPoints(p.getVictoryPoints());
            this.bldgs.add(ab);
        }
//        this.bldgs.addAll(bldgs);
        
    }
    
    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        HtmlElement bldgTR = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        for(Building ab : bldgs) {
            HtmlElement bldgTD = bldgTR.createChildElement(HtmlElementType.TD, HtmlAttr.width("33%"));
            if(ab != null) {
                new BuildingRenderer(ab, true).render(bldgTD);
//                bldgTD.setValue(generateBuildingDisplay(ab));
            } else {
                bldgTD.setValue("No building found...");
            }
        }
    }

}

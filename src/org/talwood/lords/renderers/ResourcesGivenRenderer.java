package org.talwood.lords.renderers;

import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.resources.Resources;

public class ResourcesGivenRenderer extends RendererHelpers implements IStateRenderer {

    private Resources res;
    public ResourcesGivenRenderer(Resources res) {
        this.res = res;
    }

    @Override
    public void render(HtmlElement outerTD) {
        if(res.isNotEmpty()) {
            HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);

            HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
            HtmlElement td = tr.createChildElement(HtmlElementType.TD);
            td.createChildElement(HtmlElementType.B, "Resources given:&nbsp;");
            tr.createChildElement(HtmlElementType.TD, buildImagesForCost(res));
            
        }
        
    }
}

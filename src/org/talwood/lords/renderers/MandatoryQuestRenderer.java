package org.talwood.lords.renderers;

import org.talwood.lords.gamerunner.GamePlayer;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.quests.Quest;

public class MandatoryQuestRenderer implements IStateRenderer {

    private Quest taken;
    private GamePlayer player;
    
    public MandatoryQuestRenderer(GamePlayer player, Quest taken) {
        this.player = player;
        this.taken = taken;
    }
    
    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        HtmlElement fontset = td.createChildElement(HtmlElementType.B, "Mandatory Quest gicen to " + player.getName() + ":&nbsp;");
        
        td = tr.createChildElement(HtmlElementType.TD);
        new QuestRenderer(taken).render(td);
        
    }
    
}

package org.talwood.lords.renderers;

import org.talwood.lords.gamerunner.GamePlayer;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.intrigues.Intrigue;

public class DrawIntrigueRenderer implements IStateRenderer {

    private Intrigue card;
    private GamePlayer player;
    
    public DrawIntrigueRenderer(Intrigue card) {
        this.card = card;
    }
    
    public DrawIntrigueRenderer(GamePlayer player, Intrigue card) {
        this.card = card;
        this.player = player;
    }
    
    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        if(player != null) {
            td.createChildElement(HtmlElementType.B, "Intrigue card drawn by " + player.getName() + ":&nbsp;");
        } else {
            td.createChildElement(HtmlElementType.B, "Intrigue card drawn:&nbsp;");
        }
        
        td = tr.createChildElement(HtmlElementType.TD);
        new IntrigueRenderer(card).render(td);
    }
    
}

package org.talwood.lords.renderers;

import static org.talwood.lords.enums.IntrigueActionTypes.MandatoryQuest;
import org.talwood.lords.enums.QuestTypes;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.helpers.StringHelper;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.quests.PlotQuest;
import org.talwood.lords.quests.Quest;
import org.talwood.lords.resources.Resources;

public class QuestRenderer extends RendererHelpers implements IStateRenderer {

    private Quest quest;
    private Quests questInfo;
    
    public QuestRenderer(Quest quest) {
        this.quest = quest;
        this.questInfo = quest.getQuestInfo();
    }

    public void renderTitle(HtmlElement parent) {
        HtmlElement tablea = parent.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"), HtmlAttr.border("1"));
        HtmlElement tra = tablea.createChildElement(HtmlElementType.TR);
        HtmlElement tda = tra.createChildElement(HtmlElementType.TD);
        HtmlElement table = tda.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"), HtmlAttr.border("0"));
        HtmlElement tr = table.createChildElement(HtmlElementType.TR);
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        
        HtmlElement innertable = td.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        HtmlElement innertr = innertable.createChildElement(HtmlElementType.TR);
        HtmlElement innertd = innertr.createChildElement(HtmlElementType.TD, HtmlAttr.bgcolor(questInfo.getHouse().getBackground()));
        HtmlElement centered = innertd.createChildElement(HtmlElementType.CENTER);
        if(questInfo.getQuestType() == QuestTypes.Mandatory) {
            centered.createChildElement(HtmlElementType.FONT, questInfo.getName(), HtmlAttr.face("Papyrus"), HtmlAttr.color(questInfo.getHouse().getForeground()), HtmlAttr.size("-1"));
        } else {
            centered.createChildElement(HtmlElementType.FONT, questInfo.getHouse().getDescription() + " - " + questInfo.getName(), HtmlAttr.face("Papyrus"), HtmlAttr.color(questInfo.getHouse().getForeground()), HtmlAttr.size("-1"));
        }
        
    }
    
    @Override
    public void render(HtmlElement parent) {
        HtmlElement tablea = parent.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"), HtmlAttr.border("1"));
        HtmlElement tra = tablea.createChildElement(HtmlElementType.TR);
        HtmlElement tda = tra.createChildElement(HtmlElementType.TD);
        HtmlElement table = tda.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"), HtmlAttr.border("0"));
        HtmlElement tr = table.createChildElement(HtmlElementType.TR);
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        
        HtmlElement innertable = td.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        HtmlElement innertr = innertable.createChildElement(HtmlElementType.TR);
        HtmlElement innertd = innertr.createChildElement(HtmlElementType.TD, HtmlAttr.bgcolor(questInfo.getHouse().getBackground()));
        HtmlElement centered = innertd.createChildElement(HtmlElementType.CENTER);
        HtmlElement fontset = centered.createChildElement(HtmlElementType.FONT, questInfo.getHouse().getDescription() + " - " + questInfo.getName(), HtmlAttr.face("Papyrus"), HtmlAttr.color(questInfo.getHouse().getForeground()), HtmlAttr.size("+1"));

        // Requires row
        Resources wap = quest.getCost();
        innertr = innertable.createChildElement(HtmlElementType.TR);
        innertd = innertr.createChildElement(HtmlElementType.TD);
        centered = innertd.createChildElement(HtmlElementType.CENTER, "Requires:&nbsp;" + buildImagesForCost(wap));

        // Requires row
        StringBuilder reward = new StringBuilder();
        reward.append("Reward:&nbsp;");

        // Victory points
        reward.append(quest.getQuestInfo().getVictoryPoints());
        reward.append(" victory points ");

        // Images for reward.
        String rewards = buildImagesForCost(quest.getReward());
        if(StringHelper.isNotEmpty(rewards)) {
            reward.append("<br>");
            reward.append(rewards);
                    
        }
        innertr = innertable.createChildElement(HtmlElementType.TR);
        innertd = innertr.createChildElement(HtmlElementType.TD);
        centered = innertd.createChildElement(HtmlElementType.CENTER, reward.toString());

        if(StringHelper.isNotEmpty(quest.getDescription())) {
            innertr = innertable.createChildElement(HtmlElementType.TR);
            innertd = innertr.createChildElement(HtmlElementType.TD);
            centered = innertd.createChildElement(HtmlElementType.CENTER, buildImagesFromMask(quest.getDescription()));
        }
        
        // Plot quest line?
        if(quest instanceof PlotQuest) {
            PlotQuest plq = (PlotQuest)quest;
            innertr = innertable.createChildElement(HtmlElementType.TR);
            innertd = innertr.createChildElement(HtmlElementType.TD, HtmlAttr.bgcolor(questInfo.getHouse().getBackground()));
            centered = innertd.createChildElement(HtmlElementType.CENTER);
            HtmlElement bolded = centered.createChildElement(HtmlElementType.B);
            fontset = bolded.createChildElement(HtmlElementType.FONT, "Plot Quest", HtmlAttr.color(questInfo.getHouse().getForeground()));
        }
        if(questInfo.getQuestType() == QuestTypes.Mandatory) {
            innertr = innertable.createChildElement(HtmlElementType.TR);
            innertd = innertr.createChildElement(HtmlElementType.TD, HtmlAttr.bgcolor(questInfo.getHouse().getBackground()));
            centered = innertd.createChildElement(HtmlElementType.CENTER);
            HtmlElement bolded = centered.createChildElement(HtmlElementType.B);
            fontset = bolded.createChildElement(HtmlElementType.FONT, "Mandatory Quest", HtmlAttr.color(questInfo.getHouse().getForeground()));
        }
    }
    
    
}

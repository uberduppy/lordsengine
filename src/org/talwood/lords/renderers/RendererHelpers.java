/* Prototype header
*/

package org.talwood.lords.renderers;

import java.util.List;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.helpers.StringHelper;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.resources.Res;
import org.talwood.lords.resources.Resources;

/**
 *
 * @author twalker
 */
public class RendererHelpers {

    protected static String buildImagesFromResources(List<Res> resources) {
        StringBuilder imageData = new StringBuilder();
        for(Res r : resources) {
            if(r.getCount() > 0) {
                switch(r.getResources()) {
                    case Cleric:
                        imageData.append(buildImageSetForCost(r.getCount(), "cleric.jpg"));
                        break;
                    case Fighter:
                        imageData.append(buildImageSetForCost(r.getCount(), "fighter.jpg"));
                        break;
                    case Wizard:
                        imageData.append(buildImageSetForCost(r.getCount(), "wizard.jpg"));
                        break;
                    case Rogue:
                        imageData.append(buildImageSetForCost(r.getCount(), "rogue.jpg"));
                        break;
                    case Gold:
                        imageData.append(buildImageSetForCost(r.getCount(), "goldone.jpg"));
                        break;
                }
            }
        }
        return imageData.toString();
    }
    
//    protected static String buildImagesForCost(WaterdeepAdventurerPool wap) {
//        StringBuilder imageData = new StringBuilder();
//        imageData.append(buildImageSetForCost(wap.getAnyResource(), "any.jpg"));
//        imageData.append(buildImageSetForCost(wap.getCleric(), "cleric.jpg"));
//        imageData.append(buildImageSetForCost(wap.getFighter(), "fighter.jpg"));
//        imageData.append(buildImageSetForCost(wap.getRogue(), "rogue.jpg"));
//        imageData.append(buildImageSetForCost(wap.getWizard(), "wizard.jpg"));
//        imageData.append(buildImageSetForCost(wap.getGold() / 5, "goldfive.jpg"));
//        imageData.append(buildImageSetForCost(wap.getGold() % 5, "goldone.jpg"));
//        imageData.append(buildImageSetForCost(wap.getIntrigue(), "intrigue.jpg"));
//        
//        return imageData.toString();
//    }
    protected static String buildImagesForCostWithBreaks(Resources res) {
        boolean needBreaks = res.getTotalAdventurers() > 25;
        StringBuilder imageData = new StringBuilder();
        imageData.append(buildImageSetForCostWithBreaks(res.getAnyResource(), "any.jpg", needBreaks));
        imageData.append(buildImageSetForCostWithBreaks(res.getCleric(), "cleric.jpg", needBreaks));
        imageData.append(buildImageSetForCostWithBreaks(res.getFighter(), "fighter.jpg", needBreaks));
        imageData.append(buildImageSetForCostWithBreaks(res.getRogue(), "rogue.jpg", needBreaks));
        imageData.append(buildImageSetForCostWithBreaks(res.getWizard(), "wizard.jpg", needBreaks));
        imageData.append(buildImageSetForCostWithBreaks(res.getIntrigue(), "intrigue.jpg", needBreaks));
        imageData.append(buildImageSetForCostWithBreaks(res.getGold() / 5, "goldfive.jpg", needBreaks));
        imageData.append(buildImageSetForCostWithBreaks(res.getGold() % 5, "goldone.jpg", needBreaks));
        imageData.append(buildImageSetForCostWithBreaks(res.getCorruption(), "corrupt.jpg", needBreaks));
        
        return imageData.toString();
    }
    protected static String buildImagesForCost(Resources res) {
        StringBuilder imageData = new StringBuilder();
        imageData.append(buildImageSetForCost(res.getAnyResource(), "any.jpg"));
        imageData.append(buildImageSetForCost(res.getCleric(), "cleric.jpg"));
        imageData.append(buildImageSetForCost(res.getFighter(), "fighter.jpg"));
        imageData.append(buildImageSetForCost(res.getRogue(), "rogue.jpg"));
        imageData.append(buildImageSetForCost(res.getWizard(), "wizard.jpg"));
        imageData.append(buildImageSetForCost(res.getIntrigue(), "intrigue.jpg"));
        imageData.append(buildImageSetForCost(res.getGold() / 5, "goldfive.jpg"));
        imageData.append(buildImageSetForCost(res.getGold() % 5, "goldone.jpg"));
        imageData.append(buildImageSetForCost(res.getCorruption(), "corrupt.jpg"));
        
        return imageData.toString();
    }

    protected static String buildImagesForResources(Resources res) {
        StringBuilder imageData = new StringBuilder();
        imageData.append(buildImageSetForCost(res.getAnyResource(), "any.jpg"));
        imageData.append(buildImageSetForCost(res.getCleric(), "cleric.jpg"));
        imageData.append(buildImageSetForCost(res.getFighter(), "fighter.jpg"));
        imageData.append(buildImageSetForCost(res.getRogue(), "rogue.jpg"));
        imageData.append(buildImageSetForCost(res.getWizard(), "wizard.jpg"));
        imageData.append(buildImageSetForCost(res.getGold() / 5, "goldfive.jpg"));
        imageData.append(buildImageSetForCost(res.getGold() % 5, "goldone.jpg"));
        imageData.append(buildImageSetForCost(res.getCorruption(), "corrupt.jpg"));
        
        return imageData.toString();
    }

    protected static String buildTroopImagesForResources(Resources res) {
        StringBuilder imageData = new StringBuilder();
        boolean breakit = res.getCritterCount() > 30;
        imageData.append(buildImageSetForCostWithBreaks(res.getCleric(), "cleric.jpg", breakit));
        imageData.append(buildImageSetForCostWithBreaks(res.getFighter(), "fighter.jpg", breakit));
        imageData.append(buildImageSetForCostWithBreaks(res.getRogue(), "rogue.jpg", breakit));
        imageData.append(buildImageSetForCostWithBreaks(res.getWizard(), "wizard.jpg", breakit));
        
        return imageData.toString();
    }

    protected static String buildImageSetForCost(int count, String imageName) {
        StringBuilder imageData = new StringBuilder();
        for(int x = 0; x < count ; x++) {
            HtmlElement img = new HtmlElement(HtmlElementType.IMG, HtmlAttr.src(imageName));
            imageData.append(img.render());
        }
        return imageData.toString();
    }
    
    protected static String buildImageSetForCostWithBreaks(int count, String imageName, boolean needBreaks) {
        StringBuilder imageData = new StringBuilder();
        for(int x = 0; x < count ; x++) {
            HtmlElement img = new HtmlElement(HtmlElementType.IMG, HtmlAttr.src(imageName));
            imageData.append(img.render());
        }
        if(needBreaks && count > 0) {
            imageData.append("<br>");
        }
            
        return imageData.toString();
    }
    
    protected static String buildImagesFromMask(String maskedData) {
        StringBuilder imageData = new StringBuilder();
        if(StringHelper.isNotEmpty(maskedData)) {
            for(String[] mask : ResourceConstants.MASKS) {
                maskedData = StringHelper.replaceAll(maskedData, mask[0], new HtmlElement(HtmlElementType.IMG, HtmlAttr.src(mask[1])).render());
            }
            imageData.append(maskedData);
        }
        return imageData.toString();
    }
    
}

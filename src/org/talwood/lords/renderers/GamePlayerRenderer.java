package org.talwood.lords.renderers;

import java.util.List;
import org.talwood.lords.gamerunner.GamePlayer;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.quests.Quest;

public class GamePlayerRenderer implements IStateRenderer {

    private GamePlayer player;
    
    public GamePlayerRenderer(GamePlayer player) {
        this.player = player;
    }

    @Override
    public void render(HtmlElement outerTD) {
        // Need to render the active quests
        if(player.getCurrentQuests().size() > 0) {
            new H4Renderer("Pending Quests").render(outerTD);
            HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));

            HtmlElement questTR1 = table.createChildElement(HtmlElementType.TR);
            List<Quest> quests = player.getCurrentQuests();
            for(Quest q : quests) {
                HtmlElement questTD1 = questTR1.createChildElement(HtmlElementType.TD);
                new QuestRenderer(q).render(questTD1);
            }
        } else {
            new H4Renderer("No Pending Quests").render(outerTD);
        }
        // Need to render held intrigue cards
        new H4Renderer("Intrigue Cards").render(outerTD);
        // Need to render resources
        new H4Renderer("Tavern Resources").render(outerTD);
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

package org.talwood.lords.renderers;

import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.intrigues.Intrigue;

public class IntrigueRenderer extends RendererHelpers implements IStateRenderer {

    private Intrigue intrigue;
    
    public IntrigueRenderer(Intrigue intrigue) {
        this.intrigue = intrigue;
    }
    
    public void renderTitle(HtmlElement outerTD) {
        
        HtmlElement tablea = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"), HtmlAttr.border("1"));
        HtmlElement tra = tablea.createChildElement(HtmlElementType.TR);
        HtmlElement tda = tra.createChildElement(HtmlElementType.TD);
        HtmlElement table = tda.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"), HtmlAttr.border("0"));
        HtmlElement tr = table.createChildElement(HtmlElementType.TR);
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        
        HtmlElement innertable = td.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        HtmlElement innertr = innertable.createChildElement(HtmlElementType.TR);
        HtmlElement innertd = innertr.createChildElement(HtmlElementType.TD, HtmlAttr.bgcolor("#bbbbbb"));
        HtmlElement centered = innertd.createChildElement(HtmlElementType.CENTER);
        HtmlElement fontset = centered.createChildElement(HtmlElementType.FONT, intrigue.getIntrigueInfo().getName(), HtmlAttr.face("Papyrus"), HtmlAttr.color("#111111"), HtmlAttr.size("-1"));
    }
    
    @Override
    public void render(HtmlElement outerTD) {
        
        HtmlElement tablea = outerTD.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"), HtmlAttr.border("1"));
        HtmlElement tra = tablea.createChildElement(HtmlElementType.TR);
        HtmlElement tda = tra.createChildElement(HtmlElementType.TD);
        HtmlElement table = tda.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"), HtmlAttr.border("0"));
        HtmlElement tr = table.createChildElement(HtmlElementType.TR);
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        
        HtmlElement innertable = td.createChildElement(HtmlElementType.TABLE, HtmlAttr.width("100%"));
        HtmlElement innertr = innertable.createChildElement(HtmlElementType.TR);
        HtmlElement innertd = innertr.createChildElement(HtmlElementType.TD, HtmlAttr.bgcolor("#bbbbbb"));
        HtmlElement centered = innertd.createChildElement(HtmlElementType.CENTER);
        HtmlElement fontset = centered.createChildElement(HtmlElementType.FONT, intrigue.getIntrigueInfo().getName(), HtmlAttr.face("Papyrus"), HtmlAttr.color("#111111"), HtmlAttr.size("+1"));
        
        innertr = innertable.createChildElement(HtmlElementType.TR);
        innertd = innertr.createChildElement(HtmlElementType.TD, HtmlAttr.bgcolor("#444444"));
        centered = innertd.createChildElement(HtmlElementType.CENTER);
        fontset = centered.createChildElement(HtmlElementType.FONT, intrigue.getIntrigueInfo().getType().getDescription() + " (" + intrigue.getIntrigueInfo().getExpansion().getDescription() + ")", HtmlAttr.face("Papyrus"), HtmlAttr.color("#cccccc"), HtmlAttr.size("-1"));
        
        innertr = innertable.createChildElement(HtmlElementType.TR);
        innertd = innertr.createChildElement(HtmlElementType.TD);
        centered = innertd.createChildElement(HtmlElementType.CENTER, buildImagesFromMask(intrigue.getIntrigueInfo().getDescription()));
        
        
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

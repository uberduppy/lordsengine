package org.talwood.lords.renderers;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;

public class AgentRelocationRenderer extends RendererHelpers implements IStateRenderer {

    private Building buildingFrom;
    
    public AgentRelocationRenderer(Building buildingFrom) {
        this.buildingFrom = buildingFrom;
    }

    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        HtmlElement fontset = td.createChildElement(HtmlElementType.B, "Agent relocated from&nbsp;");
        
        td = tr.createChildElement(HtmlElementType.TD);
        new BuildingRenderer(buildingFrom, false).render(td);
        
    }
}

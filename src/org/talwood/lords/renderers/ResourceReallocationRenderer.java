package org.talwood.lords.renderers;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import static org.talwood.lords.renderers.RendererHelpers.buildImagesForCost;
import org.talwood.lords.resources.Resources;

public class ResourceReallocationRenderer extends RendererHelpers implements IStateRenderer {
    
    private Building bldg;
    private Resources res;
    public ResourceReallocationRenderer(Building bldg, Resources res) {
        this.bldg = bldg;
        this.res = new Resources(res);
    }

    @Override
    public void render(HtmlElement outerTD) {
        if(res.isNotEmpty()) {
            HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);

            HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
            HtmlElement td = tr.createChildElement(HtmlElementType.TD, buildImagesForCost(res));
            tr.createChildElement(HtmlElementType.TD, "added to");
            new BuildingRenderer(bldg, false).renderTitle(tr.createChildElement(HtmlElementType.TD));
            
        }
    }
}

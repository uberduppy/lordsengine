package org.talwood.lords.renderers;

import org.talwood.lords.gamerunner.GamePlayer;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.resources.Resources;

public class BuildingOwnerRewardRenderer extends RendererHelpers implements IStateRenderer {

    private GamePlayer buildingOwner;
    private Resources res;
    
    public BuildingOwnerRewardRenderer(GamePlayer buildingOwner, Resources res) {
        this.buildingOwner = buildingOwner;
        this.res = res;
    }

    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        HtmlElement fontset = td.createChildElement(HtmlElementType.B, "Building owner " + buildingOwner.getName() + "&nbsp;");

        tr.createChildElement(HtmlElementType.TD, buildImagesForCost(res));
        
    }
}

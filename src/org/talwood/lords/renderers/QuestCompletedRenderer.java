package org.talwood.lords.renderers;

import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.quests.Quest;

public class QuestCompletedRenderer implements IStateRenderer {

    private Quest quest;
    
    public QuestCompletedRenderer(Quest quest) {
        this.quest = quest;
    }

    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        HtmlElement fontset = td.createChildElement(HtmlElementType.B, "Quest completed:&nbsp;");
        
        td = tr.createChildElement(HtmlElementType.TD);
        new QuestRenderer(quest).render(td);
    }
}

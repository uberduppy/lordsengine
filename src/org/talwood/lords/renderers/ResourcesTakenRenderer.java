package org.talwood.lords.renderers;

import org.talwood.lords.gamerunner.GamePlayer;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.resources.Resources;

public class ResourcesTakenRenderer extends RendererHelpers implements IStateRenderer {

    private Resources res;
    private GamePlayer player;
    public ResourcesTakenRenderer(Resources res) {
        this.res = res;
    }
    public ResourcesTakenRenderer(GamePlayer player, Resources res) {
        this.res = res;
        this.player = player;
    }

    @Override
    public void render(HtmlElement outerTD) {
        if(res.isNotEmpty()) {
            HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);

            HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
            HtmlElement td = tr.createChildElement(HtmlElementType.TD);
            if(player != null) {
                td.createChildElement(HtmlElementType.B, "Resources taken from " + player.getName() + ":&nbsp;");
            } else {
                td.createChildElement(HtmlElementType.B, "Resources taken:&nbsp;");
            }
            tr.createChildElement(HtmlElementType.TD, buildImagesForCost(res));
            
        }
        
    }
}

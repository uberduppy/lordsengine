package org.talwood.lords.renderers;

import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.intrigues.Intrigue;
import org.talwood.lords.resources.Resources;

public class IntriguePlayedRenderer extends RendererHelpers implements IStateRenderer {

    private Intrigue card;
    private Resources reward;
    private boolean asAction;
    
    public IntriguePlayedRenderer(Intrigue card, Resources reward) {
        this.card = card;
        this.reward = new Resources(reward);
    }
    
    public IntriguePlayedRenderer(Intrigue card) {
        this.card = card;
        this.reward = new Resources();
    }

    public IntriguePlayedRenderer(Intrigue card, boolean asAction) {
        this.card = card;
        this.reward = new Resources();
        this.asAction = asAction;
    }

    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        td.createChildElement(HtmlElementType.B, "Intrigue Card Played" + (asAction ? " as action" : "") + ":&nbsp;");

        new IntrigueRenderer(card).render(tr.createChildElement(HtmlElementType.TD));
        if(reward.isNotEmpty()) {
            HtmlElement innertd = tr.createChildElement(HtmlElementType.TD);
            innertd.createChildElement(HtmlElementType.CENTER, "<b>Resources taken:</b>&nbsp;" + buildImagesForResources(reward));
        }
    }
    
    
}

package org.talwood.lords.renderers;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.gamerunner.GamePlayer;
import org.talwood.lords.html.HtmlAttr;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;

public class AgentPlacementRenderer extends RendererHelpers implements IStateRenderer {

    private Building building;
    
    public AgentPlacementRenderer(Building building) {
        this.building = building;
    }
    
    @Override
    public void render(HtmlElement outerTD) {
        HtmlElement table = outerTD.createChildElement(HtmlElementType.TABLE);
        
        HtmlElement tr = table.createChildElement(HtmlElementType.TR, HtmlAttr.valign("top"));
        HtmlElement td = tr.createChildElement(HtmlElementType.TD);
        td.createChildElement(HtmlElementType.B, "Agent placed on&nbsp;");
        
        td = tr.createChildElement(HtmlElementType.TD);
        new BuildingRenderer(building, false).render(td);
        
    }
    
}

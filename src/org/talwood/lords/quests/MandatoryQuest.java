package org.talwood.lords.quests;

import org.talwood.lords.enums.QuestTypes;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.resources.QuestCost;
import org.talwood.lords.resources.QuestReward;

public class MandatoryQuest extends Quest {
    
    public MandatoryQuest(Quests questInfo, QuestCost questCost, QuestReward questReward) {
        super(questInfo, questCost, questReward);
        if(questInfo.getQuestType() != QuestTypes.Mandatory) {
            throw new UnsupportedOperationException("Incorrect quest type used for " + questInfo.getName());
        }
    }

    @Override
    public boolean isSpecialQuest() {
        return false;
    }

    @Override
    public void postProcessing() {
    }
    
    
}

package org.talwood.lords.quests;

import org.talwood.lords.enums.QuestTypes;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.resources.QuestCost;
import org.talwood.lords.resources.QuestReward;

public class OptionsQuest extends Quest {
    private QuestCost optionalExtraCost = new QuestCost();
    private QuestReward optionalExtraReward = new QuestReward();
    
    public OptionsQuest(Quests questInfo, QuestCost questCost, QuestReward questReward) {
        super(questInfo, questCost, questReward);
        if(questInfo.getQuestType() != QuestTypes.Options) {
            throw new UnsupportedOperationException("Incorrect quest type used for " + questInfo.getName());
        }
    }
    public OptionsQuest(Quests questInfo, QuestCost questCost, QuestReward questReward, QuestCost extraCost, QuestReward extraReward) {
        super(questInfo, questCost, questReward);
        this.optionalExtraCost = extraCost;
        this.optionalExtraReward = extraReward;
        if(questInfo.getQuestType() != QuestTypes.Options) {
            throw new UnsupportedOperationException("Incorrect quest type used for " + questInfo.getName());
        }
    }

    public QuestCost getOptionalExtraCost() {
        return optionalExtraCost;
    }

    public QuestReward getOptionalExtraReward() {
        return optionalExtraReward;
    }

    @Override
    public boolean isSpecialQuest() {
        return false;
    }

    @Override
    public void postProcessing() {
    }
    
    
}

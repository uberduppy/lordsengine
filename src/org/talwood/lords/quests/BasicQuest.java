package org.talwood.lords.quests;

import org.talwood.lords.enums.QuestTypes;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.resources.QuestCost;
import org.talwood.lords.resources.QuestReward;

public class BasicQuest extends Quest {
    
    
    public BasicQuest(Quests questInfo, QuestCost cost, QuestReward reward) {
        super(questInfo, cost, reward);
        if(questInfo.getQuestType() != QuestTypes.Basic) {
            throw new UnsupportedOperationException("Incorrect quest type used for " + questInfo.getName());
        }
    }

    @Override
    public boolean isSpecialQuest() {
        return false;
    }

    @Override
    public void postProcessing() {
        // Basic quests have no special post processing, that's why they're basic.
    }

    
    
}

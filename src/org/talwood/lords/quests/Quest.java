package org.talwood.lords.quests;

import org.talwood.lords.enums.Quests;
import org.talwood.lords.resources.QuestCost;
import org.talwood.lords.resources.QuestReward;


public abstract class Quest {

    protected String description;
    private Quests questInfo;
    private QuestCost cost;
    private QuestReward reward;
    
    private Quest() { }
    
    protected Quest(Quests questInfo, QuestCost cost, QuestReward reward) {
        this.questInfo = questInfo;
        this.cost = cost;
        this.reward = reward;
    }

    public Quests getQuestInfo() {
        return questInfo;
    }

    public QuestCost getCost() {
        return cost;
    }

    public QuestReward getReward() {
        return reward;
    }
    
    public boolean isPlotQuest() {
        return false;
    }
    
    
    
    public abstract boolean isSpecialQuest();
    public abstract void postProcessing();
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Quest andSetDescription(String description) {
        this.description = description;
        return this;
    }
    
}

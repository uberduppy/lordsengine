package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingYawningPortal extends Building {

    public BuildingYawningPortal() {
        super(Buildings.YawningPortal, BuildingActions.RequiresVariableReward, new BuildingReward(Res.anyResource(2)), new BuildingOwnerReward(Res.anyResource(1)));
        this.buildingRewardOptions.add(ResourceChoices.TwoClerics);
        this.buildingRewardOptions.add(ResourceChoices.OneClericOneFighter);
        this.buildingRewardOptions.add(ResourceChoices.OneClericOneRogue);
        this.buildingRewardOptions.add(ResourceChoices.OneClericOneWizard);
        this.buildingRewardOptions.add(ResourceChoices.TwoFighters);
        this.buildingRewardOptions.add(ResourceChoices.TwoRogues);
        this.buildingRewardOptions.add(ResourceChoices.TwoWizards);
        this.buildingRewardOptions.add(ResourceChoices.OneFighterOneRogue);
        this.buildingRewardOptions.add(ResourceChoices.OneFighterOneWizard);
        this.buildingRewardOptions.add(ResourceChoices.OneRogueOneWizard);
        
        addBuildingAction(BuildingActions.RequiresVariableOwnerReward);
        
        this.ownerRewardOptions.add(ResourceChoices.OneCleric);
        this.ownerRewardOptions.add(ResourceChoices.OneFighter);
        this.ownerRewardOptions.add(ResourceChoices.OneRogue);
        this.ownerRewardOptions.add(ResourceChoices.OneWizard);
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_ANY_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_ANY_DATA;
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

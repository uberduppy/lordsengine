package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingHelmstarWarehouse extends Building {
    
    public BuildingHelmstarWarehouse() {
        super(Buildings.HelmstarWarehouse, new BuildingReward(Res.rogue(2), Res.gold(2)), new BuildingOwnerReward(Res.rogue(1)));
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_ROGUE_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA
                + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA;
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }

}

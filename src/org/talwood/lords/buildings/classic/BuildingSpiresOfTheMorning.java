package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingStackedReward;
import org.talwood.lords.resources.Res;

public class BuildingSpiresOfTheMorning extends Building {

    public BuildingSpiresOfTheMorning() {
        super(Buildings.SpiresOfTheMorning, BuildingActions.OwnerReceivesTwoVictoryPoints, new BuildingStackedReward(Res.cleric(1)));
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "2 Victory points";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "Take " + buildGenericRewardMask(retrieveExtraRewardForDisplay()) + " from this space";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "When purchased/start of round,<br>place " 
        + ResourceConstants.MASK_CLERIC_DATA 
        + " on this space";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "Take all " + ResourceConstants.MASK_CLERIC_DATA + " from this space";
    }
}

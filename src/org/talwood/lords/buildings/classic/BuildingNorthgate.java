package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingNorthgate extends Building {

    public BuildingNorthgate() {
        super(Buildings.Northgate, BuildingActions.OwnerReceivesTwoVictoryPoints, new BuildingReward(Res.anyResource(1), Res.gold(2)));
        addBuildingAction(BuildingActions.RequiresVariableReward);
        buildingRewardOptions.add(ResourceChoices.OneCleric);
        buildingRewardOptions.add(ResourceChoices.OneFighter);
        buildingRewardOptions.add(ResourceChoices.OneRogue);
        buildingRewardOptions.add(ResourceChoices.OneWizard);
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "2 victory points";
    }

    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA;
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

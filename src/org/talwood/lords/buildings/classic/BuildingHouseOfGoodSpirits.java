package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingHouseOfGoodSpirits extends Building {
    public BuildingHouseOfGoodSpirits() {
        super(Buildings.HouseOfGoodSpirits, BuildingActions.RequiresVariableReward, new BuildingReward(Res.fighter(1), Res.anyResource(1)), new BuildingOwnerReward(Res.fighter(1)));
        buildingRewardOptions.add(ResourceChoices.TwoFighters);
        buildingRewardOptions.add(ResourceChoices.OneClericOneFighter);
        buildingRewardOptions.add(ResourceChoices.OneFighterOneRogue);
        buildingRewardOptions.add(ResourceChoices.OneFighterOneWizard);
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_FIGHTER_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_ANY_DATA;
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;

public class BuildingHeroesGarden extends Building {
    public BuildingHeroesGarden() {
        super(Buildings.HeroesGarden, 2, BuildingActions.TakeQuest, BuildingActions.OwnerReceivesTwoVictoryPoints);
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "2 victory points";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "Take 1 face-up quest.<br>You may immediately complete that quest.<br>If you do, score 4 victory points";
    }
}

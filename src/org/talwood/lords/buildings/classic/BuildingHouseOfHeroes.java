package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingHouseOfHeroes extends Building {

    public BuildingHouseOfHeroes() {
        super(Buildings.HouseOfHeroes, BuildingActions.RequiresVariableOwnerReward, new BuildingReward(Res.fighter(2), Res.cleric(1)));
        ownerRewardOptions.add(ResourceChoices.OneFighter);
        ownerRewardOptions.add(ResourceChoices.OneCleric);
        
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_FIGHTER_DATA + " or " + ResourceConstants.MASK_CLERIC_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_CLERIC_DATA;
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

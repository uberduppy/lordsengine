package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingActivationCost;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingThreePearls extends Building {

    public BuildingThreePearls() {
        super(Buildings.ThreePearls, new BuildingActivationCost(Res.anyResource(2)), new BuildingReward(Res.anyResource(3)), new BuildingOwnerReward(Res.gold(2)));
        
        addBuildingAction(BuildingActions.RequiresVariableActivation);
        addBuildingAction(BuildingActions.RequiresVariableOwnerReward);
        addBuildingAction(BuildingActions.RequiresVariableReward);
        
        activationCostOptions.add(ResourceChoices.TwoClerics);
        activationCostOptions.add(ResourceChoices.OneClericOneFighter);
        activationCostOptions.add(ResourceChoices.OneClericOneRogue);
        activationCostOptions.add(ResourceChoices.OneClericOneWizard);
        activationCostOptions.add(ResourceChoices.TwoFighters);
        activationCostOptions.add(ResourceChoices.OneFighterOneRogue);
        activationCostOptions.add(ResourceChoices.OneFighterOneWizard);
        activationCostOptions.add(ResourceChoices.TwoRogues);
        activationCostOptions.add(ResourceChoices.OneRogueOneWizard);
        activationCostOptions.add(ResourceChoices.TwoWizards);
        
        buildingRewardOptions.add(ResourceChoices.ThreeClerics);
        buildingRewardOptions.add(ResourceChoices.ThreeFighters);
        buildingRewardOptions.add(ResourceChoices.ThreeRogues);
        buildingRewardOptions.add(ResourceChoices.ThreeWizards);
        buildingRewardOptions.add(ResourceChoices.TwoClericsOneFighter);
        buildingRewardOptions.add(ResourceChoices.TwoClericsOneRogue);
        buildingRewardOptions.add(ResourceChoices.TwoClericsOneWizard);
        buildingRewardOptions.add(ResourceChoices.TwoFightersOneRogue);
        buildingRewardOptions.add(ResourceChoices.TwoFightersOneWizard);
        buildingRewardOptions.add(ResourceChoices.TwoRoguesOneWizard);
        buildingRewardOptions.add(ResourceChoices.OneClericOneFighterOneRogue);
        buildingRewardOptions.add(ResourceChoices.OneClericOneFighterOneWizard);
        buildingRewardOptions.add(ResourceChoices.OneClericOneRogueOneWizard);
        buildingRewardOptions.add(ResourceChoices.OneClericTwoFighters);
        buildingRewardOptions.add(ResourceChoices.OneClericTwoRogues);
        buildingRewardOptions.add(ResourceChoices.OneClericTwoWizards);
        buildingRewardOptions.add(ResourceChoices.OneFighterOneRogueOneWizard);
        buildingRewardOptions.add(ResourceChoices.OneFighterTwoRogues);
        buildingRewardOptions.add(ResourceChoices.OneFighterTwoWizards);
        buildingRewardOptions.add(ResourceChoices.OneRogueTwoWizards);
        
        ownerRewardOptions.add(ResourceChoices.OneCleric);
        ownerRewardOptions.add(ResourceChoices.OneFighter);
        ownerRewardOptions.add(ResourceChoices.OneRogue);
        ownerRewardOptions.add(ResourceChoices.OneWizard);
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_ANY_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_ANY_DATA + " " + ResourceConstants.MASK_ANY_DATA + " " + ResourceConstants.MASK_ANY_DATA;
    }

    @Override
    public String getBestCostMask() {
        return "Return " + ResourceConstants.MASK_ANY_DATA + " " + ResourceConstants.MASK_ANY_DATA;
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

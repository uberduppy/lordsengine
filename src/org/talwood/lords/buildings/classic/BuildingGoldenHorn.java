package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingStackedReward;
import org.talwood.lords.resources.Res;

public class BuildingGoldenHorn extends Building {

    public BuildingGoldenHorn() {
        super(Buildings.GoldenHorn, new BuildingStackedReward(Res.gold(4)));
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return "Take " + buildGenericRewardMask(retrieveExtraRewardForDisplay()) + " from this space";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "When purchased/start of round,<br>place " 
        + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
        + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
        + " on this space";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "Take all " + ResourceConstants.MASK_GOLD_DATA + " from this space";
    }
}

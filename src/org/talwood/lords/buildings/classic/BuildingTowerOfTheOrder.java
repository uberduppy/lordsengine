package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingStackedReward;
import org.talwood.lords.resources.Res;

public class BuildingTowerOfTheOrder extends Building {

    public BuildingTowerOfTheOrder() {
        super(Buildings.SpiresOfTheMorning, BuildingActions.DrawIntrigue, new BuildingStackedReward(Res.wizard(1)));
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "Draw 1 " + ResourceConstants.MASK_INTRIGUE_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return "Take " + buildGenericRewardMask(retrieveExtraRewardForDisplay()) + " from this space";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "When purchased/start of round,<br>place " 
        + ResourceConstants.MASK_WIZARD_DATA 
        + " on this space";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "Take all " + ResourceConstants.MASK_WIZARD_DATA + " from this space";
    }
}

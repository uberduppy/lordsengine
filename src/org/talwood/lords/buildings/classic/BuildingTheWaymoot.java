package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingStackedReward;
import org.talwood.lords.resources.Res;

public class BuildingTheWaymoot extends Building {
    public BuildingTheWaymoot() {
        super(Buildings.Waymoot, BuildingActions.OwnerReceivesTwoVictoryPoints, new BuildingStackedReward(Res.victoryPoints(3)));
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "2 Victory points";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "When purchased/start of round,<br>place 3 victory point tokens on this space";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "Take all victory points from this space";
    }
}

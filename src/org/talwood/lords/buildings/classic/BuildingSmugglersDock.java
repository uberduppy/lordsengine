package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingActivationCost;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingSmugglersDock extends Building {

    public BuildingSmugglersDock() {
        super(Buildings.SmugglersDock, BuildingActions.RequiresVariableReward, new BuildingActivationCost(Res.gold(2)), new BuildingReward(Res.anyResource(4)), new BuildingOwnerReward(Res.gold(2)));
        buildingRewardOptions.add(ResourceChoices.FourFighters);
        buildingRewardOptions.add(ResourceChoices.ThreeFightersOneRogue);
        buildingRewardOptions.add(ResourceChoices.TwoFightersTwoRogues);
        buildingRewardOptions.add(ResourceChoices.OneFighterThreeRogues);
        buildingRewardOptions.add(ResourceChoices.FourRogues);
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_FIGHTER_DATA + " / " + ResourceConstants.MASK_ROGUE_DATA + "&nbsp;&nbsp;" + ResourceConstants.MASK_FIGHTER_DATA + " / " + ResourceConstants.MASK_ROGUE_DATA
                + "<br>" 
                + ResourceConstants.MASK_FIGHTER_DATA + " / " + ResourceConstants.MASK_ROGUE_DATA + "&nbsp;&nbsp;" + ResourceConstants.MASK_FIGHTER_DATA + " / " + ResourceConstants.MASK_ROGUE_DATA;
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

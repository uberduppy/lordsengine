package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingActivationCost;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingHouseOfWonder extends Building {

    public BuildingHouseOfWonder() {
        super(Buildings.HouseOfWonder, BuildingActions.RequiresVariableReward, new BuildingReward(Res.anyResource(2)), new BuildingActivationCost(Res.gold(2)), new BuildingOwnerReward(Res.gold(2)));
        buildingRewardOptions.add(ResourceChoices.TwoClerics);
        buildingRewardOptions.add(ResourceChoices.TwoWizards);
        buildingRewardOptions.add(ResourceChoices.OneClericOneWizard);
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_GOLD_DATA + " " + ResourceConstants.MASK_GOLD_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_CLERIC_DATA + " / " + ResourceConstants.MASK_WIZARD_DATA
                + "<br>" 
                + ResourceConstants.MASK_CLERIC_DATA + " / " + ResourceConstants.MASK_WIZARD_DATA;
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

package org.talwood.lords.buildings.classic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;

public class BuildingPalaceOfWaterdeep extends Building {

    public BuildingPalaceOfWaterdeep() {
        super(Buildings.PalaceOfWaterdeep, 2, BuildingActions.OwnerReceivesTwoVictoryPoints, BuildingActions.TakeAmbassadorNextTurn);
    }

    @Override
    public void doSpecialProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "2 victory points";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "Take the Ambassador.<br>At the start of the next round,<br>you assign the Ambassador before any other player's turn.";
    }
}

package org.talwood.lords.buildings.undermountain;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.Res;

public class BuildingHallOfManyPillars extends Building {

    public BuildingHallOfManyPillars() {
        super(Buildings.HallOfManyPillars, BuildingActions.PlayThreeIntrigues, new BuildingOwnerReward(Res.intrigue(1)));
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "Draw 1 intrigue";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "Play " + ResourceConstants.MASK_INTRIGUE_DATA + ResourceConstants.MASK_INTRIGUE_DATA + ResourceConstants.MASK_INTRIGUE_DATA;
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

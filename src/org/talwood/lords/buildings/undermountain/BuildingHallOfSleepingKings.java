package org.talwood.lords.buildings.undermountain;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingHallOfSleepingKings extends Building {

    public BuildingHallOfSleepingKings() {
        super(Buildings.HallOfSleepingKings, BuildingActions.PlayIntrigue, new BuildingReward(Res.fighter(1), Res.rogue(1)));
        
        addBuildingAction(BuildingActions.RequiresVariableOwnerReward);
        
        ownerRewardOptions.add(ResourceChoices.OneRogue);
        ownerRewardOptions.add(ResourceChoices.OneFighter);
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_FIGHTER_DATA + " or " + ResourceConstants.MASK_ROGUE_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_ROGUE_DATA + " <br>Play " + ResourceConstants.MASK_INTRIGUE_DATA;
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

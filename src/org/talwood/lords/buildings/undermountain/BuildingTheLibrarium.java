package org.talwood.lords.buildings.undermountain;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReallocatedReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingTheLibrarium extends Building {

    public BuildingTheLibrarium() {
        super(Buildings.Librarium, BuildingActions.ReallocateResources, new BuildingReward(Res.wizard(2)), new BuildingOwnerReward(Res.wizard(1)), new BuildingReallocatedReward(1, Res.wizard(1)));
    }

    @Override
    public void doSpecialProcessing() {
    }
    
    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_WIZARD_DATA;
    }
    
    @Override
    public String getDescriptiveText() {
        return "";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "Place "  + ResourceConstants.MASK_WIZARD_DATA + " from the supply on 1 action space.";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }
    
}

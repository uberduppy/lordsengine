package org.talwood.lords.buildings.undermountain;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingActivationCost;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.Res;

public class BuildingBelkramsTomb extends Building {

    public BuildingBelkramsTomb() {
        super(Buildings.BelkramsTomb, BuildingActions.PlayIntrigue, new BuildingActivationCost(Res.gold(5)), new BuildingOwnerReward(Res.gold(2)));
    }

    @Override
    public void doSpecialProcessing() {
    }
    
    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA;
    }
    
    @Override
    public String getDescriptiveText() {
        return "Play an intrigue";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "Take 5 " + ResourceConstants.MASK_GOLD_DATA + ", play " + ResourceConstants.MASK_INTRIGUE_DATA;
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }
    
}

package org.talwood.lords.buildings.undermountain;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReallocatedReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingCitadelOfTheBloodyHand extends Building {

    public BuildingCitadelOfTheBloodyHand() {
        super(Buildings.CitadelOfTheBloodyHand, BuildingActions.ReallocateResources, new BuildingReward(Res.fighter(4)), new BuildingOwnerReward(Res.fighter(2)), new BuildingReallocatedReward(2, Res.fighter(1)));
    }

    @Override
    public void doSpecialProcessing() {
    }
    
    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA;
    }
    
    @Override
    public String getDescriptiveText() {
        return "";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "Place "  + ResourceConstants.MASK_FIGHTER_DATA + " from the supply on each<br>of 2 different action spaces.";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }
    
}

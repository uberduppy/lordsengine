package org.talwood.lords.buildings.undermountain;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReallocatedReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingHallOfThreeLords extends Building {

    public BuildingHallOfThreeLords() {
        super(Buildings.HallOfThreeLords, BuildingActions.RequiresVariableActivation, new BuildingReward(Res.victoryPoints(10)), new BuildingOwnerReward(Res.victoryPoints(2)), new BuildingReallocatedReward(3, Res.anyResource(1)));
        
        activationCostOptions.add(ResourceChoices.ThreeClerics);
        activationCostOptions.add(ResourceChoices.ThreeFighters);
        activationCostOptions.add(ResourceChoices.ThreeRogues);
        activationCostOptions.add(ResourceChoices.ThreeWizards);
        activationCostOptions.add(ResourceChoices.TwoClericsOneFighter);
        activationCostOptions.add(ResourceChoices.TwoClericsOneRogue);
        activationCostOptions.add(ResourceChoices.TwoClericsOneWizard);
        activationCostOptions.add(ResourceChoices.TwoFightersOneRogue);
        activationCostOptions.add(ResourceChoices.TwoFightersOneWizard);
        activationCostOptions.add(ResourceChoices.TwoRoguesOneWizard);
        activationCostOptions.add(ResourceChoices.OneClericOneFighterOneRogue);
        activationCostOptions.add(ResourceChoices.OneClericOneFighterOneWizard);
        activationCostOptions.add(ResourceChoices.OneClericOneRogueOneWizard);
        activationCostOptions.add(ResourceChoices.OneClericTwoFighters);
        activationCostOptions.add(ResourceChoices.OneClericTwoRogues);
        activationCostOptions.add(ResourceChoices.OneClericTwoWizards);
        activationCostOptions.add(ResourceChoices.OneFighterOneRogueOneWizard);
        activationCostOptions.add(ResourceChoices.OneFighterTwoRogues);
        activationCostOptions.add(ResourceChoices.OneFighterTwoWizards);
        activationCostOptions.add(ResourceChoices.OneRogueTwoWizards);
        
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "2 victory points";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "10 victory points";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "Remove " + ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_ANY_DATA + " from your tavern<br>and place 1 of those " + ResourceConstants.MASK_ANY_DATA + " on each<br>of 3 different action spaces.";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

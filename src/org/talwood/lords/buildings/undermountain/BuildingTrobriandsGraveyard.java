package org.talwood.lords.buildings.undermountain;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.Res;

public class BuildingTrobriandsGraveyard extends Building {

    public BuildingTrobriandsGraveyard() {
        super(Buildings.TroibriandsGraveyard, BuildingActions.RequiresTwoIntrigueDiscard, new BuildingOwnerReward(Res.intrigue(2), Res.gold(4)));
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_INTRIGUE_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA
                + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_INTRIGUE_DATA + ResourceConstants.MASK_INTRIGUE_DATA;
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "Discard " + ResourceConstants.MASK_INTRIGUE_DATA + ResourceConstants.MASK_INTRIGUE_DATA;
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

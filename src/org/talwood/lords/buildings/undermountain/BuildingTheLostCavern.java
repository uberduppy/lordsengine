package org.talwood.lords.buildings.undermountain;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingReward;

public class BuildingTheLostCavern extends Building {

    public BuildingTheLostCavern() {
        super(Buildings.LostCavern, 3, BuildingActions.RequiresVariableReward, BuildingActions.RequiresVariableOwnerReward, BuildingActions.RequiresQuestDiscard);
        buildingRewardOptions.add(ResourceChoices.OneClericOneFighterOneRogue);
        buildingRewardOptions.add(ResourceChoices.TwoFightersOneRogue);
        buildingRewardOptions.add(ResourceChoices.OneFighterTwoRogues);
        buildingRewardOptions.add(ResourceChoices.OneFighterOneRogueOneWizard);
        
        ownerRewardOptions.add(ResourceChoices.OneCleric);
        ownerRewardOptions.add(ResourceChoices.OneFighter);
        ownerRewardOptions.add(ResourceChoices.OneRogue);
        ownerRewardOptions.add(ResourceChoices.OneWizard);
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_ANY_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "Discard 1 non-Mandatory Quest:<br>" + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ANY_DATA;
    }
}

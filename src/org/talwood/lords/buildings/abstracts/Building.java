package org.talwood.lords.buildings.abstracts;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingActivationCost;
import org.talwood.lords.resources.BuildingExtraReward;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReallocatedReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.BuildingStackedReward;
import org.talwood.lords.resources.Resources;

public abstract class Building {

    protected Buildings buildingInfo;
    // BuildingInfo contains ID, explansion, cost, and name.

    private int storedVictoryPoints;
    
    // Data members that "may" be needed.
    // Each type of Resource used in a building MUST be of a different type.
    private BuildingReward buildingReward = new BuildingReward();
    private BuildingActivationCost activationCost = new BuildingActivationCost();
    private BuildingOwnerReward ownerReward = new BuildingOwnerReward();
    private List<BuildingActions> buildingActions = new ArrayList<BuildingActions>();
    
    // Certain buildings build up resources (like Caravan Court), so we need a container for those.
    private BuildingStackedReward totalStackedReward = new BuildingStackedReward();
    private BuildingStackedReward singleStackedReward = new BuildingStackedReward();
    private BuildingReallocatedReward reallocatedReward = new BuildingReallocatedReward();
    
    // Certain buildings allow you to put resources on other buildings (like Citadel of the Bloody Hand)
    private BuildingExtraReward extraReward = new BuildingExtraReward();
    
    protected List<ResourceChoices> activationCostOptions = new ArrayList<ResourceChoices>();
    protected List<ResourceChoices> buildingRewardOptions = new ArrayList<ResourceChoices>();
    protected List<ResourceChoices> ownerRewardOptions = new ArrayList<ResourceChoices>();
    
    private Long playerOwnerID;
    private Long playerOccupantID;
    
    public Building(Buildings buildingInfo,  int numberOfOptions, BuildingActions... actions) {
        this.buildingInfo = buildingInfo;
        if(numberOfOptions > 0 && numberOfOptions != actions.length) {
            throw new UnsupportedOperationException("Logic size mismatch");
        }
        for(BuildingActions ba : actions) {
            buildingActions.add(ba);
        }
    }
    
    public Building(Buildings buildingInfo, Resources... definedResources) {
        this(buildingInfo, null, definedResources);
    }
    
    public Building(Buildings buildingInfo, BuildingActions actions, Resources... definedResources) {
        this.buildingInfo = buildingInfo;
        if(actions != null) {
            this.buildingActions.add(actions);
        }
        for(Resources res : definedResources) {
            if(res instanceof BuildingReward) {
                buildingReward = new BuildingReward(res);
            } else if (res instanceof BuildingActivationCost) {
                activationCost = new BuildingActivationCost(res);
            } else if (res instanceof BuildingOwnerReward) {
                ownerReward = new BuildingOwnerReward(res);
            } else if (res instanceof BuildingStackedReward) {
                singleStackedReward = new BuildingStackedReward(res);
            } else if (res instanceof BuildingReallocatedReward) {
                reallocatedReward = new BuildingReallocatedReward((BuildingReallocatedReward)res);
            }
        }
    }
    
    // Singleton abstract method to force special work.
    public abstract void doSpecialProcessing();
    public abstract String getOwnerRewardDisplayMask();
    public abstract String getTotalActionRewardMask();
    public abstract String getBestCostMask();
    public abstract String getDescriptiveHeader();
    public abstract String getGeneralDescription();
    public abstract String getDescriptiveText();
    
    // Special actions that are called on either all or a single building
    public void processStartOfTurn() {
        // Always update any stacked resources...
        totalStackedReward.incrementFromResources(singleStackedReward);
    }

    public void processComingIntoPlay() {
        // Always update any stacked resources...
        totalStackedReward.incrementFromResources(singleStackedReward);
    }

    public Buildings getBuildingInfo() {
        return buildingInfo;
    }

    public BuildingReward getBuildingReward() {
        return buildingReward;
    }

    public BuildingActivationCost getActivationCost() {
        return activationCost;
    }

    public BuildingOwnerReward getOwnerReward() {
        return ownerReward;
    }

    public List<BuildingActions> getBuildingActions() {
        return buildingActions;
    }

    public BuildingStackedReward getTotalStackedReward() {
        return totalStackedReward;
    }

    public BuildingStackedReward getSingleStackedReward() {
        return singleStackedReward;
    }

    public BuildingExtraReward getExtraReward() {
        return extraReward;
    }
    
    public void addBuildingAction(BuildingActions action) {
        buildingActions.add(action);
    }

    public void addExtraReward(BuildingExtraReward ber) {
        extraReward.incrementFromResources(ber);
    }
    
    public void addOneStoredVictoryPoints() {
        storedVictoryPoints++;
    }

    public int getStoredVictoryPoints() {
        return storedVictoryPoints;
    }

    public void setStoredVictoryPoints(int storedVictoryPoints) {
        this.storedVictoryPoints = storedVictoryPoints;
    }

    public List<ResourceChoices> getActivationCostOptions() {
        return activationCostOptions;
    }

    public List<ResourceChoices> getBuildingRewardOptions() {
        return buildingRewardOptions;
    }

    public List<ResourceChoices> getOwnerRewardOptions() {
        return ownerRewardOptions;
    }

    public BuildingReallocatedReward getReallocatedReward() {
        return reallocatedReward;
    }

    
    public boolean isHasExtraRewards() {
        return extraReward.isNotEmpty() || totalStackedReward.isNotEmpty();
    }

    public boolean containsBuildingAction(BuildingActions action) {
        return buildingActions.contains(action);
    }
    
    public Resources retrieveFullRewardAndReset() {
        Resources res = new Resources(buildingReward);
        res.incrementFromResources(extraReward);
        res.incrementFromResources(totalStackedReward);
        extraReward.reset();
        totalStackedReward.reset();
        return res;
    }
    public Resources retrieveExtraRewardForDisplay() {
        Resources res = new Resources(extraReward);
        res.incrementFromResources(totalStackedReward);
        return res;
    }

    public String buildGenericRewardMask(Resources res) {
        StringBuilder sb = new StringBuilder();
        for(int x = 0; x < res.getCleric(); x++) {
            sb.append(ResourceConstants.MASK_CLERIC_DATA).append(" ");
        }
        for(int x = 0; x < res.getFighter(); x++) {
            sb.append(ResourceConstants.MASK_FIGHTER_DATA).append(" ");
        }
        for(int x = 0; x < res.getRogue(); x++) {
            sb.append(ResourceConstants.MASK_ROGUE_DATA).append(" ");
        }
        for(int x = 0; x < res.getWizard(); x++) {
            sb.append(ResourceConstants.MASK_WIZARD_DATA).append(" ");
        }
        for(int x = 0; x < res.getGold(); x++) {
            sb.append(ResourceConstants.MASK_GOLD_DATA).append(" ");
        }
        return sb.toString();
    }
    
    public boolean checkActivationCost(ResourceChoices choice) {
        return activationCostOptions.contains(choice);
    }
    
    public boolean checkBuildingOwnerReward(ResourceChoices choice) {
        return ownerRewardOptions.contains(choice);
    }
    
    public boolean checkBuildingReward(ResourceChoices choice) {
        return buildingRewardOptions.contains(choice);
    }

    public Long getPlayerOwnerID() {
        return playerOwnerID;
    }

    public void setPlayerOwnerID(Long playerOwnerID) {
        this.playerOwnerID = playerOwnerID;
    }

    public Long getPlayerOccupantID() {
        return playerOccupantID;
    }

    public void setPlayerOccupantID(Long playerOccupantID) {
        this.playerOccupantID = playerOccupantID;
    }

    public boolean isWaterdeepHarbor() {
        boolean result = false;
        switch (buildingInfo) {
            case WaterdeepHarborOne:
            case WaterdeepHarborTwo:
            case WaterdeepHarborThree:
                result = true;
                break;
            default:
                break;
        }
        return result;
    }
    
    public boolean hasBuildingAction(BuildingActions ba) {
        return buildingActions != null && buildingActions.contains(ba);
    }
}

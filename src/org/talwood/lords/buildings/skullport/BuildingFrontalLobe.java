package org.talwood.lords.buildings.skullport;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingActivationCost;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingFrontalLobe extends Building {

    public BuildingFrontalLobe() {
        super(Buildings.FrontalLobe, BuildingActions.RequiresVariableActivation, new BuildingActivationCost(Res.anyResource(0)), new BuildingReward(Res.wizard(3), Res.corruption(1)), new BuildingOwnerReward(Res.anyResource(1)));
        addBuildingAction(BuildingActions.RequiresVariableOwnerReward);
        
        activationCostOptions.add(ResourceChoices.OneCleric);
        activationCostOptions.add(ResourceChoices.OneFighter);
        activationCostOptions.add(ResourceChoices.OneRogue);
        activationCostOptions.add(ResourceChoices.OneWizard);
        
        ownerRewardOptions.add(ResourceChoices.OneCleric);
        ownerRewardOptions.add(ResourceChoices.OneFighter);
        ownerRewardOptions.add(ResourceChoices.OneRogue);
        ownerRewardOptions.add(ResourceChoices.OneWizard);
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "The returned " + ResourceConstants.MASK_ANY_DATA + " used to pay the activayion cost";
    }

    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_WIZARD_DATA + ResourceConstants.MASK_WIZARD_DATA + ResourceConstants.MASK_WIZARD_DATA + ResourceConstants.MASK_CORRUPTION_DATA; 
    }

    @Override
    public String getBestCostMask() {
        return "Return:" + ResourceConstants.MASK_ANY_DATA;
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

package org.talwood.lords.buildings.skullport;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingDeepfires extends Building {

    public BuildingDeepfires() {
        super(Buildings.Deepfires, BuildingActions.TakeQuest, new BuildingReward(Res.anyResource(1), Res.gold(5), Res.corruption(1)), new BuildingOwnerReward(Res.victoryPoints(3)));
        addBuildingAction(BuildingActions.RequiresVariableReward);
        buildingRewardOptions.add(ResourceChoices.OneCleric);
        buildingRewardOptions.add(ResourceChoices.OneFighter);
        buildingRewardOptions.add(ResourceChoices.OneRogue);
        buildingRewardOptions.add(ResourceChoices.OneWizard);
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "3 Victory points";
    }

    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_FIVE_GOLD_DATA + ResourceConstants.MASK_CORRUPTION_DATA;
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

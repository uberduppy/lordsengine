package org.talwood.lords.buildings.skullport;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.Res;

public class BuildingProminadeOfTheDarkMaiden extends Building {

    public BuildingProminadeOfTheDarkMaiden() {
        super(Buildings.PromenadeOfTheDarkMaiden, BuildingActions.RemoveUpToTwoCorruption, new BuildingOwnerReward(Res.victoryPoints(3)));
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "3 Victory Points";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "Take up to two Corruption (" + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + ") from your Tavern and remove the from the game.";
    }
}

package org.talwood.lords.buildings.skullport;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingHellHoundsMuzzle extends Building {

    public BuildingHellHoundsMuzzle() {
        super(Buildings.HellHoundsMuzzle, BuildingActions.RequiresVariableOwnerReward, new BuildingReward(Res.cleric(1), Res.fighter(1), Res.rogue(1), Res.wizard(1), Res.corruption(1)), new BuildingOwnerReward(Res.anyResource(1)));
        
        ownerRewardOptions.add(ResourceChoices.OneCleric);
        ownerRewardOptions.add(ResourceChoices.OneFighter);
        ownerRewardOptions.add(ResourceChoices.OneRogue);
        ownerRewardOptions.add(ResourceChoices.OneWizard);
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_ANY_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

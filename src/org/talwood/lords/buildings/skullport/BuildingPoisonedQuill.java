package org.talwood.lords.buildings.skullport;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingActivationCost;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.Res;

public class BuildingPoisonedQuill extends Building {

    public BuildingPoisonedQuill() {
        super(Buildings.PoisonedQuill, BuildingActions.PlayIntrigue, new BuildingActivationCost(Res.corruption(1)), new BuildingOwnerReward(Res.intrigue(1)));
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_INTRIGUE_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "Return: " + ResourceConstants.MASK_CORRUPTION_DATA + "<br>Play " + ResourceConstants.MASK_INTRIGUE_DATA;
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

package org.talwood.lords.buildings.skullport;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingCryptkeyFacilitations extends Building {

    public BuildingCryptkeyFacilitations() {
        super(Buildings.CryptkeyFacilitations, new BuildingReward(Res.rogue(3), Res.gold(5), Res.corruption(1)), new BuildingOwnerReward(Res.gold(3)));
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
//        return ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA;
        return "";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

package org.talwood.lords.buildings.skullport;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingActivationCost;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingSecretShrine extends Building {

    public BuildingSecretShrine() {
        super(Buildings.SecretShrine, new BuildingActivationCost(Res.corruption(1)), new BuildingReward(Res.cleric(0)), new BuildingOwnerReward(Res.cleric(1)));
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_CLERIC_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "Return 1 " + ResourceConstants.MASK_CORRUPTION_DATA + ": Take " + ResourceConstants.MASK_CLERIC_DATA;
    }
}

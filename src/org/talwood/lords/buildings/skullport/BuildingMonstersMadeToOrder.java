package org.talwood.lords.buildings.skullport;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingStackedReward;
import org.talwood.lords.resources.Res;

public class BuildingMonstersMadeToOrder extends Building {

    public BuildingMonstersMadeToOrder() {
        super(Buildings.MonstersMadeToOrder, BuildingActions.TakeCorruptionReturnAgents, new BuildingStackedReward(Res.corruption(1)), new BuildingOwnerReward(Res.victoryPoints(2)));
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "2 victory points";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "Take " + buildGenericRewardMask(retrieveExtraRewardForDisplay()) + " from this space";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "When purchased/start of round,<br>place " 
        + ResourceConstants.MASK_CORRUPTION_DATA
        + " on this space";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "Take all " + ResourceConstants.MASK_CORRUPTION_DATA + " from this space, return the same number of agents to your pool";
    }
}

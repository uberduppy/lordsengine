package org.talwood.lords.buildings.skullport;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingActivationCost;
import org.talwood.lords.resources.BuildingOwnerReward;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingThimblewinesPawnshop extends Building {

    public BuildingThimblewinesPawnshop() {
        super(Buildings.ThimblewinesPawnshop, new BuildingActivationCost(Res.corruption(1)), new BuildingReward(Res.gold(1)), new BuildingOwnerReward(Res.gold(2)));
    }

    @Override
    public void doSpecialProcessing() {
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA;
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "Return " + ResourceConstants.MASK_CORRUPTION_DATA + ": " + ResourceConstants.MASK_GOLD_DATA;
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
}

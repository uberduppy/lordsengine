package org.talwood.lords.buildings.basic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingHallOfTheVoice extends Building {

    public BuildingHallOfTheVoice() {
        super(Buildings.HallOfTheVoice, BuildingActions.TakeQuest, new BuildingReward(Res.gold(5), Res.corruption(1)));
        addBuildingAction(BuildingActions.DrawIntrigue);
    }

    @Override
    public void doSpecialProcessing() {
    }
    
    @Override
    public String getDescriptiveText() {
        return "Take 1 Quest, 1 Intrigue, 5 Gold and 1 Corruption counter";
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }
    
}

package org.talwood.lords.buildings.basic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;

public class BuildingWaterdeepHarborTwo extends Building {
    public BuildingWaterdeepHarborTwo() {
        super(Buildings.WaterdeepHarborTwo, BuildingActions.PlayIntrigue);
    }

    @Override
    public void doSpecialProcessing() {
        // None needed for basic buildings
    }
    @Override
    public String getDescriptiveText() {
        return "Play intrigue card";
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }
}

package org.talwood.lords.buildings.basic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;

public class BuildingBuildersHall extends Building {
    public BuildingBuildersHall() {
        super(Buildings.BuildersHall, 1, BuildingActions.BuyBuilding);
    }

    @Override
    public void doSpecialProcessing() {
        // None needed for basic buildings
    }

    @Override
    public String getDescriptiveText() {
        return "Buy a building";
    }
    
    @Override
    public String getOwnerRewardDisplayMask() {
        return "";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

}

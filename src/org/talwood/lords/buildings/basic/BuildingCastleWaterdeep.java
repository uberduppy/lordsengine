package org.talwood.lords.buildings.basic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;

public class BuildingCastleWaterdeep extends Building {
    public BuildingCastleWaterdeep() {
        super(Buildings.CastleWaterdeep, 2, BuildingActions.DrawIntrigue, BuildingActions.FirstTurnMarker);
    }

    @Override
    public void doSpecialProcessing() {
        // None needed for basic buildings
    }

    @Override
    public String getDescriptiveText() {
        return "Take first turn marker, draw " + ResourceConstants.MASK_INTRIGUE_DATA;
    }
    
    @Override
    public String getOwnerRewardDisplayMask() {
        return "";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

}

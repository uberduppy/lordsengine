package org.talwood.lords.buildings.basic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingSkullIsland extends Building {

    public BuildingSkullIsland() {
        super(Buildings.SkullIsland, BuildingActions.RequiresVariableReward, new BuildingReward(Res.anyResource(2)));

        buildingRewardOptions.add(ResourceChoices.OneClericOneFighterOneCorruption);
        buildingRewardOptions.add(ResourceChoices.OneClericOneRogueOneCorruption);
        buildingRewardOptions.add(ResourceChoices.OneClericOneWizardOneCorruption);
        buildingRewardOptions.add(ResourceChoices.OneFighterOneRogueOneCorruption);
        buildingRewardOptions.add(ResourceChoices.OneFighterOneWizardOneCorruption);
        buildingRewardOptions.add(ResourceChoices.OneRogueOneWizardOneCorruption);
        buildingRewardOptions.add(ResourceChoices.TwoClericsOneCorruption);
        buildingRewardOptions.add(ResourceChoices.TwoFightersOneCorruption);
        buildingRewardOptions.add(ResourceChoices.TwoRoguesOneCorruption);
        buildingRewardOptions.add(ResourceChoices.TwoWizardsOneCorruption);
    }

    @Override
    public void doSpecialProcessing() {
        // Do nothing
    }
    
    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_CORRUPTION_DATA;
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
    
    
}

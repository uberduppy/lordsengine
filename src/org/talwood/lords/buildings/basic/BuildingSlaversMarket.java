package org.talwood.lords.buildings.basic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingSlaversMarket extends Building {

    public BuildingSlaversMarket() {
        super(Buildings.SlaversMarket, new BuildingReward(Res.fighter(2), Res.rogue(2), Res.corruption(1)));
    }

    @Override
    public void doSpecialProcessing() {
        // Do nothing
    }
    
    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_CORRUPTION_DATA;
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
    
}

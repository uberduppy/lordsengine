package org.talwood.lords.buildings.basic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;

public class BuildingTheGrimStatue extends Building {

    public BuildingTheGrimStatue() {
        super(Buildings.TheGrimStatue, BuildingActions.DrawTwoIntrigue);
    }

    @Override
    public void doSpecialProcessing() {
    }
    
    @Override
    public String getDescriptiveText() {
        return "Draw 2 Intrigue";
    }
    @Override
    public String getOwnerRewardDisplayMask() {
        return "";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }
}

package org.talwood.lords.buildings.basic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.resources.BuildingReward;
import org.talwood.lords.resources.Res;

public class BuildingCliffwatchInnTwoGold extends Building {
    public BuildingCliffwatchInnTwoGold() {
        super(Buildings.CliffwatchInnTwoGold, BuildingActions.TakeQuest, new BuildingReward(Res.gold(2)));
    }

    @Override
    public void doSpecialProcessing() {
        // None needed for basic buildings
    }

    @Override
    public String getDescriptiveText() {
        return "Take quest from Cliffwatch Inn, take " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA;
    }
    @Override
    public String getOwnerRewardDisplayMask() {
        return "";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

}

package org.talwood.lords.buildings.basic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;

public class BuildingEntryWell extends Building {

    public BuildingEntryWell() {
        super(Buildings.EntryWell, 2, BuildingActions.TakeQuest, BuildingActions.PlayIntrigue);
    }

    @Override
    public void doSpecialProcessing() {
        // None needed for basic buildings
    }
    
    @Override
    public String getDescriptiveText() {
        return "Take a Quest, play an Intrigue";
    }
    
    @Override
    public String getOwnerRewardDisplayMask() {
        return "";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

}

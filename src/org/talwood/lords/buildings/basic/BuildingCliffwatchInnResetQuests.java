package org.talwood.lords.buildings.basic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;

public class BuildingCliffwatchInnResetQuests extends Building {
    public BuildingCliffwatchInnResetQuests() {
        super(Buildings.CliffwatchInnResetQuests, 2, BuildingActions.TakeQuest, BuildingActions.ResetQuests);
    }

    @Override
    public void doSpecialProcessing() {
        // None needed for basic buildings
    }

    @Override
    public String getDescriptiveText() {
        return "Reset quests the draw 1 Quest";
    }
    
    @Override
    public String getOwnerRewardDisplayMask() {
        return "";
    }

    @Override
    public String getTotalActionRewardMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

}

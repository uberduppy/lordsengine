package org.talwood.lords.buildings.basic;

import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.resources.BuildingReward;

public class BuildingHallOfMirrors extends Building {

    public BuildingHallOfMirrors() {
        super(Buildings.HallOfMirrors, BuildingActions.RequiresVariableReward, new BuildingReward());
        
        buildingRewardOptions.add(ResourceChoices.OneRogue);
        buildingRewardOptions.add(ResourceChoices.OneCleric);
        buildingRewardOptions.add(ResourceChoices.OneFighter);
        buildingRewardOptions.add(ResourceChoices.OneWizard);
        buildingRewardOptions.add(ResourceChoices.OneFighterOneRogue);
    }

    @Override
    public void doSpecialProcessing() {
        // None here
    }
    
    @Override
    public String getTotalActionRewardMask() {
        return ResourceConstants.MASK_ANY_DATA + " or " + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_ROGUE_DATA;
    }

    @Override
    public String getOwnerRewardDisplayMask() {
        return "";
    }

    @Override
    public String getBestCostMask() {
        return "";
    }

    @Override
    public String getDescriptiveHeader() {
        return "";
    }

    @Override
    public String getGeneralDescription() {
        return "";
    }

    @Override
    public String getDescriptiveText() {
        return "";
    }
    
}

package org.talwood.lords.comparators;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.intrigues.Intrigue;

public class IntrigueStackComparator implements Comparator<Intrigue> {
    
    private Map<Intrigues, Long> orderedIntrigues = new HashMap<Intrigues, Long>();
    
    private IntrigueStackComparator() {}
    
    public IntrigueStackComparator(List<Intrigues> intrigues) {
        for(Intrigues i : intrigues) {
            orderedIntrigues.put(i, new Long(orderedIntrigues.size() + 1));
        }
    }

    public IntrigueStackComparator(Intrigues... intrigues) {
        for(Intrigues i : intrigues) {
            orderedIntrigues.put(i, new Long(orderedIntrigues.size() + 1));
        }
    }
    
    public void addQuestsToStack(Intrigues... intrigues) {
        for(Intrigues i : intrigues) {
            orderedIntrigues.put(i, new Long(orderedIntrigues.size() + 1));
        }
    }

    @Override
    public int compare(Intrigue lhs, Intrigue rhs) {
        int result = 0;
        Long lhsIndex = (lhs == null) ? null : orderedIntrigues.get(lhs.getIntrigueInfo());
        Long rhsIndex = (rhs == null) ? null : orderedIntrigues.get(rhs.getIntrigueInfo());
        if(lhsIndex == null) {
            // lhs not found
            if(rhsIndex != null) {
                // Found only rhs move it up
                result = 1;
            }
        } else {
            if(rhsIndex == null) {
                // Only found lhs, it's good
                result = -1;
            } else {
                // Both exist, they won't be the same, so whichever index is lower is the better card
                result = lhsIndex.intValue() - rhsIndex.intValue();
            }
        }
        return result;
    }

}

package org.talwood.lords.comparators;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.quests.Quest;

public class QuestStackComparator implements Comparator<Quest> {
    
    private Map<Quests, Long> orderedQuests = new HashMap<Quests, Long>();
    
    private QuestStackComparator() {}
    
    public QuestStackComparator(List<Quests> quests) {
        for(Quests q : quests) {
            orderedQuests.put(q, new Long(orderedQuests.size() + 1));
        }
    }

    public QuestStackComparator(Quests... quests) {
        for(Quests q : quests) {
            orderedQuests.put(q, new Long(orderedQuests.size() + 1));
        }
    }
    
    public void addQuestsToStack(Quests... quests) {
        for(Quests q : quests) {
            orderedQuests.put(q, new Long(orderedQuests.size() + 1));
        }
    }

    @Override
    public int compare(Quest lhs, Quest rhs) {
        int result = 0;
        Long lhsIndex = (lhs == null) ? null : orderedQuests.get(lhs.getQuestInfo());
        Long rhsIndex = (rhs == null) ? null : orderedQuests.get(rhs.getQuestInfo());
        if(lhsIndex == null) {
            // lhs not found
            if(rhsIndex != null) {
                // Found only rhs move it up
                result = 1;
            }
        } else {
            if(rhsIndex == null) {
                // Only found lhs, it's good
                result = -1;
            } else {
                // Both exist, they won't be the same, so whichever index is lower is the better card
                result = lhsIndex.intValue() - rhsIndex.intValue();
            }
        }
        return result;
    }

}

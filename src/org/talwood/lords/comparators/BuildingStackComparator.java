package org.talwood.lords.comparators;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.quests.Quest;

public class BuildingStackComparator implements Comparator<Building> {
    
    private Map<Buildings, Long> orderedBuildings = new HashMap<Buildings, Long>();
    
    private BuildingStackComparator() {}
    
    public BuildingStackComparator(List<Buildings> buildings) {
        for(Buildings b : buildings) {
            orderedBuildings.put(b, new Long(orderedBuildings.size() + 1));
        }
    }

    public BuildingStackComparator(Buildings... buildings) {
        for(Buildings b : buildings) {
            orderedBuildings.put(b, new Long(orderedBuildings.size() + 1));
        }
    }
    
    public void addQuestsToStack(Buildings... buildings) {
        for(Buildings b : buildings) {
            orderedBuildings.put(b, new Long(orderedBuildings.size() + 1));
        }
    }

    @Override
    public int compare(Building lhs, Building rhs) {
        int result = 0;
        Long lhsIndex = (lhs == null) ? null : orderedBuildings.get(lhs.getBuildingInfo());
        Long rhsIndex = (rhs == null) ? null : orderedBuildings.get(rhs.getBuildingInfo());
        if(lhsIndex == null) {
            // lhs not found
            if(rhsIndex != null) {
                // Found only rhs move it up
                result = 1;
            }
        } else {
            if(rhsIndex == null) {
                // Only found lhs, it's good
                result = -1;
            } else {
                // Both exist, they won't be the same, so whichever index is lower is the better card
                result = lhsIndex.intValue() - rhsIndex.intValue();
            }
        }
        return result;
    }

}

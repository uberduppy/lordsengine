package org.talwood.lords.constants;

public class GameBoardSpecs {

    public static final int QUESTS_IN_CLIFFWATCH_INN = 4;
    public static final int BUILDINGS_IN_BUILDERS_HALL = 3;
    public static final int NUMBER_OF_TURNS = 8;
    
    public static int[] NUMBER_OF_AGENTS = {-1, 4, 4, 3, 2, 2, -1};
    public static int[] NUMBER_OF_AGENTS_LONG = {-1, 5, 5, 4, 3, 3, 2};
    
}

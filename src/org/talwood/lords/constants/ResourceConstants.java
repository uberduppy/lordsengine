package org.talwood.lords.constants;

public class ResourceConstants {

    public static final String MASK_FIGHTER_DATA = "[[fighter]]";
    public static final String MASK_FIGHTER_IMAGE = "fighter.jpg";
    
    public static final String MASK_ROGUE_DATA = "[[rogue]]";
    public static final String MASK_ROGUE_IMAGE = "rogue.jpg";
    
    public static final String MASK_CLERIC_DATA = "[[cleric]]";
    public static final String MASK_CLERIC_IMAGE = "cleric.jpg";
    
    public static final String MASK_WIZARD_DATA = "[[wizard]]";
    public static final String MASK_WIZARD_IMAGE = "wizard.jpg";
    
    public static final String MASK_ANY_DATA = "[[any]]";
    public static final String MASK_ANY_IMAGE = "any.jpg";
    
    public static final String MASK_GOLD_DATA = "[[gold]]";
    public static final String MASK_GOLD_IMAGE = "goldone.jpg";
    
    public static final String MASK_FIVE_GOLD_DATA = "[[fivegold]]";
    public static final String MASK_FIVE_GOLD_IMAGE = "goldfive.jpg";
    
    public static final String MASK_CORRUPTION_DATA = "[[corruption]]";
    public static final String MASK_CORRUPTION_IMAGE = "corrupt.jpg";
    
    public static final String MASK_QUEST_BIG_DATA = "[[bigquest]]";
    public static final String MASK_QUEST_BIG_IMAGE = "bigquest.jpg";
    
    public static final String MASK_INTRIGUE_DATA = "[[imtrigue]]";
    public static final String MASK_INTRIGUE_IMAGE = "intrigue.jpg";
    
    public static final String[][] MASKS = {
        {MASK_FIGHTER_DATA, MASK_FIGHTER_IMAGE},
        {MASK_ROGUE_DATA, MASK_ROGUE_IMAGE},
        {MASK_CLERIC_DATA, MASK_CLERIC_IMAGE},
        {MASK_WIZARD_DATA, MASK_WIZARD_IMAGE},
        {MASK_GOLD_DATA, MASK_GOLD_IMAGE},
        {MASK_FIVE_GOLD_DATA, MASK_FIVE_GOLD_IMAGE},
        {MASK_CORRUPTION_DATA, MASK_CORRUPTION_IMAGE},
        {MASK_ANY_DATA, MASK_ANY_IMAGE},
        {MASK_QUEST_BIG_DATA, MASK_QUEST_BIG_IMAGE},
        {MASK_INTRIGUE_DATA, MASK_INTRIGUE_IMAGE},
    };
    
}

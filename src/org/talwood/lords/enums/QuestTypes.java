package org.talwood.lords.enums;

public enum QuestTypes {
    Basic("Basic"),
    Options("Options"),
    Mandatory("Mandatory"),
    Plot("Plot");
    
    private String description;
    
    private QuestTypes(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    
    
}

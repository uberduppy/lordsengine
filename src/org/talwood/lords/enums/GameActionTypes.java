package org.talwood.lords.enums;

public enum GameActionTypes {
    QuestTaken,
    QuestRedeemed,
    IntriguePlayed,
    IntrigueResource,
    VariableActivationCost,
    VariableBuildingReward,
    VariableOwnerReward,
    VariableIntrigueReward,
    BuildingInBuildersHall
}

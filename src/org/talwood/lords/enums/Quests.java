package org.talwood.lords.enums;

import org.talwood.lords.constants.ResourceConstants;

public enum Quests {
    ExposeRedWizardsSpies(1, "Expose Red Wizards' Spies", Expansions.Classic, Houses.Arcana, QuestTypes.Basic, 20, 1),
    HostFestivalForSune(2, "Host Festival for Sune", Expansions.Classic, Houses.Arcana, QuestTypes.Basic, 9),
    InfiltrateHalastersCircle(3, "Infiltrate Halaster's Circle", Expansions.Classic, Houses.Arcana, QuestTypes.Basic, 25),
    InvestigateAberrantInfestation(4, "Investigate Aberrant Infestation", Expansions.Classic, Houses.Arcana, QuestTypes.Basic, 13, 1),
    RecruitForBlackstaffAcademy(5, "Recruit for Blackstaff Academy", Expansions.Classic, Houses.Arcana, QuestTypes.Basic, 6),
    ResearchChronomacy(6, "Research Chronomany", Expansions.Classic, Houses.Arcana, QuestActions.ReturnAgent, 4),
    RetrieveAncientArtifacts(7, "Retrieve Ancient Artifacts", Expansions.Classic, Houses.Arcana, QuestTypes.Basic, 11),
    StealSpellbookFromSilverhand(8, "Steal Spellbook from Silverhand", Expansions.Classic, Houses.Arcana, QuestTypes.Basic, 7, 2),
    DomesticateOwlbears(9, "Domesticate Owlbears", Expansions.Classic, Houses.Arcana, QuestTypes.Basic, 8),
    
    // Commerce
    AllyWithHouseThann(10, "Ally with House Thann", Expansions.Classic, Houses.Commerce, QuestTypes.Basic, 25),
    ImpersonateAdarBrentNoble(11, "Impersonate Adarbrent Noble", Expansions.Classic, Houses.Commerce, QuestTypes.Basic, 18, 2),
    LootTheCryptOfChauntea(12, "Loot the Crypt of Chauntea", Expansions.Classic, Houses.Commerce, QuestTypes.Basic, QuestActions.DrawQuest, 7, 1),
    LureArtisansOfMirabar(13, "Lure Artisans of Mirabar", Expansions.Classic, Houses.Commerce, QuestActions.BuildingFromHall, 4),
    PlacateTheWalkingStatue(14, "Placate the Walking Status", Expansions.Classic, Houses.Commerce, QuestActions.BuildingFromDeck, 10),
    SafeguardEltorchulMage(15, "Safeguard Eltorchal Mage", Expansions.Classic, Houses.Commerce, 4),
    SendAidToTheHarpers(16, "Send Aid To The Harpers", Expansions.Classic, Houses.Commerce, QuestActions.OpponentGetsFourGold, 15),
    SpyOnTheHouseOfLight(17, "Spy on the House of Light", Expansions.Classic, Houses.Commerce, 6),
    ThinTheCityWatch(18, "Thin the City Watch", Expansions.Classic, Houses.Commerce, 9),
    
    // Piety
    ConvertANobleToLathander(19, "Convert a Noble to Lathandar", Expansions.Classic, Houses.Piety, QuestActions.QuestFromCliffwatch, 8),
    CreateShrineToOghma(20, "Create a Shrine to Oghma", Expansions.Classic, Houses.Piety, QuestTypes.Basic, 25),
    DiscoverHiddenTempleOfLolth(21, "Discover Hidden Temple of Lolth", Expansions.Classic, Houses.Piety, QuestActions.QuestFromCliffwatch, 10),
    EliminateVampireCoven(22, "Eliminate Vampire Coven", Expansions.Classic, Houses.Piety, QuestTypes.Basic, 11),
    FormAllianceWithTheRashemi(23, "Form an Alliance with the Rashemi", Expansions.Classic, Houses.Piety, QuestActions.QuestFromCliffwatch, 10),
    HealFallenGrayHandSoldiers(24, "Heal Fallen Gray Hand Soldiers", Expansions.Classic, Houses.Piety, QuestTypes.Basic, 6),
    PerformThePenanceOfDuty(25, "Perform the Penance of Duty", Expansions.Classic, Houses.Piety, QuestTypes.Basic, 12),
    RecruitPaladinsForTyr(26, "Recruit Paladins for Tyr", Expansions.Classic, Houses.Piety, QuestTypes.Basic, 10),
    SealGateToCyricsRealm(27, "Seal Gate to Cyric's Realm", Expansions.Classic, Houses.Piety, QuestTypes.Basic, 20),
    
    // Skullduggery
    BuildReputationInSkullport(28, "Build a Reputation in Skullport", Expansions.Classic, Houses.Skullduggery, QuestTypes.Basic, 10, 1),
    EstablishHarpersSafeHouse(29, "Establish Harpers Safe House", Expansions.Classic, Houses.Skullduggery, QuestActions.ExtraPointsForBuildings, 8),
    EstablishShadowThievesGuild(30, "Establish Shadow Thieves' Guild", Expansions.Classic, Houses.Skullduggery, QuestTypes.Basic, 25),
    ExposeCultCorruption(31, "Expose Cult Corruption", Expansions.Classic, Houses.Skullduggery, QuestTypes.Basic, 4),
    PrisonBreak(32, "Prison Break", Expansions.Classic, Houses.Skullduggery, QuestActions.PlayIntrique, 14),
    ProcureStolenGoods(33, "Procure Stolen Goods", Expansions.Classic, Houses.Skullduggery, 8, 2),
    RaidOnUndermountain(34, "Raid on Undermountain", Expansions.Classic, Houses.Skullduggery, 20),
    StealFromHouseAdarbrent(35, "Steal from House Adarbrent", Expansions.Classic, Houses.Skullduggery, 10),
    TakeOverRivalOrganization(36, "Take Over Rival Organization", Expansions.Classic, Houses.Skullduggery, 10),
    
    // Warfare
    AmbushArtorMorlin(37, "Ambush Artor Morlin", Expansions.Classic, Houses.Warfare, 8),
    BolsterCityGuard(38, "Bolster City Guard", Expansions.Classic, Houses.Warfare, 25),
    ConfrontTheXanathar(39, "Confront the Xanathar", Expansions.Classic, Houses.Warfare, 20),
    DefeatUprisingFromUndermountain(40, "Defeat Uprising from Undermountain", Expansions.Classic, Houses.Warfare, 11),
    DeliverAnUltimatum(41, "Deliver an Ultimatum", Expansions.Classic, Houses.Warfare, 11),
    DeliverWeaponsToSelunesTemple(42, "Deliver Weapons to Selune's Temple", Expansions.Classic, Houses.Warfare, 9),
    RaidOrcStronghold(43, "Raid Orc Stronghold", Expansions.Classic, Houses.Warfare, 8),
    RepelSeawraiths(44, "Repel Seawraiths", Expansions.Classic, Houses.Warfare, 15),
    TrainBladesingers(45, "Train Bladesingers", Expansions.Classic, Houses.Warfare, 4),
    
    // PLot quests
    ExploreAhghaironsTower(46, "Explore Ahghairon's Tower", Expansions.Classic, Houses.Arcana, QuestTypes.Plot, QuestActions.WizardDrawsIntrigue, 6),
    RecoverTheMagistersOrb(47, "Recover the Magister's Orb", Expansions.Classic, Houses.Arcana, QuestTypes.Plot, QuestActions.PlaceAgentOnOpponent, 6),
    StudyTheIlluskArch(48, "Study the Illusk Arch", Expansions.Classic, Houses.Arcana, QuestTypes.Plot, QuestActions.TwoVictoryForHouseMatch, 8),
    BribeTheShipwrights(49, "Bribe the Shipwrights", Expansions.Classic, Houses.Commerce, QuestTypes.Plot, QuestActions.GoldGivesRogues, 10),
    EstablishNewMerchantGuild(50, "Establish New Merchant Guild", Expansions.Classic, Houses.Commerce, QuestTypes.Plot, QuestActions.TwoVictoryForHouseMatch, 8),
    InfiltrateBuildersHall(51, "Infiltrate Builders Hall", Expansions.Classic, Houses.Commerce, QuestTypes.Plot, QuestActions.FourVictoryForHousePurchase, 6),
    DefendTowerOfLuck(52, "Defend the Tower of Luck", Expansions.Classic, Houses.Piety, QuestTypes.Plot, QuestActions.TakeAnyResourceStartOfTurn, 0),
    ProduceMiracleForTheMasses(53, "Produce a Miracle for the Masses", Expansions.Classic, Houses.Piety, QuestTypes.Plot, QuestActions.ExchangeResourceOnClericTaken, 5),
    ProtectTheHouseOfWonder(54, "Protect the House of Wonder", Expansions.Classic, Houses.Piety, QuestTypes.Plot, QuestActions.TwoVictoryForHouseMatch, 8),
    FenceGoodsForDukeOfDarkness(55, "Fence Goods for Duke of Darkness", Expansions.Classic, Houses.Skullduggery, QuestTypes.Plot, QuestActions.RogueGivesGold, 6),
    InstallSpyInCastleWaterdeep(56, "Install a Spy in Castle Waterdeep", Expansions.Classic, Houses.Skullduggery, QuestTypes.Plot, QuestActions.TwoVictoryForHouseMatch, 8),
    PlaceSleeperAgentInSkullport(57, "Place a Sleeper Agent in Skullport", Expansions.Classic, Houses.Skullduggery, QuestTypes.Plot, QuestActions.TwoVictoryForIntriguePlayed, 0),
    BolsterGriffinCavalry(58, "Bolster Griffin Calvary", Expansions.Classic, Houses.Warfare, QuestTypes.Plot, QuestActions.FighterGivesFighter, 6),
    QuellMercenaryUprising(59, "Quell Mercenary Uprising", Expansions.Classic, Houses.Warfare, QuestTypes.Plot, QuestActions.TwoVictoryForHouseMatch, 8),
    RecruitLieutenant(60, "Recruit Lieutenant", Expansions.Classic, Houses.Warfare, QuestTypes.Plot, QuestActions.ExtraAgent, 0),
    
    // Undermountain
    DealWithTheBlackViper(101, "Deal with the Black Viper", Expansions.Undermountian, Houses.Arcana, QuestTypes.Options, QuestActions.CanPlayIntriguesAsDrawn, 10),
    ExploreTrobriandsGraveyard(102, "Explore Trobriand's Graveyard", Expansions.Undermountian, Houses.Arcana, 40),
    ResurrectDeadWizards(103, "Resurrect Dead Wizards", Expansions.Undermountian, Houses.Arcana, 6),
    SurviveMeetingWithHalaster(104, "Survive a Meeting with Halaster", Expansions.Undermountian, Houses.Arcana, QuestTypes.Options, QuestActions.DrawIntrigueForEachWizardTaken, 15),
    RansackWhitehelmsTomb(105, "Ransack Whitehelm's Tomb", Expansions.Undermountian, Houses.Commerce, 40),
    RecruitForCityWatch(106, "Recruit for City Watch", Expansions.Undermountian, Houses.Commerce, QuestTypes.Options, 8),
    StealGemsFromTheBoneThrone(107, "Steal Gems from the Bone Throne", Expansions.Undermountian, Houses.Commerce, 7),
    ThreatenTheBuildersGuilds(108, "Threaten the Builders' Guilds", Expansions.Undermountian, Houses.Commerce, QuestTypes.Options, QuestActions.TakeAllBuildingsInBuildersHall, 13),
    EstablishTempleToIbrandul(109, "Establish Temple to Ibrandul", Expansions.Undermountian, Houses.Piety, QuestTypes.Options, QuestActions.TakeAllQuestsInCliffwatchInn, 11),
    PlunderTheIslandTemple(110, "Plunder the Island Temple", Expansions.Undermountian, Houses.Piety, 40),
    RescueClericsOfTymora(111, "Rescue Clerics of Tymora", Expansions.Undermountian, Houses.Piety, 10),
    RootOutLoviatarsFaithful(112, "Root Out Loviatar's Faithful", Expansions.Undermountian, Houses.Piety, QuestTypes.Options, QuestActions.TwoVictoryPointsForEachClericUsed, 15),
    BreakIntoBlackstaffTower(113, "Break Into Blackstaff Tower", Expansions.Undermountian, Houses.Skullduggery, 40),
    DefendTheLanceboardRoom(114, "Defend the Lanceboard Room", Expansions.Undermountian, Houses.Skullduggery, 12),
    SurviveArcturiasTransformarion(115, "Survive Arcturia's Transformation", Expansions.Undermountian, Houses.Skullduggery, 6),
    UnleashCrimeSpree(116, "Unleash Crime Spree", Expansions.Undermountian, Houses.Skullduggery, QuestTypes.Options, 12),
    BattleInMuiralsGauntlet(117, "Battle in Muiral's Gauntlet", Expansions.Undermountian, Houses.Warfare, 40),
    DefendTheYawningPortal(118, "Defend the Yawning Portal", Expansions.Undermountian, Houses.Warfare, QuestTypes.Options, QuestActions.WildCardQuestRewardAndReturnUpToThreeAgents, 15),
    DestroyTempleOfSelvetarm(119, "Destroy the Temple of Selvetarm", Expansions.Undermountian, Houses.Warfare, QuestTypes.Options, 10),
    WakeTheSixSleepers(120, "Wake the Six Sleeprs", Expansions.Undermountian, Houses.Warfare, 8),
    // Undermountain plot quests
    EstablishWizardAcademy(121, "Establish Wizard Academy", Expansions.Undermountian, Houses.Arcana, QuestTypes.Plot, QuestActions.ArcanaQuestTakenGivesWizard, 12),
    StudyInTheLibrarium(122, "Study in the Librarium", Expansions.Undermountian, Houses.Arcana, QuestTypes.Plot, QuestActions.DrawIntrigueWhenPlayIntrigue, 11),
    ObtainBuildersPlans(123, "Obtain Builders' Plans", Expansions.Undermountian, Houses.Commerce, QuestTypes.Plot, QuestActions.AssignAgentToBuildersHall, 13),
    SponsorBountyHunters(124, "Sponsor Bounty Hunters", Expansions.Undermountian, Houses.Commerce, QuestTypes.Plot, QuestActions.CommerceQuestTakenGivesFourGold, 12),
    DiplomaticMissionToSuzail(125, "Diplomatic Mission to Suzail", Expansions.Undermountian, Houses.Piety, QuestTypes.Plot, QuestActions.CompleteQuestsFromCliffwatchInn, 10),
    SanctifyDesecratedTemple(126, "Sanctify a Desecrated Temple", Expansions.Undermountian, Houses.Piety, QuestTypes.Plot, QuestActions.PietyQuestTakenGivesCleric, 13),
    AllyWithTheXanatharsGuild(127, "Ally with the Xanathar's Guild", Expansions.Undermountian, Houses.Skullduggery, QuestTypes.Plot, QuestActions.SkullduggeryQuestTakenGivesTwoRogues, 10),
    ImpersonateTaxCollector(128, "Impersonate Tax Collector", Expansions.Undermountian, Houses.Skullduggery, QuestTypes.Plot, QuestActions.GainOwnerRewardOnBuildings, 9),
    RecoverTheFlameOfTheNorth(129, "Recover the Flame of the North", Expansions.Undermountian, Houses.Warfare, QuestTypes.Plot, QuestActions.WarfareQuestTakenGivesTwoFighters, 10),
    SeizeControlOfTheBloodyHand(130, "Seize Control of the Bloody Hand", Expansions.Undermountian, Houses.Warfare, QuestTypes.Plot, QuestActions.ReturnOneResourceFromCompletedQuest, 6),
    
    // Skullport - starts at 201
    EstablishCultCell(201, "Establish Cult Cell", Expansions.Skullport, Houses.Arcana, 18),
    InvestigateThayanVessel(202, "Investigate Thayan Vessel", Expansions.Skullport, Houses.Arcana, 13),
    RenewGuardsAndWards(203, "Renew Guards and Wards", Expansions.Skullport, Houses.Arcana, QuestActions.ReturnUpToTwoCorruption, 9),
    SealAnEntranceToSkullport(204, "Seal an Entrance to Skullport", Expansions.Skullport, Houses.Arcana, QuestActions.ReturnUpToThreeCorruption, 10),
    DonateToTheCity(205, "Donate to the City", Expansions.Skullport, Houses.Commerce, QuestActions.ReturnUpToThreeCorruption, 13),
    FundAlchemicalResearch(206, "Fund Alchemical Research", Expansions.Skullport, Houses.Commerce, 20),
    FundPilgrimageOfWaukeen(207, "Fund Pilgrimage of Waukeen", Expansions.Skullport, Houses.Commerce, 16),
    PayFines(208, "Pay Fines", Expansions.Skullport, Houses.Commerce, QuestActions.ReturnUpToTwoCorruption, 4),
    BanishEvilSpirits(209, "Banish Evil Spirits", Expansions.Skullport, Houses.Piety, QuestActions.ReturnUpToTwoCorruption, 5),
    EnterTheTowerOfSevenWoes(210, "Enter the Tower of Seven Woes", Expansions.Skullport, Houses.Piety, 19),
    InstituteReforms(211, "Institute Reforms", Expansions.Skullport, Houses.Piety, QuestActions.ReturnUpToThreeCorruption, 13),
    SanctifyTempleToOghma(212, "Sanctify Temple to Oghma", Expansions.Skullport, Houses.Piety, 18),
    BuryTheBodies(213, "Bury the Bodies", Expansions.Skullport, Houses.Skullduggery, 20),
    RescueVictimFromTheSkulls(214, "Rescue a Victim from the Skulls", Expansions.Skullport, Houses.Skullduggery, QuestActions.ReturnOneCorruption, 9),
    SaveKidnappedNobles(215, "Save Kidnapped Nobles", Expansions.Skullport, Houses.Skullduggery, QuestActions.ReturnUpToThreeCorruption, 9),
    SwindleTheBuildersGuilds(216, "Swindle the Builders' Guild", Expansions.Skullport, Houses.Skullduggery, QuestActions.TwoBuildingsFromBuildersHall, 13),
    AssassinateRivals(217, "Assassinate Rivals", Expansions.Skullport, Houses.Warfare, QuestActions.EachOpponentReturnsOneResource, 16),
    ImprovePrisonSecurity(218, "Improve Prison Security", Expansions.Skullport, Houses.Warfare, QuestActions.RemoveUpToThreeCorruption, 8),
    PatrolDockWard(219, "Patrol Dock Ward", Expansions.Skullport, Houses.Warfare, 9),
    UncoverDrowPlot(220, "Uncover Drow Plot", Expansions.Skullport, Houses.Warfare, QuestActions.ReturnUpToTwoCorruption, 18),
    // Plot quests
    RecruitAcademyCastoffs(221, "Recruit Academy Castoffs", Expansions.Skullport, Houses.Arcana, QuestTypes.Plot, QuestActions.ExtraFromBlackstaffTower, 8),
    UncoverForbiddenLore(222, "Uncover Forbidden Lore", Expansions.Skullport, Houses.Arcana, QuestTypes.Plot, QuestActions.ExtraFromWaterdeepHarbor, 17),
    DefameRivalBusiness(223, "Defame Rival Business", Expansions.Skullport, Houses.Commerce, QuestTypes.Plot, QuestActions.ReallocateCorruptionOnBuildingPurchase, 9),
    ExtortAurora(224, "Extort Aurora", Expansions.Skullport, Houses.Commerce, QuestTypes.Plot, QuestActions.ExtraFromAurorasRealmShop, 8),
    GiveHonorToMask(225, "Give Honor to Mask", Expansions.Skullport, Houses.Piety, QuestTypes.Plot, QuestActions.ExtraFromPlinth, 8),
    ProtectConvertsToEilistraee(226, "Protect Converts to Eilistraee", Expansions.Skullport, Houses.Piety, QuestTypes.Plot, QuestActions.ReturnExtraCorruption, 10),
    ExpandGuildActivities(227, "Expand Guild Activities", Expansions.Skullport, Houses.Skullduggery, QuestTypes.Plot, QuestActions.ExtraFromGrinningLion, 8),
    ShelterZhentarimAgents(228, "Shelter Zhentarim Agents", Expansions.Skullport, Houses.Skullduggery, QuestTypes.Plot, QuestActions.DrawIntrigueOnCorruptionTaken, 16),
    FixTheChampionsGames(229, "Fix the Champions' Games", Expansions.Skullport, Houses.Warfare, QuestTypes.Plot, QuestActions.ExtraFromFieldOfTriumph, 8),
    TrainCastleguards(230, "Train Castle guards", Expansions.Skullport, Houses.Warfare, QuestTypes.Plot, QuestActions.GainFightersOnFirstTurnMarker, 10),

    // Mandatory Qyests get their own
    FendOffBandits(1001, "Fend Off Bandits", 
            Expansions.Classic, Houses.Arcana, QuestTypes.Mandatory, QuestActions.None, 2),
    FoilTheZhentarim(1002, "Foil the Zhentarim", 
            Expansions.Classic, Houses.Arcana, QuestTypes.Mandatory, QuestActions.None, 2),
    PlacateAngryMerchants(1003, "Placate Angry Merchants", 
            Expansions.Classic, Houses.Arcana, QuestTypes.Mandatory, QuestActions.None, 4),
    QuellRiots(1004, "Quell Riots", 
            Expansions.Classic, Houses.Arcana, QuestTypes.Mandatory, QuestActions.None, 4),
    RepelDrowInvaders(1005, "Repel Drow Invaders", 
            Expansions.Classic, Houses.Arcana, QuestTypes.Mandatory, QuestActions.None, 2),
    StampOutCultists(1006, "Stamp Out Cultists", 
            Expansions.Classic, Houses.Arcana, QuestTypes.Mandatory, QuestActions.None, 2),
    EvadeAssassins(1007, "Evade Assassins", 
            Expansions.Undermountian, Houses.Arcana, QuestTypes.Mandatory, QuestActions.None, 0),
    SubdueIllithidMenace(1008, "Subdue Illithid Menace", 
            Expansions.Undermountian, Houses.Arcana, QuestTypes.Mandatory, QuestActions.None, 0),
    UnveilAbyssalAgent(1009, "Unveil Abyssal Agent", 
            Expansions.Undermountian, Houses.Arcana, QuestTypes.Mandatory, QuestActions.None, 0),
    
    ClearRustMonsterNest(1010, "Clear Rust Monster Nest", 
            Expansions.Skullport, Houses.Arcana, QuestTypes.Mandatory, QuestActions.None, 2),
    CoverUpScandal(1011, "Cover Up a Scandal", 
            Expansions.Skullport, Houses.Arcana, QuestTypes.Mandatory, QuestActions.None, 2),
    HuntHiddenGhoul(1012, "Hunt hidden Ghoul", 
            Expansions.Skullport, Houses.Arcana, QuestTypes.Mandatory, QuestActions.None, 2),

    ;
    
    private int questID;
    private String name;
    private Expansions expansion;
    private QuestTypes questType;
    private QuestActions questAction;
    private Houses house;
    private int victoryPoints;
    private int drawIntrigue;
    
    private Quests(int questID, String name, Expansions expansion, Houses house, QuestTypes questType, int victoryPoints) {
        this(questID, name, expansion, house, questType, QuestActions.None, victoryPoints, 0);
    }
    private Quests(int questID, String name, Expansions expansion, Houses house, QuestTypes questType, int victoryPoints, int drawIntrigue) {
        this(questID, name, expansion, house, questType, QuestActions.None, victoryPoints, drawIntrigue);
    }
    private Quests(int questID, String name, Expansions expansion, Houses house, QuestActions questAction, int victoryPoints) {
        this(questID, name, expansion, house, QuestTypes.Basic, questAction, victoryPoints, 0);
    }
    private Quests(int questID, String name, Expansions expansion, Houses house, int victoryPoints) {
        this(questID, name, expansion, house, QuestTypes.Basic, QuestActions.None, victoryPoints, 0);
    }
    private Quests(int questID, String name, Expansions expansion, Houses house, int victoryPoints, int drawIntrigue) {
        this(questID, name, expansion, house, QuestTypes.Basic, QuestActions.None, victoryPoints, drawIntrigue);
    }
    private Quests(int questID, String name, Expansions expansion, Houses house, QuestTypes questType, QuestActions questAction, int victoryPoints) {
        this(questID, name, expansion, house, questType, questAction, victoryPoints, 0);
    }
    private Quests(int questID, String name, Expansions expansion, Houses house, QuestTypes questType, QuestActions questAction, int victoryPoints, int drawIntrigue) {
        this.questID = questID;
        this.name = name;
        this.expansion = expansion;
        this.house = house;
        this.questType = questType;
        this.questAction = questAction;
        this.victoryPoints = victoryPoints;
        this.drawIntrigue = drawIntrigue;
    }

    public int getQuestID() {
        return questID;
    }

    public String getName() {
        return name;
    }

    public Expansions getExpansion() {
        return expansion;
    }

    public QuestTypes getQuestType() {
        return questType;
    }

    public Houses getHouse() {
        return house;
    }

    public int getVictoryPoints() {
        return victoryPoints;
    }

    public QuestActions getQuestAction() {
        return questAction;
    }

    public int getDrawIntrigue() {
        return drawIntrigue;
    }
    
}

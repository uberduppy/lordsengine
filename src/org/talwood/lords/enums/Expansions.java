/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.talwood.lords.enums;

/**
 *
 * @author Todd
 */
public enum Expansions {
    Classic(1, "Classic"),
    Undermountian(2, "Undermountain"),
    Skullport(3, "Skullport");
    
    private int id;
    private String description;
    
    private Expansions(int id, String desription) {
        this.id = id;
        this.description = desription;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
    
    
}

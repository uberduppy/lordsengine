package org.talwood.lords.enums;

public enum Lords {

    BrianneByndraeth(1, "Brianne Byndraeth", 
            "At the end of the game, you score 4 Victory Points for each completed Arcana quest and Skullduggery quest you completed.", 
            Expansions.Classic, Houses.Arcana, Houses.Skullduggery),
    CaladornCassalanter(2, "Caladorn Cassalanter", 
            "At the end of the game, you score 4 Victory Points for each completed Skullduggery quest and Warfare quest you completed.", 
            Expansions.Classic, Houses.Skullduggery, Houses.Warfare),
    DurnamTheWanderer(3, "Durnam the Wanderer", 
            "At the end of the game, you score 4 Victory Points for each completed Commerce quest and Warfare quest you completed.", 
            Expansions.Classic, Houses.Commerce, Houses.Warfare),
    KhelbenArunsen(4, "Khelben Arunsen, the Blackstaff", 
            "At the end of the game, you score 4 Victory Points for each completed Arcana quest and Warfare quest you completed.", 
            Expansions.Classic, Houses.Arcana, Houses.Warfare),
    KyrianiAgrivar(5, "Kyriani Agrivar", 
            "At the end of the game, you score 4 Victory Points for each completed Arcana quest and Piety quest you completed.",
            Expansions.Classic, Houses.Arcana, Houses.Piety),
    LarissaNeathal(6, "Larissa Neathal", 
            "At the end of the game, you score 6 Victory Points for each building you control.", 
            Expansions.Classic, true, false, false, false, false, false, false),
    MirtTheMoneylender(7, "Mirt the Moneylender", 
            "At the end of the game, you score 4 Victory Points for each completed Commerce quest and Piety quest you completed.", 
            Expansions.Classic, Houses.Commerce, Houses.Piety),
    NindilJalbuck(8, "Nindil Jalbuck", 
            "At the end of the game, you score 4 Victory Points for each completed Piety quest and Skullduggery quest you completed.", 
            Expansions.Classic, Houses.Piety, Houses.Skullduggery),
    NymaraScheiron(9, "Nymara Scheiron", 
            "At the end of the game, you score 4 Victory Points for each completed Commerce quest and Skullduggery quest you completed.", 
            Expansions.Classic, Houses.Commerce, Houses.Skullduggery),
    PiergeironThePaladinson(10, "Piergeiron the Paladinson", 
            "At the end of the game, you score 4 Victory Points for each completed Piety quest and Warfare quest you completed.", 
            Expansions.Classic, Houses.Piety, Houses.Warfare),
    SammerezaSulphontis(11, "Sammereza Sulphontis", 
            "At the end of the game, you score 4 Victory Points for each completed Arcana quest and Commerce quest you completed.", 
            Expansions.Classic, Houses.Arcana, Houses.Commerce),
    
        HalasterBlackcloak(101, "Halaster Blackcloak",
                "At the end of the game, you score 4 Victory Points for each Undermountain quest completed and each Undermountain building you own.",
                Expansions.Undermountian, false, false, true, false, false, false, false),
        Trobriand(102, "Trobriand", 
                "At the end of the game, you score 5 Victory Points for each completed quest worth at least 10 Victory Points", 
                Expansions.Undermountian, false, false, false, false, false, false, true),
        DaniloThann(103, "Danilo Thann", 
                "At the end of the game, you score 3 Victory Points for each completed non-mandatory quest.", 
                Expansions.Undermountian, false, false, false, false, false, true, false),
    
        TheXanathar(201, "The Xanathar", 
                "At the end of the game, you score 4 Victory Points for each corruption counter in your tavern.", 
                Expansions.Skullport, false, false, false, false, true, false, false),
        IrusylEraneth(202, "Irusyl Eraneth", 
                "At the end of the game, select 1 quest type. You score 6 Victory Points for each completed quest of that type.", 
                Expansions.Skullport, false, false, false, true, false, false, false),
        Sangalor(203, "Sangalor", 
                "At the end of the game, you score 4 Victory Points for each Skullport quest completed and each Skullport building you own.", 
                Expansions.Skullport, false, true, false, false, false, false, false),
        
    ;
    private int lordID;
    private String lordName;
    private String description;
    private Expansions expansion;
    // These are all separate flags.
    private Houses house1;
    private Houses house2;
    private boolean buildingLord;
    private boolean skullportLord;
    private boolean undermountainLord;
    private boolean singleHouseLord;
    private boolean corruptionLord;
    private boolean allQuestLord;
    private boolean tenPointLord;
      
    private Lords(int lordID, String lordName, String description, Expansions expansion, boolean buildingLord,
        boolean skullportLord, boolean undermountainLord, boolean singleHouseLord, boolean corruptionLord,
        boolean allQuestLord,boolean tenPointLord) {
        this(lordID, lordName, description, expansion);
        this.buildingLord = buildingLord;
        this.skullportLord = skullportLord;
        this.undermountainLord = undermountainLord;
        this.singleHouseLord = singleHouseLord;
        this.corruptionLord = corruptionLord;
        this.allQuestLord = allQuestLord;
        this.tenPointLord = tenPointLord;

}
    private Lords(int lordID, String lordName, String description, Expansions expansion, Houses house1, Houses house2) {
        this(lordID, lordName, description, expansion);
        this.house1 = house1;
        this.house2 = house2;
    }
    private Lords(int lordID, String lordName, String description, Expansions expansion) {
        this.lordID = lordID;
        this.lordName = lordName;
        this.description = description;
        this.expansion = expansion;
    }

    public int getLordID() {
        return lordID;
    }

    public String getLordName() {
        return lordName;
    }

    public String getDescription() {
        return description;
    }

    public Expansions getExpansion() {
        return expansion;
    }

    public Houses getHouse1() {
        return house1;
    }

    public Houses getHouse2() {
        return house2;
    }

    public boolean isBuildingLord() {
        return buildingLord;
    }

    public boolean isSkullportLord() {
        return skullportLord;
    }

    public boolean isUndermountainLord() {
        return undermountainLord;
    }

    public boolean isSingleHouseLord() {
        return singleHouseLord;
    }

    public boolean isCorruptionLord() {
        return corruptionLord;
    }

    public boolean isAllQuestLord() {
        return allQuestLord;
    }

    public boolean isTenPointLord() {
        return tenPointLord;
    }
    
    
}

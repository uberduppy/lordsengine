package org.talwood.lords.enums;

public enum GameStepTypes {
    DrawQuest,
    CompleteQuest,
    PlayIntrigue,
    VariableCost,
    VariableReward,
    VariableOwnerReward,
}

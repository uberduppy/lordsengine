package org.talwood.lords.enums;

public enum ResourceTypes {
    Gold(1, "Gold", "Gold"),
    Cleric(2, "Cleric", "Clerics"),
    Fighter(3, "Fighter", "Fighters"),
    Rogue(4, "Rogue", "Rogues"),
    Wizard(5, "Wizard", "Wizards"),
    AnyResource(100, "Any", "Any"),
    Corruption(100, "Corruption", "Corruption"),
    Intrigue(900, "Intrigue card", "Intrigue cards"),
    VictoryPoints(1000, "Victory point", "Victory points");
    
    private int code;
    private String description;
    private String pluralDescription;
    
    private ResourceTypes(int code, String description, String pluralDescription) {
        this.code = code;
        this.description = description;
        this.pluralDescription = pluralDescription;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String getPluralDescription() {
        return pluralDescription;
    }
    
    public void appendDescription(StringBuilder sb, int count) {
        if(count > 0) {
            if(sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(count);
            sb.append(" ");
            if(count == 0) {
                sb.append(description);
            } else {
                sb.append(pluralDescription);
            }
        }
    }
    
}

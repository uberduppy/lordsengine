package org.talwood.lords.enums;

public enum BuildingActions {
//    None,
    TakeResources,
    BuyBuilding,
    TakeQuest,
    DrawIntrigue,
    DrawTwoIntrigue,
    PlayIntrigue,
    PlayThreeIntrigues,
    FirstTurnMarker,
    ResetQuests,
    OwnerReceivesTwoVictoryPoints,
    TakeAmbassadorNextTurn,
    GainOneGoldForEachBuilding,
    ChooseOpponentBuilding,
    RequiresVariableActivation,
    RequiresVariableReward,
    RequiresVariableOwnerReward,
    RequiresQuestDiscard,
    RequiresTwoIntrigueDiscard,
    ReallocateCorruption,
    TakeCorruptionReturnAgents,
    RemoveUpToTwoCorruption,
    ReallocateResources,
    
}

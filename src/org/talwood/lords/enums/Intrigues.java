package org.talwood.lords.enums;

import org.talwood.lords.constants.ResourceConstants;

public enum Intrigues {

    Ambush1(1, "Ambush", 
            "Each opponent removes " + ResourceConstants.MASK_FIGHTER_DATA + " from his or her Tavern and returns it to the supply.<br>For each opponent that could not do so,<br>take 1 " + ResourceConstants.MASK_FIGHTER_DATA + " from the supply and place it in your Tavern.", 
            Expansions.Classic, IntrigueTypes.Attack, IntrigueActionTypes.ReallocateResources),
    Ambush2(2, "Ambush", 
            "Each opponent removes " + ResourceConstants.MASK_FIGHTER_DATA + " from his or her Tavern and returns it to the supply.<br>For each opponent that could not do so,<br>take 1 " + ResourceConstants.MASK_FIGHTER_DATA + " from the supply and place it in your Tavern.", 
            Expansions.Classic, IntrigueTypes.Attack, IntrigueActionTypes.ReallocateResources),
    ArcaneMishap1(3, "Arcane Mishap", 
            "Each opponent removes " + ResourceConstants.MASK_WIZARD_DATA + " from his or her Tavern and returns it to the supply.<br>For each opponent that could not do so,<br>take 1 " + ResourceConstants.MASK_WIZARD_DATA + " from the supply and place it in your Tavern.", 
            Expansions.Classic, IntrigueTypes.Attack, IntrigueActionTypes.ReallocateResources),
    ArcaneMishap2(4, "Arcane Mishap", 
            "Each opponent removes " + ResourceConstants.MASK_WIZARD_DATA + " from his or her Tavern and returns it to the supply.<br>For each opponent that could not do so,<br>take 1 " + ResourceConstants.MASK_WIZARD_DATA + " from the supply and place it in your Tavern.", 
            Expansions.Classic, IntrigueTypes.Attack, IntrigueActionTypes.ReallocateResources),
    Assassination1(5, "Assassination", 
            "Each opponent removes " + ResourceConstants.MASK_ROGUE_DATA + " from his or her Tavern and returns it to the supply.<br>For each opponent that could not do so,<br>take 1 " + ResourceConstants.MASK_ROGUE_DATA + " from the supply and place it in your Tavern.", 
            Expansions.Classic, IntrigueTypes.Attack, IntrigueActionTypes.ReallocateResources),
    Assassination2(6, "Assassination", 
            "Each opponent removes " + ResourceConstants.MASK_ROGUE_DATA + " from his or her Tavern and returns it to the supply.<br>For each opponent that could not do so,<br>take 1 " + ResourceConstants.MASK_ROGUE_DATA + " from the supply and place it in your Tavern.", 
            Expansions.Classic, IntrigueTypes.Attack, IntrigueActionTypes.ReallocateResources),
    BribeAgent1(7, "Bribe Agent", 
            "Pay " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ":<br>Choose an action space containing an opponent's Agent.<br>You use that space's action as though you had assigned an Agent to it.", 
            Expansions.Classic, IntrigueTypes.Attack, IntrigueActionTypes.DuplicatePlacement),
    BribeAgent2(8, "Bribe Agent", 
            "Pay " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ":<br>Choose an action space containing an opponent's Agent.<br>You use that space's action as though you had assigned an Agent to it.", 
            Expansions.Classic, IntrigueTypes.Attack, IntrigueActionTypes.DuplicatePlacement),
    FreeDrinks1(9, "Free Drinks", 
            "Remove " + ResourceConstants.MASK_ANY_DATA + " from an opponent's Tavern and the place it in your Tavern.", 
            Expansions.Classic, IntrigueTypes.Attack, IntrigueActionTypes.StealResources),
    FreeDrinks2(10, "Free Drinks", 
            "Remove " + ResourceConstants.MASK_ANY_DATA + " from an opponent's Tavern and the place it in your Tavern.", 
            Expansions.Classic, IntrigueTypes.Attack, IntrigueActionTypes.StealResources),
    LackOfFaith1(11, "Lack of Faith", 
            "Each opponent removes " + ResourceConstants.MASK_CLERIC_DATA + " from his or her Tavern and returns it to the supply.<br>For each opponent that could not do so,<br>take 1 " + ResourceConstants.MASK_CLERIC_DATA + " from the supply and place it in your Tavern.", 
            Expansions.Classic, IntrigueTypes.Attack, IntrigueActionTypes.ReallocateResources),
    LackOfFaith2(12, "Lack of Faith", 
            "Each opponent removes " + ResourceConstants.MASK_CLERIC_DATA + " from his or her Tavern and returns it to the supply.<br>For each opponent that could not do so,<br>take 1 " + ResourceConstants.MASK_CLERIC_DATA + " from the supply and place it in your Tavern.", 
            Expansions.Classic, IntrigueTypes.Attack, IntrigueActionTypes.ReallocateResources),
    AcceleratedPlans(13, "Accelerated Plans", 
            "Choose 1 of your Agent assigned to Waterdeep Harbor.<br>Return it to your pool, then immediately assign 2 Agents", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.AgentPlacement),
    BiddingWar1(14, "Bidding War", 
            "Draw a Quest from the deck equal to the number of players.<br>Keep one and pass the remaining Quests to the player on your left.<br>Each player, in turn, chooses 1 Quest to keep,<br>and passes the rest to the left until every player has taken a Quest.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.DrawQuests),
    BiddingWar2(15, "Bidding War", 
            "Draw a Quest from the deck equal to the number of players.<br>Keep one and pass the remaining Quests to the player on your left.<br>Each player, in turn, chooses 1 Quest to keep,<br>and passes the rest to the left until every player has taken a Quest.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.DrawQuests),
    BiddingWar3(16, "Bidding War", 
            "Draw a Quest from the deck equal to the number of players.<br>Keep one and pass the remaining Quests to the player on your left.<br>Each player, in turn, chooses 1 Quest to keep,<br>and passes the rest to the left until every player has taken a Quest.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.DrawQuests),
    CallForAdventurers1(17, "Call for Adventurers", 
            "Take " + ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_ANY_DATA + " from the supply and place them in your Tavern.<br>Each opponent takes " + ResourceConstants.MASK_ANY_DATA + " from the supply and places it in his or her tavern.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ShareResources),
    CallForAdventurers2(18, "Call for Adventurers", 
            "Take " + ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_ANY_DATA + " from the supply and place them in your Tavern.<br>Each opponent takes " + ResourceConstants.MASK_ANY_DATA + " from the supply and places it in his or her tavern.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ShareResources),
    CallInAFavor1(19, "Call in a Favor", 
            "Choose 1 of the following to take from the supply:<br>" 
                    + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
                    + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + "<br>" 
                    + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + "<br>" 
                    + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + "<br>" 
                    + ResourceConstants.MASK_WIZARD_DATA + "<br>" + ResourceConstants.MASK_CLERIC_DATA, 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.GainResources),
    CallInAFavor2(20, "Call in a Favor", 
            "Choose 1 of the following to take from the supply:<br>" 
                    + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
                    + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + "<br>" 
                    + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + "<br>" 
                    + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + "<br>" 
                    + ResourceConstants.MASK_WIZARD_DATA + "<br>" + ResourceConstants.MASK_CLERIC_DATA, 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.GainResources),
    ChangeOfPlans(21, "Change of Plans", 
            "Discard an uncompleted Quest.<br>Score 6 VP.<br>Each opponent can choose to discard un uncompleted Quest to score 3 VP (You can discard a Mandatory Quest in this way)", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.DiscardQuest),
    Conscription1(22, "Conscription", 
            "Take " + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + " from the supply and place it in your Tavern.<br>Choose 1 opponent.<br>That opponent takes " + ResourceConstants.MASK_FIGHTER_DATA + " from the supply and places it in his or her Tavern", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ShareResourcesSingle),
    Conscription2(23, "Conscription", 
            "Take " + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + " from the supply and place it in your Tavern.<br>Choose 1 opponent.<br>That opponent takes " + ResourceConstants.MASK_FIGHTER_DATA + " from the supply and places it in his or her Tavern", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ShareResourcesSingle),
    CrimeWave1(24, "Crime Wave", 
            "Take " + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + " from the supply and place it in your Tavern.<br>Choose 1 opponent.<br>That opponent takes " + ResourceConstants.MASK_ROGUE_DATA + " from the supply and places it in his or her Tavern", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ShareResourcesSingle),
    CrimeWave2(25, "Crime Wave", 
            "Take " + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + " from the supply and place it in your Tavern.<br>Choose 1 opponent.<br>That opponent takes " + ResourceConstants.MASK_ROGUE_DATA + " from the supply and places it in his or her Tavern", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ShareResourcesSingle),
    GoodFaith1(26, "Good Faith", 
            "Take " + ResourceConstants.MASK_CLERIC_DATA + ResourceConstants.MASK_CLERIC_DATA + " from the supply and place it in your Tavern.<br>Choose 1 opponent.<br>That opponent takes " + ResourceConstants.MASK_CLERIC_DATA + " from the supply and places it in his or her Tavern", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ShareResourcesSingle),
    GoodFaith2(27, "Good Faith", 
            "Take " + ResourceConstants.MASK_CLERIC_DATA + ResourceConstants.MASK_CLERIC_DATA + " from the supply and place it in your Tavern.<br>Choose 1 opponent.<br>That opponent takes " + ResourceConstants.MASK_CLERIC_DATA + " from the supply and places it in his or her Tavern", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ShareResourcesSingle),
    GraduationDay1(28, "Graduation Day", 
            "Take " + ResourceConstants.MASK_WIZARD_DATA + ResourceConstants.MASK_WIZARD_DATA + " from the supply and place it in your Tavern.<br>Choose 1 opponent.<br>That opponent takes " + ResourceConstants.MASK_WIZARD_DATA + " from the supply and places it in his or her Tavern", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ShareResourcesSingle),
    GraduationDay2(29, "Graduation Day", 
            "Take " + ResourceConstants.MASK_WIZARD_DATA + ResourceConstants.MASK_WIZARD_DATA + " from the supply and place it in your Tavern.<br>Choose 1 opponent.<br>That opponent takes " + ResourceConstants.MASK_WIZARD_DATA + " from the supply and places it in his or her Tavern", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ShareResourcesSingle),
    RealEstateDeal(30, "Real Estate Deal", 
            "Discard a Building under you control that currently has no Agent assigned to it.<br>Then choose 1 Building in Builder's Hall and putit in play under your control at no cost.  (Replace that Building afterward.)", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.DiscardBuilding),
    RecallAgent1(31, "Recall Agent", 
            "Return 1 of your assigned Agent to your pool.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ReturnAgent),
    RecallAgent2(32, "Recall Agent", 
            "Return 1 of your assigned Agent to your pool.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ReturnAgent),
    RecruitSpies(33, "Recruit Spies", 
            "Take " + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + " from the supply and place it in your Tavern.<br>Each opponent can choose to give you " + ResourceConstants.MASK_ROGUE_DATA + " once to score 3 Victory Points.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ResourceForVP),
    RequestAssistance(34, "Request Assistance", 
            "Take " + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + " from the supply and place it in your Tavern.<br>Each opponent can choose to give you " + ResourceConstants.MASK_FIGHTER_DATA + " once to score 3 Victory Points.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ResourceForVP),
    ResearchAgreement(35, "Research Agreement", 
            "Take " + ResourceConstants.MASK_WIZARD_DATA + " from the supply and place it in your Tavern.<br>Each opponent can choose to give you " + ResourceConstants.MASK_WIZARD_DATA + " once to score 5 Victory Points.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ResourceForVP),
    SampleWares1(36, "Sample Wares", 
            "Assign 1 of your unused Agent to a Building in Builder's Hall.<br>You immediately use the effect of that Building as though you controlled it.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.UseBuildersHall),
    SampleWares2(37, "Sample Wares", 
            "Assign 1 of your unused Agent to a Building in Builder's Hall.<br>You immediately use the effect of that Building as though you controlled it.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.UseBuildersHall),
    SpecialAssignment1(38, "Special Assignment", 
            "Choose a Quest type: Arcana, Commerce, Piety, Skullduggery, or Warfare.<br>Draw and reveal Quests until you reveal a card of the choosen type.<br>Keep that Quest and discard the rest.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.DrawQuestsForHouse),
    SpecialAssignment2(39, "Special Assignment", 
            "Choose a Quest type: Arcana, Commerce, Piety, Skullduggery, or Warfare.<br>Draw and reveal Quests until you reveal a card of the choosen type.<br>Keep that Quest and discard the rest.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.DrawQuestsForHouse),
    SpreadTheWealth1(40, "Spread the Wealth", 
            "Take " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
                    + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
                    + " from the supply and choose 1 opponent.<br>That opponent takes " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + " from the supply.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ShareResourcesSingle),
    SpreadTheWealth2(41, "Spread the Wealth", 
            "Take " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
                    + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
                    + " from the supply and choose 1 opponent.<br>That opponent takes " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + " from the supply.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ShareResourcesSingle),
    SummonTheFaithful(42, "Summon the Faithful", 
            "Take " + ResourceConstants.MASK_CLERIC_DATA + " from the supply and place it in your Tavern.<br>Each opponent can choose to give you " 
                    + ResourceConstants.MASK_CLERIC_DATA + " once to score 5 Victory Points.", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ResourceForVP),
    TaxCollection1(43, "Tax Collection", 
            "Take " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
                    + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
                    + " from the supply.<br>Each opponent can choose to pay you " 
                    + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
                    + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + " to score 4 Victory Points", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ResourceForVP),
    TaxCollection2(44, "Tax Collection", 
            "Take " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
                    + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
                    + " from the supply.<br>Each opponent can choose to pay you " 
                    + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA 
                    + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + " to score 4 Victory Points", 
            Expansions.Classic, IntrigueTypes.Utility, IntrigueActionTypes.ResourceForVP),
    FendOffBandits(45, "Fend Off Bandits", 
            ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_WIZARD_DATA + " - 2 Victory points",
            Expansions.Classic, IntrigueTypes.MandatoryQuest, IntrigueActionTypes.MandatoryQuest),
    FoilTheZhentarim(46, "Foil the Zhentarim", 
            ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_WIZARD_DATA + " - 2 Victory points",
            Expansions.Classic, IntrigueTypes.MandatoryQuest, IntrigueActionTypes.MandatoryQuest),
    PlacateAngryMerchants(47, "Placate Angry Merchants", 
            ResourceConstants.MASK_CLERIC_DATA + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_WIZARD_DATA + " - 4 Victory points",
            Expansions.Classic, IntrigueTypes.MandatoryQuest, IntrigueActionTypes.MandatoryQuest),
    QuellRiots(48, "Quell Riots", 
            ResourceConstants.MASK_CLERIC_DATA + ResourceConstants.MASK_CLERIC_DATA + ResourceConstants.MASK_FIGHTER_DATA + " - 4 Victory points",
            Expansions.Classic, IntrigueTypes.MandatoryQuest, IntrigueActionTypes.MandatoryQuest),
    RepelDrowInvaders(49, "Repel Drow Invaders", 
            ResourceConstants.MASK_CLERIC_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + " - 2 Victory points",
            Expansions.Classic, IntrigueTypes.MandatoryQuest, IntrigueActionTypes.MandatoryQuest),
    StampOutCultists(50, "Stamp Out Cultists", 
            ResourceConstants.MASK_CLERIC_DATA + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_ROGUE_DATA + " - 2 Victory points",
            Expansions.Classic, IntrigueTypes.MandatoryQuest, IntrigueActionTypes.MandatoryQuest),

    // Undermountain
    CallForAssistance1(101, "Call for Assistance", 
            "Choose one opponent.<br>Remove " + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA
             + ", " + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + ", or "
            + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_ROGUE_DATA 
                    + " from his or her Tavern and place them into your Tavern.", 
            Expansions.Undermountian, IntrigueTypes.Attack, IntrigueActionTypes.StealResources),
    CallForAssistance2(102, "Call for Assistance", 
            "Choose one opponent.<br>Remove " + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA
             + ", " + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + ", or "
            + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_ROGUE_DATA 
                    + " from his or her Tavern and place them into your Tavern.", 
            Expansions.Undermountian, IntrigueTypes.Attack, IntrigueActionTypes.StealResources),
    
    DemolishBuilding(103, "Demolish",
            "Choose 1 building in play that has no agent assigned to it and discard it.<br>If you were not the owner, pay the Gold cost of the Building to its owner.<br>Put 1 Building in Builder's Hall in play under your control at no cost.",
            Expansions.Undermountian, IntrigueTypes.Attack, IntrigueActionTypes.DemolishBuilding),
    InevitableBetrayal(104, "Inevitable Betrayal",
            "Choose 1 opponent.<br>Remove " + ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA
            + "from his or her Tavern and place them in your Tavern.<br>Give this card to the chosen opponent after playing it.",
            Expansions.Undermountian, IntrigueTypes.Attack, IntrigueActionTypes.StealResources),
    
    Manipulate(105, "Manipulate", "Choose an action space containing an opponent's agent and move that agent to an unoccupied space. The opponent can take the new space's action. Then assign 1 of your unassigned agents.",
            Expansions.Undermountian, IntrigueTypes.Attack, IntrigueActionTypes.PlaceHolder),
    PreferentialTreatment(106, "Preferential Treatment", 
            "Take " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + " from the supply and place it in your Tavern, then take the first tunr marker.",
            Expansions.Undermountian, IntrigueTypes.Attack, IntrigueActionTypes.PlaceHolder),
    AlliedFaiths(107, "Allied Faiths", 
            "Give 1 opponent " + ResourceConstants.MASK_CLERIC_DATA + " from your Tavern and gain 8 Victory Points.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.GiveResourceForVP),
    ArchitecturalInnovation(108, "Architectural Innovation", 
            "Take " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + " from the supply and place it in your Tavern. You can purchase 1 face-up Building in Builder's Hall.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.BuyBuilding),
    FriendlyLoan(109, "Friendly Loan", 
            "Give 1 opponent " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + " from your Tavern and gain 8 Victory Points.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.GiveResourceForVP),
    HonorAmongThieves(110, "Honor Among Thieves", 
            "Give 1 opponent " + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + " from your Tavern and gain 8 Victory Points.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.GiveResourceForVP),
    InformationBroker1(111, "Information Broker", 
            "Draw " + ResourceConstants.MASK_INTRIGUE_DATA + ResourceConstants.MASK_INTRIGUE_DATA + ResourceConstants.MASK_INTRIGUE_DATA + "<br>Each opponent draws " + ResourceConstants.MASK_INTRIGUE_DATA,
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.DrawThreeIntrigueOppsDrawOne),
    InformationBroker2(112, "Information Broker", 
            "Draw " + ResourceConstants.MASK_INTRIGUE_DATA + ResourceConstants.MASK_INTRIGUE_DATA + ResourceConstants.MASK_INTRIGUE_DATA + "<br>Each opponent draws " + ResourceConstants.MASK_INTRIGUE_DATA,
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.DrawThreeIntrigueOppsDrawOne),
    MercenaryContract(113, "Mercenary Contract", 
            "Give 1 opponent " + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + " from your Tavern and gain 8 Victory Points.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.GiveResourceForVP),
    OpenLord(114, "Open Lord", 
            "Reveal your Lord card and discard any of your uncompleted Mandatory Quests. For the rest of the game, you cannot be affected by Attack and Mandatory Quest Intrigue cards played by opponents.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.PlaceHolder),
    OrganizedCrime(115, "Organized Crime", 
            "Take " + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + " from the supply and place them in your Tavern. Place " + ResourceConstants.MASK_ROGUE_DATA + " from the supply on each of 2 different action spaces.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.PlaceExtraOnTwoSpaces),
    Proselytize(116, "Proselytize", 
            "Take " + ResourceConstants.MASK_CLERIC_DATA + " from the supply and place them in your Tavern. Place " + ResourceConstants.MASK_CLERIC_DATA + " from the supply on any action space.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.PlaceExtraOnOneSpace),
    RecruitmentDrive(117, "Recruitment Drive", 
            "Take " + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + " from the supply and place them in your Tavern. Place " + ResourceConstants.MASK_FIGHTER_DATA + " from the supply on each of 2 different action spaces.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.PlaceExtraOnTwoSpaces),
    SponsorApprentices(118, "Sponsor Apprentices", 
            "Take " + ResourceConstants.MASK_WIZARD_DATA + " from the supply and place them in your Tavern. Place " + ResourceConstants.MASK_WIZARD_DATA + " from the supply on any action space.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.PlaceExtraOnOneSpace),
    TaxRevolt(119, "Tax Revolt", 
            "Take " + ResourceConstants.MASK_FIVE_GOLD_DATA + " from the supply and place them in your Tavern. Place " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + " from the supply on each of 2 different action spaces.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.PlaceExtraOnTwoSpaces),
    UnexpectedSuccess1(120, "Unexpected Success", 
            "Draw " + ResourceConstants.MASK_INTRIGUE_DATA + ResourceConstants.MASK_INTRIGUE_DATA + ". As you draw each card, you may play it immediately as an action.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.DrawTwoIntrigues),
    UnexpectedSuccess2(121, "Unexpected Success", 
            "Draw " + ResourceConstants.MASK_INTRIGUE_DATA + ResourceConstants.MASK_INTRIGUE_DATA + ". As you draw each card, you may play it immediately as an action.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.DrawTwoIntrigues),
    UnlikelyAssistance(122, "Unlikely Assistance", 
            "Give 1 opponent " + ResourceConstants.MASK_WIZARD_DATA + " from your Tavern and gain 8 Victory Points.",
            Expansions.Undermountian, IntrigueTypes.Utility, IntrigueActionTypes.GiveResourceForVP),

            // Mandatory Quests
    EvadeAssassins(123, "Evade Assassins", 
            ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_WIZARD_DATA + " - " + ResourceConstants.MASK_INTRIGUE_DATA,
            Expansions.Undermountian, IntrigueTypes.MandatoryQuest, IntrigueActionTypes.MandatoryQuest),
    SubdueIllithidMenace(124, "Subdue Illithid Menace", 
            ResourceConstants.MASK_CLERIC_DATA + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + " - " + ResourceConstants.MASK_INTRIGUE_DATA,
            Expansions.Undermountian, IntrigueTypes.MandatoryQuest, IntrigueActionTypes.MandatoryQuest),
    UnveilAbyssalAgent(125, "Unveil Abyssal Agent", 
            ResourceConstants.MASK_CLERIC_DATA + ResourceConstants.MASK_WIZARD_DATA + " - " + ResourceConstants.MASK_INTRIGUE_DATA,
            Expansions.Undermountian, IntrigueTypes.MandatoryQuest, IntrigueActionTypes.MandatoryQuest),
    
    // Skullport
    Blackmail(201, "Blackmail",
            "Choose 1 opponent. Remove " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + " from his or her Tavern and place it in your Tavern. Look at that opponent's Lord card.",
            Expansions.Skullport, IntrigueTypes.Attack, IntrigueActionTypes.PlaceHolder),
    Doppelganger(202, "Doppelganger!",
            "Choose an action space containing an agent of an opponent with more " + ResourceConstants.MASK_CORRUPTION_DATA 
                + " than you. You use that space's action as though you had assigned an agent to it.",
            Expansions.Skullport, IntrigueTypes.Attack, IntrigueActionTypes.PlaceHolder),
    ExposeCorruption1(203, "Expose Corruption",
            "Each opponent with more " + ResourceConstants.MASK_CORRUPTION_DATA + " than you takes " + ResourceConstants.MASK_CORRUPTION_DATA + " from the Corruption track and places it in his or her Tavern.",
            Expansions.Skullport, IntrigueTypes.Attack, IntrigueActionTypes.PlaceHolder),
    ExposeCorruption2(204, "Expose Corruption",
            "Each opponent with more " + ResourceConstants.MASK_CORRUPTION_DATA + " than you takes " + ResourceConstants.MASK_CORRUPTION_DATA + " from the Corruption track and places it in his or her Tavern.",
            Expansions.Skullport, IntrigueTypes.Attack, IntrigueActionTypes.PlaceHolder),
    FoistResponsibility(205, "Foist Responsibility",
            "Take " + ResourceConstants.MASK_CORRUPTION_DATA 
               + " from the Corruption track, place it in your Tavern, and give 1 of your uncompleted Mandatory Quests to an opponent with at least 1 " + ResourceConstants.MASK_CORRUPTION_DATA + ".",
            Expansions.Skullport, IntrigueTypes.Attack, IntrigueActionTypes.PlaceHolder),
    ForgeDeed(206, "Forge Deed",
            "Take " + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA 
               + " from the Corruption track, place it in your Tavern, and gain control of 1 Building under an opponent's control.",
            Expansions.Skullport, IntrigueTypes.Attack, IntrigueActionTypes.PlaceHolder),
    BlackMarketMoney(207, "Black Market Money",
            "Take " + ResourceConstants.MASK_FIVE_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA  + ResourceConstants.MASK_GOLD_DATA  + 
                    ResourceConstants.MASK_GOLD_DATA + " from the supply and " + ResourceConstants.MASK_CORRUPTION_DATA  + " from the Corruption track, and place them in your Tavern. Each opponent can take " + 
                    ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + 
                    ResourceConstants.MASK_CORRUPTION_DATA + " and place it in his or her Tavern.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.OptionalResources),
    BribeTheWatch(208, "Bribe the Watch",
            "Spend " + ResourceConstants.MASK_GOLD_DATA  + ResourceConstants.MASK_GOLD_DATA  + 
                    ": Return up to " + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + 
                    " from your Tavern to the the Corruption track. Each opponent can spend " + 
                    ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + 
                    " to return " + ResourceConstants.MASK_CORRUPTION_DATA + " to the Corruption track.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.PlaceHolder),
    CorruptingInfluence1(209, "Corrupting Influence",
            "Place " + ResourceConstants.MASK_CORRUPTION_DATA + 
                    " from the Corruption track on each of 2 different action spaces.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.PlaceHolder),
    CorruptingInfluence2(210, "Corrupting Influence",
            "Place " + ResourceConstants.MASK_CORRUPTION_DATA + 
                    " from the Corruption track on each of 2 different action spaces.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.PlaceHolder),
    DarkDaggerAssassination(211, "Dark Dagger Assassination",
            "Take " + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA  + ResourceConstants.MASK_ROGUE_DATA  + 
                    ResourceConstants.MASK_ROGUE_DATA + " from the supply and " + ResourceConstants.MASK_CORRUPTION_DATA  + " from the Corruption track, and place them in your Tavern. Each opponent can take " + 
                    ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + 
                    ResourceConstants.MASK_CORRUPTION_DATA + " and place it in his or her Tavern.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.OptionalResources),
    DonationsForCyric(212, "Donations for Cyric",
            "Take " + ResourceConstants.MASK_CLERIC_DATA + 
                    ResourceConstants.MASK_CLERIC_DATA + " from the supply and " + ResourceConstants.MASK_CORRUPTION_DATA  + " from the Corruption track, and place them in your Tavern. Each opponent can take " + 
                    ResourceConstants.MASK_CLERIC_DATA + 
                    ResourceConstants.MASK_CORRUPTION_DATA + " and place it in his or her Tavern.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.OptionalResources),
    HonorableExample(213, "Honorable Example",
            "Score 6 Victory points if each opponent has more " + ResourceConstants.MASK_CORRUPTION_DATA  + " than you.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.VictoryForCorruption),
    IronRingSlaves(214, "Iron Ring Slaves",
            "Take " + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA  + ResourceConstants.MASK_FIGHTER_DATA  + 
                    ResourceConstants.MASK_FIGHTER_DATA + " from the supply and " + ResourceConstants.MASK_CORRUPTION_DATA  + " from the Corruption track, and place them in your Tavern. Each opponent can take " + 
                    ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + 
                    ResourceConstants.MASK_CORRUPTION_DATA + " and place it in his or her Tavern.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.OptionalResources),
    MindFlayerMercenaries(215, "Mind Flayer Mercenaries",
            "Take " + ResourceConstants.MASK_WIZARD_DATA + 
                    ResourceConstants.MASK_WIZARD_DATA + " from the supply and " + ResourceConstants.MASK_CORRUPTION_DATA  + " from the Corruption track, and place them in your Tavern. Each opponent can take " + 
                    ResourceConstants.MASK_WIZARD_DATA + 
                    ResourceConstants.MASK_CORRUPTION_DATA + " and place it in his or her Tavern.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.OptionalResources),
    ReleaseTheHounds1(216, "Release the Hounds",
            "Take " + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + 
                    " from the Corruption track and remove them from the game.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.PlaceHolder),
    ReleaseTheHounds2(217, "Release the Hounds",
            "Take " + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + 
                    " from the Corruption track and remove them from the game.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.PlaceHolder),
    Repent1(218, "Repent",
            "Return " + ResourceConstants.MASK_CORRUPTION_DATA + 
                    " from your Tavern to the Corruption track.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.ReturnOneCorruption),
    Repent2(219, "Repent",
            "Return " + ResourceConstants.MASK_CORRUPTION_DATA + 
                    " from your Tavern to the Corruption track.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.ReturnOneCorruption),
    Repent3(220, "Repent",
            "Return " + ResourceConstants.MASK_CORRUPTION_DATA + 
                    " from your Tavern to the Corruption track.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.ReturnOneCorruption),
    Scapegoat1(221, "Scapegoat",
            "Return " + ResourceConstants.MASK_ANY_DATA + " to the supply and up to " +
                    ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + 
                    " to the Corruption track from your Tavern. Each opponent can return " + 
                    ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_CORRUPTION_DATA + 
                    " from his or her Tavern.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.TwoCorruptionAndResource),
    Scapegoat2(222, "Scapegoat",
            "Return " + ResourceConstants.MASK_ANY_DATA + " to the supply and up to " +
                    ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + 
                    " to the Corruption track from your Tavern. Each opponent can return " + 
                    ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_ANY_DATA + ResourceConstants.MASK_CORRUPTION_DATA + 
                    " from his or her Tavern.",
            Expansions.Skullport, IntrigueTypes.Utility, IntrigueActionTypes.TwoCorruptionAndResource),

    ClearRustMonsterNest(223, "Clear Rust Monster Nest", 
            ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_WIZARD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + " - 2 Victory points",
            Expansions.Skullport, IntrigueTypes.MandatoryQuest, IntrigueActionTypes.MandatoryQuest),
    CoverUpScandal(224, "Cover Up a Scandal", 
            ResourceConstants.MASK_CLERIC_DATA + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_WIZARD_DATA + " - 2 Victory points and return a " + ResourceConstants.MASK_CORRUPTION_DATA + " to the Corruption track.",
            Expansions.Skullport, IntrigueTypes.MandatoryQuest, IntrigueActionTypes.MandatoryQuest),
    HuntHiddenGhoul(225, "Hunt Hidden Ghoul", 
            ResourceConstants.MASK_CLERIC_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + " - 2 Victory points",
            Expansions.Skullport, IntrigueTypes.MandatoryQuest, IntrigueActionTypes.MandatoryQuest),
    
    ;
    
    private IntrigueTypes type;
    private IntrigueActionTypes actionType;
    private Expansions expansion;
    private int intrigueID;
    private String name;
    private String description;
    
    private Intrigues(int intrigueID, String name, String description, Expansions expansion, IntrigueTypes type, IntrigueActionTypes actionTypes) {
        this.intrigueID = intrigueID;
        this.name = name;
        this.description = description;
        this.expansion = expansion;
        this.type = type;
        this.actionType = actionTypes;
    }

    public IntrigueTypes getType() {
        return type;
    }

    public IntrigueActionTypes getActionType() {
        return actionType;
    }

    public Expansions getExpansion() {
        return expansion;
    }

    public int getIntrigueID() {
        return intrigueID;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
    
    
}

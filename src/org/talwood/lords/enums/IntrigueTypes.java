package org.talwood.lords.enums;

public enum IntrigueTypes {
    Attack("Attack"),
    Utility("Utility"),
    MandatoryQuest("Mandatory Quest");
    
    private String description;
    
    private IntrigueTypes(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    
}

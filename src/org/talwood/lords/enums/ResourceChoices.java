package org.talwood.lords.enums;

import org.talwood.lords.resources.Res;
import org.talwood.lords.resources.Resources;

public enum ResourceChoices {
    OneCleric("1 Cleric", new Resources(Res.cleric(1))),
    OneFighter("1 Fighter", new Resources(Res.fighter(1))),
    OneRogue("1 Rogue", new Resources(Res.rogue(1))),
    OneWizard("1 Wizard", new Resources(Res.wizard(1))),
    
    OneClericTwoCorruption("1 Cleric, 2 Corruption", new Resources(Res.cleric(1), Res.corruption(2))),
    OneFighterTwoCorruption("1 Fighter, 2 Corruption", new Resources(Res.fighter(1), Res.corruption(2))),
    OneRogueTwoCorruption("1 Rogue, 2 Corruption", new Resources(Res.rogue(1), Res.corruption(2))),
    OneWizardTwoCorruption("1 Wizard, 2 Corruption", new Resources(Res.wizard(1), Res.corruption(2))),
    
    // Need the ones for Smugglers Dock
    FourFighters("4 Fighters", new Resources(Res.fighter(4))),
    ThreeFightersOneRogue("3 Fighters, 1 Rogue", new Resources(Res.fighter(3), Res.rogue(1))),
    TwoFightersTwoRogues("2 Fighters, 2 Rogues", new Resources(Res.fighter(2), Res.rogue(2))),
    OneFighterThreeRogues("1 Fighter, 3 Rogues", new Resources(Res.fighter(1), Res.rogue(3))),
    FourRogues("4 Rogues", new Resources(Res.rogue(4))),
    
    // For House of Wonder (actually, all combinations of two adventurers)
    TwoClerics("2 Clerics", new Resources(Res.cleric(2))),
    OneClericOneWizard("1 Cleric, 1 Wizard", new Resources(Res.cleric(1), Res.wizard(1))),
    OneClericOneFighter("1 Cleric, 1 Fighter", new Resources(Res.cleric(1), Res.fighter(1))),
    OneClericOneRogue("1 Cleric, 1 Rogue", new Resources(Res.cleric(1), Res.rogue(1))),
    TwoFighters("2 Fighters", new Resources(Res.fighter(2))),
    OneFighterOneRogue("1 Fighter, 1 Rogue", new Resources(Res.fighter(1), Res.rogue(1))),
    OneFighterOneWizard("1 Fighter, 1 Wizard", new Resources(Res.fighter(1), Res.wizard(1))),
    TwoRogues("2 Rogues", new Resources(Res.rogue(2))),
    OneRogueOneWizard("1 Rogue, 1 Wizard", new Resources(Res.rogue(1), Res.wizard(1))),
    TwoWizards("2 Wizards", new Resources(Res.wizard(2))),
    
    // For House of Wonder (actually, all combinations of two adventurers)
    TwoClericsOneCorruption("2 Clerics", new Resources(Res.cleric(2), Res.corruption(1))),
    OneClericOneWizardOneCorruption("1 Cleric, 1 Wizard", new Resources(Res.cleric(1), Res.wizard(1), Res.corruption(1))),
    OneClericOneFighterOneCorruption("1 Cleric, 1 Fighter", new Resources(Res.cleric(1), Res.fighter(1), Res.corruption(1))),
    OneClericOneRogueOneCorruption("1 Cleric, 1 Rogue", new Resources(Res.cleric(1), Res.rogue(1), Res.corruption(1))),
    TwoFightersOneCorruption("2 Fighters", new Resources(Res.fighter(2), Res.corruption(1))),
    OneFighterOneRogueOneCorruption("1 Fighter, 1 Rogue", new Resources(Res.fighter(1), Res.rogue(1), Res.corruption(1))),
    OneFighterOneWizardOneCorruption("1 Fighter, 1 Wizard", new Resources(Res.fighter(1), Res.wizard(1), Res.corruption(1))),
    TwoRoguesOneCorruption("2 Rogues", new Resources(Res.rogue(2), Res.corruption(1))),
    OneRogueOneWizardOneCorruption("1 Rogue, 1 Wizard", new Resources(Res.rogue(1), Res.wizard(1), Res.corruption(1))),
    TwoWizardsOneCorruption("2 Wizards", new Resources(Res.wizard(2), Res.corruption(1))),
    
    // For the three pearls (and any other place needing a combination of three)
    ThreeClerics("3 Clerics", new Resources(Res.cleric(3))),
    TwoClericsOneFighter("2 Clerics, 1 Fighter", new Resources(Res.cleric(2), Res.fighter(1))),
    TwoClericsOneRogue("2 Clerics, 1 Rogue", new Resources(Res.cleric(2), Res.rogue(1))),
    TwoClericsOneWizard("2 Clerics, 1 Wizard", new Resources(Res.cleric(2), Res.wizard(1))),
    OneClericTwoFighters("1 Cleric, 2 Fighters", new Resources(Res.cleric(1), Res.fighter(2))),
    OneClericOneFighterOneRogue("1 Cleric, 1 Fighter, 1 Rogue", new Resources(Res.cleric(1), Res.fighter(1), Res.rogue(1))),
    OneClericOneFighterOneWizard("1 Cleric, 1 Fighter, 1 Wizard", new Resources(Res.cleric(1), Res.fighter(1), Res.wizard(1))),
    OneClericTwoRogues("1 Cleric, 2 Rogues", new Resources(Res.cleric(1), Res.rogue(2))),
    OneClericOneRogueOneWizard("1 Cleric, 1 Rogue, 1 Wizard", new Resources(Res.cleric(1), Res.rogue(1), Res.wizard(1))),
    OneClericTwoWizards("1 Cleric, 2 Wizards", new Resources(Res.cleric(1), Res.wizard(2))),
    ThreeFighters("3 Fighters", new Resources(Res.fighter(3))),
    TwoFightersOneRogue("2 Fighters, 1 Rogue", new Resources(Res.fighter(2), Res.rogue(1))),
    TwoFightersOneWizard("2 Fighters, 1 Wizard", new Resources(Res.fighter(2), Res.wizard(1))),
    OneFighterTwoRogues("1 Fighter, 2 Rogues", new Resources(Res.fighter(1), Res.rogue(2))),
    OneFighterOneRogueOneWizard("1 Fighter, 1 Rogue, 1 Wizard", new Resources(Res.fighter(1), Res.rogue(1), Res.wizard(1))),
    OneFighterTwoWizards("1 Fighter, 2 Wizards", new Resources(Res.fighter(1), Res.wizard(2))),
    ThreeRogues("3 Rogues", new Resources(Res.rogue(3))),
    TwoRoguesOneWizard("2 Rogues, 1 Wizard", new Resources(Res.rogue(2), Res.wizard(1))),
    OneRogueTwoWizards("1 Rogue, 2 Wizards", new Resources(Res.rogue(1), Res.wizard(2))),
    ThreeWizards("3 Wizards", new Resources(Res.wizard(3))),
    
    OneClericTwoGold("1 Cleric, 2 Gold", new Resources(Res.cleric(1), Res.gold(2))),
    OneFighterTwoGold("1 Fighter, 2 Gold", new Resources(Res.fighter(1), Res.gold(2))),
    OneRogueTwoGold("1 Rogue, 2 Gold", new Resources(Res.rogue(1), Res.gold(2))),
    OneWizardTwoGold("1 Wizard, 2 Gold", new Resources(Res.wizard(1), Res.gold(2))),
    
    FourGold("4 Gold", new Resources(Res.gold(4))),
    TwoGold("2 Gold", new Resources(Res.gold(2))),
    
    ;
    
    private String description;
    private Resources resources;
    
    
    private ResourceChoices(String description, Resources resources) {
        this.description = description;
        this.resources = resources;
    }

    public String getDescription() {
        return description;
    }

    public Resources getResources() {
        return new Resources(resources);
    }
    
    
}

package org.talwood.lords.enums;

public enum IntrigueActions {
    OneOpponentGetsOneResource,
    RequiresVariableResource,
    ResourcesForVictoryPoints,
    GiveResourcesForVictoryPoints,
    GainResourceChoice,
    RequiresReturningAgent,
    RequiresHouseDraw,
    StealResourcesFromPlayer,
    PlaceExtraOnOneSpace,
    PlaceExtraOnTwoSpaces,
    ReturnTwoCorruptionAndResource,
    RequiresMandatoryQuestTarget,
}

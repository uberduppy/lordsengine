package org.talwood.lords.enums;

public enum Houses {
    Warfare(1, "Warfare",  "#000000", "#e0791d"),
    Piety(2, "Piety", "#000000", "#dddddd"),
    Arcana(3, "Arcana", "#ffffff", "#8800d4"),
    Commerce(4, "Commerce", "#ffffff", "#306b31"),
    Skullduggery(5, "Skullduggery", "#ffffff", "#606060");
    
    private int house;
    private String description;
    private String foreground;
    private String background;
    
    private Houses(int house, String description, String foreground, String background) {
        this.house = house;
        this.description = description;
        this.foreground = foreground;
        this.background = background;
    }

    public int getHouse() {
        return house;
    }

    public String getDescription() {
        return description;
    }

    public String getForeground() {
        return foreground;
    }

    public String getBackground() {
        return background;
    }
    
}

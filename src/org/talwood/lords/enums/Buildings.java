package org.talwood.lords.enums;

public enum Buildings {
    AurorasRealm(1, "Aurora's Realm Shop", "Take 4 Gold", Expansions.Classic),
    BlackstaffTower(2, "Blackstaff Tower", "Take 1 Wizard",  Expansions.Classic),
    BuildersHall(3, "Builder's Hall", "Choose 1 Building, pay it's cost, and put it into play", Expansions.Classic),
    CastleWaterdeep(4, "Castle Waterdeep", "Take 1 Intrigue and first player marker", Expansions.Classic),
    CliffwatchInnIntrigue(5, "Cliffwatch Inn - Draw Intrigue", "Take 1 Quest and 1 Intrigue", Expansions.Classic),
    CliffwatchInnResetQuests(6, "Cliffwatch Inn - Reset Quests", "Reset Quests, then take 1 Quest", Expansions.Classic),
    CliffwatchInnTwoGold(7, "Cliffwatch Inn - 2 Gold", "Take 1 Quest and 2 Gold", Expansions.Classic),
    FieldOfTriumph(8, "Field of Triumph", "Take 2 Fighters", Expansions.Classic),
    GrinningLionTavern(9, "Grinning Lion Tavern", "Take 2 Rogues", Expansions.Classic),
    ThePlinth(10, "The Plinth", "Take 1 Cleric", Expansions.Classic),
    WaterdeepHarborOne(11, "Waterdeep Harbor (1)", "Play an intrigue card", Expansions.Classic),
    WaterdeepHarborTwo(12, "Waterdeep Harbor (2)", "Play an intrigue card", Expansions.Classic),
    WaterdeepHarborThree(13, "Waterdeep Harbor (3)", "Play an intrigue card", Expansions.Classic),
    // Undermountain free buildings
    EntryWell(14, "Entry Well", "Take a quest, then play an intrigue", Expansions.Undermountian),
    HallOfMirrors(15, "Hall of Mirrors", "Take 1 of any resource, or 1 Fighter and 1 Rogue", Expansions.Undermountian),
    TheGrimStatue(16, "The Grim Statue", "Draw 2 intrigue cards", Expansions.Undermountian),
    // Skullport free buildings
    HallOfTheVoice(17, "Hall of the Voice", "Take 1 Quest, draw 1 Intrigue, take 5 Gold and 1 Corruption", Expansions.Skullport),
    SlaversMarket(18, "Slavers' Market", "Take 2 Fighters, 2 Rogus and 1 Corruption", Expansions.Skullport),
    SkullIsland(19, "Skull Island", "Take 2 of any resource and 1 Corruption", Expansions.Skullport),
    

    // Here are the purchased buildings defined.
    CaravanCourt(20, "Caravan Court", "Take all Fighters from this space.", 4, Expansions.Classic),
    DragonTower(21, "Dragon Tower", "Take 1 Wizard, draw 1 Intrigue.", 3, Expansions.Classic),
    FetlockCourt(22, "Fetlock Court", "Take 2 Fighters and 1 Wizard", 8, Expansions.Classic),
    GoldenHorn(23, "The Golden Horn", "Take all Gold from this space", 4, Expansions.Classic),
    HelmstarWarehouse(24, "Helmstar Warehouse", "Take 2 Rogues and 2 Gold", 3, Expansions.Classic),
    HeroesGarden(25, "The Heroes' Garden", "Take 1 face-up Quest", 4, Expansions.Classic),
    HouseOfGoodSpirits(26, "House of Good Spirits", "Take 1 Fighter and 1 adventurer", 3, Expansions.Classic),
    HouseOfHeroes(27, "House of Heroes", "Take 1 Cleric and 2 Fighters", 8, Expansions.Classic),
    HouseOfTheMoon(28, "House of the Moon", "Take 1 Cleric and 1 faceup Quest", 3, Expansions.Classic),
    HouseOfWonder(29, "House of Wonder", "Pay 2 Gold: Take 2 of a combination of Clerics/Wizards", 4, Expansions.Classic),
    JestersCourt(30, "Jesters' Court", "Take all Rogues from this space.", 4, Expansions.Classic),
    NewOlamn(31, "New Olamn", "Take 2 Rogues and 1 Wizard", 8, Expansions.Classic),
    Northgate(32, "Northgate", "Take 1 Adventurer and 2 Gold", 3, Expansions.Classic),
    PalaceOfWaterdeep(33, "Palace of Waterdeep", "Take the Ambassador", 4, Expansions.Classic),
    Skulkway(34, "The Skulkway", "Take 1 Fighter, 1 Rogue and 2 Gold", 4, Expansions.Classic),
    SmugglersDock(35, "Smuggler's Dock", "Pay 2 Gold, Take any combination of 4 Fighters or Rogues", 4, Expansions.Classic),
    SpiresOfTheMorning(36, "Spires of the Morning", "Take all Clerics from this space.", 4, Expansions.Classic),
    StoneHouse(37, "Stone House", "Gain one Gold for each building in play.", 4, Expansions.Classic),
    ThreePearls(38, "Three Pearls", "Return 2 Adventurers: Gain 3 Adventurers", 4, Expansions.Classic),
    TowerOfLuck(39, "Tower of Luck", "Take 1 Cleric and 2 Fighters", 8, Expansions.Classic),
    TowerOfTheOrder(40, "Tower of the Order", "Take all Wizards from this space", 4, Expansions.Classic),
    Waymoot(41, "The Waymoot", "Take all the Victory points from this space", 4, Expansions.Classic),
    YawningPortal(42, "The Yawning Portal", "Take 2 Adventurers", 4, Expansions.Classic),
    Zoarstar(43, "The Zoarstar", "Place agent on space an opponent is on", 8, Expansions.Classic),
    
    // Undermountain
    BelkramsTomb(44, "Belkram's Tomb", "Pay 5 gold, play an intrigue", 5, Expansions.Undermountian),
    CitadelOfTheBloodyHand(45, "Citadel of the Bloody Hand", "Take 4 Fighters.<br>Place 1 Fighter from the supply<br>on each of 2 different action spaces", 7, Expansions.Undermountian),
    EyesLair(46, "The Eye's Lair", "Take 1 Adventurer and play an intrigue", 3, Expansions.Undermountian),
    HallOfManyPillars(47, "Hall of Many Pillars", "Play 3 intrigues", 5, Expansions.Undermountian),
    HallOfSleepingKings(48, "Hall of Sleeping Kings", "Take 1 Fighter, 1 Rogue and play an intrigue", 4, Expansions.Undermountian),
    HallOfThreeLords(49, "Hall of Three Lords", "Remove 3 Adventurers and place them on 3 different action spaces. Gain 10 VP", 6, Expansions.Undermountian),
    HighDukesTomb(50, "High Duke's Tomb", "Take 8 Gold.<br>Place 2 Gold from the supply<br>on each of 2 different action spaces", 7, Expansions.Undermountian),
    Librarium(51, "The Librarium", "Take 2 Wizards<br>Place 1 Wizard from the supply<br>on 1 action space.", 7, Expansions.Undermountian),
    LostCavern(52, "The Lost Cavern", "Discard 1 non-Mandatory Quest:<br>Take 1 Fighter, 1 Rogue ans 1 Adventurer", 6, Expansions.Undermountian),
    RoomOfWisdom(53, "Room of Wisdom", "Take 2 Clerics<br>Place 1 Cleric from the supply<br>on 1 action space.", 7, Expansions.Undermountian),
    ShadowduskHold(54, "Shadowdusk Hold", "Take 4 Rogues.<br>Place 1 Rogue from the supply<br>on each of 2 different action spaces", 7, Expansions.Undermountian),
    TroibriandsGraveyard(55, "Trobriand's Graveyard", "Take 4 Gold, draw 2 Intrigue, then discard 2 Intrigue", 3, Expansions.Undermountian),
    
    
    // Skullport
    CryptkeyFacilitations(60, "Cryptkey Facilitations", "Take 3 Rogues, 5 gold and a Corruption counter", 7, Expansions.Skullport),
    Deepfires(61, "The Deepfires", "Take 1 adventurer, 5 Gold, 1 Corrpution counter and 1 face-up Quest", 6, Expansions.Skullport),
    DelversFolly(62, "Delver's Folly", "Remove 1 corruption from your Tavern and place it<br>on any action space", 6, Expansions.Skullport),
    FrontalLobe(64, "The Frontal Lobe", "Return 1 Adventurer: Take 3 Wizards and 1 Corruption", 4, Expansions.Skullport),
    HellHoundsMuzzle(65, "The Hell Hound's Muzzle", "Take 1 Cleric, 1 Fighter, 1 Rogue, 1 Wizard and 1 Corruption", 8, Expansions.Skullport),
    MonstersMadeToOrder(66, "Monsters Made to Order", "Take all Corruption from this space.<br>Return 1 agent for each Corruption taken", 3, Expansions.Skullport),
    PoisonedQuill(67, "The Poisoned Quill", "Return 1 Corruption: Play an Intrigue", 5, Expansions.Skullport),
    PromenadeOfTheDarkMaiden(68, "Promenade of the Dark Maiden", "Remove up to 2 Corruption in your tavern from the game", 9, Expansions.Skullport),
    SecretShrine(69, "Secret Shrine", "Return 1 Corruption: Take 1 Cleric", 8, Expansions.Skullport),
    ShiradinsExcellentZombies(70, "Shiradin's Excellent Zombies", "Take 3 Fighters, 1 Cleric and 1 Corruption", 6, Expansions.Skullport),
    ThimblewinesPawnshop(71, "Thimblewine's Pawnshop", "Return 1 Corruption: Take 1 Gold", 4, Expansions.Skullport),
    ThrownGauntlet(72, "The Thrown Gauntlet", "Take 3 Fighters, 3 Rogues and 1 Corruption", 8, Expansions.Skullport),
    
    ;
    
    private Expansions expansion;
    private int cost; // 0 means in play at start
    private int buildingID;
    private String name;
    private String description;
    
    private Buildings(int buildingID, String name, String description, Expansions expansion) {
        this(buildingID, name, description, 0, expansion);
    }
    private Buildings(int buildingID, String name, String description, int cost, Expansions expansion) {
        this.buildingID = buildingID;
        this.name = name;
        this.cost = cost;
        this.description = description;
        this.expansion = expansion;
    }

    public Expansions getExpansion() {
        return expansion;
    }

    public int getCost() {
        return cost;
    }

    public int getBuildingID() {
        return buildingID;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
    
    
    
}

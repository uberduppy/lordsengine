package org.talwood.lords.resources;

public class BuildingOwnerReward extends Resources {
    public BuildingOwnerReward() {
        super();
    }
    
    public BuildingOwnerReward(Res... res) {
        super(res);
    }
    
    public BuildingOwnerReward(Resources res) {
        super(res);
    }
    
    public static BuildingOwnerReward makeReward(Res... res) {
        return new BuildingOwnerReward(res);
    }
}

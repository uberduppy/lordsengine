package org.talwood.lords.resources;

public class BuildingActivationCost extends Resources {
    public BuildingActivationCost() {
        super();
    }
    
    public BuildingActivationCost(Res... res) {
        super(res);
    }
    
    public BuildingActivationCost(Resources res) {
        super(res);
    }
}

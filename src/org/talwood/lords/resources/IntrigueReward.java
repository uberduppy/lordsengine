package org.talwood.lords.resources;

public class IntrigueReward extends Resources {
    public IntrigueReward() {
        super();
    }
    
    public IntrigueReward(Res... res) {
        super(res);
    }
    
    public IntrigueReward(Resources res) {
        super(res);
    }
}

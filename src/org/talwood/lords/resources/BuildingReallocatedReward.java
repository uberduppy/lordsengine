package org.talwood.lords.resources;

public class BuildingReallocatedReward extends Resources {
    
    private int numberOfReallocations;
    public BuildingReallocatedReward() {
        super();
        numberOfReallocations = 1;
    }
    
    public BuildingReallocatedReward(int numberOfReallocations, Res... res) {
        super(res);
        this.numberOfReallocations = numberOfReallocations;
    }
    
    public BuildingReallocatedReward(BuildingReallocatedReward res) {
        super(res);
        this.numberOfReallocations = res.getNumberOfReallocations();
    }
    
    public static BuildingReallocatedReward makeReward(int numberOfReallocations, Res... res) {
        return new BuildingReallocatedReward(numberOfReallocations, res);
    }

    public int getNumberOfReallocations() {
        return numberOfReallocations;
    }
    
    
}

package org.talwood.lords.resources;

public class BuildingReward extends Resources {
    public BuildingReward() {
        super();
    }
    
    public BuildingReward(Res... res) {
        super(res);
    }
    
    public BuildingReward(Resources res) {
        super(res);
    }
    
    public static BuildingReward makeReward(Res... res) {
        return new BuildingReward(res);
    }
}

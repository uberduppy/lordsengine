package org.talwood.lords.resources;

import org.talwood.lords.enums.ResourceTypes;

public class Res {
    private int count;
    private ResourceTypes resources; 
    
    private Res() {}
    
    private Res(ResourceTypes resources, int count) {
        this.resources = resources;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public ResourceTypes getResources() {
        return resources;
    }
    
    public static Res anyResource(int count) {
        return new Res(ResourceTypes.AnyResource, count);
    }
    
    public static Res intrigue(int count) {
        return new Res(ResourceTypes.Intrigue, count);
    }
    
    public static Res fighter(int count) {
        return new Res(ResourceTypes.Fighter, count);
    }
    
    public static Res rogue(int count) {
        return new Res(ResourceTypes.Rogue, count);
    }
    
    public static Res wizard(int count) {
        return new Res(ResourceTypes.Wizard, count);
    }
    
    public static Res cleric(int count) {
        return new Res(ResourceTypes.Cleric, count);
    }
    
    public static Res corruption(int count) {
        return new Res(ResourceTypes.Corruption, count);
    }
    
    public static Res gold(int count) {
        return new Res(ResourceTypes.Gold, count);
    }
    
    public static Res victoryPoints(int count) {
        return new Res(ResourceTypes.VictoryPoints, count);
    }
    
    
    
}

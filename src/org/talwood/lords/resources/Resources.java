package org.talwood.lords.resources;

import org.talwood.lords.enums.ResourceTypes;

public class Resources {

    private int fighter;
    private int rogue;
    private int wizard;
    private int cleric;
    private int gold;
    private int anyResource;
    private int intrigue;
    private int victoryPoints;
    private int corruption;
    
    public Resources() {
        
    }
    
    public Resources(Res... r) {
        incrementFromResources(r);
    }
    
    public Resources(Resources wbp) {
        this.cleric = wbp.cleric;
        this.fighter = wbp.fighter;
        this.gold = wbp.gold;
        this.rogue = wbp.rogue;
        this.wizard = wbp.wizard;
        this.corruption = wbp.corruption;
        this.intrigue = wbp.intrigue;
    }
    
    public void reset() {
        fighter = rogue = wizard = cleric = gold = 0;
    }
    
    public void incrementFromResources(Resources wbp) {
        addAnyResource(wbp.anyResource);
        addCleric(wbp.cleric);
        addFighter(wbp.fighter);
        addWizard(wbp.wizard);
        addRogue(wbp.rogue);
        addGold(wbp.gold);
        addCorruption(wbp.corruption);
    }
    public void decrementFromResources(Resources wbp) {
        removeCleric(wbp.cleric);
        removeFighter(wbp.fighter);
        removeWizard(wbp.wizard);
        removeRogue(wbp.rogue);
        removeGold(wbp.gold);
        removeCorruption(wbp.corruption);
    }
    public void incrementFromResources(Res... res) {
        for(Res r : res) {
            switch(r.getResources()) {
                case Cleric:
                    addCleric(r.getCount());
                    break;
                case Fighter:
                    addFighter(r.getCount());
                    break;
                case Rogue:
                    addRogue(r.getCount());
                    break;
                case Wizard:
                    addWizard(r.getCount());
                    break;
                case Gold:
                    addGold(r.getCount());
                    break;
                case AnyResource:
                    addAnyResource(r.getCount());
                    break;
                case VictoryPoints:
                    addVictoryPoints(r.getCount());
                    break;
                case Intrigue:
                    addIntrigue(r.getCount());
                    break;
                case Corruption:
                    addCorruption(r.getCount());
                    break;
                default:
                    throw new UnsupportedOperationException("Wrong resource type: " + r.getResources().getDescription());
                    
            }
        }
    }
    public void decrementFromResources(Res... res) {
    for(Res r : res) {
            switch(r.getResources()) {
                case Cleric:
                    removeCleric(r.getCount());
                    break;
                case Fighter:
                    removeFighter(r.getCount());
                    break;
                case Rogue:
                    removeRogue(r.getCount());
                    break;
                case Wizard:
                    removeWizard(r.getCount());
                    break;
                case Gold:
                    removeGold(r.getCount());
                    break;
                case Corruption:
                    removeCorruption(r.getCount());
                    break;
                case AnyResource:
                    removeAnyResource(r.getCount());
                    break;
                default:
                    throw new UnsupportedOperationException("Wrong resource type: " + r.getResources().getDescription());
            }
        }
    }
    public void populateFromResources(Res... res) {
        fighter = rogue = wizard = cleric = gold = 0;
        incrementFromResources(res);
    }
    
    public boolean containsResources(Resources pool) {
        return fighter >= pool.getFighter() 
            && rogue >= pool.getRogue()
            && wizard >= pool.getWizard()
            && cleric >= pool.getCleric()
            && gold >= pool.getGold();
    }
    
    public boolean isNotEmpty() {
        return (fighter > 0 || rogue > 0 || wizard > 0 || cleric > 0 || gold > 0 || victoryPoints > 0 || corruption > 0 || intrigue > 0);
    }
    
    public boolean isEmpty() {
        return !isNotEmpty();
    }
    
    public int getCritterCount() {
        return fighter + rogue + wizard + cleric;
    }
    public int getFighter() {
        return fighter;
    }

    public void setFighter(int fighter) {
        this.fighter = fighter;
    }

    public int getRogue() {
        return rogue;
    }

    public void setRogue(int rogue) {
        this.rogue = rogue;
    }

    public int getWizard() {
        return wizard;
    }

    public void setWizard(int wizard) {
        this.wizard = wizard;
    }

    public int getCleric() {
        return cleric;
    }

    public int getAnyResource() {
        return anyResource;
    }

    public void setCleric(int cleric) {
        this.cleric = cleric;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getIntrigue() {
        return intrigue;
    }

    public void setIntrigue(int intrigue) {
        this.intrigue = intrigue;
    }

    public int getCorruption() {
        return corruption;
    }

    public void setCorruption(int corruption) {
        this.corruption = corruption;
    }

    
    public void addFighter(int fighter) {
        this.fighter += fighter;
    }

    public void addRogue(int rogue) {
        this.rogue += rogue;
    }

    public void addWizard(int wizard) {
        this.wizard += wizard;
    }

    public void addCleric(int cleric) {
        this.cleric += cleric;
    }

    public void addCorruption(int corruption) {
        this.corruption += corruption;
    }

    public void addGold(int gold) {
        this.gold += gold;
    }
    
    public void addAnyResource(int anyResource) {
        this.anyResource += anyResource;
    }
    
    public void addVictoryPoints(int victoryPoints) {
        this.victoryPoints += victoryPoints;
    }
    
    public void addIntrigue(int intrigueCount) {
        this.intrigue += intrigueCount;
    }
    
    public void assertEnough(int start, int toRemove) {
        if(toRemove > start) {
            throw new UnsupportedOperationException("You can't remove more than you own.");
        }
    }

    public void removeFighter(int fighter) {
        assertEnough(this.fighter, fighter);
        this.fighter -= fighter;
    }

    public void removeRogue(int rogue) {
        assertEnough(this.rogue, rogue);
        this.rogue -= rogue;
    }

    public void removeWizard(int wizard) {
        assertEnough(this.wizard, wizard);
        this.wizard -= wizard;
    }

    public void removeCleric(int cleric) {
        assertEnough(this.cleric, cleric);
        this.cleric -= cleric;
    }

    public void removeCorruption(int corruption) {
        assertEnough(this.corruption, corruption);
        this.corruption -= corruption;
    }

    public void removeGold(int gold) {
        assertEnough(this.gold, gold);
        this.gold -= gold;
    }

    public void removeAnyResource(int anyResource) {
        assertEnough(this.anyResource, anyResource);
        this.anyResource -= anyResource;
    }

    public int getTotalAdventurers() {
        return fighter + rogue + cleric + wizard;
    }
    
    public String makeDisplayable() {
        StringBuilder sb = new StringBuilder();
        
        // In order...
        // Cleric, Fighter, Rogue, Wizard, Gold, Corruption
        ResourceTypes.Cleric.appendDescription(sb, cleric);
        ResourceTypes.Fighter.appendDescription(sb, fighter);
        ResourceTypes.Rogue.appendDescription(sb, rogue);
        ResourceTypes.Wizard.appendDescription(sb, wizard);
        ResourceTypes.Gold.appendDescription(sb, gold);
        
        return sb.toString();
    }

    public boolean hasAnyWildcardResources() {
        return anyResource > 0;
    }
    
}

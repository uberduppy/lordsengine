package org.talwood.lords.resources;

public class MandatoryQuestCost extends Resources {
    public MandatoryQuestCost() {
        super();
    }
    
    public MandatoryQuestCost(Res... res) {
        super(res);
    }
    
    public MandatoryQuestCost(Resources res) {
        super(res);
    }
}

package org.talwood.lords.resources;

public class IntrigueOpponentResources extends Resources {
    public IntrigueOpponentResources() {
        super();
    }
    
    public IntrigueOpponentResources(Res... res) {
        super(res);
    }
    
    public IntrigueOpponentResources(Resources res) {
        super(res);
    }
}

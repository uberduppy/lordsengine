package org.talwood.lords.resources;

public class BuildingExtraReward extends Resources {
    public BuildingExtraReward() {
        super();
    }
    
    public BuildingExtraReward(Res... res) {
        super(res);
    }
    
    public BuildingExtraReward(Resources res) {
        super(res);
    }
    
    public static BuildingExtraReward makeReward(Res... res) {
        return new BuildingExtraReward(res);
    }
}

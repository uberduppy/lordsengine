package org.talwood.lords.resources;

public class BuildingStackedReward extends Resources {
    public BuildingStackedReward() {
        super();
    }
    
    public BuildingStackedReward(Res... res) {
        super(res);
    }
    
    public BuildingStackedReward(Resources res) {
        super(res);
    }
    
    public static BuildingStackedReward makeReward(Res... res) {
        return new BuildingStackedReward(res);
    }
}

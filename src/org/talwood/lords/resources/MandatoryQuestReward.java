package org.talwood.lords.resources;

public class MandatoryQuestReward extends Resources {
    public MandatoryQuestReward() {
        super();
    }
    
    public MandatoryQuestReward(Res... res) {
        super(res);
    }
    
    public MandatoryQuestReward(Resources res) {
        super(res);
    }
}

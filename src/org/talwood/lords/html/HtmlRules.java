
package org.talwood.lords.html;

import java.io.Serializable;

public class HtmlRules implements Serializable {
    private boolean breaksWanted;
    private boolean breaksAfterCompletion;
    private boolean hardClose;
    private int indention;
    private int indentionSpacing;
    
    public HtmlRules() {
        
    }

    public HtmlRules(boolean breaksWanted, boolean hardClose, boolean breaksAfterCompletion) {
        this.breaksWanted = breaksWanted;
        this.breaksAfterCompletion = breaksAfterCompletion;
        this.hardClose = hardClose;
    }
    
    public HtmlRules(boolean breaksWanted, boolean hardClose, boolean breaksAfterCompletion, int indention, int indentionSpacing) {
        this.breaksWanted = breaksWanted;
        this.breaksAfterCompletion = breaksAfterCompletion;
        this.hardClose = hardClose;
        this.indention = indention;
        this.indentionSpacing = indentionSpacing;
    }
    
    public HtmlRules makeClone() {
        return new HtmlRules(isBreaksWanted(), isHardClose(), isBreaksAfterCompletion(), getIndention(), getIndentionSpacing());
    }
    
    public boolean isBreaksWanted() {
        return breaksWanted;
    }

    public void setBreaksWanted(boolean breaksWanted) {
        this.breaksWanted = breaksWanted;
    }

    public boolean isHardClose() {
        return hardClose;
    }

    public void setHardClose(boolean hardClose) {
        this.hardClose = hardClose;
    }

    public int getIndention() {
        return indention;
    }

    public void setIndention(int indention) {
        this.indention = indention;
    }

    public int getIndentionSpacing() {
        return indentionSpacing;
    }

    public void setIndentionSpacing(int indentionSpacing) {
        this.indentionSpacing = indentionSpacing;
    }

    public boolean isBreaksAfterCompletion() {
        return breaksAfterCompletion;
    }

    public void setBreaksAfterCompletion(boolean breaksAfterCompletion) {
        this.breaksAfterCompletion = breaksAfterCompletion;
    }
}

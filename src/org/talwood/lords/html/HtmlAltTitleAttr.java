package org.talwood.lords.html;

public class HtmlAltTitleAttr extends HtmlAttr {
    private static final long serialVersionUID = 1L;
    protected HtmlAltTitleAttr() {
        super();
    }
    
    public HtmlAltTitleAttr(String value) {
        super(null, value);
    }

    @Override
    public String render() {
        StringBuilder result = new StringBuilder();
        if(getValue() != null) {
            result.append(" alt=").append("\"").append(getValue()).append("\"");
            result.append(" title=").append("\"").append(getValue()).append("\"");
        }
        return result.toString();
    }

}

package org.talwood.lords.html;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.talwood.lords.helpers.StringHelper;

public class HtmlElement extends HtmlBaseElement implements Serializable {
    
    private List<HtmlAttr> attributes = new ArrayList<HtmlAttr>();
    private List<HtmlElement> elements = new ArrayList<HtmlElement>();
    private HtmlRules rules = new HtmlRules(true, true, false);
    
    protected HtmlElement() {
        super();
    }
    
    public HtmlElement(String name) {
        super(name, null);
    }
    
    public HtmlElement(HtmlElementType type) {
        this(type, null, (HtmlAttr[])null);
    }
    
    public HtmlElement(String name, String value) {
        super(name, value);
    }

    public HtmlElement(HtmlElementType type, String value) {
        this(type, value, (HtmlAttr[])null);
    }
    
    
    public HtmlElement(String name, HtmlAttr... attrs) {
        this(name);
        if(attrs != null) {
            attributes.addAll(Arrays.asList(attrs));
        }
    }
    
    public HtmlElement(HtmlElementType type, HtmlAttr... attrs) {
        this(type);
        if(attrs != null) {
            attributes.addAll(Arrays.asList(attrs));
        }
    }
    
    public HtmlElement(String name, String value, HtmlAttr... attrs) {
        this(name, value);
        if(attrs != null) {
            attributes.addAll(Arrays.asList(attrs));
        }
    }
    
    public HtmlElement(HtmlElementType type, String value, HtmlAttr... attrs) {
        super(type.getElementName(), value);
        if(type.getDefaultRules() != null) {
            rules = type.getDefaultRules().makeClone();
        }
        if(type.getDefaultAttributes() != null) {
            attributes.addAll(Arrays.asList(type.getDefaultAttributes()));
        }
        if(attrs != null) {
            attributes.addAll(Arrays.asList(attrs));
        }
    }
    
    public HtmlElement makeEmptyTR() {
        return createChildElement(HtmlElementType.TR);
    }
    
    public HtmlElement createChildElement(HtmlElementType type) {
        HtmlElement child = new HtmlElement(type);
        HtmlRules childRules = child.getRules();
        childRules.setIndention(rules.getIndention() + rules.getIndentionSpacing());
        childRules.setIndentionSpacing(rules.getIndentionSpacing());
        getElements().add(child);
        return child;
    }
    
    public HtmlElement createChildElement(String name) {
        HtmlElement child = new HtmlElement(name);
        HtmlRules childRules = child.getRules();
        childRules.setIndention(rules.getIndention() + rules.getIndentionSpacing());
        childRules.setIndentionSpacing(rules.getIndentionSpacing());
        childRules.setBreaksWanted(rules.isBreaksWanted());
        childRules.setHardClose(rules.isHardClose());
        getElements().add(child);
        return child;
    }

    public HtmlElement createChildElement(HtmlElementType type, HtmlRules overrideRules, HtmlAttr... attrs) {
        HtmlElement child = new HtmlElement(type, attrs);
        child.setRules(overrideRules);
        getElements().add(child);
        return child;
    }
    public HtmlElement createChildElement(HtmlElementType type, HtmlAttr... attrs) {
        HtmlElement child = new HtmlElement(type, attrs);
        HtmlRules childRules = child.getRules();
        childRules.setIndention(rules.getIndention() + rules.getIndentionSpacing());
        childRules.setIndentionSpacing(rules.getIndentionSpacing());
        getElements().add(child);
        return child;
    }
    
    public HtmlElement createChildElement(String name, HtmlAttr... attrs) {
        HtmlElement child = new HtmlElement(name, attrs);
        HtmlRules childRules = child.getRules();
        childRules.setIndention(rules.getIndention() + rules.getIndentionSpacing());
        childRules.setIndentionSpacing(rules.getIndentionSpacing());
        childRules.setBreaksWanted(rules.isBreaksWanted());
        childRules.setHardClose(rules.isHardClose());
        getElements().add(child);
        return child;
    }

    public HtmlElement createChildElement(HtmlElementType type, String value, HtmlAttr... attrs) {
        HtmlElement child = new HtmlElement(type, value, attrs);
        HtmlRules childRules = child.getRules();
        childRules.setIndention(rules.getIndention() + rules.getIndentionSpacing());
        childRules.setIndentionSpacing(rules.getIndentionSpacing());
        getElements().add(child);
        return child;
        
    }
    public HtmlElement createChildElement(String name, String value, HtmlAttr... attrs) {
        HtmlElement child = new HtmlElement(name, value, attrs);
        HtmlRules childRules = child.getRules();
        childRules.setIndention(rules.getIndention() + rules.getIndentionSpacing());
        childRules.setIndentionSpacing(rules.getIndentionSpacing());
        childRules.setBreaksWanted(rules.isBreaksWanted());
        childRules.setHardClose(rules.isHardClose());
        getElements().add(child);
        return child;
    }

    @Override
    public String render() {
        // Start the process by building the innards.
        StringBuilder sb = new StringBuilder();
        for(HtmlElement child : elements) {
            sb.append(child.render());
        }
        if(!StringHelper.isEmpty(getValue())) {
            if(rules.getIndention() > 0) {
                sb.append(StringHelper.createString(' ', rules.getIndention() + rules.getIndentionSpacing()));
            }
            sb.append(getValue());
        }
        // Now, let's build the outer entry...
        String endLine = rules.isBreaksAfterCompletion() ? "\r\n" : "";
        return HtmlHelper.renderData(getName(), sb.toString(), rules, attributes) + endLine;
    }
    
    public HtmlElement append(String data) {
        if(!StringHelper.isEmpty(data)) {
            appendData(data);
        }
        return this;
    }

    public HtmlElement append(HtmlElement element) {
        if(element != null) {
            appendData(element.render());
        }
        return this;
    }

    public HtmlElement appendToList(HtmlElement element) {
        if(element != null) {
            elements.add(element);
        }
        return this;
    }

    public HtmlElement appendToList(HtmlAttr attr) {
        if(attr != null) {
            attributes.add(attr);
        }
        return this;
    }
    
    public List<HtmlAttr> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<HtmlAttr> attributes) {
        this.attributes = attributes;
    }

    public void addAttributes(HtmlAttr... attributes) {
        if(attributes != null) {
            this.attributes.addAll(Arrays.asList(attributes));
        }
    }
    
    public List<HtmlElement> getElements() {
        return elements;
    }

    public void setElements(List<HtmlElement> elements) {
        this.elements = elements;
    }

    public HtmlRules getRules() {
        return rules;
    }

    public void setRules(HtmlRules rules) {
        this.rules = rules;
    }

    
}

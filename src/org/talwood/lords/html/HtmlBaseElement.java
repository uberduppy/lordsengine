package org.talwood.lords.html;

import org.talwood.lords.helpers.StringHelper;



public abstract class HtmlBaseElement {
    private String name;
    private StringBuilder value = new StringBuilder();
    
    protected HtmlBaseElement() {}
    
    public HtmlBaseElement(String name, String value) {
        this.name = name;
        if(!StringHelper.isEmpty(value)) {
            this.value.append(value);
        }
    }
    
    public abstract String render();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value.toString();
    }

    public void setValue(String data) {
        value = new StringBuilder();
        value.append(data);
    }
    
    protected void appendData(String data) {
        value.append(data);
    }
}

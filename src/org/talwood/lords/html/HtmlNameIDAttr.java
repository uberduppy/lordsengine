package org.talwood.lords.html;

public class HtmlNameIDAttr extends HtmlAttr {
    private static final long serialVersionUID = 1L;
    protected HtmlNameIDAttr() {
        super();
    }
    
    public HtmlNameIDAttr(String value) {
        super(null, value);
    }

    @Override
    public String render() {
        StringBuilder result = new StringBuilder();
        if(getValue() != null) {
            result.append(" name=").append("\"").append(getValue()).append("\"");
            result.append(" id=").append("\"").append(getValue()).append("\"");
        }
        return result.toString();
    }
}

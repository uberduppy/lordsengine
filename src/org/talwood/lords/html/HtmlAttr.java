package org.talwood.lords.html;

import java.io.Serializable;

public class HtmlAttr extends HtmlBaseElement implements Serializable {
    private static final long serialVersionUID = 1L;

    protected HtmlAttr() {
        super();
    }
    
    public HtmlAttr(String name, String value) {
        super(name, value);
    }
    
    @Override
    public String render() {
        StringBuilder sb = new StringBuilder();
        sb.append(" ").append(getName()).append("=\"").append(getValue()).append("\"");
        return sb.toString();
    }

    public static HtmlAttr align(String value) {
        return new HtmlAttr("align", value);
    }
    
    public static HtmlAttr alt(String value) {
        return new HtmlAttr("alt", value);
    }
    
    public static HtmlAttr altAndTitle(String value) {
        return new HtmlAltTitleAttr(value);
    }

    public static HtmlAttr bgcolor(String value) {
        return new HtmlAttr("bgcolor", value);
    }
    
    public static HtmlAttr border(String value) {
        return new HtmlAttr("border", value);
    }
    
    public static HtmlAttr cellpadding(String value) {
        return new HtmlAttr("cellpadding", value);
    }
    
    public static HtmlAttr cellspacing(String value) {
        return new HtmlAttr("cellspacing", value);
    }
    
    public static HtmlAttr color(String value) {
        return new HtmlAttr("color", value);
    }
    
    public static HtmlAttr colspan(String value) {
        return new HtmlAttr("colspan", value);
    }
    
    public static HtmlAttr checked(String value) {
        return new HtmlAttr("checked", value);
    }
    
    public static HtmlAttr frame(String value) {
        return new HtmlAttr("frame", value);
    }
    
    public static HtmlAttr face(String value) {
        return new HtmlAttr("face", value);
    }
    
    public static HtmlAttr height(String value) {
        return new HtmlAttr("height", value);
    }

    public static HtmlAttr href(String value) {
        return new HtmlAttr("href", value);
    }

    public static HtmlAttr hspace(String value) {
        return new HtmlAttr("hspace", value);
    }

    public static HtmlAttr htmlclass(String value) {
        return new HtmlAttr("class", value);
    }
    
    public static HtmlAttr id(String value) {
        return new HtmlAttr("id", value);
    }
    
    public static HtmlAttr label(String value) {
        return new HtmlAttr("label", value);
    }
    
    public static HtmlAttr name(String value) {
        return new HtmlAttr("name", value);
    }

    public static HtmlAttr nameAndID(String value) {
        return new HtmlNameIDAttr(value);
    }
    
    public static HtmlAttr nowrap() {
        return new HtmlSingleAttr("nowrap");
    }
    
    public static HtmlAttr onchange(String value) {
        return new HtmlAttr("onchange", value);
    }
    
    public static HtmlAttr onclick(String value) {
        return new HtmlAttr("onclick", value);
    }
    
    public static HtmlAttr selected() {
        return new HtmlSingleAttr("selected");
    }
    
    public static HtmlAttr size(String value) {
        return new HtmlAttr("size", value);
    }
    
    public static HtmlAttr src(String value) {
        return new HtmlAttr("src", value);
    }
    
    public static HtmlAttr style(String value) {
        return new HtmlAttr("style", value);
    }
    
    public static HtmlAttr target(String value) {
        return new HtmlAttr("target", value);
    }
    
    public static HtmlAttr title(String value) {
        return new HtmlAttr("title", value);
    }
    
    public static HtmlAttr type(String value) {
        return new HtmlAttr("type", value);
    }
    
    public static HtmlAttr usemap(String value) {
        return new HtmlAttr("usemap", value);
    }
    
    public static HtmlAttr valign(String value) {
        return new HtmlAttr("valign", value);
    }
    
    public static HtmlAttr value(String value) {
        return new HtmlAttr("value", value);
    }
    
    public static HtmlAttr vspace(String value) {
        return new HtmlAttr("vspace", value);
    }
    
    public static HtmlAttr width(String value) {
        return new HtmlAttr("width", value);
    }
    
}

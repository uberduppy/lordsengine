package org.talwood.lords.html;

public class HtmlUrlParam extends HtmlBaseElement {

    protected HtmlUrlParam() {
        super();
    }
    
    public HtmlUrlParam(String name, String value) {
        super(name, value);
    }
    
    public HtmlUrlParam(String name, Long value) {
        super(name, value == null ? "" : value.toString());
    }
    
    public HtmlUrlParam(String name, Integer value) {
        super(name, value == null ? "" : value.toString());
    }
    
    public HtmlUrlParam(String name, Boolean value) {
        super(name, value == null ? "" : value.toString());
    }
    
    @Override
    public String render() {
        return getName() + "=" + getValue();
    }

}

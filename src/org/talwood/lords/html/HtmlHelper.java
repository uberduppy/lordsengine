package org.talwood.lords.html;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.talwood.lords.helpers.StringHelper;
import org.talwood.lords.helpers.TupleContainer;

public class HtmlHelper {
    private static String renderInnerData(String type, HtmlRules rules, List<HtmlAttr> attrs) {
        StringBuilder sb = new StringBuilder();
        if(!StringHelper.isEmpty(type)) {
            if(rules.getIndention() > 0) {
                sb.append(StringHelper.createString(' ', rules.getIndention()));
            }
            sb.append("<").append(type);
            if(attrs != null) {
                for(HtmlAttr attr : attrs) {
                    sb.append(attr.render());
                }
            }
            if(rules.isHardClose()) {
                sb.append("/");
            }
            sb.append(">");
        }
        return sb.toString();
    }
    
    private static String renderWrappedData(String type, String data, HtmlRules rules, List<HtmlAttr> attrs) {
        StringBuilder sb = new StringBuilder();
        if(!StringHelper.isEmpty(type)) {
            if(rules.getIndention() > 0) {
                sb.append(StringHelper.createString(' ', rules.getIndention()));
            }
            sb.append("<").append(type);
            if(attrs != null) {
                for(HtmlAttr attr : attrs) {
                    sb.append(attr.render());
                }
            }
            sb.append(">");
            if(rules.isBreaksWanted()) {
                sb.append("\r\n");
            }
        }
        if(!StringHelper.isEmpty(data)) {
            sb.append(data);
            if(rules.isBreaksWanted() && !data.endsWith("\r\n")) {
                sb.append("\r\n");
            }
        }
        if(!StringHelper.isEmpty(type)) {
            if(rules.getIndention() > 0) {
                sb.append(StringHelper.createString(' ', rules.getIndention()));
            }
            sb.append("</").append(type).append(">");
            if(rules.isBreaksWanted()) {
                sb.append("\r\n");
            }
        }
        return sb.toString();
    }
    
    public static String renderData(HtmlElementType type, String data, HtmlAttr... attrs) {
        List<HtmlAttr> l = new ArrayList<HtmlAttr>();
        if(attrs != null) {
            l.addAll(Arrays.asList(attrs));
        }
        return renderData(type.getElementName(), data, type.getDefaultRules(), l);
    }
    
    public static String renderData(HtmlElementType type, String data, List<HtmlAttr> attrs) {
        return renderData(type.getElementName(), data, type.getDefaultRules(), attrs);
    }
    
    public static String renderData(String type, String data, HtmlRules rules, List<HtmlAttr> attrs) {
        StringBuilder sb = new StringBuilder();
        if(StringHelper.isEmpty(data)) {
            sb.append(renderInnerData(type, rules, attrs));
        } else {
            sb.append(renderWrappedData(type, data, rules, attrs));
        }
        return sb.toString();
    }
    
    public static String renderData(String type, String data, HtmlRules rules, HtmlAttr... attrs) {
        return renderData(type, data, rules, Arrays.asList(attrs));
    }
    
    public static String renderData(String type, String data, HtmlAttr... attrs) {
        return renderData(type, data, new HtmlRules(true, true, false), Arrays.asList(attrs));
    }
    
    public static String renderCheckbox(String name, boolean isChecked, HtmlAttr... attrs) {
        StringBuilder sb = new StringBuilder();
        
        List<HtmlAttr> l = new ArrayList<HtmlAttr>();
        l.add(HtmlAttr.type("checkbox"));
        l.add(HtmlAttr.name(name));
        if(isChecked) {
            l.add(HtmlAttr.checked("checked"));
        }
        if(attrs != null) {
            l.addAll(Arrays.asList(attrs));
        }
        sb.append(renderData("input", null, new HtmlRules(false, false, false), l));
        return sb.toString();
    }
    
    public static String renderDropdown(String nameAndID, List<TupleContainer<Long, String>> options, Long selectedID) {
        return renderDropdown(nameAndID, options, selectedID, null, (HtmlAttr[])null);
    }
    public static String renderDropdown(String nameAndID, List<TupleContainer<Long, String>> options, Long selectedID, Integer truncateLength, HtmlAttr... attrs) { 
        StringBuilder sb = new StringBuilder();
        
        // Make the options.
        HtmlElement select = new HtmlElement(HtmlElementType.SELECT, HtmlAttr.nameAndID(nameAndID));
        select.addAttributes(attrs);
        HtmlElement workingElement = select;
        for(TupleContainer<Long, String> lsl : options) {
            String desc = lsl.getRightSide();
            if(HtmlSpecs.OPTGROUP_ID.equals(lsl.getLeftSide())) {
                workingElement = select.createChildElement(HtmlElementType.OPTGROUP);
                workingElement.addAttributes(HtmlAttr.label(desc));
            } else {
                HtmlElement option = workingElement.createChildElement(HtmlElementType.OPTION);
                option.addAttributes(HtmlAttr.value(lsl.getLeftSide().toString()));
                if(selectedID != null && selectedID.equals(lsl.getLeftSide())) {
                    option.addAttributes(HtmlAttr.selected());
                }
                
                if (truncateLength != null){
                    if ( desc.length() > truncateLength.intValue()) {
                        option.addAttributes(HtmlAttr.altAndTitle(desc));
                    }
                    desc = StringHelper.truncate(desc, truncateLength.intValue(), true);
                }
                option.setValue(desc);
            }
        }
        
        sb.append(select.render());
        
        return sb.toString();
    }
    
    public static String renderDropdown(String nameAndID, List<TupleContainer<String, String>> options, String selectedID) {
        return renderDropdown(nameAndID, options, selectedID, null, (HtmlAttr[])null);
    }
    
    public static String renderDropdown(String nameAndID, List<TupleContainer<String, String>> options, String selectedID, Integer truncateLength, HtmlAttr... attrs) { 
        StringBuilder sb = new StringBuilder();
        
        // Make the options.
        HtmlElement select = new HtmlElement(HtmlElementType.SELECT, HtmlAttr.nameAndID(nameAndID));
        select.addAttributes(attrs);
        HtmlElement workingElement = select;
        for(TupleContainer<String, String> lsl : options) {
            String desc = lsl.getRightSide();
            if(HtmlSpecs.OPTGROUP_TEXT.equals(lsl.getLeftSide())) {
                workingElement = select.createChildElement(HtmlElementType.OPTGROUP);
                workingElement.addAttributes(HtmlAttr.label(desc));
            } else {
                HtmlElement option = workingElement.createChildElement(HtmlElementType.OPTION);
                option.addAttributes(HtmlAttr.value(lsl.getLeftSide()));
                if(selectedID != null && selectedID.equals(lsl.getLeftSide())) {
                    option.addAttributes(HtmlAttr.selected());
                }
                
                if (truncateLength != null){
                    if ( desc.length() > truncateLength.intValue()) {
                        option.addAttributes(HtmlAttr.altAndTitle(desc));
                    }
                    desc = StringHelper.truncate(desc, truncateLength.intValue(), true);
                }
                option.setValue(desc);
            }
        }
        
        sb.append(select.render());
        
        return sb.toString();
    }
    
    public static String renderButton(String nameAndID, HtmlAttr... attrs) {
        return renderButton(nameAndID, false, attrs);
    }
    
    public static String renderButton(String nameAndID, boolean appendNbsp, HtmlAttr... attrs) {
        StringBuilder sb = new StringBuilder();
        List<HtmlAttr> l = new ArrayList<HtmlAttr>();
        l.add(HtmlAttr.type("image"));
        l.add(HtmlAttr.nameAndID(nameAndID));
        if(attrs != null) {
            l.addAll(Arrays.asList(attrs));
        }
        sb.append(renderData(HtmlElementType.INPUT, null, l));
        if(appendNbsp) {
            sb.append("&nbsp;");
        }
        return sb.toString();
    }
    
    public static String renderImageLink(String href, String localizedSource, HtmlAttr[] linkAttrs, HtmlAttr[] imageAttrs) {
        StringBuilder sb = new StringBuilder();
        sb.append(renderLink(href, renderImage(localizedSource, imageAttrs), linkAttrs));
        return sb.toString();
    }
    
    public static String renderUrl(String baseUrl, HtmlUrlParam... params) {
        String url = baseUrl;
        if(!StringHelper.isEmpty(url) && params != null) {
            for(HtmlUrlParam param : params) {
                url = StringHelper.addHREFParam(url, param.getName(), param.getValue());
            }
        }
        return url;
    }
    
    public static String renderImage(String localizedSource, HtmlAttr... attrs) {
        StringBuilder sb = new StringBuilder();
        List<HtmlAttr> l = new ArrayList<HtmlAttr>();
        l.add(HtmlAttr.src(localizedSource));
        if(attrs != null) {
            l.addAll(Arrays.asList(attrs));
        }
        sb.append(renderData(HtmlElementType.IMG, null, l));
        return sb.toString();
    }
    
    public static String renderImage(HtmlAttr... attrs) {
        StringBuilder sb = new StringBuilder();
        List<HtmlAttr> l = new ArrayList<HtmlAttr>();
        if(attrs != null) {
            l.addAll(Arrays.asList(attrs));
        }
        sb.append(renderData(HtmlElementType.IMG, null, l));
        return sb.toString();
    }
    
    public static String renderLink(String href, String linkText) {
        return renderLink(href, linkText, (HtmlAttr[])null);
    }
    
    public static String renderLink(String href, String linkText, HtmlAttr... attrs) {
        StringBuilder sb = new StringBuilder();
        List<HtmlAttr> l = new ArrayList<HtmlAttr>();
        l.add(HtmlAttr.href(href));
        if(attrs != null) {
            l.addAll(Arrays.asList(attrs));
        }
        sb.append(renderData(HtmlElementType.A, linkText, l));
        return sb.toString();
    }
    
    

}

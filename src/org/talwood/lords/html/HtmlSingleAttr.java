package org.talwood.lords.html;


public class HtmlSingleAttr extends HtmlAttr {
    private static final long serialVersionUID = 1L;

    protected HtmlSingleAttr() {
        super();
    }
    
    public HtmlSingleAttr(String name) {
        super(name, "");
    }
    
    @Override
    public String render() {
        StringBuilder sb = new StringBuilder();
        sb.append(" ").append(getName());
        return sb.toString();
    }
    
}

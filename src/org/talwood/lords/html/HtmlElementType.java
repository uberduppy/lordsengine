package org.talwood.lords.html;

public enum HtmlElementType {
    EMPTY("", null, null),
    A("a", new HtmlRules(false, false, false), null),
    B("b", new HtmlRules(false, false, false), null),
    BR("BR", new HtmlRules(true, true, false), null),
    BODY("body", new HtmlRules(true, true, false, 3, 3), null),
    CENTER("center", new HtmlRules(true, true, false, 3, 3), null),
    FONT("font", new HtmlRules(false, true, false), null),
    H2("H2", new HtmlRules(true, true, false, 3, 3), null),
    H3("H3", new HtmlRules(true, true, false, 3, 3), null),
    H4("H4", new HtmlRules(true, true, false, 3, 3), null),
    HR("HR", new HtmlRules(true, true, false, 3, 3), null),
    HEAD("head", new HtmlRules(true, true, false, 3, 3), null),
    HTML("html", new HtmlRules(true, true, false, 3, 3), null),
    IMG("img", new HtmlRules(false, true, false), null),
    INPUT("input", new HtmlRules(false, false, false), null),
    OPTGROUP("optgroup", new HtmlRules(true, false, false), null),
    OPTION("option", new HtmlRules(false, false, true), null),
    SELECT("select", new HtmlRules(true, false, false), null),
    SPAN("span", new HtmlRules(false, false, false), null),
    TABLE("table", new HtmlRules(true, true, false, 3, 3), null),
    TD("td", new HtmlRules(true, true, false, 3, 3), null),
    TR("tr", new HtmlRules(true, true, false, 3, 3), null),
    ;
    
    private String elementName;
    private HtmlRules defaultRules;
    private HtmlAttr[] defaultAttributes;
    
    private HtmlElementType(String elementName, HtmlRules defaultRules, HtmlAttr[] defaultAttributes) {
        this.elementName = elementName;
        this.defaultRules = defaultRules;
        this.defaultAttributes = defaultAttributes;
    }
    
    public static HtmlElementType findByElementName(String elementName) {
        HtmlElementType result = EMPTY;
        for(HtmlElementType type : values()) {
            if(type.getElementName().equals(elementName)) {
                result = type;
                break;
            }
        }
        return result;
    }

    public String getElementName() {
        return elementName;
    }

    public HtmlRules getDefaultRules() {
        return defaultRules;
    }

    public HtmlAttr[] getDefaultAttributes() {
        return defaultAttributes;
    }
}

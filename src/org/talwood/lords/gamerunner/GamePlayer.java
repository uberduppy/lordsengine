package org.talwood.lords.gamerunner;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.Houses;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.Lords;
import org.talwood.lords.enums.QuestTypes;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.enums.TurnActions;
import org.talwood.lords.intrigues.Intrigue;
import org.talwood.lords.quests.Quest;
import org.talwood.lords.resources.Res;
import org.talwood.lords.resources.Resources;

public class GamePlayer {

    private List<Quest> completedQuests = new ArrayList<Quest>();
    private List<Quest> currentQuests = new ArrayList<Quest>();
    private List<Building> ownedBuildings = new ArrayList<Building>();
    private List<Intrigue> intrigueCards  = new ArrayList<Intrigue>();
    private Resources resources = new Resources();
    private Lords lord;
    private GameRunner runner;
    private String name;
    private Long playerID;
    private TurnState turnState;
    private Houses definedBestHouse;
    
    
    private int agentsPlaced = 0;
    private int agentsInHarbor = 0;
    private int agentCount = 0;
    private int victoryPoints = 0;
    private int intriguesPlayed = 0;
    
    private boolean hasAmbassador;
    
    protected GamePlayer() {
        
    }
    
    public GamePlayer(GameRunner runner, Lords lord, String playerName, Long playerID) {
        this.runner = runner;
        this.name = playerName;
        this.playerID = playerID;
        this.lord = lord;
    }

    public GamePlayer(GameRunner runner, Lords lord, Houses bestHouse, String playerName, Long playerID) {
        this.runner = runner;
        this.name = playerName;
        this.playerID = playerID;
        this.lord = lord;
        this.definedBestHouse = bestHouse;
    }

    public List<Quest> getCompletedQuests() {
        return completedQuests;
    }

    public void setCompletedQuests(List<Quest> completedQuests) {
        this.completedQuests = completedQuests;
    }

    public List<Quest> getCurrentQuests() {
        return currentQuests;
    }

    public void setCurrentQuests(List<Quest> currentQuests) {
        this.currentQuests = currentQuests;
    }

    public List<Building> getOwnedBuildings() {
        return ownedBuildings;
    }

    public void setOwnedBuildings(List<Building> ownedBuildings) {
        this.ownedBuildings = ownedBuildings;
    }

    public void addOwnedBuildings(Building ownedBuilding) {
        this.ownedBuildings.add(ownedBuilding);
    }

    public List<Intrigue> getIntrigueCards() {
        return intrigueCards;
    }

    public void setIntrigueCards(List<Intrigue> intrigueCards) {
        this.intrigueCards = intrigueCards;
    }

    
    public void incrementIntriguesPlayed() {
        intriguesPlayed++;
    }
    public int getIntriguesPlayed() {
        return intriguesPlayed;
    }

    public Resources getResources() {
        return resources;
    }

    public void setResources(Resources resources) {
        this.resources = resources;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void addIntrigueCard(Intrigue card) {
        intrigueCards.add(card);
    }
    
    public void removeIntrigueCard(Intrigue card) {
//        intrigueCards.removeIf(c -> c.getIntrigueInfo() == card.getIntrigueInfo());
        intrigueCards.remove(card);
    }
    
    public void addCurrentQuest(Quest quest) {
        currentQuests.add(quest);
    }
    
    public void addCompletedQuest(Quest quest) {
        completedQuests.add(quest);
    }
    
    public void addAnotherAgent() {
        agentCount++;
    }
    
    public void completeQuest(Quest quest) {
        if(quest.getQuestInfo().getQuestType() != QuestTypes.Mandatory) {
            completedQuests.add(quest);
        }
//        currentQuests.removeIf(q -> q.getQuestInfo() == quest.getQuestInfo());
        currentQuests.remove(quest);
    }
    
    public void setStartingResources(Resources res) {
        resources.reset();
        resources.incrementFromResources(res);
    }
    
    public void checkTurnState() {
        if(turnState == null) {
            throw new UnsupportedOperationException("No turn state defined");
        }
        turnState.checkTurnState(this);
    }
    
    public GamePlayer andEndTurn() {
        checkTurnState();
        if(resources.getAnyResource() > 0) {
            throw new UnsupportedOperationException("Wildcard resources need to be replaced with andReplaceGenericResources");
        }
        setTurnState(null);
        runner.endTurn();

        return this;
    }
    
    public GamePlayer andAddResourcesToPlayer(GamePlayer oppo, Res res) {
        oppo.getResources().incrementFromResources(res);
        return this;
    }
    public GamePlayer andReplaceGenericResources(Resources res) {
        if(resources.getAnyResource() >= res.getCritterCount()) {
            resources.incrementFromResources(res);
            resources.removeAnyResource(res.getCritterCount());
        } else {
            throw new UnsupportedOperationException("Unable to replace wildcard resources");
        }
        return this;
    }
    
    public GamePlayer andBuyBuilding(Buildings buildingToBuy) {
        runner.buyBuilding(this, buildingToBuy);
        
        return this;
    }
    public GamePlayer andTakeQuest(Quests questToTake) {
        runner.takeQuestFromCliffwatch(this, questToTake);
        
        return this;
    }
    
    public GamePlayer andTakeQuestAsQuestReward(Quests questToTake) {
        runner.takeQuestAsQuestReward(this, questToTake);
        
        return this;
    }
    
    public GamePlayer andTakeBuildingFromBuildersHallAsQuestReward(Buildings buildingToTake) {
        runner.takeBuildingFromBuildersHallAsQuestReward(this, buildingToTake);
        
        return this;
    }
    
    public GamePlayer andTakeBuildingsFromBuildersHallAsQuestReward(Buildings... buildingsToTake) {
        runner.takeBuildingsFromBuildersHallAsQuestReward(this, buildingsToTake);
        
        return this;
    }
    
    public GamePlayer andPayVariableActivation(ResourceChoices choice) {
        runner.payVariableActivation(this, choice);
        
        return this;
    }
    
    public GamePlayer andReturnOneQuestResourceUsed(Res used) {
        runner.returnOneQuestResourceUsed(this, used);
        return this;
    }
    public GamePlayer andChooseOpponentForReward(GamePlayer opponent) {
        runner.chooseOpponentForReward(this, opponent);
        
        return this;
    }
    
    public GamePlayer andChooseOpponentForMandatoryQuest(GamePlayer opponent) {
        runner.chooseOpponentForMandatoryQuest(this, opponent);
        
        return this;
    }
    
    public GamePlayer andReturnResourcesForIntrigue(ResourceChoices choice) {
        runner.returnResources(this, choice);
        
        return this;
        
    }
    public GamePlayer andStealResources(GamePlayer opponent, ResourceChoices choice) {
        runner.stealResources(this, opponent, choice);
        
        return this;
    }
    public GamePlayer andPlayIntrigue(Intrigues card) {
        runner.playIntrigue(this, card, false);
        incrementIntriguesPlayed();
        
        return this;
    }
    public GamePlayer andSelectHouse(Houses house) {
        runner.selectHouse(this, house);
        
        return this;
    }
    
    public GamePlayer andRecallAgent(Buildings buildingWithAgent) {
        runner.recallAgent(this, buildingWithAgent);
        
        return this;
    }
    
    public GamePlayer andRecallAgents(Buildings... buildingsWithAgent) {
        runner.recallAgents(this, buildingsWithAgent);
        
        return this;
    }
    
    public GamePlayer andPlayIntrigueDrawnAsAction() {
        runner.playIntrigueDrawnAsAction(this);
        incrementIntriguesPlayed();
        
        return this;
    }
    public GamePlayer andCompleteQuest(Quests quest) {
        runner.completeQuest(this, quest);
        
        return this;
    }
    public GamePlayer andCompleteMandatoryQuest(Quests quest) {
        runner.completeMandatoryQuest(this, quest);
        
        return this;
    }
    
    public GamePlayer andCompleteQuestInCliffwatchInn(Quests quest) {
        runner.completeQuestInCliffwatchInn(this, quest);
        
        return this;
    }
    
    public GamePlayer andSelectTargetBuildings(Buildings... bldgs) {
        runner.selectTargetBuildings(this, bldgs);
        return this;
        
    }
    public GamePlayer andTakeVariableReward(ResourceChoices choice) {
        runner.takeVariableReward(this, choice);
        
        return this;
    }
    
    public GamePlayer andPlayMultiIntrigue(Intrigues intrigue) {
        turnState.setInMultiState(true);
        return andPlayIntrigue(intrigue);
    }
    
    public GamePlayer andFinishMultiIntrigue() {
        turnState.checkTurnState(this);
        turnState.resetMultiIntrigue();
        turnState.removeTurnAction(TurnActions.IntriguePlayed);
        turnState.removeTurnAction(TurnActions.ResourcesForVPGiven);
        turnState.removeTurnAction(TurnActions.AgentReturned);
        turnState.removeTurnAction(TurnActions.IntrigueDrawnAsAction);
        turnState.removeTurnAction(TurnActions.MandatoryQuestGiven);
        turnState.setIntrigueDrawnAsAction(null);
        return this;
    }
    
    public GamePlayer andTakeVariableIntrigueReward(ResourceChoices choice) {
        runner.takeVariableIntrigueReward(this, choice);
        
        return this;
    }
    
    public GamePlayer andTakeVariableOwnerReward(ResourceChoices choice) {
        runner.takeVariableOwnerReward(this, choice);
        
        return this;
    }
    
    public GamePlayer andTakeVariableIntrigueOwnerReward(ResourceChoices choice) {
        runner.takeVariableIntrigueOwnerReward(this, choice);
        
        return this;
    }
    
    
    
    public GamePlayer reallocateAgent(Buildings sourceBuilding, Buildings targetBuilding) {
        if(turnState != null) {
            throw new UnsupportedOperationException("Last tunr was not finalized");
        }
        runner.reallocateAgent(this, sourceBuilding, targetBuilding);
        return this;
    }
    
    public GamePlayer placeAmbassador(Buildings buildingToPlace) {
        if(!isHasAmbassador()) {
            throw new UnsupportedOperationException("You need to have the ambassador to place it");
        }
        runner.placeAgent(this, buildingToPlace);
        setHasAmbassador(false);

        return this;
    }
    public GamePlayer placeAgent(Buildings buildingToPlace) {
        if(turnState != null) {
            throw new UnsupportedOperationException("Last turn was not finalized");
        }
        if(isHasAmbassador()) {
            throw new UnsupportedOperationException("You need to place the ambassador first");
        }
        runner.placeAgent(this, buildingToPlace);
        
        return this;
    }

    public Long getPlayerID() {
        return playerID;
    }

    public TurnState getTurnState() {
        return turnState;
    }

    public void setTurnState(TurnState turnState) {
        this.turnState = turnState;
    }

    public int getAgentsPlaced() {
        return agentsPlaced;
    }

    public void setAgentsPlaced(int agentsPlaced) {
        this.agentsPlaced = agentsPlaced;
    }

    public void incrementAgentsPlaced() {
        this.agentsPlaced++;
    }

    public void decrementAgentsPlaced() {
        if(agentsPlaced <= 0) {
            throw new UnsupportedOperationException("Trying to decrement agent count with none there");
        }
        this.agentsPlaced--;
    }

    public int getAgentsInHarbor() {
        return agentsInHarbor;
    }

    public void setAgentsInHarbor(int agentsInHarbor) {
        this.agentsInHarbor = agentsInHarbor;
    }

    public void incrementAgentsInHarbor() {
        this.agentsInHarbor++;
    }

    public void decrementAgentsInHarbor() {
        if(agentsInHarbor <= 0) {
            throw new UnsupportedOperationException("Trying to decrement harbor agents with none there");
        }
        this.agentsInHarbor--;
    }

    public int getAgentCount() {
        return agentCount;
    }

    public void setAgentCount(int agentCount) {
        this.agentCount = agentCount;
    }

    public void incrementAgentCount() {
        this.agentCount++;
    }

    public int getVictoryPoints() {
        return victoryPoints;
    }

    public void setVictoryPoints(int victoryPoints) {
        this.victoryPoints = victoryPoints;
    }
    
    public void addVictoryPoints(int victoryPoints) {
        this.victoryPoints += victoryPoints;
    }

    public GamePlayer makeCopyForRenderer() {
        GamePlayer gp = new GamePlayer(runner, lord, name, playerID);
        gp.agentCount = agentCount;
        gp.agentsInHarbor = agentsInHarbor;
        gp.agentsPlaced = agentsPlaced;
        gp.resources = new Resources(resources);
        gp.victoryPoints = victoryPoints;
        return gp;
    }

    public Lords getLord() {
        return lord;
    }

    public Houses getDefinedBestHouse() {
        return definedBestHouse;
    }

    public void setDefinedBestHouse(Houses definedBestHouse) {
        this.definedBestHouse = definedBestHouse;
    }

    public boolean isHasAmbassador() {
        return hasAmbassador;
    }

    public void setHasAmbassador(boolean hasAmbassador) {
        this.hasAmbassador = hasAmbassador;
    }

    
}

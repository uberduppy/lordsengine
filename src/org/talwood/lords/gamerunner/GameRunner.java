package org.talwood.lords.gamerunner;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.comparators.BuildingStackComparator;
import org.talwood.lords.comparators.IntrigueStackComparator;
import org.talwood.lords.comparators.QuestStackComparator;
import org.talwood.lords.constants.GameBoardSpecs;
import org.talwood.lords.containers.BuildingContainer;
import org.talwood.lords.containers.BuildingPointContainer;
import org.talwood.lords.containers.IntrigueContainer;
import org.talwood.lords.containers.QuestContainer;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Houses;
import org.talwood.lords.enums.IntrigueActions;
import org.talwood.lords.enums.IntrigueTypes;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.Lords;
import org.talwood.lords.enums.QuestActions;
import org.talwood.lords.enums.QuestTypes;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.enums.TurnActions;
import org.talwood.lords.helpers.HtmlDisplayHelper;
import org.talwood.lords.helpers.LogicHelper;
import org.talwood.lords.helpers.TupleContainer;
import org.talwood.lords.html.HtmlElement;
import org.talwood.lords.html.HtmlElementType;
import org.talwood.lords.interfaces.IStateRenderer;
import org.talwood.lords.intrigues.AgentPlacementIntrigue;
import org.talwood.lords.intrigues.Intrigue;
import org.talwood.lords.intrigues.BuyBuildingIntrigue;
import org.talwood.lords.intrigues.DrawQuestForHouseIntrigue;
import org.talwood.lords.intrigues.DrawQuestIntrigue;
import org.talwood.lords.intrigues.DrawThreeOppsOneIntrigue;
import org.talwood.lords.intrigues.DrawTwoIntrigue;
import org.talwood.lords.intrigues.GainResourcesIntrigue;
import org.talwood.lords.intrigues.GiveResourceForVPIntrigue;
import org.talwood.lords.intrigues.MandatoryQuestIntrigue;
import org.talwood.lords.intrigues.OptionalResourceIntrigue;
import org.talwood.lords.intrigues.PlaceExtraOnOneSpaceIntrigue;
import org.talwood.lords.intrigues.PlaceExtraOnTwoSpacesIntrigue;
import org.talwood.lords.intrigues.ReallocateResourcesIntrigue;
import org.talwood.lords.intrigues.ResourceForVictoryIntrigue;
import org.talwood.lords.intrigues.ReturnAgentIntrigue;
import org.talwood.lords.intrigues.ReturnOneCorruptionIntrigue;
import org.talwood.lords.intrigues.ReturnTwoCorruptionAndResourceIntrigue;
import org.talwood.lords.intrigues.ShareResourcesIntrigue;
import org.talwood.lords.intrigues.ShareResourcesSingleIntrigue;
import org.talwood.lords.intrigues.StealResourcesIntrigue;
import org.talwood.lords.intrigues.VictoryForCorruptionIntrigue;
import org.talwood.lords.quests.MandatoryQuest;
import org.talwood.lords.quests.PlotQuest;
import org.talwood.lords.quests.Quest;
import org.talwood.lords.renderers.AgentPlacementRenderer;
import org.talwood.lords.renderers.AgentRelocationRenderer;
import org.talwood.lords.renderers.AgentRemovalRenderer;
import org.talwood.lords.renderers.AtAGlanceRenderer;
import org.talwood.lords.renderers.BoughtBuildingRenderer;
import org.talwood.lords.renderers.BuildersHallRenderer;
import org.talwood.lords.renderers.CliffwatchInnRenderer;
import org.talwood.lords.renderers.DrawIntrigueRenderer;
import org.talwood.lords.renderers.DrawQuestRenderer;
import org.talwood.lords.renderers.DrawnBuildingRenderer;
import org.talwood.lords.renderers.EndOfGameRenderer;
import org.talwood.lords.renderers.GamePlayerResourceRenderer;
import org.talwood.lords.renderers.H2Renderer;
import org.talwood.lords.renderers.H3Renderer;
import org.talwood.lords.renderers.H4Renderer;
import org.talwood.lords.renderers.HRRenderer;
import org.talwood.lords.renderers.IntriguePlayedRenderer;
import org.talwood.lords.renderers.IntrigueRenderer;
import org.talwood.lords.renderers.MandatoryQuestRenderer;
import org.talwood.lords.renderers.PlayerPaidRenderer;
import org.talwood.lords.renderers.PlayerReceivedRenderer;
import org.talwood.lords.renderers.QuestCompletedRenderer;
import org.talwood.lords.renderers.QuestRenderer;
import org.talwood.lords.renderers.ResourceReallocationRenderer;
import org.talwood.lords.renderers.ResourcesGivenRenderer;
import org.talwood.lords.renderers.ResourcesTakenRenderer;
import org.talwood.lords.renderers.SpecialQuestEffectRenderer;
import org.talwood.lords.renderers.StartTurnRenderer;
import org.talwood.lords.renderers.VariableActivationRenderer;
import org.talwood.lords.renderers.VariableOwnerRewardRenderer;
import org.talwood.lords.renderers.VariableRewardRenderer;
import org.talwood.lords.resources.BuildingExtraReward;
import org.talwood.lords.resources.BuildingReallocatedReward;
import org.talwood.lords.resources.Res;
import org.talwood.lords.resources.Resources;

public class GameRunner {

    private List<GamePlayer> players = new ArrayList<GamePlayer>();
    private GamePlayer currentPlayer;
    private int turnNumber;
    private List<IStateRenderer> renderers = new ArrayList<>();
    private List<Quest> cliffWatchInnQuests;
    private List<Quest> quests;
    private List<Quest> discardedQuests = new ArrayList<>();
    private List<Intrigue> intrigues;
    private List<Building> buildings;
    private List<Building> buildingsUsed;
    private List<Building> buildingsInBuildersHall;
    private int corruptionRemoved;
    private boolean longGame;
    
    
    // Specialty containers
    private List<Quests> questsToStack = new ArrayList<Quests>();
    private List<Intrigues> intriguesToStack = new ArrayList<Intrigues>();
    private List<Buildings> buildingsToStack = new ArrayList<Buildings>();
    protected Map<Buildings, Building> buildingsInPlay = new HashMap<Buildings, Building>();
    
    public GameRunner(boolean longGame) {
        this();
        this.longGame = longGame;
    }
    public GameRunner() {
        this.buildingsInBuildersHall = new ArrayList<>();
        this.buildings = new ArrayList<>();
        this.buildingsUsed = new ArrayList<>();
        this.intrigues = new ArrayList<>();
        this.quests = new ArrayList<>();
        this.cliffWatchInnQuests = new ArrayList<>();
        
    }
    
    public GamePlayer addPlayer(Lords lord, String playerName, Long playerID) {
        GamePlayer player = new GamePlayer(this, lord, playerName, playerID);
        players.add(player);
        return player;
    }
    
    public GamePlayer addPlayer(Lords lord, Houses bestHouse, String playerName, Long playerID) {
        GamePlayer player = new GamePlayer(this, lord, bestHouse, playerName, playerID);
        players.add(player);
        return player;
    }
    
    public void setupBuildings(Buildings... buildingsInOrder) {
        setStartingBuildingOrder(buildingsInOrder);
    }

    public void setupQuests(Quests... questsInOrder) {
        setStartingQuestOrder(questsInOrder);
    }
    
    public void setupIntrigues(Intrigues... intriguesInOrder) {
        setStartingIntrigueOrder(intriguesInOrder);
    }
    
    protected void setStartingBuildingOrder(Buildings... buildingsInOrder) {
        for(Buildings b : buildingsInOrder) {
            if(buildingsToStack.contains(b)) {
                throw new UnsupportedOperationException(b.getName() + " already exists in array");
            }
            buildingsToStack.add(b);
        }
    }

    protected void setStartingQuestOrder(Quests... questsInOrder) {
        for(Quests q : questsInOrder) {
            if(questsToStack.contains(q)) {
                throw new UnsupportedOperationException(q.getName() + " already exists in array");
            }
            questsToStack.add(q);
        }
    }

    protected void setStartingIntrigueOrder(Intrigues... intriguesInOrder) {
        for(Intrigues i : intriguesInOrder) {
            if(intriguesToStack.contains(i)) {
                throw new UnsupportedOperationException(i.getName() + " already exists in array");
            }
            intriguesToStack.add(i);
        }
    }

    public void addRenderer(IStateRenderer renderer) {
        renderers.add(renderer);
    }
    
    public void startTurn() {
        turnNumber++;

        if(turnNumber > 8) {
            throw new UnsupportedOperationException("You only get 8 turns, dood!");
        }
        if(turnNumber > 1) {
            for(GamePlayer gp : players) {
                if(gp.getAgentsInHarbor() > 0) {
                    throw new UnsupportedOperationException("Player " + gp.getName() + " has agents in Waterdeep Harbor");
                }
                if(gp.getAgentsPlaced() != gp.getAgentCount()) {
                    throw new UnsupportedOperationException("Player " + gp.getName() + " has agents unaccounted for");
                }
                gp.setAgentsInHarbor(0);
                gp.setAgentsPlaced(0);
            }
        }
        
        if(turnNumber == 5) {
            for(GamePlayer gp : players) {
                gp.incrementAgentCount();
            }
            
        }
        
        buildingsUsed.clear();
        for(Building bldg : buildingsInBuildersHall) {
            bldg.setStoredVictoryPoints(bldg.getStoredVictoryPoints() + 1);
        }
        for(Building bldg : buildingsInPlay.values()) {
            bldg.processStartOfTurn();
            bldg.setPlayerOccupantID(null);
        }
        
        showStartOfTurnRenderers();
        
    }
    
    public void showStartOfTurnRenderers() {
        addRenderer(new StartTurnRenderer(turnNumber));
        addRenderer(new H3Renderer("Cliffwatch Inn"));
        addRenderer(new CliffwatchInnRenderer(cliffWatchInnQuests));
        addRenderer(new H3Renderer("Builder's Hall"));
        addRenderer(new BuildersHallRenderer(buildBuildingContainer(buildingsInBuildersHall)));
        for(GamePlayer gp : players) {
            addRenderer(new GamePlayerResourceRenderer(gp));
        }
    }
    
    public void showEndOfGameRenderers() {
        addRenderer(new HRRenderer());
        addRenderer(new H2Renderer("End of Game State"));
        addRenderer(new H3Renderer("Cliffwatch Inn"));
        addRenderer(new CliffwatchInnRenderer(cliffWatchInnQuests));
        addRenderer(new H3Renderer("Builder's Hall"));
        addRenderer(new BuildersHallRenderer(buildBuildingContainer(buildingsInBuildersHall)));
    }
    
    protected List<BuildingPointContainer> buildBuildingContainer(List<Building> bldg) {
        List<BuildingPointContainer> ctr = new ArrayList<>();
        for(Building ab : bldg) {
            ctr.add(new BuildingPointContainer(ab.getBuildingInfo(), ab.getStoredVictoryPoints()));
        }
        return ctr;
    }
    
    public void startGame(Expansions... expansionsToUse) {
        // Determine which quests, intrigues, 
        loadAndShuffleQuests(expansionsToUse);
        loadAndShuffleBuildings(expansionsToUse);
        loadAndShuffleIntrigues(expansionsToUse);
        
        // Deal stuff to each player
        for(int x = 0; x < 2; x++) {
            // Two intrigues and 2 quests to each
            for(GamePlayer p : players) {
                p.addCurrentQuest(dealQuestFromDeck());
                p.addIntrigueCard(dealIntrigueFromDeck());
            }
        }
        
        // 4, 3, 2, 2 for 2, 3, 4, 5
        int goldCount = 4;
        int agentCount = longGame ? GameBoardSpecs.NUMBER_OF_AGENTS_LONG[players.size()] : GameBoardSpecs.NUMBER_OF_AGENTS[players.size()];
        if(agentCount == -1) {
            throw new UnsupportedOperationException("Not a valid number of players");
        }
        for(GamePlayer p : players) {
            p.setStartingResources(new Resources(Res.gold(goldCount++)));
            p.setAgentCount(agentCount);
        }
        for(int x = 0; x < GameBoardSpecs.QUESTS_IN_CLIFFWATCH_INN; x++) {
            cliffWatchInnQuests.add(dealQuestFromDeck());
        }
        for(int x = 0; x < GameBoardSpecs.BUILDINGS_IN_BUILDERS_HALL; x++) {
            buildingsInBuildersHall.add(dealBuildingFromDeck());
        }
        
        currentPlayer = players.get(0);
    }

    private void loadAndShuffleIntrigues(Expansions... expansionsToUse) {
        IntrigueContainer ic = new IntrigueContainer(expansionsToUse);
        for(Intrigue i : ic.getAllBaseIntrigues()) {
            getIntrigues().add(i);
        }
        Collections.shuffle(getIntrigues());
        if(!intriguesToStack.isEmpty()) {
            Collections.sort(getIntrigues(), new IntrigueStackComparator(intriguesToStack));
        }
    }
    private void loadAndShuffleQuests(Expansions... expansionsToUse) {
        QuestContainer qc = new QuestContainer(expansionsToUse);
        for(Quest q : qc.getQuests()) {
            getQuests().add(q);
        }
        Collections.shuffle(getQuests());
        if(!questsToStack.isEmpty()) {
            Collections.sort(getQuests(), new QuestStackComparator(questsToStack));
        }
    }
    
    private void loadAndShuffleBuildings(Expansions... expansionsToUse) {
        BuildingContainer bc = new BuildingContainer(expansionsToUse);
        for(Building b : bc.getAllBuildings()) {
            if(b.getBuildingInfo().getCost() == 0) {
                // This is a free building and it should be in play
                buildingsInPlay.put(b.getBuildingInfo(), b);
            } else {
                getBuildings().add(b);
                // This is a purchased building and needs to be in builders hall
            }
        }
        Collections.shuffle(getBuildings());
        if(!buildingsToStack.isEmpty()) {
            Collections.sort(getBuildings(), new BuildingStackComparator(buildingsToStack));
        }
    }

    private Building dealBuildingFromDeck() {
        return buildings.remove(0);
    }
    
    private Quest dealQuestFromDeck() {
        return quests.remove(0);
    }
    
    private Intrigue dealIntrigueFromDeck() {
        return intrigues.remove(0);
    }
    
    public void listQuestsLessThanPoints(GamePlayer player, int minimumVictoryPoints) {
        addRenderer(new HRRenderer());
        addRenderer(new H2Renderer("Completed quests by " + player.getName() + " under " + minimumVictoryPoints + " points"));
        for(Quest q : player.getCompletedQuests()) {
            if(q.getQuestInfo().getVictoryPoints() < minimumVictoryPoints) {
                addRenderer(new QuestRenderer(q));
            }
        }
    }
    public void listCompletableQuests(GamePlayer player, int minimumVictoryPoints) {
        addRenderer(new HRRenderer());
        addRenderer(new H2Renderer("Quests completable by " + player.getName()));
        for(Quest q : cliffWatchInnQuests) {
            if(player.getResources().containsResources(q.getCost()) && q.getQuestInfo().getVictoryPoints() >= minimumVictoryPoints) {
                addRenderer(new QuestRenderer(q));
            }
        }
        for(Quest q : quests) {
            if(player.getResources().containsResources(q.getCost()) && q.getQuestInfo().getVictoryPoints() >= minimumVictoryPoints) {
                addRenderer(new QuestRenderer(q));
            }
        }
    }
    public void finishGame() {
        int totalCorruption = corruptionRemoved;
        
        for(GamePlayer gp : players) {
            totalCorruption += gp.getResources().getCorruption();
        }
        // 
        int penalty = 0;
        if(totalCorruption > 0) {
            penalty = (totalCorruption / 3) + 1;
        }
        showEndOfGameRenderers();
        for(GamePlayer gp : players) {
            addRenderer(new GamePlayerResourceRenderer(gp));
            addRenderer(new EndOfGameRenderer(gp, penalty));
        }
    }
    public void renderToFile(String fileName) {
        TupleContainer<HtmlElement, HtmlElement> result = renderGameState();
        try {
            try (FileOutputStream fos = new FileOutputStream(new File(fileName))) {
                fos.write(result.getLeftSide().render().getBytes());
                fos.flush();
            }
        } catch (Exception ex) {
            throw new UnsupportedOperationException("Unable to create output for " + fileName);
        }
    }

    public TupleContainer<HtmlElement, HtmlElement> renderGameState() {
        TupleContainer<HtmlElement, HtmlElement> gameState = HtmlDisplayHelper.startHtmlToTable(true);
        for(IStateRenderer renderer : renderers) {
            HtmlElement tra = gameState.getRightSide().createChildElement(HtmlElementType.TR);
            HtmlElement tda = tra.createChildElement(HtmlElementType.TD);
            renderer.render(tra);
        }
        
        // Start of game, we need to set up starting tavern, Cliffwatch Inn and Buildings.
        
        return gameState;
    }

    public void buyBuilding(GamePlayer player, Buildings buildingToBuy) {
        if(player.getTurnState().hasBuildingAction(BuildingActions.BuyBuilding)) {
            if(LogicHelper.hasBuildingInCollection(buildingsInBuildersHall, buildingToBuy)) {
                Building bldg = LogicHelper.findBuildingInCollection(buildingsInBuildersHall, buildingToBuy);
                // Can I afford it?
                if(bldg.getBuildingInfo().getCost() <= player.getResources().getGold()) {
                    // I can afford it
                    // Add it to the buildings in play
                    bldg.processComingIntoPlay();
                    buildingsInPlay.put(buildingToBuy, bldg);
                    buildingsInBuildersHall.remove(bldg);
                    // Mark it as owned by player
                    bldg.setPlayerOwnerID(player.getPlayerID());
                    player.addOwnedBuildings(bldg);
                    // Pay for it
                    player.getResources().decrementFromResources(Res.gold(bldg.getBuildingInfo().getCost()));
                    // Put a new building in builder's hall
                    player.getTurnState().addTurnAction(TurnActions.BuildingPurchased);
                    // Give the VP to the player who bought it
                    player.addVictoryPoints(bldg.getStoredVictoryPoints());
                    // Put another building in builders hall
                    Building replacedBuilding = dealBuildingFromDeck();
                    buildingsInBuildersHall.add(replacedBuilding);
                    addRenderer(new BoughtBuildingRenderer(bldg, replacedBuilding));
                    
                }
            }
        }
        
    }
    
    public void takeBuildingAsAction(GamePlayer player, Buildings buildingToBuy) {
        if(LogicHelper.hasBuildingInCollection(buildingsInBuildersHall, buildingToBuy)) {
            Building bldg = LogicHelper.findBuildingInCollection(buildingsInBuildersHall, buildingToBuy);
            // Add it to the buildings in play
            bldg.processComingIntoPlay();
            buildingsInPlay.put(buildingToBuy, bldg);
            buildingsInBuildersHall.remove(bldg);
            // Mark it as owned by player
            bldg.setPlayerOwnerID(player.getPlayerID());
            player.addOwnedBuildings(bldg);
            // Give the VP to the player who bought it
            player.addVictoryPoints(bldg.getStoredVictoryPoints());
            // Put another building in builders hall
            Building replacedBuilding = dealBuildingFromDeck();
            buildingsInBuildersHall.add(replacedBuilding);
            addRenderer(new BoughtBuildingRenderer(bldg, replacedBuilding));

        }
        
    }
    
    public void payVariableActivation(GamePlayer player, ResourceChoices choice) {
        if(player.getTurnState().hasBuildingAction(BuildingActions.RequiresVariableActivation)) {
           Building bldg = player.getTurnState().getBuilding();
           List<ResourceChoices> choices = bldg.getActivationCostOptions();
           if(choices.contains(choice)) {
               if(player.getResources().containsResources(choice.getResources())) {
                   player.getResources().decrementFromResources(choice.getResources());
                   player.getTurnState().addTurnAction(TurnActions.VariableActivationHandled);
                   // Add the renderer here for it!
                   addRenderer(new VariableActivationRenderer(bldg.getBuildingInfo(), choice));
               } else {
                   throw new UnsupportedOperationException("You don't have " + choice.getDescription());
               }
           }
        }
    }
    
    private void handleOptionalResourceIntrigue(GamePlayer player, OptionalResourceIntrigue card) {
        Resources baseReward = card.getOwnerReward();
        addRenderer(new ResourcesTakenRenderer(baseReward));
        player.getResources().incrementFromResources(handlePlotQuestResourceRewards(player, baseReward));
    }
    
    private void handleBuyBuildingIntrigue(GamePlayer player, BuyBuildingIntrigue card) {
        Resources baseReward = card.getReward();
        addRenderer(new ResourcesTakenRenderer(baseReward));
        player.getResources().incrementFromResources(handlePlotQuestResourceRewards(player, baseReward));
        player.getTurnState().addBuildingAction(BuildingActions.BuyBuilding);
    }
    
    private void handleDrawThreeOppsOneIntrigue(GamePlayer player, DrawThreeOppsOneIntrigue card) {
        for(int x = 0; x < 3; x++) {
            Intrigue bi = dealIntrigueFromDeck();
            player.addIntrigueCard(bi);
            addRenderer(new DrawIntrigueRenderer(bi));
        }
        for(GamePlayer p : players) {
            if(!LogicHelper.isSamePlayer(p, player)) {
                Intrigue bi = dealIntrigueFromDeck();
                p.addIntrigueCard(bi);
                addRenderer(new DrawIntrigueRenderer(p, bi));
            }
        }
    }
    
    private void handleDrawTwoIntrigue(GamePlayer player, DrawTwoIntrigue card) {
        for(int x = 0; x < 2; x++) {
            Intrigue bi = dealIntrigueFromDeck();
            player.addIntrigueCard(bi);
            addRenderer(new DrawIntrigueRenderer(bi));
        }
    }
    
    private void handleStealResourcesIntrigue(GamePlayer player, StealResourcesIntrigue card) {
        player.getTurnState().addIntrigueAction(IntrigueActions.StealResourcesFromPlayer);
    }
    
    private void handleShareResourcesIntrigue(GamePlayer player, ShareResourcesIntrigue card) {
        if(card.getPlayerChoices().size() == 1) {
            // Go ahead and add the resources now.
            Resources baseReward = card.getPlayerChoices().get(0).getResources();
            addRenderer(new ResourcesTakenRenderer(baseReward));
            player.getResources().incrementFromResources(handlePlotQuestResourceRewards(player, baseReward));
        }
    }
    private void handleReallocateResourcesIntrigue(GamePlayer player, ReallocateResourcesIntrigue card) {
        Resources total = new Resources();
        for(GamePlayer p : players) {
            if(!p.getPlayerID().equals(player.getPlayerID())) {
                if(p.getResources().containsResources(card.getReallocated().getResources())) {
                    p.getResources().decrementFromResources(card.getReallocated().getResources());
                    addRenderer(new ResourcesTakenRenderer(p, card.getReallocated().getResources()));
                } else {
                    total.incrementFromResources(card.getReallocated().getResources());
                }
            }
        }
        if(total.isNotEmpty()) {
            addRenderer(new ResourcesTakenRenderer(total));
            player.getResources().incrementFromResources(handlePlotQuestResourceRewards(player, total));
        }
    }
    private void handleGiveResourceForVictoryIntrigue(GamePlayer player, GiveResourceForVPIntrigue card) {
        player.getTurnState().addIntrigueAction(IntrigueActions.GiveResourcesForVictoryPoints);
    }
    private void handleReturnTwoCorruptionAndResourceIntrigue(GamePlayer player, ReturnTwoCorruptionAndResourceIntrigue card) {
        player.getTurnState().addIntrigueAction(IntrigueActions.ReturnTwoCorruptionAndResource);
        
    }
    private void handleGainResourcesIntrigue(GamePlayer player, GainResourcesIntrigue card) {
        player.getTurnState().addIntrigueAction(IntrigueActions.GainResourceChoice);
    }
    private void handlePlaceExtraOnOneSpaceIntrigue(GamePlayer player, PlaceExtraOnOneSpaceIntrigue card) {
        player.getTurnState().addIntrigueAction(IntrigueActions.PlaceExtraOnOneSpace);
        addRenderer(new ResourcesTakenRenderer(card.getReward()));
        player.getResources().incrementFromResources(handlePlotQuestResourceRewards(player, card.getReward()));
    }
    private void handlePlaceExtraOnTwoSpacesIntrigue(GamePlayer player, PlaceExtraOnTwoSpacesIntrigue card) {
        player.getTurnState().addIntrigueAction(IntrigueActions.PlaceExtraOnTwoSpaces);
        addRenderer(new ResourcesTakenRenderer(card.getReward()));
        player.getResources().incrementFromResources(handlePlotQuestResourceRewards(player, card.getReward()));
    }
    private void handleResourceForVictoryIntrigue(GamePlayer player, ResourceForVictoryIntrigue card) {
        // Go ahead and add the resources now.
        player.getTurnState().addIntrigueAction(IntrigueActions.ResourcesForVictoryPoints);
        Resources baseReward = card.getOwnerReward().getResources();
        addRenderer(new ResourcesTakenRenderer(baseReward));
        player.getResources().incrementFromResources(handlePlotQuestResourceRewards(player, baseReward));
    }
    private void handleShareResourcesSingleIntrigue(GamePlayer player, ShareResourcesSingleIntrigue card) {
        if(card.getPlayerChoices().size() == 1) {
            // Go ahead and add the resources now.
            Resources baseReward = card.getPlayerChoices().get(0).getResources();
            addRenderer(new ResourcesTakenRenderer(baseReward));
            player.getResources().incrementFromResources(handlePlotQuestResourceRewards(player, baseReward));
        }
    }
    
    private void handleIntrigueWork(GamePlayer player, Intrigue card) {
        // Plot quests that help this?
        handlePlotQuestIntriguePlayedRewards(player);
        if(card instanceof DrawQuestIntrigue) {
            // nothing extra needed for this one, we already added it.
        } else if (card instanceof MandatoryQuestIntrigue) {
            player.getTurnState().addIntrigueAction(IntrigueActions.RequiresMandatoryQuestTarget);
        } else if (card instanceof ReturnOneCorruptionIntrigue) {
            if(player.getResources().getCorruption() > 0) {
                player.getResources().decrementFromResources(Res.corruption(1));
            }
        } else if (card instanceof ReturnAgentIntrigue) {
            player.getTurnState().addIntrigueAction(IntrigueActions.RequiresReturningAgent);
        } else if (card instanceof AgentPlacementIntrigue) {
            // Remove the agent from the spot that this came from
            player.getTurnState().getBuilding().setPlayerOccupantID(null);
            player.decrementAgentsInHarbor();
            player.decrementAgentsPlaced();
        } else if (card instanceof ReallocateResourcesIntrigue) {
            handleReallocateResourcesIntrigue(player, (ReallocateResourcesIntrigue)card);
        } else if (card instanceof GiveResourceForVPIntrigue) {
            handleGiveResourceForVictoryIntrigue(player, (GiveResourceForVPIntrigue)card);
        } else if (card instanceof GainResourcesIntrigue) {
            handleGainResourcesIntrigue(player, (GainResourcesIntrigue)card);
        } else if (card instanceof ResourceForVictoryIntrigue) {
            handleResourceForVictoryIntrigue(player, (ResourceForVictoryIntrigue)card);
        } else if (card instanceof DrawThreeOppsOneIntrigue) {
            handleDrawThreeOppsOneIntrigue(player, (DrawThreeOppsOneIntrigue)card);
        } else if (card instanceof DrawTwoIntrigue) {
            handleDrawTwoIntrigue(player, (DrawTwoIntrigue)card);
        } else if (card instanceof BuyBuildingIntrigue) {
            handleBuyBuildingIntrigue(player, (BuyBuildingIntrigue)card);
        } else if (card instanceof OptionalResourceIntrigue) {
            handleOptionalResourceIntrigue(player, (OptionalResourceIntrigue)card);
        } else if (card instanceof StealResourcesIntrigue) {
            handleStealResourcesIntrigue(player, (StealResourcesIntrigue)card);
        } else if (card instanceof ShareResourcesIntrigue) {
            handleShareResourcesIntrigue(player, (ShareResourcesIntrigue)card);
        } else if (card instanceof DrawQuestForHouseIntrigue) {
            player.getTurnState().addIntrigueAction(IntrigueActions.RequiresHouseDraw);
        } else if (card instanceof PlaceExtraOnTwoSpacesIntrigue) {
            handlePlaceExtraOnTwoSpacesIntrigue(player, (PlaceExtraOnTwoSpacesIntrigue)card);
        } else if (card instanceof PlaceExtraOnOneSpaceIntrigue) {
            handlePlaceExtraOnOneSpaceIntrigue(player, (PlaceExtraOnOneSpaceIntrigue)card);
        } else if (card instanceof ShareResourcesSingleIntrigue) {
            handleShareResourcesSingleIntrigue(player, (ShareResourcesSingleIntrigue)card);
        } else if (card instanceof VictoryForCorruptionIntrigue) {
            boolean success = true;
            for(GamePlayer p : players) {
                if(!p.getPlayerID().equals(player.getPlayerID())) {
                    if(p.getResources().getCorruption() <= player.getResources().getCorruption()) {
                        success = false;
                    }
                }
            }
            if(success) {
                player.addVictoryPoints(6);
                addRenderer(new H4Renderer("Gained 6 victory points for Honorable Example"));
            }
        } else if (card instanceof ReturnTwoCorruptionAndResourceIntrigue) {
            handleReturnTwoCorruptionAndResourceIntrigue(player, (ReturnTwoCorruptionAndResourceIntrigue)card);
        } else {
            throw new UnsupportedOperationException("No handler for this intrigue " + card.getIntrigueInfo().getName());
        }
        // Remove the intrigue card from the player
        player.removeIntrigueCard(card);
    }

    public void completeQuestInCliffwatchInn(GamePlayer player, Quests questTofinish) {
        if(LogicHelper.hasQuestInCollection(player.getCompletedQuests(), Quests.DiplomaticMissionToSuzail)) {
            if(LogicHelper.hasQuestInCollection(cliffWatchInnQuests, questTofinish)) {
                
                Quest quest = LogicHelper.findQuestInCollection(cliffWatchInnQuests, questTofinish);
                cliffWatchInnQuests.remove(quest);
                Quest newQuest = dealQuestFromDeck();
                cliffWatchInnQuests.add(newQuest);
                addRenderer(new DrawQuestRenderer(quest, newQuest, "selected to complete via Diplomatic Mission to Suzial"));
                completeQuestGuts(player, quest);
            } else {
                throw new UnsupportedOperationException("Quest " + questTofinish.getName() + " is not in Cliffwatch inn!");
            }
        } else {
            throw new UnsupportedOperationException("completeQuestInCliffwatchInn can only be used if the player has completed Diplomatic Mission to Suzial");
        }
    }
    
    private void completeQuestGuts(GamePlayer player, Quest q) {
        if(player.getResources().containsResources(q.getCost())) {
            player.getResources().decrementFromResources(q.getCost());
            int intrigueCount = q.getReward().getIntrigue();
            player.getResources().incrementFromResources(q.getReward());
            player.addVictoryPoints(q.getQuestInfo().getVictoryPoints());
            addRenderer(new QuestCompletedRenderer(q));
            player.completeQuest(q);
            handlePlotQuestQuestCompleted(player, q.getQuestInfo());
            // Special one time offers
            switch(q.getQuestInfo().getQuestAction()) {
                case None:
                    // Nothing needed here
                    break;
                case ExtraAgent:
                    player.addAnotherAgent();
                    break;
                case OpponentGetsFourGold:
                    player.getTurnState().addQuestAction(QuestActions.OpponentGetsFourGold);
                    break;
                case TakeAllQuestsInCliffwatchInn:
                    player.getTurnState().addQuestAction(QuestActions.TakeAllQuestsInCliffwatchInn);
                    List<Quests> lq = new ArrayList<Quests>();
                    for(Quest cwq : cliffWatchInnQuests) {
                        lq.add(cwq.getQuestInfo());
                    }
                    for(Quests qs : lq) {
                        takeQuestAsQuestReward(player, qs);
                    }
                    break;
                case TakeAllBuildingsInBuildersHall:
                    List<Buildings> lb = new ArrayList<Buildings>();
                    for(Building b : buildingsInBuildersHall) {
                        lb.add(b.getBuildingInfo());
                    }
                    for(Buildings bs : lb) {
                        takeBuildingAsAction(player, bs);
                    }
                    break;
                case ReturnAgent:
                    player.getTurnState().addQuestAction(QuestActions.ReturnAgent);
                    break;
                case QuestFromCliffwatch:
                    // Need to add an action for this!
                    player.getTurnState().addQuestAction(QuestActions.QuestFromCliffwatch);
                    break;
                case RemoveUpToThreeCorruption:
                    if(player.getResources().getCorruption() > 3) {
                        addCorruptionRemoved(3);
                        player.getResources().decrementFromResources(Res.corruption(3));
                    } else {
                        addCorruptionRemoved(player.getResources().getCorruption());
                        player.getResources().setCorruption(0);
                    }
                    break;
                case ReturnUpToThreeCorruption:
                    if(player.getResources().getCorruption() > 3) {
                        player.getResources().decrementFromResources(Res.corruption(3));
                    } else {
                        player.getResources().setCorruption(0);
                    }
                    break;
                case ReturnOneCorruption:
                    if(player.getResources().getCorruption() > 0) {
                        player.getResources().decrementFromResources(Res.corruption(1));
                    }
                    break;
                case BuildingFromHall:
                    player.getTurnState().addQuestAction(QuestActions.BuildingFromHall);
                    break;
                case BuildingFromDeck:
                    player.getTurnState().addQuestAction(QuestActions.BuildingFromDeck);
                    Building b = dealBuildingFromDeck();
                    b.processComingIntoPlay();
                    buildingsInPlay.put(b.getBuildingInfo(), b);
                    b.setPlayerOwnerID(player.getPlayerID());
                    player.addOwnedBuildings(b);
                    addRenderer(new DrawnBuildingRenderer(b));
                    break;
                case TwoBuildingsFromBuildersHall:
                    player.getTurnState().addQuestAction(QuestActions.TwoBuildingsFromBuildersHall);
                    break;
                case WildCardQuestRewardAndReturnUpToThreeAgents:
                    player.getTurnState().addQuestAction(QuestActions.WildCardQuestRewardAndReturnUpToThreeAgents);
                    break;
                case EachOpponentReturnsOneResource:
                    break;
                case ExtraPointsForBuildings:
                    //Get 4 points for each owned building
                    player.addVictoryPoints(player.getOwnedBuildings().size() * 4);
                    // TODO Need to code this.
                    break;
                case ReturnUpToTwoCorruption:
                    if(player.getResources().getCorruption() > 2) {
                        player.getResources().decrementFromResources(Res.corruption(2));
                    } else {
                        player.getResources().setCorruption(0);
                    }
                    break;
                default:
                    if(!(q instanceof PlotQuest)) {
                        throw new UnsupportedOperationException("Did not handle quest action for " + q.getQuestInfo().getName());

                    }
            }
            for(int ic = 0; ic < intrigueCount; ic++) {
                Intrigue bi = dealIntrigueFromDeck();
                player.addIntrigueCard(bi);
                addRenderer(new DrawIntrigueRenderer(player, bi));
            }
        } else {
            throw new UnsupportedOperationException("You can't afford to complete the quest " + q.getQuestInfo().getName());
        }
    }
    
    public void playIntrigueDrawnAsAction(GamePlayer player) {
        Intrigue workingIntrigue = player.getTurnState().getIntrigueDrawnAsAction();
        if(workingIntrigue != null) {
            playIntrigue(player, workingIntrigue.getIntrigueInfo(), true);
            player.getTurnState().removeTurnAction(TurnActions.IntrigueDrawnAsAction);
            player.getTurnState().setIntrigueDrawnAsAction(null);
            player.getTurnState().setWorkingIntrigue(workingIntrigue);
        } else {
            throw new UnsupportedOperationException("You don't have an intrigue drawn as an action");
        }
    }
    
    public void completeMandatoryQuest(GamePlayer player, Quests quest) {
        if(LogicHelper.hasQuestInCollection(player.getCurrentQuests(), quest)) {
            Quest q = LogicHelper.findQuestInCollection(player.getCurrentQuests(), quest);
            completeQuestGuts(player, q);
        } else {
            throw new UnsupportedOperationException("You don't have the mandatory quest " + quest.getName());
            
        }
        
    }
    public void completeQuest(GamePlayer player, Quests quest) {
        if(LogicHelper.hasQuestInCollection(player.getCurrentQuests(), quest)) {
            Quest q = LogicHelper.findQuestInCollection(player.getCurrentQuests(), quest);
            completeQuestGuts(player, q);
        } else {
            throw new UnsupportedOperationException("You don't have the quest " + quest.getName());
        }
    }
    
    public void chooseOpponentForMandatoryQuest(GamePlayer player, GamePlayer opponent) {
        if(!LogicHelper.isSamePlayer(player, opponent)) {
            Intrigue bi = player.getTurnState().getWorkingIntrigue();
            if(bi.getIntrigueInfo().getType() == IntrigueTypes.MandatoryQuest) {
                MandatoryQuest mq = (MandatoryQuest)LogicHelper.findMandatoryQuest(bi.getIntrigueInfo());
                opponent.getCurrentQuests().add(mq);
                addRenderer(new MandatoryQuestRenderer(opponent, mq));
                player.getTurnState().addTurnAction(TurnActions.MandatoryQuestGiven);
            } else {
                throw new UnsupportedOperationException("This intrigue is not a mandatory quest");
            }
        } else {
            throw new UnsupportedOperationException("You must choose an opponent for the mandatory quest");
        }
        
    }
    public void chooseOpponentForReward(GamePlayer player, GamePlayer opponent) {
        if(!LogicHelper.isSamePlayer(player, opponent)) {
            Intrigue bi = player.getTurnState().getWorkingIntrigue();
            if(bi instanceof GiveResourceForVPIntrigue) {
                GiveResourceForVPIntrigue grvi = (GiveResourceForVPIntrigue)bi;
                if(player.getResources().containsResources(grvi.getResGiven())) {
                    player.getResources().decrementFromResources(grvi.getResGiven());
                    opponent.getResources().incrementFromResources(grvi.getResGiven());
                    player.addVictoryPoints(grvi.getVictoryPoints());
                    addRenderer(new PlayerPaidRenderer(player, grvi, grvi.getResGiven()));
                    addRenderer(new PlayerReceivedRenderer(opponent, grvi, grvi.getResGiven()));
                    addRenderer(new PlayerReceivedRenderer(opponent, grvi, new Resources(Res.victoryPoints(grvi.getVictoryPoints()))));
                    player.getTurnState().addTurnAction(TurnActions.ResourcesForVPGiven);
                    
                }
            } else if(bi instanceof OptionalResourceIntrigue) {
                OptionalResourceIntrigue ori = (OptionalResourceIntrigue)bi;
                Resources baseReward = ori.getOptionalReward();
                addRenderer(new PlayerReceivedRenderer(opponent, bi, baseReward));
                opponent.getResources().incrementFromResources(baseReward);
            } else if(bi instanceof ShareResourcesSingleIntrigue) {
                ShareResourcesSingleIntrigue srsi = (ShareResourcesSingleIntrigue)bi;
                if(srsi.getOpponentsChoices().size() == 1) {
                    Resources baseReward = srsi.getOpponentsChoices().get(0).getResources();
                    addRenderer(new PlayerReceivedRenderer(opponent, bi, baseReward));
                    opponent.getResources().incrementFromResources(baseReward);
                } else {
                    throw new UnsupportedOperationException("Need to supply choice for intrigue");
                }
            }
        } else {
            throw new UnsupportedOperationException("Cannot choose same player for opponent reward");
        }
    }
    
    public void recallAgents(GamePlayer player, Buildings... buildingsWithAgent) {
        if(player.getTurnState().hasBuildingAction(BuildingActions.TakeCorruptionReturnAgents)) {
            if(player.getTurnState().getCorruptionAgents() == buildingsWithAgent.length) {
                for(Buildings buildingWithAgent : buildingsWithAgent) {
                    Building bldg = findBuildingInPlay(buildingWithAgent);
                    if(bldg != null) {
                        if(player.getPlayerID().equals(bldg.getPlayerOccupantID())) {
                            bldg.setPlayerOccupantID(null);
                            player.decrementAgentsPlaced();
                            addRenderer(new AgentRemovalRenderer(bldg));
                        } else {
                            throw new UnsupportedOperationException("Player " + player.getName() + " does not have and agent on " + buildingWithAgent.getName());
                        }
                    }
                }
                player.getTurnState().addTurnAction(TurnActions.HandledCorruptionReturnAgents);
            } else {
                throw new UnsupportedOperationException("Invalid number of buildings to recall agents from: expected " + player.getTurnState().getCorruptionAgents());
            }
        } else if(player.getTurnState().hasQuestAction(QuestActions.WildCardQuestRewardAndReturnUpToThreeAgents)) {
            if(buildingsWithAgent != null && buildingsWithAgent.length <= 3) {
                for(Buildings buildingWithAgent : buildingsWithAgent) {
                    Building bldg = findBuildingInPlay(buildingWithAgent);
                    if(bldg != null) {
                        if(player.getPlayerID().equals(bldg.getPlayerOccupantID())) {
                            bldg.setPlayerOccupantID(null);
                            player.decrementAgentsPlaced();
                            addRenderer(new AgentRemovalRenderer(bldg));
                        }
                    }
                    
                }
                player.getTurnState().addTurnAction(TurnActions.AgentsReturned);
            } else {
                throw new UnsupportedOperationException("Invalid number of buildings to recall agents from");
            }
        }
        
    }
    public void recallAgent(GamePlayer player, Buildings buildingWithAgent) {
        if(player.getTurnState().hasIntrigueAction(IntrigueActions.RequiresReturningAgent) || player.getTurnState().hasQuestAction(QuestActions.ReturnAgent)) {
            Building bldg = findBuildingInPlay(buildingWithAgent);
            if(bldg != null) {
                if(player.getPlayerID().equals(bldg.getPlayerOccupantID())) {
                    bldg.setPlayerOccupantID(null);
                    player.decrementAgentsPlaced();
                    addRenderer(new AgentRemovalRenderer(bldg));
                    player.getTurnState().addTurnAction(TurnActions.AgentReturned);
                }
            }
        }
    }
    
    public void returnResources(GamePlayer player, ResourceChoices choice) {
        if(player.getTurnState().hasIntrigueAction(IntrigueActions.ReturnTwoCorruptionAndResource)) {
            ReturnTwoCorruptionAndResourceIntrigue sri = (ReturnTwoCorruptionAndResourceIntrigue)player.getTurnState().getWorkingIntrigue();
            if(sri.getOptions().contains(choice)) {
                if(player.getResources().containsResources(choice.getResources())) {
                    player.getResources().decrementFromResources(choice.getResources());
                } else {
                    throw new UnsupportedOperationException("Resources not available");
                }
            } else {
                throw new UnsupportedOperationException("Invalid resource choice");
            }
        }
        
    }
    public void stealResources(GamePlayer player, GamePlayer opponent, ResourceChoices choice) {
        if(player.getTurnState().hasIntrigueAction(IntrigueActions.StealResourcesFromPlayer)) {
            if(!player.getPlayerID().equals(opponent.getPlayerID())) {
                StealResourcesIntrigue sri = (StealResourcesIntrigue)player.getTurnState().getWorkingIntrigue();
                if(sri.getChoices().contains(choice)) {
                    if(opponent.getResources().containsResources(choice.getResources())) {
                        opponent.getResources().decrementFromResources(choice.getResources());
                        addRenderer(new ResourcesTakenRenderer(choice.getResources()));
                        player.getResources().incrementFromResources(handlePlotQuestResourceRewards(player, choice.getResources()));
                        player.getTurnState().addTurnAction(TurnActions.ResourcesStolenFromPlayer);
                        
                    }
                }
            }
        }
    }
    
    public void selectHouse(GamePlayer player, Houses house) {
        if(player.getTurnState().hasIntrigueAction(IntrigueActions.RequiresHouseDraw)) {
            boolean questFound = false;
            do {
                Quest q = dealQuestFromDeck();
                if(q.getQuestInfo().getHouse() == house) {
                    player.addCurrentQuest(q);
                    addRenderer(new DrawQuestRenderer(q, null, "drawn"));
                    questFound = true;
                } else {
                    discardedQuests.add(q);
                    addRenderer(new DrawQuestRenderer(q, null, "discarded"));
                }
                
            } while(!questFound);
            player.getTurnState().addTurnAction(TurnActions.HouseSelected);
        } else {
            throw new UnsupportedOperationException("addHouse is not a legal action");
        }
    }
    
    public void playIntrigue(GamePlayer player, Intrigues card, boolean asAction) {
        if(player.getTurnState().hasBuildingAction(BuildingActions.PlayIntrigue) || player.getTurnState().hasBuildingAction(BuildingActions.PlayThreeIntrigues)) {
            if(LogicHelper.hasIntrigueInCollection(player.getIntrigueCards(), card)) {
                Intrigue bi = LogicHelper.findIntrigueInCollection(player.getIntrigueCards(), card);
                player.getTurnState().setWorkingIntrigue(bi);
                if(!asAction) {
                    player.getTurnState().addTurnAction(TurnActions.IntriguePlayed);
                }
                addRenderer(new IntriguePlayedRenderer(bi, asAction));
                handleIntrigueWork(player, bi);
            } else {
                throw new UnsupportedOperationException("Player " + player.getName() + " does not have " + card.getName());
            }
        }
    }
    
    public void takeVariableIntrigueReward(GamePlayer player, ResourceChoices choice) {
        Intrigue bi = player.getTurnState().getWorkingIntrigue();
        if(bi instanceof ShareResourcesIntrigue) {
            ShareResourcesIntrigue sri = (ShareResourcesIntrigue)bi;
            if(sri.getPlayerChoices().contains(choice)) {
               addRenderer(new VariableRewardRenderer(null, choice));
               Resources reward = choice.getResources();
               player.getResources().incrementFromResources(handlePlotQuestResourceRewards(player, reward));
            }
        } else if(bi instanceof GainResourcesIntrigue) {
            GainResourcesIntrigue gri = (GainResourcesIntrigue)bi;
            if(gri.getChoices().contains(choice)) {
               addRenderer(new VariableRewardRenderer(null, choice));
               Resources reward = choice.getResources();
               player.getResources().incrementFromResources(handlePlotQuestResourceRewards(player, reward));
               player.getTurnState().addTurnAction(TurnActions.GainResourcesChosen);
            }
        }
    }
    
    public void selectTargetBuildings(GamePlayer player, Buildings... bldgs) {
        if(player.getTurnState().hasBuildingAction(BuildingActions.ReallocateResources)) {
           Building bldg = player.getTurnState().getBuilding();
            BuildingReallocatedReward reward = bldg.getReallocatedReward();
            if(reward.getNumberOfReallocations() == bldgs.length) {
                for(Buildings b : bldgs) {
                    Building target = findBuildingInPlay(b);
                    target.addExtraReward(new BuildingExtraReward(reward));
                    addRenderer(new ResourceReallocationRenderer(target, reward));
                }
                player.getTurnState().addTurnAction(TurnActions.ReallocateResourcesHandled);
            }
        } else if(player.getTurnState().hasIntrigueAction(IntrigueActions.PlaceExtraOnOneSpace)) {
            if(bldgs.length == 1) {
                PlaceExtraOnOneSpaceIntrigue osi = (PlaceExtraOnOneSpaceIntrigue)player.getTurnState().getWorkingIntrigue();
                Building target = findBuildingInPlay(bldgs[0]);
                target.addExtraReward(new BuildingExtraReward(osi.getReward()));
                addRenderer(new ResourceReallocationRenderer(target, osi.getReward()));
                player.getTurnState().addTurnAction(TurnActions.ResourcesPlacedOneSpace);
            }
        } else if(player.getTurnState().hasIntrigueAction(IntrigueActions.PlaceExtraOnTwoSpaces)) {
            if(bldgs.length == 2) {
                PlaceExtraOnTwoSpacesIntrigue osi = (PlaceExtraOnTwoSpacesIntrigue)player.getTurnState().getWorkingIntrigue();
                for(Buildings b : bldgs) {
                    Building target = findBuildingInPlay(b);
                    target.addExtraReward(new BuildingExtraReward(osi.getReward()));
                    addRenderer(new ResourceReallocationRenderer(target, osi.getReward()));
                }
                player.getTurnState().addTurnAction(TurnActions.ResourcesPlacedTwoSpaces);
            }
        } else {
            throw new UnsupportedOperationException("Building not ready to handle reallocation");
        }
    }
    
    public void takeVariableReward(GamePlayer player, ResourceChoices choice) {
        if (player.getTurnState().hasBuildingAction(BuildingActions.RequiresVariableReward)) {
            Building bldg = player.getTurnState().getBuilding();
            List<ResourceChoices> choices = bldg.getBuildingRewardOptions();
            if (choices.contains(choice)) {
                addRenderer(new VariableRewardRenderer(bldg.getBuildingInfo(), choice));
                Resources reward = choice.getResources();
                Resources extraReward = bldg.retrieveFullRewardAndReset();
                if (extraReward.isNotEmpty()) {
                    reward.incrementFromResources(extraReward);
                }
                player.getResources().incrementFromResources(handlePlotQuestResourceRewards(player, reward));
                player.getTurnState().addTurnAction(TurnActions.VariableRewardHandled);
            }
        }
    }
    
    public void takeVariableOwnerReward(GamePlayer player, ResourceChoices choice) {
        if (player.getTurnState().hasBuildingAction(BuildingActions.RequiresVariableOwnerReward)) {
            Building bldg = player.getTurnState().getBuilding();
            GamePlayer owner = findPlayerWithID(bldg.getPlayerOwnerID());
            if((owner != null && !owner.equals(player.getPlayerID())) || LogicHelper.hasQuestInCollectionWithAction(player.getCompletedQuests(), QuestActions.GainOwnerRewardOnBuildings)) {
                List<ResourceChoices> choices = bldg.getOwnerRewardOptions();
                if (choices.contains(choice)) {
                    addRenderer(new VariableOwnerRewardRenderer(bldg.getBuildingInfo(), choice));
                    owner.getResources().incrementFromResources(choice.getResources());
                    player.getTurnState().addTurnAction(TurnActions.VariableOwnerRewardHandled);
                }
            }
        }
    }
    
    public void takeVariableIntrigueOwnerReward(GamePlayer player, ResourceChoices choice) {
        Intrigue bi = player.getTurnState().getWorkingIntrigue();
        if (bi instanceof ShareResourcesIntrigue) {
            ShareResourcesIntrigue sri = (ShareResourcesIntrigue) bi;
            if (sri.getOpponentsChoices().contains(choice)) {
                for (GamePlayer gp : players) {
                    if (!LogicHelper.isSamePlayer(gp, player)) {
                        addRenderer(new PlayerReceivedRenderer(gp, bi, choice.getResources()));
                        gp.getResources().incrementFromResources(choice.getResources());
                    }
                }
            }
        }
    }
    
    public void takeQuestFromCliffwatch(GamePlayer player, Quests questToTake) {
        if(player.getTurnState().hasBuildingAction(BuildingActions.TakeQuest)) {
            if(LogicHelper.hasQuestInCollection(cliffWatchInnQuests, questToTake)) {
                Quest quest = LogicHelper.findQuestInCollection(cliffWatchInnQuests, questToTake);
                player.addCurrentQuest(quest);
                cliffWatchInnQuests.remove(quest);
                Quest newQuest = dealQuestFromDeck();
                cliffWatchInnQuests.add(newQuest);
                player.getTurnState().addTurnAction(TurnActions.QuestTaken);
                addRenderer(new DrawQuestRenderer(quest, newQuest));
                handlePlotQuestQuestTaken(player, questToTake, true);
            }
        }
    }
    
    public void takeBuildingsFromBuildersHallAsQuestReward(GamePlayer player, Buildings... buildingsToTake) {
        if(player.getTurnState().hasQuestAction(QuestActions.TwoBuildingsFromBuildersHall)) {
            if(buildingsToTake.length == 2) {
                for(Buildings thisBuilding : buildingsToTake) {
                    if(LogicHelper.hasBuildingInCollection(buildingsInBuildersHall, thisBuilding)) {
                        takeBuildingAsAction(player, thisBuilding);
                    } else {
                        throw new UnsupportedOperationException("Building " + thisBuilding.getName() + " is not in Builders' Hall");
                    }
                }
            }
            player.getTurnState().addTurnAction(TurnActions.QuestActionTwoBuildingsTaken);
        }
    }
    public void takeBuildingFromBuildersHallAsQuestReward(GamePlayer player, Buildings buildingToTake) {
        if(player.getTurnState().hasQuestAction(QuestActions.BuildingFromHall)) {
            if(LogicHelper.hasBuildingInCollection(buildingsInBuildersHall, buildingToTake)) {
                takeBuildingAsAction(player, buildingToTake);
                player.getTurnState().addTurnAction(TurnActions.QuestActionBuildingTaken);
            }
        }
        
    }
    public void takeQuestAsQuestReward(GamePlayer player, Quests questToTake) {
        if(player.getTurnState().hasQuestAction(QuestActions.QuestFromCliffwatch)) {
            if(LogicHelper.hasQuestInCollection(cliffWatchInnQuests, questToTake)) {
                Quest quest = LogicHelper.findQuestInCollection(cliffWatchInnQuests, questToTake);
                player.addCurrentQuest(quest);
                cliffWatchInnQuests.remove(quest);
                Quest newQuest = dealQuestFromDeck();
                cliffWatchInnQuests.add(newQuest);
                player.getTurnState().addTurnAction(TurnActions.QuestActionQuestTaken);
                addRenderer(new DrawQuestRenderer(quest, newQuest));
            }
        } else if(player.getTurnState().hasQuestAction(QuestActions.TakeAllQuestsInCliffwatchInn)) {
            if(LogicHelper.hasQuestInCollection(cliffWatchInnQuests, questToTake)) {
                Quest quest = LogicHelper.findQuestInCollection(cliffWatchInnQuests, questToTake);
                player.addCurrentQuest(quest);
                handlePlotQuestQuestTaken(player, questToTake, false);
                cliffWatchInnQuests.remove(quest);
                Quest newQuest = dealQuestFromDeck();
                cliffWatchInnQuests.add(newQuest);
                addRenderer(new DrawQuestRenderer(quest, newQuest));
            }
        }
    }
    
    protected GamePlayer findPlayerWithID(Long playerID) {
        GamePlayer result = null;
        for(GamePlayer p : players) {
            if(p.getPlayerID().equals(playerID)) {
                result = p;
                break;
            }
        }
        return result;
    }
    
    public void handlePlotQuestIntriguePlayedRewards(GamePlayer player) {
        for(Quest q : player.getCompletedQuests()) {
            switch(q.getQuestInfo().getQuestAction()) {
                case DrawIntrigueWhenPlayIntrigue:
                    // If this is an "real" intruge action, rather than the second phase
                    if(!player.getTurnState().hasTurnAction(TurnActions.IntrigueDrawnAsAction)) {
                        Intrigue i = dealIntrigueFromDeck();
                        player.addIntrigueCard(i);
                        player.getTurnState().addTurnAction(TurnActions.IntrigueDrawnAsAction);
                        player.getTurnState().setIntrigueDrawnAsAction(i);
                        addRenderer(new DrawIntrigueRenderer(i));
                    }
                    break;
                case TwoVictoryForIntriguePlayed:
                    addRenderer(new SpecialQuestEffectRenderer(q, Res.victoryPoints(2)));
                    player.addVictoryPoints(2);
                    break;
            }
        }
    }
    
    public Resources handlePlotQuestResourceRewards(GamePlayer player, Resources res) {
        Resources result = new Resources(res);
        for(Quest q : player.getCompletedQuests()) {
            switch(q.getQuestInfo().getQuestAction()) {
                case FighterGivesFighter:
                    if(res.getFighter() > 0) {
                        result.addFighter(1);
                        addRenderer(new SpecialQuestEffectRenderer(q, Res.fighter(1)));
                    }
                    break;
                case WizardDrawsIntrigue:
                    if(res.getWizard() > 0) {
                        Intrigue i = dealIntrigueFromDeck();
                        player.addIntrigueCard(i);
                        addRenderer(new SpecialQuestEffectRenderer(q, i));
                    }
                    break;
                case GoldGivesRogues:
                    if(res.getGold() > 0) {
                        result.addRogue(1);
                        addRenderer(new SpecialQuestEffectRenderer(q, Res.rogue(1)));
                    }
                    break;
                case RogueGivesGold:
                    if(res.getRogue() > 0) {
                        result.addGold(2);
                        addRenderer(new SpecialQuestEffectRenderer(q, Res.gold(2)));
                    }
                    break;
                default:
                    // This is all of the plot quests that give extra stuff.
                    break;
            }
        }
        return result;
    }

    public void handlePlotQuestQuestTaken(GamePlayer player, Quests quest, boolean takenViaAction) {
        for(Quest q : player.getCompletedQuests()) {
            switch(q.getQuestInfo().getQuestAction()) {
                case ArcanaQuestTakenGivesWizard:
                    if(quest.getHouse() == Houses.Arcana) {
                        addRenderer(new SpecialQuestEffectRenderer(q, Res.wizard(1)));
                        Resources r = new Resources(Res.wizard(1));
                        if(takenViaAction) {
                            r = handlePlotQuestResourceRewards(player, r);
                        }
                        player.getResources().incrementFromResources(r);
                    }
                    break;
                case CommerceQuestTakenGivesFourGold:
                    if(quest.getHouse() == Houses.Commerce) {
                        addRenderer(new SpecialQuestEffectRenderer(q, Res.gold(4)));
                        Resources r = new Resources(Res.gold(4));
                        if(takenViaAction) {
                            r = handlePlotQuestResourceRewards(player, r);
                        }
                        player.getResources().incrementFromResources(r);
                    }
                    break;
                case PietyQuestTakenGivesCleric:
                    if(quest.getHouse() == Houses.Piety) {
                        addRenderer(new SpecialQuestEffectRenderer(q, Res.cleric(1)));
                        Resources r = new Resources(Res.cleric(1));
                        if(takenViaAction) {
                            r = handlePlotQuestResourceRewards(player, r);
                        }
                        player.getResources().incrementFromResources(r);
                    }
                    break;
                case SkullduggeryQuestTakenGivesTwoRogues:
                    if(quest.getHouse() == Houses.Skullduggery) {
                        addRenderer(new SpecialQuestEffectRenderer(q, Res.rogue(2)));
                        Resources r = new Resources(Res.rogue(2));
                        if(takenViaAction) {
                            r = handlePlotQuestResourceRewards(player, r);
                        }
                        player.getResources().incrementFromResources(r);
                    }
                    break;
                case WarfareQuestTakenGivesTwoFighters:
                    if(quest.getHouse() == Houses.Warfare) {
                        addRenderer(new SpecialQuestEffectRenderer(q, Res.fighter(2)));
                        Resources r = new Resources(Res.fighter(2));
                        if(takenViaAction) {
                            r = handlePlotQuestResourceRewards(player, r);
                        }
                        player.getResources().incrementFromResources(r);
                    }
                    break;
                default:
                    break;
            }
            
        }
        
    }
    
    public void returnOneQuestResourceUsed(GamePlayer player, Res used) {
        if(player.getTurnState().hasQuestAction(QuestActions.ReturnOneResourceFromCompletedQuest)) {
            Quest quest = LogicHelper.findQuestInCollection(player.getCompletedQuests(), Quests.SeizeControlOfTheBloodyHand);
            Resources res = player.getTurnState().getQuestResourcesUsed();
            if(quest != null && res.containsResources(new Resources(used))) {
                if(used.getCount() == 1) {
                    player.getResources().incrementFromResources(used);
                    addRenderer(new SpecialQuestEffectRenderer(quest, used));
                    player.getTurnState().addTurnAction(TurnActions.QuestActionReturnedResource);
                } else {
                    throw new UnsupportedOperationException("You only get to reclaim one resource");
                }
            } else {
                if(quest == null) {
                    throw new UnsupportedOperationException("Quest not found to determine resources");
                } else {
                    throw new UnsupportedOperationException("Resource not used in quest-" + res.makeDisplayable());
                }
            }
        } else {
            throw new UnsupportedOperationException("Cannot return quest resource with proper completed quest");
        }
    }
    
    public void handlePlotQuestQuestCompleted(GamePlayer player, Quests quest) {
        Quest thisQuest = LogicHelper.findQuestInCollection(player.getCompletedQuests(), quest);
        for(Quest q : player.getCompletedQuests()) {
            if(q.getQuestInfo() != quest) {
                // The plot quest will already be in the list, so skip it...
                switch(q.getQuestInfo().getQuestAction()) {
                    case ReturnOneResourceFromCompletedQuest:
                        player.getTurnState().addQuestAction(QuestActions.ReturnOneResourceFromCompletedQuest);
                        player.getTurnState().setQuestResourcesUsed(thisQuest.getCost());
                        break;
                    case TwoVictoryForHouseMatch:
                        if(q.getQuestInfo().getHouse() == quest.getHouse()) {
                            player.addVictoryPoints(2);
                            addRenderer(new SpecialQuestEffectRenderer(q, Res.victoryPoints(2)));
                        }
                        break;
                    default:
                        // This is all of the plot quests that give extra stuff.
                        break;
                }
            }
        }
    }

    public void endTurn() {
        addRenderer(new AtAGlanceRenderer(players));
    }
    
    protected boolean regularTurnDone() {
        boolean result = true;
        for(GamePlayer gp : players) {
            if(gp.getAgentCount() != gp.getAgentsPlaced()) {
                throw new UnsupportedOperationException("Player " + gp.getName() + " has placed " + gp.getAgentsPlaced() + " out of " + gp.getAgentCount());
//                result = false;
//                break;
            }
        }
        return result;
    }
    
    public void reallocateAgent(GamePlayer player, Buildings sourceBuilding, Buildings targetBuilding) {
        if(regularTurnDone()) {
            Building b = findBuildingInPlay(sourceBuilding);
            if(b == null) {
                throw new UnsupportedOperationException(sourceBuilding.getName() + " is not in play");
            } else {
                if(b.isWaterdeepHarbor()) {
                    if(player.getPlayerID().equals(b.getPlayerOccupantID())) {
                        b.setPlayerOccupantID(null);
                        player.decrementAgentsInHarbor();
                        player.decrementAgentsPlaced();
                        placeAgent(player, sourceBuilding, targetBuilding);
                    } else {
                        throw new UnsupportedOperationException(sourceBuilding.getName() + " does not have an agent for " + player.getName());
                    }
                } else {
                    throw new UnsupportedOperationException("Can only relocate from Waterdeep Harbor");
                }
            }
            
        } else {
            throw new UnsupportedOperationException("Not all regular agents have been played");
        }
    }
    
    public void placeAgent(GamePlayer player, Buildings buildingToPlace) {
        placeAgent(player, null, buildingToPlace);
    }
    
    public void placeAgent(GamePlayer player, Buildings buildingToRelocateFrom, Buildings buildingToPlace) {
        // Make sure this player is the proper player;
        if(!player.getName().equals(currentPlayer.getName())) {
            if(currentPlayer == null) {
                currentPlayer = player;
            } else if(currentPlayer.getTurnState() == null) {
                // This is cool, we're doing the next player's turn
                currentPlayer = player;
            } else {
                throw new UnsupportedOperationException("Previous player's turn did not finish properly");
            }
            // WRONG PLAYER!!!
        }
        if(player.getAgentsPlaced() >= player.getAgentCount()) {
            throw new UnsupportedOperationException("Trying to place too many agents!");
        }
        if(!player.isHasAmbassador()) {
            player.incrementAgentsPlaced();
        }
        Building b = checkAgentPlacement(player, buildingToPlace);
        // Set turn state flags based on building taken.
        player.setTurnState(new TurnState(b));
        addRenderer(new HRRenderer());
        if(buildingToRelocateFrom == null) {
            if(player.isHasAmbassador()) {
                addRenderer(new H4Renderer("Player " + player.getName() + " - placing Ambassador"));
            } else {
                addRenderer(new H4Renderer("Player " + player.getName() + " - placing Agent " + player.getAgentsPlaced() + " of " + player.getAgentCount()));
            }
        } else {
            addRenderer(new H4Renderer("Player " + player.getName() + " - relocating Agent"));
            Building bf = findBuildingInPlay(buildingToRelocateFrom);
            addRenderer(new AgentRelocationRenderer(bf));
        }
        addRenderer(new AgentPlacementRenderer(b));

        b.setPlayerOccupantID(player.getPlayerID());
        if(b.isWaterdeepHarbor()) {
            player.incrementAgentsInHarbor();
        }
        if(b.hasBuildingAction(BuildingActions.TakeCorruptionReturnAgents)) {
            player.getTurnState().setCorruptionAgents(b.getTotalStackedReward().getCorruption());
        }
        if(!b.hasBuildingAction(BuildingActions.RequiresVariableActivation)) {
            if(b.getActivationCost().isNotEmpty()) {
                addRenderer(new ResourcesGivenRenderer(b.getActivationCost()));
            }
            player.getResources().decrementFromResources(b.getActivationCost());
        }
        if(!b.hasBuildingAction(BuildingActions.RequiresVariableReward)) {
            Resources baseReward = b.retrieveFullRewardAndReset();
            addRenderer(new ResourcesTakenRenderer(baseReward));
            player.getResources().incrementFromResources(handlePlotQuestResourceRewards(player, baseReward));
        }
        if(!b.hasBuildingAction(BuildingActions.RequiresVariableOwnerReward)) {
            if((b.getPlayerOwnerID() != null && !b.getPlayerOwnerID().equals(player.getPlayerID())) || LogicHelper.hasQuestInCollectionWithAction(player.getCompletedQuests(), QuestActions.GainOwnerRewardOnBuildings)) {
                GamePlayer p = findPlayerWithID(b.getPlayerOwnerID());
                if(p != null) {
                p.getResources().incrementFromResources(b.getOwnerReward());
                }
            }
        }
        
        if(b.hasBuildingAction(BuildingActions.TakeAmbassadorNextTurn)) {
            player.setHasAmbassador(true);
        }
        if(b.hasBuildingAction(BuildingActions.DrawIntrigue)) {
            Intrigue i = dealIntrigueFromDeck();
            player.addIntrigueCard(i);
            player.getTurnState().addTurnAction(TurnActions.IntrigueDrawn);
            addRenderer(new DrawIntrigueRenderer(i));
        }
        if(b.hasBuildingAction(BuildingActions.DrawTwoIntrigue)) {
            for(int x = 0; x < 2; x++) {
                Intrigue i = dealIntrigueFromDeck();
                player.addIntrigueCard(i);
                addRenderer(new DrawIntrigueRenderer(i));
            }
        }
        
        buildingsUsed.add(b);
    }

    public Building findBuildingInPlay(Buildings buildingToCheck) {
        return buildingsInPlay.get(buildingToCheck);
    }
    
    public void debugShowQuests() {
        debugShowQuests(false);
    }
    public void debugShowQuests(boolean plotQuestsOnly) {
        addRenderer(new HRRenderer());
        addRenderer(new H2Renderer("Remaining quests"));
        for(Quest q : cliffWatchInnQuests) {
            if(!plotQuestsOnly || q.getQuestInfo().getQuestType() == QuestTypes.Plot)
            addRenderer(new QuestRenderer(q));
        }
        for(Quest q : quests) {
            if(!plotQuestsOnly || q.getQuestInfo().getQuestType() == QuestTypes.Plot)
            addRenderer(new QuestRenderer(q));
        }
    }
    
    public void debugShowIntrigues() {
        addRenderer(new HRRenderer());
        addRenderer(new H2Renderer("Remaining intrigues"));
        for(Intrigue i : intrigues) {
            addRenderer(new IntrigueRenderer(i));
        }
    }
    
    protected Building checkAgentPlacement(GamePlayer player, Buildings building) {
        Building b = findBuildingInPlay(building);
        if(b == null) {
            throw new UnsupportedOperationException(building.getName() + " is not in play");
        } else {
            if(b.getPlayerOccupantID() != null) {
                if(LogicHelper.hasQuestWithQuestAction(player.getCompletedQuests(), QuestActions.PlaceAgentOnOpponent)) {
                    if(player.getPlayerID().equals(b.getPlayerOccupantID())) {
                        throw new UnsupportedOperationException(b.getBuildingInfo().getName() + " is already occupied by you");
                    }
                } else {
                    throw new UnsupportedOperationException(b.getBuildingInfo().getName() + " is occupied");
                }
            }
        }
        return b;
    }

    /**
     * @return the cliffWatchInnQuests
     */
    public List<Quest> getCliffWatchInnQuests() {
        return cliffWatchInnQuests;
    }

    /**
     * @param cliffWatchInnQuests the cliffWatchInnQuests to set
     */
    public void setCliffWatchInnQuests(List<Quest> cliffWatchInnQuests) {
        this.cliffWatchInnQuests = cliffWatchInnQuests;
    }

    /**
     * @return the quests
     */
    public List<Quest> getQuests() {
        return quests;
    }

    /**
     * @param quests the quests to set
     */
    public void setQuests(List<Quest> quests) {
        this.quests = quests;
    }

    /**
     * @return the intrigues
     */
    public List<Intrigue> getIntrigues() {
        return intrigues;
    }

    /**
     * @param intrigues the intrigues to set
     */
    public void setIntrigues(List<Intrigue> intrigues) {
        this.intrigues = intrigues;
    }

    /**
     * @return the buildings
     */
    public List<Building> getBuildings() {
        return buildings;
    }

    /**
     * @param buildings the buildings to set
     */
    public void setBuildings(List<Building> buildings) {
        this.buildings = buildings;
    }

    /**
     * @return the buildingsInBuildersHall
     */
    public List<Building> getBuildingsInBuildersHall() {
        return buildingsInBuildersHall;
    }

    /**
     * @param buildingsInBuildersHall the buildingsInBuildersHall to set
     */
    public void setBuildingsInBuildersHall(List<Building> buildingsInBuildersHall) {
        this.buildingsInBuildersHall = buildingsInBuildersHall;
    }

    public int getCorruptionRemoved() {
        return corruptionRemoved;
    }

    public void setCorruptionRemoved(int corruptionRemoved) {
        this.corruptionRemoved = corruptionRemoved;
    }

    public void debugNotifyNextIntrigue() {
        throw new UnsupportedOperationException("The next intrigue is " + intrigues.get(0).getIntrigueInfo().name());
    }
    public void addCorruptionRemoved(int corruptionRemoved) {
        this.corruptionRemoved += corruptionRemoved;
    }

    
}

package org.talwood.lords.gamerunner;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.enums.BuildingActions;
import org.talwood.lords.enums.IntrigueActions;
import org.talwood.lords.enums.QuestActions;
import org.talwood.lords.enums.TurnActions;
import org.talwood.lords.intrigues.Intrigue;
import org.talwood.lords.resources.Resources;

public class TurnState {
    
    private Building building;
    private List<BuildingActions> buildingActions = new ArrayList<BuildingActions>();
    private List<TurnActions> turnActions = new ArrayList<TurnActions>();
    private List<IntrigueActions> intrigueActions = new ArrayList<IntrigueActions>();
    private List<QuestActions> questActions = new ArrayList<QuestActions>();
    private Intrigue workingIntrigue;
    private Intrigue intrigueDrawnAsAction;
    private Resources questResourcesUsed;
    private int corruptionAgents;
    private int multiIntriguesPlayer;
    private boolean inMultiState;
    
    private TurnState() {}
    
    public TurnState(Building bldg) {
        this.building = bldg;
        buildingActions.addAll(bldg.getBuildingActions());
    }
    
    public boolean hasBuildingAction(BuildingActions action) {
        return buildingActions.contains(action);
    }
    
    public boolean hasQuestAction(QuestActions action) {
        return questActions.contains(action);
    }
    
    public boolean hasTurnAction(TurnActions action) {
        return turnActions.contains(action);
    }
    
    public boolean hasIntrigueAction(IntrigueActions action) {
        return intrigueActions.contains(action);
    }
    
    public void addBuildingAction(BuildingActions action) {
        if(hasBuildingAction(action)) {
            throw new UnsupportedOperationException("Duplicate building action");
        }
        buildingActions.add(action);
    }
    
    public void setQuestResourcesUsed(Resources resources) {
        questResourcesUsed = new Resources(resources);
    }

    public Resources getQuestResourcesUsed() {
        return questResourcesUsed;
    }
    
    public void addQuestAction(QuestActions action) {
        if(hasQuestAction(action)) {
            throw new UnsupportedOperationException("Duplicate quest action");
        }
        questActions.add(action);
    }
    
    public void addTurnAction(TurnActions action) {
        if(hasTurnAction(action)) {
            throw new UnsupportedOperationException("Duplicate turn action");
        }
        turnActions.add(action);
    }
    
    public void removeTurnAction(TurnActions action) {
        if(hasTurnAction(action)) {
            turnActions.remove(action);
        }
    }
    
    public void addIntrigueAction(IntrigueActions action) {
        if(hasIntrigueAction(action)) {
            throw new UnsupportedOperationException("Duplicate Intrigue action");
        }
        intrigueActions.add(action);
    }

    public int getMultiIntriguesPlayer() {
        return multiIntriguesPlayer;
    }

    public void setMultiIntriguesPlayer(int multiIntriguesPlayer) {
        this.multiIntriguesPlayer = multiIntriguesPlayer;
    }
    public void incrementMultiIntriguesPlayer() {
        this.multiIntriguesPlayer++;
    }

    public boolean isInMultiState() {
        return inMultiState;
    }

    public void setInMultiState(boolean inMultiState) {
        this.inMultiState = inMultiState;
    }

    public int getCorruptionAgents() {
        return corruptionAgents;
    }

    public void setCorruptionAgents(int corruptionAgents) {
        this.corruptionAgents = corruptionAgents;
    }

    public void resetMultiIntrigue() {
        incrementMultiIntriguesPlayer();
        intrigueActions.clear();
        inMultiState = false;
        setWorkingIntrigue(null);
    }
    public Building getBuilding() {
        return building;
    }

    public List<BuildingActions> getBuildingActions() {
        return buildingActions;
    }

    public Intrigue getWorkingIntrigue() {
        return workingIntrigue;
    }

    public void setWorkingIntrigue(Intrigue workingIntrigue) {
        this.workingIntrigue = workingIntrigue;
    }

    public Intrigue getIntrigueDrawnAsAction() {
        return intrigueDrawnAsAction;
    }

    public void setIntrigueDrawnAsAction(Intrigue intrigueDrawnAsAction) {
        this.intrigueDrawnAsAction = intrigueDrawnAsAction;
    }

    
    public void checkTurnState(GamePlayer player) {
        if(buildingActions.contains(BuildingActions.BuyBuilding)) {
            if(!turnActions.contains(TurnActions.BuildingPurchased)) {
                throw new UnsupportedOperationException("Building should have been purchased");
            }
        }
        if(buildingActions.contains(BuildingActions.PlayIntrigue)) {
            if(!turnActions.contains(TurnActions.IntriguePlayed)) {
                throw new UnsupportedOperationException("Intrigue card should have been played");
            }
            
        }
        if(buildingActions.contains(BuildingActions.TakeQuest)) {
            if(!turnActions.contains(TurnActions.QuestTaken)) {
                throw new UnsupportedOperationException("Quest should have been taken");
            }
        }
        if(buildingActions.contains(BuildingActions.RequiresVariableActivation)) {
            if(!turnActions.contains(TurnActions.VariableActivationHandled)) {
                throw new UnsupportedOperationException("Variable activation cost should have been defined");
            }
        }
        if(buildingActions.contains(BuildingActions.TakeCorruptionReturnAgents)) {
            if(!turnActions.contains(TurnActions.HandledCorruptionReturnAgents)) {
                throw new UnsupportedOperationException("Agents need to be returned");
            }
            
        }
        if(buildingActions.contains(BuildingActions.RequiresVariableReward)) {
            if(!turnActions.contains(TurnActions.VariableRewardHandled)) {
                throw new UnsupportedOperationException("Variable reward should have been defined");
            }
        }
        if(buildingActions.contains(BuildingActions.RequiresVariableOwnerReward)) {
            if(!turnActions.contains(TurnActions.VariableOwnerRewardHandled)) {
                if(!player.getPlayerID().equals(building.getPlayerOwnerID())) {
                    throw new UnsupportedOperationException("Variable owner reward should have been defined");
                }
            }
        }
        if(buildingActions.contains(BuildingActions.ReallocateResources)) {
            if(!turnActions.contains(TurnActions.ReallocateResourcesHandled)) {
                throw new UnsupportedOperationException("Resources were not reallocated");
            }
        }
        if(!inMultiState && buildingActions.contains(BuildingActions.PlayThreeIntrigues)) {
            if(multiIntriguesPlayer != 3) {
                throw new UnsupportedOperationException("Three intrigues required, only played " + multiIntriguesPlayer);
            }
        }

        if(questActions.contains(QuestActions.QuestFromCliffwatch)) {
            if(!turnActions.contains(TurnActions.QuestActionQuestTaken)) {
                throw new UnsupportedOperationException("Quest was not taken");
            }
        }
        
        if(questActions.contains(QuestActions.BuildingFromHall)) {
            if(!turnActions.contains(TurnActions.QuestActionBuildingTaken)) {
                throw new UnsupportedOperationException("Building was not taken");
            }
        }
        
        if(questActions.contains(QuestActions.TwoBuildingsFromBuildersHall)) {
            if(!turnActions.contains(TurnActions.QuestActionTwoBuildingsTaken)) {
                throw new UnsupportedOperationException("Two Buildings were not taken");
            }
        }
        
        if(questActions.contains(QuestActions.ReturnOneResourceFromCompletedQuest)) {
            if(!turnActions.contains(TurnActions.QuestActionReturnedResource)) {
                throw new UnsupportedOperationException("You need to return a resource used");
            }
        }
        
        if(questActions.contains(QuestActions.WildCardQuestRewardAndReturnUpToThreeAgents)) {
            if(!turnActions.contains(TurnActions.AgentsReturned)) {
                throw new UnsupportedOperationException("Agents were not returned");
            }
        }
        
        if(questActions.contains(QuestActions.ReturnAgent)) {
            if(!turnActions.contains(TurnActions.AgentReturned)) {
                throw new UnsupportedOperationException("Agent was not returned");
            }
        }
        
        if(intrigueActions.contains(IntrigueActions.RequiresReturningAgent)) {
            if(!turnActions.contains(TurnActions.AgentReturned)) {
                throw new UnsupportedOperationException("Agent was not returned");
            }
        }
        if(intrigueActions.contains(IntrigueActions.RequiresHouseDraw)) {
            if(!turnActions.contains(TurnActions.HouseSelected)) {
                throw new UnsupportedOperationException("House was not selected");
            }
        }
        if(intrigueActions.contains(IntrigueActions.StealResourcesFromPlayer)) {
            if(!turnActions.contains(TurnActions.ResourcesStolenFromPlayer)) {
                throw new UnsupportedOperationException("Resources were not taken from player");
            }
        }
        if(intrigueActions.contains(IntrigueActions.GainResourceChoice)) {
            if(!turnActions.contains(TurnActions.GainResourcesChosen)) {
                throw new UnsupportedOperationException("Resources were not given to opponent");
            }
        }
        if(intrigueActions.contains(IntrigueActions.GiveResourcesForVictoryPoints)) {
            if(!turnActions.contains(TurnActions.ResourcesForVPGiven)) {
                throw new UnsupportedOperationException("Resources were not given to opponent");
            }
        }
        if(intrigueActions.contains(IntrigueActions.PlaceExtraOnOneSpace)) {
            if(!turnActions.contains(TurnActions.ResourcesPlacedOneSpace)) {
                throw new UnsupportedOperationException("Resources were not placed properly");
            }
        }
        if(intrigueActions.contains(IntrigueActions.PlaceExtraOnTwoSpaces)) {
            if(!turnActions.contains(TurnActions.ResourcesPlacedTwoSpaces)) {
                throw new UnsupportedOperationException("Resources were not placed properly");
            }
        }
        if(intrigueActions.contains(IntrigueActions.RequiresMandatoryQuestTarget)) {
            if(!turnActions.contains(TurnActions.MandatoryQuestGiven)) {
                throw new UnsupportedOperationException("You need a target for the mandatory quest");
            }
        }
    }
    
}

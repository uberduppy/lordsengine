package org.talwood.lords.containers;

import org.talwood.lords.enums.Buildings;

public class BuildingPointContainer {
    private Buildings buildingInfo;
    private int victoryPoints;
    
    public BuildingPointContainer(Buildings buildingInfo, int victoryPoints) {
        this.buildingInfo = buildingInfo;
        this.victoryPoints = victoryPoints;
    }

    public Buildings getBuildingInfo() {
        return buildingInfo;
    }

    public void setBuildingInfo(Buildings buildingInfo) {
        this.buildingInfo = buildingInfo;
    }

    public int getVictoryPoints() {
        return victoryPoints;
    }

    public void setVictoryPoints(int victoryPoints) {
        this.victoryPoints = victoryPoints;
    }
    

}

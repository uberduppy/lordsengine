package org.talwood.lords.containers;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.buildings.abstracts.Building;
import org.talwood.lords.buildings.basic.BuildingAurorasRealmsShop;
import org.talwood.lords.buildings.basic.BuildingBlackstaffTower;
import org.talwood.lords.buildings.basic.BuildingBuildersHall;
import org.talwood.lords.buildings.basic.BuildingCastleWaterdeep;
import org.talwood.lords.buildings.basic.BuildingCliffwatchInnDrawIntrigue;
import org.talwood.lords.buildings.basic.BuildingCliffwatchInnResetQuests;
import org.talwood.lords.buildings.basic.BuildingCliffwatchInnTwoGold;
import org.talwood.lords.buildings.basic.BuildingEntryWell;
import org.talwood.lords.buildings.basic.BuildingFieldOfTriumph;
import org.talwood.lords.buildings.basic.BuildingGrinningLionTavern;
import org.talwood.lords.buildings.basic.BuildingHallOfMirrors;
import org.talwood.lords.buildings.basic.BuildingHallOfTheVoice;
import org.talwood.lords.buildings.basic.BuildingSkullIsland;
import org.talwood.lords.buildings.basic.BuildingSlaversMarket;
import org.talwood.lords.buildings.basic.BuildingTheGrimStatue;
import org.talwood.lords.buildings.basic.BuildingThePlinth;
import org.talwood.lords.buildings.basic.BuildingWaterdeepHarborOne;
import org.talwood.lords.buildings.basic.BuildingWaterdeepHarborThree;
import org.talwood.lords.buildings.basic.BuildingWaterdeepHarborTwo;
import org.talwood.lords.buildings.classic.BuildingCaravanCourt;
import org.talwood.lords.buildings.classic.BuildingDragonTower;
import org.talwood.lords.buildings.classic.BuildingFetlockCourt;
import org.talwood.lords.buildings.classic.BuildingHelmstarWarehouse;
import org.talwood.lords.buildings.classic.BuildingGoldenHorn;
import org.talwood.lords.buildings.classic.BuildingHeroesGarden;
import org.talwood.lords.buildings.classic.BuildingHouseOfGoodSpirits;
import org.talwood.lords.buildings.classic.BuildingHouseOfHeroes;
import org.talwood.lords.buildings.classic.BuildingHouseOfTheMoon;
import org.talwood.lords.buildings.classic.BuildingHouseOfWonder;
import org.talwood.lords.buildings.classic.BuildingJestersCourt;
import org.talwood.lords.buildings.classic.BuildingNewOlamn;
import org.talwood.lords.buildings.classic.BuildingNorthgate;
import org.talwood.lords.buildings.classic.BuildingPalaceOfWaterdeep;
import org.talwood.lords.buildings.classic.BuildingSkulkway;
import org.talwood.lords.buildings.classic.BuildingSmugglersDock;
import org.talwood.lords.buildings.classic.BuildingSpiresOfTheMorning;
import org.talwood.lords.buildings.classic.BuildingStoneHouse;
import org.talwood.lords.buildings.classic.BuildingTheWaymoot;
import org.talwood.lords.buildings.classic.BuildingThreePearls;
import org.talwood.lords.buildings.classic.BuildingTowerOfLuck;
import org.talwood.lords.buildings.classic.BuildingTowerOfTheOrder;
import org.talwood.lords.buildings.classic.BuildingYawningPortal;
import org.talwood.lords.buildings.classic.BuildingZoarstar;
import org.talwood.lords.buildings.skullport.BuildingCryptkeyFacilitations;
import org.talwood.lords.buildings.skullport.BuildingDeepfires;
import org.talwood.lords.buildings.skullport.BuildingDelversFolly;
import org.talwood.lords.buildings.skullport.BuildingFrontalLobe;
import org.talwood.lords.buildings.skullport.BuildingHellHoundsMuzzle;
import org.talwood.lords.buildings.skullport.BuildingMonstersMadeToOrder;
import org.talwood.lords.buildings.skullport.BuildingPoisonedQuill;
import org.talwood.lords.buildings.skullport.BuildingProminadeOfTheDarkMaiden;
import org.talwood.lords.buildings.skullport.BuildingSecretShrine;
import org.talwood.lords.buildings.skullport.BuildingShiradinsExcellentZombies;
import org.talwood.lords.buildings.skullport.BuildingThimblewinesPawnshop;
import org.talwood.lords.buildings.skullport.BuildingThrownGauntlet;
import org.talwood.lords.buildings.undermountain.BuildingBelkramsTomb;
import org.talwood.lords.buildings.undermountain.BuildingCitadelOfTheBloodyHand;
import org.talwood.lords.buildings.undermountain.BuildingHallOfManyPillars;
import org.talwood.lords.buildings.undermountain.BuildingHallOfSleepingKings;
import org.talwood.lords.buildings.undermountain.BuildingHallOfThreeLords;
import org.talwood.lords.buildings.undermountain.BuildingHighDukesTomb;
import org.talwood.lords.buildings.undermountain.BuildingRoomOfWisdom;
import org.talwood.lords.buildings.undermountain.BuildingShadowduskHold;
import org.talwood.lords.buildings.undermountain.BuildingTheEyesLair;
import org.talwood.lords.buildings.undermountain.BuildingTheLibrarium;
import org.talwood.lords.buildings.undermountain.BuildingTheLostCavern;
import org.talwood.lords.buildings.undermountain.BuildingTrobriandsGraveyard;
import org.talwood.lords.enums.Buildings;
import org.talwood.lords.enums.Expansions;

public class BuildingContainer {

    List<Expansions> expansions = new ArrayList<Expansions>();
    private List<Building> allBuildings = new ArrayList<Building>();
    
    public BuildingContainer(Expansions... exp) {
        for(Expansions e : exp) {
            this.expansions.add(e);
        }
        populateBuildings();
        
    }
    
    private void addBuilding(Building bldg) {
        if(expansions.contains(bldg.getBuildingInfo().getExpansion())) {
            allBuildings.add(bldg);
        }
    }
    
    public Building findByInfo(Buildings bldg) {
        Building result = null;
        for(Building b : allBuildings) {
            if(b.getBuildingInfo() == bldg) {
                result = b;
                break;
            }
        }
        return result;
    }
    
    public void populateBuildings() {
        addBuilding(new BuildingAurorasRealmsShop());
        addBuilding(new BuildingBlackstaffTower());
        addBuilding(new BuildingBuildersHall());
        addBuilding(new BuildingCastleWaterdeep());
        addBuilding(new BuildingCliffwatchInnDrawIntrigue());
        addBuilding(new BuildingCliffwatchInnResetQuests());
        addBuilding(new BuildingCliffwatchInnTwoGold());
        addBuilding(new BuildingFieldOfTriumph());
        addBuilding(new BuildingGrinningLionTavern());
        addBuilding(new BuildingThePlinth());
        addBuilding(new BuildingWaterdeepHarborOne());
        addBuilding(new BuildingWaterdeepHarborTwo());
        addBuilding(new BuildingWaterdeepHarborThree());
        
        addBuilding(new BuildingCaravanCourt());
        addBuilding(new BuildingDragonTower());
        addBuilding(new BuildingFetlockCourt());
        addBuilding(new BuildingGoldenHorn());
        addBuilding(new BuildingHelmstarWarehouse());
        addBuilding(new BuildingHeroesGarden());
        addBuilding(new BuildingHouseOfGoodSpirits());
        addBuilding(new BuildingHouseOfHeroes());
        addBuilding(new BuildingHouseOfTheMoon());
        addBuilding(new BuildingHouseOfWonder());
        addBuilding(new BuildingJestersCourt());
        addBuilding(new BuildingNewOlamn());
        addBuilding(new BuildingNorthgate());
        addBuilding(new BuildingPalaceOfWaterdeep());
        addBuilding(new BuildingSkulkway());
        addBuilding(new BuildingSmugglersDock());
        addBuilding(new BuildingSpiresOfTheMorning());
        addBuilding(new BuildingStoneHouse());
        addBuilding(new BuildingThreePearls());
        addBuilding(new BuildingTowerOfLuck());
        addBuilding(new BuildingTowerOfTheOrder());
        addBuilding(new BuildingTheWaymoot());
        addBuilding(new BuildingYawningPortal());
        addBuilding(new BuildingZoarstar());
        
        // Undermountain
        addBuilding(new BuildingEntryWell());
        addBuilding(new BuildingHallOfMirrors());
        addBuilding(new BuildingTheGrimStatue());
        
        addBuilding(new BuildingBelkramsTomb());
        addBuilding(new BuildingCitadelOfTheBloodyHand());
        addBuilding(new BuildingTheEyesLair());
        addBuilding(new BuildingHallOfManyPillars());
        addBuilding(new BuildingHallOfSleepingKings());
        addBuilding(new BuildingHallOfThreeLords());
        addBuilding(new BuildingHighDukesTomb());
        addBuilding(new BuildingTheLibrarium());
        addBuilding(new BuildingTheLostCavern());
        addBuilding(new BuildingRoomOfWisdom());
        addBuilding(new BuildingShadowduskHold());
        addBuilding(new BuildingTrobriandsGraveyard());
        
        // Skullport
        addBuilding(new BuildingHallOfTheVoice());
        addBuilding(new BuildingSlaversMarket());
        addBuilding(new BuildingSkullIsland());
        
        addBuilding(new BuildingCryptkeyFacilitations());
        addBuilding(new BuildingDeepfires());
        addBuilding(new BuildingDelversFolly());
        addBuilding(new BuildingFrontalLobe());
        addBuilding(new BuildingHellHoundsMuzzle());
        addBuilding(new BuildingMonstersMadeToOrder());
        addBuilding(new BuildingPoisonedQuill());
        addBuilding(new BuildingProminadeOfTheDarkMaiden());
        addBuilding(new BuildingSecretShrine());
        addBuilding(new BuildingShiradinsExcellentZombies());
        addBuilding(new BuildingThimblewinesPawnshop());
        addBuilding(new BuildingThrownGauntlet());
    }

    public List<Building> getAllBuildings() {
        return allBuildings;
    }
    
    
}

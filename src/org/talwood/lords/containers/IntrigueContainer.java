package org.talwood.lords.containers;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.enums.Expansions;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.ResourceChoices;
import org.talwood.lords.intrigues.AgentPlacementIntrigue;
import org.talwood.lords.intrigues.Intrigue;
import org.talwood.lords.intrigues.BuyBuildingIntrigue;
import org.talwood.lords.intrigues.DrawQuestForHouseIntrigue;
import org.talwood.lords.intrigues.DrawQuestIntrigue;
import org.talwood.lords.intrigues.DrawThreeOppsOneIntrigue;
import org.talwood.lords.intrigues.DrawTwoIntrigue;
import org.talwood.lords.intrigues.DuplicatePlacementIntrigue;
import org.talwood.lords.intrigues.GainResourcesIntrigue;
import org.talwood.lords.intrigues.GiveResourceForVPIntrigue;
import org.talwood.lords.intrigues.MandatoryQuestIntrigue;
import org.talwood.lords.intrigues.OptionalResourceIntrigue;
import org.talwood.lords.intrigues.PlaceExtraOnOneSpaceIntrigue;
import org.talwood.lords.intrigues.PlaceExtraOnTwoSpacesIntrigue;
import org.talwood.lords.intrigues.ReallocateResourcesIntrigue;
import org.talwood.lords.intrigues.ResourceForVictoryIntrigue;
import org.talwood.lords.intrigues.ReturnAgentIntrigue;
import org.talwood.lords.intrigues.ReturnOneCorruptionIntrigue;
import org.talwood.lords.intrigues.ReturnTwoCorruptionAndResourceIntrigue;
import org.talwood.lords.intrigues.ShareResourcesIntrigue;
import org.talwood.lords.intrigues.ShareResourcesSingleIntrigue;
import org.talwood.lords.intrigues.StealResourcesIntrigue;
import org.talwood.lords.intrigues.UnsupportedIntrigue;
import org.talwood.lords.intrigues.VictoryForCorruptionIntrigue;
import org.talwood.lords.resources.MandatoryQuestCost;
import org.talwood.lords.resources.MandatoryQuestReward;
import org.talwood.lords.resources.Res;
import org.talwood.lords.resources.Resources;

public class IntrigueContainer {

    List<Expansions> expansions = new ArrayList<Expansions>();
    List<Intrigue> allBaseIntrigues = new ArrayList<Intrigue>();

    public IntrigueContainer(Expansions... exp) {
        for(Expansions e : exp) {
            this.expansions.add(e);
        }
        populateBaseIntrigues();
    }

    private void populateBaseIntrigues() {
        // 1-2 Ambush - Attack
        // Each opponent removes 1 fighter from his or her Tavern and returns it to the supply.  For each opponent that could not do so, take 1 fighter from the supply and place it in your Tavern.
        addBaseIntrigue(new ReallocateResourcesIntrigue(Intrigues.Ambush1, ResourceChoices.OneFighter));
        addBaseIntrigue(new ReallocateResourcesIntrigue(Intrigues.Ambush2, ResourceChoices.OneFighter));
        
        // 3-4 Arcane Mishap - Attack
        // Each opponent removes 1 wizard from his or her Tavern and returns it to the supply.  For each opponent that could not do so, take 1 wizard from the supply and place it in your Tavern.
        addBaseIntrigue(new ReallocateResourcesIntrigue(Intrigues.ArcaneMishap1, ResourceChoices.OneWizard));
        addBaseIntrigue(new ReallocateResourcesIntrigue(Intrigues.ArcaneMishap2, ResourceChoices.OneWizard));
        
        // 5-6 Assassination - Attack
        // Each opponent removes 1 rogue from his or her Tavern and returns it to the supply.  For each opponent that could not do so, take 1 rogue from the supply and place it in your Tavern.
        addBaseIntrigue(new ReallocateResourcesIntrigue(Intrigues.Assassination1, ResourceChoices.OneRogue));
        addBaseIntrigue(new ReallocateResourcesIntrigue(Intrigues.Assassination2, ResourceChoices.OneRogue));
        
        // 7-8 Bribe Agent - Attack
        // Pay 2 gold.  Choose an action space containing an opponent's Agent.  You use that space's action as though you had assigned an Agent to it.
        addBaseIntrigue(new DuplicatePlacementIntrigue(Intrigues.BribeAgent1));
        addBaseIntrigue(new DuplicatePlacementIntrigue(Intrigues.BribeAgent2));
        
        // 9-10 Free Drinks - Attack
        // Remove any adventurer from an opponent's Tavern and the place it in your Tavern.
        addBaseIntrigue(new StealResourcesIntrigue(Intrigues.FreeDrinks1, ResourceChoices.OneCleric, ResourceChoices.OneFighter, ResourceChoices.OneRogue, ResourceChoices.OneWizard));
        addBaseIntrigue(new StealResourcesIntrigue(Intrigues.FreeDrinks2, ResourceChoices.OneCleric, ResourceChoices.OneFighter, ResourceChoices.OneRogue, ResourceChoices.OneWizard));
        
        // 11-12 Lack of Faith - Attack
        // Each opponent removes 1 cleric from his or her Tavern and returns it to the supply.  For each opponent that could not do so, take 1 cleric from the supply and place it in your Tavern.
        addBaseIntrigue(new ReallocateResourcesIntrigue(Intrigues.LackOfFaith1, ResourceChoices.OneCleric));
        addBaseIntrigue(new ReallocateResourcesIntrigue(Intrigues.LackOfFaith2, ResourceChoices.OneCleric));
        
        // 13 - Accelerated Plans - Utility
        // Choose 1 of your Agent assigned to Waterdeep Harbor.  Return it to your pool, then immediately assign 2 Agents
        addBaseIntrigue(new AgentPlacementIntrigue(Intrigues.AcceleratedPlans));

        // 14-16 - Bidding War - Utility
        // Draw a Quest from the deck equal to the number of players.  Keep one and pass the remaining Quests to the player on your left.  Each player, in turn, chooses 1 Quest to keep, and passes the rest to the left until every player has taken a Quest.
        addBaseIntrigue(new DrawQuestIntrigue(Intrigues.BiddingWar1));
        addBaseIntrigue(new DrawQuestIntrigue(Intrigues.BiddingWar2));
        addBaseIntrigue(new DrawQuestIntrigue(Intrigues.BiddingWar3));
        
        // 17-18 Call for Adventurers - Utility
        // Take 2 adventurers from the supply and place them in your Tavern.  Each opponent takes 1 adventurer from the supply and places it in his or her tavern.
        addBaseIntrigue(new ShareResourcesIntrigue(Intrigues.CallForAdventurers1,
                new ResourceChoices[]{
                    ResourceChoices.OneClericOneFighter,
                    ResourceChoices.OneClericOneRogue,
                    ResourceChoices.OneClericOneWizard,
                    ResourceChoices.OneFighterOneRogue,
                    ResourceChoices.OneFighterOneWizard,
                    ResourceChoices.OneRogueOneWizard,
                    ResourceChoices.TwoClerics,
                    ResourceChoices.TwoFighters,
                    ResourceChoices.TwoRogues,
                    ResourceChoices.TwoWizards,
                },
                new ResourceChoices[]{
                    ResourceChoices.OneCleric,
                    ResourceChoices.OneFighter,
                    ResourceChoices.OneRogue,
                    ResourceChoices.OneWizard,
                }));
        
        addBaseIntrigue(new ShareResourcesIntrigue(Intrigues.CallForAdventurers2,
                new ResourceChoices[]{
                    ResourceChoices.OneClericOneFighter,
                    ResourceChoices.OneClericOneRogue,
                    ResourceChoices.OneClericOneWizard,
                    ResourceChoices.OneFighterOneRogue,
                    ResourceChoices.OneFighterOneWizard,
                    ResourceChoices.OneRogueOneWizard,
                    ResourceChoices.TwoClerics,
                    ResourceChoices.TwoFighters,
                    ResourceChoices.TwoRogues,
                    ResourceChoices.TwoWizards,
                },
                new ResourceChoices[]{
                    ResourceChoices.OneCleric,
                    ResourceChoices.OneFighter,
                    ResourceChoices.OneRogue,
                    ResourceChoices.OneWizard,
                }));
        
        
        // 19-20 Call in a Favor - Utility
        // Choose 1 of the following to take from the supply: 4 gold, 2 fighters, 2 rogues, 1 wizard, 1 cleric
        addBaseIntrigue(new GainResourcesIntrigue(Intrigues.CallInAFavor1, ResourceChoices.FourGold, ResourceChoices.TwoRogues, ResourceChoices.TwoFighters, ResourceChoices.OneCleric, ResourceChoices.OneWizard));
        addBaseIntrigue(new GainResourcesIntrigue(Intrigues.CallInAFavor2, ResourceChoices.FourGold, ResourceChoices.TwoRogues, ResourceChoices.TwoFighters, ResourceChoices.OneCleric, ResourceChoices.OneWizard));
        // 21 - Change of Plans - Utility
        // Discard an uncompleted Quest.  Score 6 VP.  Each opponent can choose to discard un uncompleted Quest to score 3 VP (You can discard a Mandatory Quest in this way)
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.ChangeOfPlans));
        // 22-23 Conscription - Utility
        // Take 2 fighters from the supply and place it in your Tavern.  Choose 1 opponent.  That opponent takes 1 fighter from the supply and places it in his or her Tavern
        addBaseIntrigue(new ShareResourcesSingleIntrigue(Intrigues.Conscription1, ResourceChoices.TwoFighters, ResourceChoices.OneFighter));
        addBaseIntrigue(new ShareResourcesSingleIntrigue(Intrigues.Conscription2, ResourceChoices.TwoFighters, ResourceChoices.OneFighter));
        // 24-25 Crime Wave - Utility
        // Take 2 rogues from the supply and place it in your Tavern.  Choose 1 opponent.  That opponent takes 1 rogue from the supply and places it in his or her Tavern
        addBaseIntrigue(new ShareResourcesSingleIntrigue(Intrigues.CrimeWave1, ResourceChoices.TwoRogues, ResourceChoices.OneRogue));
        addBaseIntrigue(new ShareResourcesSingleIntrigue(Intrigues.CrimeWave2, ResourceChoices.TwoRogues, ResourceChoices.OneRogue));
        // 26-27 Good Faith - Utility
        // Take 2 clerics from the supply and place it in your Tavern.  Choose 1 opponent.  That opponent takes 1 cleric from the supply and places it in his or her Tavern
        addBaseIntrigue(new ShareResourcesSingleIntrigue(Intrigues.GoodFaith1, ResourceChoices.TwoClerics, ResourceChoices.OneCleric));
        addBaseIntrigue(new ShareResourcesSingleIntrigue(Intrigues.GoodFaith2, ResourceChoices.TwoClerics, ResourceChoices.OneCleric));
        // 28-29 Graduation Day - Utility
        // Take 2 wizards from the supply and place it in your Tavern.  Choose 1 opponent.  That opponent takes 1 wizard from the supply and places it in his or her Tavern
        addBaseIntrigue(new ShareResourcesSingleIntrigue(Intrigues.GraduationDay1, ResourceChoices.TwoWizards, ResourceChoices.OneWizard));
        addBaseIntrigue(new ShareResourcesSingleIntrigue(Intrigues.GraduationDay2, ResourceChoices.TwoWizards, ResourceChoices.OneWizard));
        // 30 - Real Estate Deal - Utility
        // Discard a Building under you control that currently has no Agent assigned to it.  Then choose 1 Building in Builder's Hall and putit in play under your control at no cost.  (Replace that Building afterward.)
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.RealEstateDeal));
        // 31-32 Recall Agent - Utility
        // Return 1 of your assigned Agent to your pool.
        addBaseIntrigue(new ReturnAgentIntrigue(Intrigues.RecallAgent1));
        addBaseIntrigue(new ReturnAgentIntrigue(Intrigues.RecallAgent2));
        // 33 Recruit Spies - Utility
        // Take 2 rogues from the supply and place it in your Tavern.  Each opponent can choose to give you 1 rogue once to score 3 Victory Points.
        addBaseIntrigue(new ResourceForVictoryIntrigue(Intrigues.RecruitSpies, ResourceChoices.TwoRogues, ResourceChoices.OneRogue, 3));
        // 34 Request Assistance - Utility
        // Take 2 fighters from the supply and place it in your Tavern.  Each opponent can choose to give you 1 fighter once to score 3 Victory Points.
        addBaseIntrigue(new ResourceForVictoryIntrigue(Intrigues.RequestAssistance, ResourceChoices.TwoFighters, ResourceChoices.OneFighter, 3));
        // 35 Research Agreement - Utility
        // Take 1 wizard from the supply and place it in your Tavern.  Each opponent can choose to give you 1 wizard once to score 5 Victory Points.
        addBaseIntrigue(new ResourceForVictoryIntrigue(Intrigues.ResearchAgreement, ResourceChoices.OneWizard, ResourceChoices.OneWizard, 5));
        // 36-37 Sample Wares - Utility
        // Assign 1 of your unused Agent to a Building in Builder's Hall.  You immediately use the effect of that Building as though you controlled it.
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.SampleWares1));
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.SampleWares2));
        // 38-39 Special Assignment - Utility
        // Choose a Quest type: Arcana, Commerce, Piety, Skullduggery, or Warfare.  Draw and reveal Quests until you reveal a card of the choosen type.  Keep that Quest and discard the rest.
        addBaseIntrigue(new DrawQuestForHouseIntrigue(Intrigues.SpecialAssignment1));
        addBaseIntrigue(new DrawQuestForHouseIntrigue(Intrigues.SpecialAssignment2));
        // 40-41 Spread the Wealth - Utility
        // Take 4 gold from the supply and choose 1 opponent.  That opponent takes 2 gold from the supply
        addBaseIntrigue(new ShareResourcesSingleIntrigue(Intrigues.SpreadTheWealth1, ResourceChoices.FourGold, ResourceChoices.TwoGold));
        addBaseIntrigue(new ShareResourcesSingleIntrigue(Intrigues.SpreadTheWealth2, ResourceChoices.FourGold, ResourceChoices.TwoGold));
        // 42 - Summon the Faithful - Utility
        // Take 1 cleric from the supply and place it in your Tavern.  Each opponent can choose to give you 1 cleric once to score 5 Victory Points.
        addBaseIntrigue(new ResourceForVictoryIntrigue(Intrigues.SummonTheFaithful, ResourceChoices.OneCleric, ResourceChoices.OneCleric, 5));
        // 43-44 Tax Collection - Utility
        // Take 4 gold from the supply.  Each opponent can choose to pay you 4 gold to score 4 VP
        addBaseIntrigue(new ResourceForVictoryIntrigue(Intrigues.TaxCollection1, ResourceChoices.FourGold, ResourceChoices.FourGold, 4));
        addBaseIntrigue(new ResourceForVictoryIntrigue(Intrigues.TaxCollection2, ResourceChoices.FourGold, ResourceChoices.FourGold, 4));
        // 45 - Fend off Bandits - Mandatory Quest
        // 2 Fighters + 1 Wizard for 2 Victory Points
        addBaseIntrigue(new MandatoryQuestIntrigue(Intrigues.FendOffBandits, new MandatoryQuestCost(Res.fighter(2), Res.wizard(1)), 2));
        // 46 - Foil the Zhentarim - Mandatory Quest
        // 1 Fighter + 1 Rogue + 1 Wizard for 2 Victory points
        addBaseIntrigue(new MandatoryQuestIntrigue(Intrigues.FoilTheZhentarim, new MandatoryQuestCost(Res.fighter(1), Res.rogue(1), Res.wizard(1)), 2));
        // 47 - Placate Angry Merchants - Mandatory Quest
        // 1 Cleric + 1 Fighter + 1 Wizard for 4 Victory points
        addBaseIntrigue(new MandatoryQuestIntrigue(Intrigues.PlacateAngryMerchants, new MandatoryQuestCost(Res.cleric(1), Res.fighter(1), Res.wizard(1)), 4));
        // 48 - Quell Riots - Mandatory Quest
        // 2 Cleric + 1 Fighter for 4 Victory points
        addBaseIntrigue(new MandatoryQuestIntrigue(Intrigues.QuellRiots, new MandatoryQuestCost(Res.cleric(2), Res.fighter(1)), 4));
        // 49 - Repel Drow Invaders - Mandatory Quest
        // 1 Cleric + 2 Rogues for 2 Victory points
        addBaseIntrigue(new MandatoryQuestIntrigue(Intrigues.RepelDrowInvaders, new MandatoryQuestCost(Res.cleric(1), Res.rogue(2)), 2));
        // 50 - Stamp Out Cultists - Mandatory Quest
        // 1 Cleric + 1 Fighter + 1 Rogue for 2 Victory points
        addBaseIntrigue(new MandatoryQuestIntrigue(Intrigues.StampOutCultists, new MandatoryQuestCost(Res.cleric(1), Res.fighter(1), Res.rogue(1)), 2));
        
        // Undermountain
        addBaseIntrigue(new StealResourcesIntrigue(Intrigues.CallForAssistance1, ResourceChoices.TwoFighters, ResourceChoices.TwoRogues, ResourceChoices.OneFighterOneRogue));
        addBaseIntrigue(new StealResourcesIntrigue(Intrigues.CallForAssistance2, ResourceChoices.TwoFighters, ResourceChoices.TwoRogues, ResourceChoices.OneFighterOneRogue));
        
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.DemolishBuilding));
        
        addBaseIntrigue(new StealResourcesIntrigue(Intrigues.InevitableBetrayal, ResourceChoices.OneClericTwoGold, ResourceChoices.OneFighterTwoGold, ResourceChoices.OneRogueTwoGold, ResourceChoices.OneWizardTwoGold));
        
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.Manipulate));
        
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.PreferentialTreatment));
        
        addBaseIntrigue(new GiveResourceForVPIntrigue(Intrigues.AlliedFaiths, new Resources(Res.cleric(1)), 8));
        
        addBaseIntrigue(new BuyBuildingIntrigue(Intrigues.ArchitecturalInnovation, new Resources(Res.gold(2))));
        
        addBaseIntrigue(new GiveResourceForVPIntrigue(Intrigues.FriendlyLoan, new Resources(Res.gold(4)), 8));
        
        addBaseIntrigue(new GiveResourceForVPIntrigue(Intrigues.HonorAmongThieves, new Resources(Res.rogue(2)), 8));
        
        addBaseIntrigue(new DrawThreeOppsOneIntrigue(Intrigues.InformationBroker1));
        addBaseIntrigue(new DrawThreeOppsOneIntrigue(Intrigues.InformationBroker2));
        
        addBaseIntrigue(new GiveResourceForVPIntrigue(Intrigues.MercenaryContract, new Resources(Res.fighter(2)), 8));
        
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.OpenLord));
        
        addBaseIntrigue(new PlaceExtraOnTwoSpacesIntrigue(Intrigues.OrganizedCrime, new Resources(Res.rogue(2)), new Resources(Res.rogue(1))));
        
        addBaseIntrigue(new PlaceExtraOnOneSpaceIntrigue(Intrigues.Proselytize, new Resources(Res.cleric(1)), new Resources(Res.cleric(1))));
        
        addBaseIntrigue(new PlaceExtraOnTwoSpacesIntrigue(Intrigues.RecruitmentDrive, new Resources(Res.fighter(2)), new Resources(Res.fighter(1))));
        
        addBaseIntrigue(new PlaceExtraOnOneSpaceIntrigue(Intrigues.SponsorApprentices, new Resources(Res.wizard(1)), new Resources(Res.wizard(1))));
        
        addBaseIntrigue(new PlaceExtraOnTwoSpacesIntrigue(Intrigues.TaxRevolt, new Resources(Res.gold(5)), new Resources(Res.gold(2))));
        
        addBaseIntrigue(new DrawTwoIntrigue(Intrigues.UnexpectedSuccess1));
        addBaseIntrigue(new DrawTwoIntrigue(Intrigues.UnexpectedSuccess2));
        
        addBaseIntrigue(new GiveResourceForVPIntrigue(Intrigues.UnlikelyAssistance, new Resources(Res.wizard(1)), 8));

        addBaseIntrigue(new MandatoryQuestIntrigue(Intrigues.EvadeAssassins, new MandatoryQuestCost(Res.rogue(2), Res.wizard(1)), new MandatoryQuestReward(Res.intrigue(1))));
        addBaseIntrigue(new MandatoryQuestIntrigue(Intrigues.SubdueIllithidMenace, new MandatoryQuestCost(Res.cleric(1), Res.fighter(2)), new MandatoryQuestReward(Res.intrigue(1))));
        addBaseIntrigue(new MandatoryQuestIntrigue(Intrigues.UnveilAbyssalAgent, new MandatoryQuestCost(Res.cleric(1), Res.wizard(1)), new MandatoryQuestReward(Res.intrigue(1))));

        // Skullport
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.Blackmail));
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.Doppelganger));
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.ExposeCorruption1));
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.ExposeCorruption2));
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.FoistResponsibility));
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.ForgeDeed));
        
        addBaseIntrigue(new OptionalResourceIntrigue(Intrigues.BlackMarketMoney, new Resources(Res.gold(8), Res.corruption(1)), new Resources(Res.gold(4), Res.corruption(1))));
        
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.BribeTheWatch));
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.CorruptingInfluence1));
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.CorruptingInfluence2));
        
        addBaseIntrigue(new OptionalResourceIntrigue(Intrigues.DarkDaggerAssassination, new Resources(Res.rogue(4), Res.corruption(1)), new Resources(Res.rogue(2), Res.corruption(1))));
        
        addBaseIntrigue(new OptionalResourceIntrigue(Intrigues.DonationsForCyric, new Resources(Res.cleric(2), Res.corruption(1)), new Resources(Res.cleric(1), Res.corruption(1))));
        addBaseIntrigue(new VictoryForCorruptionIntrigue(Intrigues.HonorableExample));
        
        addBaseIntrigue(new OptionalResourceIntrigue(Intrigues.IronRingSlaves, new Resources(Res.fighter(4), Res.corruption(1)), new Resources(Res.fighter(2), Res.corruption(1))));
        
        addBaseIntrigue(new OptionalResourceIntrigue(Intrigues.MindFlayerMercenaries, new Resources(Res.wizard(2), Res.corruption(1)), new Resources(Res.wizard(1), Res.corruption(1))));
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.ReleaseTheHounds1));
        addBaseIntrigue(new UnsupportedIntrigue(Intrigues.ReleaseTheHounds2));
        addBaseIntrigue(new ReturnOneCorruptionIntrigue(Intrigues.Repent1));
        addBaseIntrigue(new ReturnOneCorruptionIntrigue(Intrigues.Repent2));
        addBaseIntrigue(new ReturnOneCorruptionIntrigue(Intrigues.Repent3));
        addBaseIntrigue(new ReturnTwoCorruptionAndResourceIntrigue(Intrigues.Scapegoat1));
        addBaseIntrigue(new ReturnTwoCorruptionAndResourceIntrigue(Intrigues.Scapegoat2));
        
        addBaseIntrigue(new MandatoryQuestIntrigue(Intrigues.ClearRustMonsterNest, new MandatoryQuestCost(Res.fighter(1), Res.wizard(1), Res.gold(2)), 2));
        addBaseIntrigue(new MandatoryQuestIntrigue(Intrigues.CoverUpScandal, new MandatoryQuestCost(Res.cleric(1), Res.fighter(1), Res.wizard(1), Res.rogue(1)), new MandatoryQuestReward(Res.victoryPoints(2), Res.corruption(1))));
        addBaseIntrigue(new MandatoryQuestIntrigue(Intrigues.HuntHiddenGhoul, new MandatoryQuestCost(Res.cleric(1), Res.rogue(1), Res.gold(2)), 2));
    }
    
    
    
    private void addBaseIntrigue(Intrigue i) {
        if(expansions.contains(i.getIntrigueInfo().getExpansion())) {
            allBaseIntrigues.add(i);
        }
    }
    
    public List<Intrigue> getAllBaseIntrigues() {
        return allBaseIntrigues;
    }
    
    
    
}

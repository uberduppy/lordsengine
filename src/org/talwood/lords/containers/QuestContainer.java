package org.talwood.lords.containers;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.constants.ResourceConstants;
import org.talwood.lords.enums.Expansions;
import static org.talwood.lords.enums.IntrigueActionTypes.MandatoryQuest;
import org.talwood.lords.enums.QuestActions;
import org.talwood.lords.enums.Quests;
import org.talwood.lords.helpers.StringHelper;
import org.talwood.lords.quests.BasicQuest;
import org.talwood.lords.quests.MandatoryQuest;
import org.talwood.lords.quests.OptionsQuest;
import org.talwood.lords.quests.PlotQuest;
import org.talwood.lords.quests.Quest;
import org.talwood.lords.resources.QuestCost;
import org.talwood.lords.resources.QuestReward;
import org.talwood.lords.resources.Res;

public class QuestContainer {
    
    List<Expansions> expansions = new ArrayList<Expansions>();
    List<Quest> quests = new ArrayList<Quest>();
    List<Quest> mandatoryQuests = new ArrayList<Quest>();
    
    public QuestContainer(Expansions... exp) {
        for(Expansions e : exp) {
            this.expansions.add(e);
        }
        populateQuests();
    }
    
    private void populateQuests() {
        addQuest(new BasicQuest(Quests.ExposeRedWizardsSpies, 
                new QuestCost(Res.cleric(1), Res.fighter(1), Res.rogue(2), Res.wizard(2), Res.gold(2)), 
                new QuestReward(Res.intrigue(1))));
        addQuest(new BasicQuest(Quests.HostFestivalForSune, 
                new QuestCost(Res.fighter(2), Res.wizard(2), Res.gold(4)), 
                new QuestReward(Res.cleric(2))));
        addQuest(new BasicQuest(Quests.InfiltrateHalastersCircle, 
                new QuestCost(Res.wizard(5), Res.gold(2)), 
                new QuestReward()));
        addQuest(new BasicQuest(Quests.InvestigateAberrantInfestation, 
                new QuestCost(Res.cleric(1), Res.fighter(1), Res.wizard(2)), 
                new QuestReward(Res.intrigue(1))));
        addQuest(new BasicQuest(Quests.RecruitForBlackstaffAcademy, 
                new QuestCost(Res.fighter(1), Res.rogue(1), Res.wizard(2), Res.gold(4)), 
                new QuestReward(Res.wizard(3))));
        addQuest(new BasicQuest(Quests.ResearchChronomacy, 
                new QuestCost(Res.wizard(2), Res.gold(4)), 
                new QuestReward(Res.wizard(1))).andSetDescription("Return 1 Agent to your Tavern."));
        addQuest(new BasicQuest(Quests.RetrieveAncientArtifacts, 
                new QuestCost(Res.fighter(2), Res.rogue(1), Res.wizard(2)), 
                new QuestReward(Res.gold(4))));
        addQuest(new BasicQuest(Quests.StealSpellbookFromSilverhand, 
                new QuestCost(Res.fighter(1), Res.rogue(2), Res.wizard(2)), 
                new QuestReward(Res.intrigue(2), Res.gold(4))));
        addQuest(new BasicQuest(Quests.DomesticateOwlbears, 
                new QuestCost(Res.cleric(1), Res.wizard(2)), 
                new QuestReward(Res.fighter(1), Res.gold(2))));
        addQuest(new BasicQuest(Quests.AllyWithHouseThann, 
                new QuestCost(Res.cleric(1), Res.rogue(3), Res.wizard(1), Res.gold(8)), 
                new QuestReward()));
        addQuest(new BasicQuest(Quests.ImpersonateAdarBrentNoble, 
                new QuestCost(Res.cleric(1), Res.fighter(2), Res.rogue(2), Res.wizard(1), Res.gold(4)), 
                new QuestReward(Res.intrigue(2))));
        addQuest(new BasicQuest(Quests.LootTheCryptOfChauntea, 
                new QuestCost(Res.cleric(1), Res.rogue(3), Res.gold(2)), 
                new QuestReward(Res.intrigue(1))).andSetDescription("Draw 1 Quest from the deck"));
        addQuest(new BasicQuest(Quests.LureArtisansOfMirabar, 
                new QuestCost(Res.cleric(1), Res.fighter(1), Res.rogue(1), Res.gold(2)), 
                new QuestReward()).andSetDescription("Put one building from Builder's Hall into play at no cost"));
        addQuest(new BasicQuest(Quests.PlacateTheWalkingStatue, 
                new QuestCost(Res.cleric(2), Res.rogue(2), Res.gold(4)), 
                new QuestReward()).andSetDescription("Put the top Building from the deck into play at no cost"));
        addQuest(new BasicQuest(Quests.SafeguardEltorchulMage, 
                new QuestCost(Res.fighter(1), Res.rogue(1), Res.wizard(1), Res.gold(4)), 
                new QuestReward(Res.wizard(2))));
        addQuest(new BasicQuest(Quests.SendAidToTheHarpers, 
                new QuestCost(Res.cleric(1), Res.fighter(1), Res.rogue(1), Res.gold(4)), 
                new QuestReward()).andSetDescription("Select one opponent to receive " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA));
        addQuest(new BasicQuest(Quests.SpyOnTheHouseOfLight, 
                new QuestCost(Res.fighter(3), Res.rogue(2), Res.gold(2)), 
                new QuestReward(Res.gold(6))));
        addQuest(new BasicQuest(Quests.ThinTheCityWatch, 
                new QuestCost(Res.cleric(1), Res.fighter(1), Res.rogue(1), Res.gold(4)), 
                new QuestReward(Res.rogue(4))));
        addQuest(new BasicQuest(Quests.ConvertANobleToLathander, 
                new QuestCost(Res.cleric(2), Res.fighter(1)), 
                new QuestReward()).andSetDescription("Take 1 Quest from Cliffwatch Inn"));
        addQuest(new BasicQuest(Quests.CreateShrineToOghma, 
                new QuestCost(Res.cleric(5), Res.gold(2)), 
                new QuestReward()));
        addQuest(new BasicQuest(Quests.DiscoverHiddenTempleOfLolth, 
                new QuestCost(Res.cleric(2), Res.fighter(1), Res.rogue(1)), 
                new QuestReward()).andSetDescription("Take 1 Quest from Cliffwatch Inn"));
        addQuest(new BasicQuest(Quests.EliminateVampireCoven, 
                new QuestCost(Res.cleric(2), Res.fighter(2), Res.rogue(1)), 
                new QuestReward(Res.gold(4))));
        addQuest(new BasicQuest(Quests.FormAllianceWithTheRashemi, 
                new QuestCost(Res.cleric(2), Res.wizard(1)), 
                new QuestReward()).andSetDescription("Take 1 Quest from Cliffwatch Inn"));
        addQuest(new BasicQuest(Quests.HealFallenGrayHandSoldiers, 
                new QuestCost(Res.cleric(2), Res.wizard(1), Res.gold(4)), 
                new QuestReward(Res.fighter(6))));
        addQuest(new BasicQuest(Quests.PerformThePenanceOfDuty, 
                new QuestCost(Res.cleric(2), Res.fighter(2), Res.gold(4)), 
                new QuestReward(Res.cleric(1), Res.fighter(1))));
        addQuest(new BasicQuest(Quests.RecruitPaladinsForTyr, 
                new QuestCost(Res.cleric(2), Res.fighter(4), Res.gold(4)), 
                new QuestReward(Res.cleric(3))));
        addQuest(new BasicQuest(Quests.SealGateToCyricsRealm, 
                new QuestCost(Res.cleric(2), Res.rogue(3), Res.gold(4)), 
                new QuestReward()));
        addQuest(new BasicQuest(Quests.BuildReputationInSkullport, 
                new QuestCost(Res.fighter(1), Res.rogue(3), Res.gold(4)), 
                new QuestReward(Res.intrigue(1))));
        addQuest(new BasicQuest(Quests.EstablishHarpersSafeHouse, 
                new QuestCost(Res.fighter(2), Res.rogue(3), Res.gold(2)), 
                new QuestReward()).andSetDescription("Whenever you buy a Building from Builder's Hall, gain 4 victory points"));
        addQuest(new BasicQuest(Quests.EstablishShadowThievesGuild, 
                new QuestCost(Res.fighter(1), Res.rogue(8), Res.wizard(1)), 
                new QuestReward()));
        addQuest(new BasicQuest(Quests.ExposeCultCorruption, 
                new QuestCost(Res.cleric(1), Res.rogue(4)), 
                new QuestReward(Res.cleric(2))));
        addQuest(new BasicQuest(Quests.PrisonBreak, 
                new QuestCost(Res.rogue(4), Res.wizard(2), Res.gold(2)), 
                new QuestReward(Res.fighter(2))).andSetDescription("Plat an " + ResourceConstants.MASK_INTRIGUE_DATA + " as an action"));
        addQuest(new BasicQuest(Quests.ProcureStolenGoods, 
                new QuestCost(Res.rogue(3), Res.gold(6)), 
                new QuestReward(Res.intrigue(2))));
        addQuest(new BasicQuest(Quests.RaidOnUndermountain, 
                new QuestCost(Res.cleric(1), Res.fighter(2), Res.rogue(4), Res.wizard(1)), 
                new QuestReward(Res.gold(2))));
        addQuest(new BasicQuest(Quests.StealFromHouseAdarbrent, 
                new QuestCost(Res.fighter(1), Res.rogue(4), Res.wizard(1)), 
                new QuestReward(Res.gold(6))));
        addQuest(new BasicQuest(Quests.TakeOverRivalOrganization, 
                new QuestCost(Res.fighter(1), Res.rogue(2), Res.wizard(1), Res.gold(6)), 
                new QuestReward(Res.rogue(4))));
        addQuest(new BasicQuest(Quests.AmbushArtorMorlin, 
                new QuestCost(Res.cleric(1), Res.fighter(3), Res.rogue(1)), 
                new QuestReward(Res.gold(4))));
        addQuest(new BasicQuest(Quests.BolsterCityGuard, 
                new QuestCost(Res.fighter(9), Res.rogue(2)), 
                new QuestReward()));
        addQuest(new BasicQuest(Quests.ConfrontTheXanathar, 
                new QuestCost(Res.cleric(1), Res.fighter(4), Res.rogue(2), Res.wizard(1)), 
                new QuestReward(Res.gold(2))));
        addQuest(new BasicQuest(Quests.DefeatUprisingFromUndermountain, 
                new QuestCost(Res.cleric(1), Res.fighter(3), Res.rogue(1), Res.gold(2)), 
                new QuestReward(Res.fighter(2))));
        addQuest(new BasicQuest(Quests.DeliverAnUltimatum, 
                new QuestCost(Res.fighter(4), Res.rogue(1), Res.wizard(1)), 
                new QuestReward(Res.gold(4))));
        addQuest(new BasicQuest(Quests.DeliverWeaponsToSelunesTemple, 
                new QuestCost(Res.fighter(4), Res.rogue(1), Res.wizard(1), Res.gold(2)), 
                new QuestReward(Res.cleric(2))));
        addQuest(new BasicQuest(Quests.RaidOrcStronghold, 
                new QuestCost(Res.fighter(4), Res.rogue(1), Res.wizard(1)), 
                new QuestReward(Res.gold(4))));
        addQuest(new BasicQuest(Quests.RepelSeawraiths, 
                new QuestCost(Res.cleric(1), Res.fighter(4), Res.wizard(1)), 
                new QuestReward(Res.gold(2))));
        addQuest(new BasicQuest(Quests.TrainBladesingers, 
                new QuestCost(Res.fighter(3), Res.wizard(1)), 
                new QuestReward(Res.fighter(1), Res.wizard(1))));
        // Start of plot quests.
        addQuest(new PlotQuest(Quests.ExploreAhghaironsTower, 
                new QuestCost(Res.fighter(1), Res.wizard(2), Res.gold(2)), 
                new QuestReward()).andSetDescription("Whenever you take an action that provides any " + ResourceConstants.MASK_WIZARD_DATA + ", also draw an Intrigue card"));
        addQuest(new PlotQuest(Quests.RecoverTheMagistersOrb, 
                new QuestCost(Res.rogue(3), Res.wizard(2)), 
                new QuestReward()).andSetDescription("Once per round, you can assign an Agent to a space containing an opponent's Agent"));
        addQuest(new PlotQuest(Quests.StudyTheIlluskArch, 
                new QuestCost(Res.cleric(1), Res.wizard(2)), 
                new QuestReward()).andSetDescription("Whenever you complete a Arcana quest, you score 2 Victory points"));
        addQuest(new PlotQuest(Quests.BribeTheShipwrights, 
                new QuestCost(Res.rogue(4), Res.wizard(1), Res.gold(4)), 
                new QuestReward()).andSetDescription("Whenever you take an action that provides any " + ResourceConstants.MASK_GOLD_DATA + ", also take " + ResourceConstants.MASK_ROGUE_DATA + " from the supply and place it in your Tavern"));
        addQuest(new PlotQuest(Quests.EstablishNewMerchantGuild, 
                new QuestCost(Res.cleric(1), Res.fighter(2), Res.gold(4)), 
                new QuestReward()).andSetDescription("Whenever you complete a Commerce Quest, you score 2 VP"));
        addQuest(new PlotQuest(Quests.InfiltrateBuildersHall, 
                new QuestCost(Res.fighter(2), Res.rogue(2), Res.gold(4)), 
                new QuestReward()).andSetDescription("Whenever you buy a Building, you score 4 VP"));
        addQuest(new PlotQuest(Quests.DefendTowerOfLuck, 
                new QuestCost(Res.cleric(2), Res.fighter(1), Res.rogue(1), Res.wizard(1)), 
                new QuestReward()).andSetDescription("At the start of each round, take any " + ResourceConstants.MASK_ANY_DATA + " from the supply and place it in your Tavern"));
        addQuest(new PlotQuest(Quests.ProduceMiracleForTheMasses, 
                new QuestCost(Res.cleric(2), Res.gold(4)), 
                new QuestReward()).andSetDescription("Once per round, whenever you take an action that provides a " + ResourceConstants.MASK_CLERIC_DATA + ", you can also replace a " + ResourceConstants.MASK_FIGHTER_DATA + ", " + ResourceConstants.MASK_ROGUE_DATA + ", or " + ResourceConstants.MASK_WIZARD_DATA + " in your Tavern with a " + ResourceConstants.MASK_CLERIC_DATA));
        addQuest(new PlotQuest(Quests.ProtectTheHouseOfWonder, 
                new QuestCost(Res.cleric(2), Res.fighter(1), Res.gold(2)), 
                new QuestReward()).andSetDescription("Whenever you complete a Piety Quest, you score 2 VP"));
        addQuest(new PlotQuest(Quests.FenceGoodsForDukeOfDarkness, 
                new QuestCost(Res.fighter(1), Res.rogue(3), Res.gold(4)), 
                new QuestReward()).andSetDescription("Whenever you take an action that provides any " + ResourceConstants.MASK_ROGUE_DATA + ", also take " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + " from the supply"));
        addQuest(new PlotQuest(Quests.InstallSpyInCastleWaterdeep, 
                new QuestCost(Res.rogue(4), Res.gold(4)), 
                new QuestReward()).andSetDescription("Whenever you complete a Skullduggery Quest, you score 2 VP"));
        addQuest(new PlotQuest(Quests.PlaceSleeperAgentInSkullport, 
                new QuestCost(Res.fighter(1), Res.rogue(4), Res.wizard(1)), 
                new QuestReward()).andSetDescription("Whenever you play Intrigue, you score 2 VP"));
        addQuest(new PlotQuest(Quests.BolsterGriffinCavalry, 
                new QuestCost(Res.fighter(4), Res.gold(4)), 
                new QuestReward()).andSetDescription("Whenever you take an action that provides any " + ResourceConstants.MASK_FIGHTER_DATA + ", also take 1 " + ResourceConstants.MASK_FIGHTER_DATA + " from supply and place it in your Tavern"));
        addQuest(new PlotQuest(Quests.QuellMercenaryUprising, 
                new QuestCost(Res.cleric(1), Res.fighter(4)), 
                new QuestReward()).andSetDescription("Whenever you complete a Warfare Quest, you score 2 VP"));
        addQuest(new PlotQuest(Quests.RecruitLieutenant, 
                new QuestCost(Res.cleric(1), Res.fighter(5), Res.rogue(1), Res.wizard(1)), 
                new QuestReward()).andSetDescription("Add the Lieutenant to your pool of Agents for the rest of the game"));
        
        // Undermountain quests.
        addQuest(new OptionsQuest(Quests.DealWithTheBlackViper, 
                new QuestCost(Res.fighter(1), Res.rogue(4), Res.wizard(2), Res.gold(5)), 
                new QuestReward(Res.intrigue(4))).andSetDescription("As you draw each card, you can play it immediately as part of completing this quest."));
        addQuest(new BasicQuest(Quests.ExploreTrobriandsGraveyard, 
                new QuestCost(Res.fighter(2), Res.rogue(3), Res.wizard(4), Res.gold(6)), 
                new QuestReward()));
        addQuest(new BasicQuest(Quests.ResurrectDeadWizards, 
                new QuestCost(Res.cleric(3), Res.gold(5)), 
                new QuestReward(Res.wizard(3))));
        addQuest(new OptionsQuest(Quests.SurviveMeetingWithHalaster, 
                new QuestCost(Res.cleric(1), Res.fighter(1), Res.anyResource(4)), 
                new QuestReward()).andSetDescription("and draw " + ResourceConstants.MASK_INTRIGUE_DATA + " for each " + ResourceConstants.MASK_WIZARD_DATA + " you used to complete this Quest"));
        addQuest(new BasicQuest(Quests.RansackWhitehelmsTomb, 
                new QuestCost(Res.cleric(2), Res.fighter(3), Res.rogue(4), Res.gold(10)), 
                new QuestReward()));
        addQuest(new OptionsQuest(Quests.RecruitForCityWatch, 
                new QuestCost(Res.cleric(1), Res.wizard(1)), 
                new QuestReward(),
                new QuestCost(Res.gold(10)), new QuestReward(Res.fighter(3), Res.rogue(3)))
                .andSetDescription("plus " + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + " if you immediately spend 10 " + ResourceConstants.MASK_GOLD_DATA));
        addQuest(new BasicQuest(Quests.StealGemsFromTheBoneThrone, 
                new QuestCost(Res.rogue(4), Res.wizard(2)), 
                new QuestReward(Res.gold(10))));
        addQuest(new OptionsQuest(Quests.ThreatenTheBuildersGuilds, 
                new QuestCost(Res.fighter(2), Res.rogue(4), Res.wizard(2), Res.gold(10)), 
                new QuestReward())
                .andSetDescription("and immediately put all face-up Buildings in Builder's Hall in play under your control at no cost."));
        addQuest(new OptionsQuest(Quests.EstablishTempleToIbrandul, 
                new QuestCost(Res.cleric(3), Res.rogue(4), Res.gold(5)), 
                new QuestReward(Res.cleric(1), Res.fighter(1), Res.rogue(1), Res.wizard(1)))
                .andSetDescription("and take all Quests from Cliffwatch Inn"));
        addQuest(new BasicQuest(Quests.PlunderTheIslandTemple, 
                new QuestCost(Res.cleric(5), Res.fighter(2), Res.rogue(2), Res.wizard(1)), 
                new QuestReward()));
        addQuest(new BasicQuest(Quests.RescueClericsOfTymora, 
                new QuestCost(Res.fighter(6), Res.wizard(2)), 
                new QuestReward(Res.cleric(3))));
        addQuest(new OptionsQuest(Quests.RootOutLoviatarsFaithful, 
                new QuestCost(Res.fighter(1), Res.wizard(1), Res.anyResource(4)), 
                new QuestReward())
                .andSetDescription("and gain 2 VP for each " + ResourceConstants.MASK_CLERIC_DATA + " you used to complete this quest"));
        addQuest(new BasicQuest(Quests.BreakIntoBlackstaffTower, 
                new QuestCost(Res.cleric(1), Res.rogue(7), Res.wizard(2), Res.gold(6)), 
                new QuestReward()));
        addQuest(new BasicQuest(Quests.DefendTheLanceboardRoom, 
                new QuestCost(Res.fighter(3), Res.rogue(6), Res.wizard(1), Res.gold(10)), 
                new QuestReward(Res.anyResource(8))).andSetDescription(" "));
        addQuest(new BasicQuest(Quests.SurviveArcturiasTransformarion, 
                new QuestCost(Res.fighter(6), Res.gold(5)), 
                new QuestReward(Res.rogue(6))));
        addQuest(new OptionsQuest(Quests.UnleashCrimeSpree, 
                new QuestCost(Res.cleric(1), Res.fighter(1), Res.rogue(1), Res.wizard(1)), 
                new QuestReward(),
                new QuestCost(Res.rogue(4)), new QuestReward(Res.victoryPoints(10)))
                .andSetDescription("plus 10 VP if you immediately return " + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + " from your Tavern to the supply"));
        addQuest(new BasicQuest(Quests.BattleInMuiralsGauntlet, 
                new QuestCost(Res.cleric(2), Res.fighter(7), Res.wizard(2), Res.gold(2)), 
                new QuestReward()));
        addQuest(new OptionsQuest(Quests.DefendTheYawningPortal, 
                new QuestCost(Res.cleric(3), Res.fighter(6), Res.rogue(1), Res.wizard(1)), 
                new QuestReward(Res.anyResource(2)))
                .andSetDescription("and return up to 3 of your agents to your pool"));
        addQuest(new OptionsQuest(Quests.DestroyTempleOfSelvetarm, 
                new QuestCost(Res.cleric(1), Res.fighter(2), Res.gold(2)), 
                new QuestReward(),
                new QuestCost(Res.fighter(4)), new QuestReward(Res.victoryPoints(10)))
                .andSetDescription("plus 10 VP if you immediately return " + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + " from your Tavern to the supply"));       
        addQuest(new BasicQuest(Quests.WakeTheSixSleepers, 
                new QuestCost(Res.cleric(3), Res.rogue(3)), 
                new QuestReward(Res.fighter(6))));
        
        addQuest(new PlotQuest(Quests.EstablishWizardAcademy, 
                new QuestCost(Res.cleric(1), Res.fighter(1), Res.rogue(3), Res.wizard(3)), 
                new QuestReward()).andSetDescription("Whenever you take an Arcana Quest, take " + ResourceConstants.MASK_WIZARD_DATA + " from the supply and place it in your Tavern"));
        addQuest(new PlotQuest(Quests.StudyInTheLibrarium, 
                new QuestCost(Res.rogue(2), Res.wizard(5), Res.gold(5)), 
                new QuestReward()).andSetDescription("After to take an action that allows you to play an intrigue card, draw an intrigue card. You can play that card immediately without taking another action."));
        addQuest(new PlotQuest(Quests.ObtainBuildersPlans, 
                new QuestCost(Res.fighter(2), Res.rogue(2), Res.wizard(1), Res.gold(10)), 
                new QuestReward()).andSetDescription("Once per round, you can assign an agent to a building in Builder's Hall"));
        addQuest(new PlotQuest(Quests.SponsorBountyHunters, 
                new QuestCost(Res.cleric(1), Res.fighter(4), Res.rogue(3), Res.gold(6)), 
                new QuestReward()).andSetDescription("Whenever you take a Commerce Quest, take " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + " from the supply and place it in your Tavern"));
        addQuest(new PlotQuest(Quests.DiplomaticMissionToSuzail, 
                new QuestCost(Res.cleric(3), Res.fighter(3), Res.gold(3)), 
                new QuestReward()).andSetDescription("You can complete any face-up Quest in Cliffwatch Inn as though they were active Quests"));
        addQuest(new PlotQuest(Quests.SanctifyDesecratedTemple, 
                new QuestCost(Res.cleric(4), Res.fighter(2), Res.gold(5)), 
                new QuestReward()).andSetDescription("Whenever you take a Piety Quest, take " + ResourceConstants.MASK_CLERIC_DATA + " from the supply and place it in your Tavern"));
        addQuest(new PlotQuest(Quests.AllyWithTheXanatharsGuild, 
                new QuestCost(Res.fighter(2), Res.rogue(5), Res.wizard(1), Res.gold(5)), 
                new QuestReward()).andSetDescription("Whenever you take a Skullduggery Quest, take " + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + " from the supply and place it in your Tavern"));
        addQuest(new PlotQuest(Quests.ImpersonateTaxCollector, 
                new QuestCost(Res.fighter(1), Res.rogue(4), Res.wizard(1), Res.gold(5)), 
                new QuestReward()).andSetDescription("Whenever you assign an agent to a Building you control, you also gain the owner benefit"));
        addQuest(new PlotQuest(Quests.RecoverTheFlameOfTheNorth, 
                new QuestCost(Res.fighter(5), Res.rogue(3), Res.wizard(1), Res.gold(3)), 
                new QuestReward()).andSetDescription("Whenever you take a Warfare Quest, take " + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + " from the supply and place it in your Tavern"));
        addQuest(new PlotQuest(Quests.SeizeControlOfTheBloodyHand, 
                new QuestCost(Res.cleric(1), Res.fighter(4), Res.wizard(2)), 
                new QuestReward()).andSetDescription("Whenever you complete a Quest, you may return " + ResourceConstants.MASK_ANY_DATA + " used to complete the Quest to your Tavern"));
        
        // Skullport
        addQuest(new BasicQuest(Quests.EstablishCultCell, new QuestCost(Res.rogue(2), Res.wizard(2), Res.gold(3)),
                new QuestReward(Res.fighter(2), Res.wizard(1), Res.intrigue(1), Res.corruption(3))));
        addQuest(new BasicQuest(Quests.InvestigateThayanVessel, new QuestCost(Res.cleric(1), Res.fighter(2), Res.rogue(2), Res.wizard(2), Res.gold(2)),
                new QuestReward(Res.anyResource(2), Res.intrigue(2))));
        addQuest(new BasicQuest(Quests.RenewGuardsAndWards, new QuestCost(Res.cleric(1), Res.fighter(1), Res.wizard(2), Res.gold(2)),
                new QuestReward()).andSetDescription(" and return up to " + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " from your Tavern to the Corruption track."));
        addQuest(new BasicQuest(Quests.SealAnEntranceToSkullport, new QuestCost(Res.cleric(1), Res.fighter(2), Res.rogue(2), Res.wizard(2)),
                new QuestReward()).andSetDescription(" and return up to " + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " from your Tavern to the Corruption track."));
        addQuest(new BasicQuest(Quests.DonateToTheCity, new QuestCost(Res.cleric(2), Res.wizard(1), Res.gold(10)),
                new QuestReward()).andSetDescription(" and return up to " + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " from your Tavern to the Corruption track."));
        addQuest(new BasicQuest(Quests.FundAlchemicalResearch, new QuestCost(Res.cleric(1), Res.rogue(1), Res.wizard(2), Res.gold(4)),
                new QuestReward(Res.gold(12), Res.corruption(3))));
        addQuest(new BasicQuest(Quests.FundPilgrimageOfWaukeen, new QuestCost(Res.cleric(1), Res.fighter(2), Res.rogue(2), Res.gold(5)),
                new QuestReward()));
        addQuest(new BasicQuest(Quests.PayFines, new QuestCost(Res.cleric(1), Res.rogue(3), Res.gold(4)),
                new QuestReward()).andSetDescription(" and return up to " + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " from your Tavern to the Corruption track."));
        addQuest(new BasicQuest(Quests.BanishEvilSpirits, new QuestCost(Res.cleric(2), Res.fighter(2), Res.wizard(1)),
                new QuestReward(Res.anyResource(1))).andSetDescription(ResourceConstants.MASK_ANY_DATA + " and return up to " + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " from your Tavern to the Corruption track."));
        addQuest(new BasicQuest(Quests.EnterTheTowerOfSevenWoes, new QuestCost(Res.anyResource(7)),
                new QuestReward(Res.cleric(3), Res.corruption(4))));
        addQuest(new BasicQuest(Quests.InstituteReforms, new QuestCost(Res.cleric(4), Res.fighter(1), Res.rogue(1), Res.gold(2)),
                new QuestReward()).andSetDescription(" and return up to " + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " from your Tavern to the Corruption track."));
        addQuest(new BasicQuest(Quests.SanctifyTempleToOghma, new QuestCost(Res.cleric(2), Res.wizard(1), Res.gold(5)),
                new QuestReward()));
        addQuest(new BasicQuest(Quests.BuryTheBodies, new QuestCost(Res.fighter(2), Res.rogue(3), Res.gold(2)),
                new QuestReward(Res.corruption(2))));
        addQuest(new BasicQuest(Quests.RescueVictimFromTheSkulls, new QuestCost(Res.fighter(2), Res.rogue(4), Res.wizard(1)),
                new QuestReward(Res.anyResource(1))).andSetDescription(ResourceConstants.MASK_ANY_DATA + " and return up to " + ResourceConstants.MASK_CORRUPTION_DATA + " from your Tavern to the Corruption track."));
        addQuest(new BasicQuest(Quests.SaveKidnappedNobles, new QuestCost(Res.rogue(6), Res.wizard(2), Res.gold(5)),
                new QuestReward(Res.fighter(4))).andSetDescription(" and return up to " + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " from your Tavern to the Corruption track."));
        addQuest(new BasicQuest(Quests.SwindleTheBuildersGuilds, new QuestCost(Res.cleric(1), Res.fighter(2), Res.rogue(3), Res.gold(5)),
                new QuestReward(Res.corruption(3))).andSetDescription(" and put 2 face-up Buildings in Builders' Hall in play under your control at no cost."));
        addQuest(new BasicQuest(Quests.AssassinateRivals, new QuestCost(Res.fighter(4), Res.rogue(1)),
                new QuestReward(Res.corruption(2))).andSetDescription("Each opponent returns " + ResourceConstants.MASK_ANY_DATA + " from his or her Tavern to the supply."));
        addQuest(new BasicQuest(Quests.ImprovePrisonSecurity, new QuestCost(Res.cleric(1), Res.fighter(4), Res.rogue(2), Res.gold(4)),
                new QuestReward()).andSetDescription(" and remove up to " + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " in your Tavern from the game."));
        addQuest(new BasicQuest(Quests.PatrolDockWard, new QuestCost(Res.cleric(1), Res.fighter(3), Res.rogue(2), Res.gold(2)),
                new QuestReward(Res.rogue(4))));
        addQuest(new BasicQuest(Quests.UncoverDrowPlot, new QuestCost(Res.cleric(1), Res.fighter(5), Res.rogue(2), Res.gold(5)),
                new QuestReward()).andSetDescription(" and return up to " + ResourceConstants.MASK_CORRUPTION_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " from your Tavern to the Corruption track."));
        addQuest(new PlotQuest(Quests.RecruitAcademyCastoffs, new QuestCost(Res.fighter(1), Res.wizard(1), Res.gold(2)), 
                new QuestReward()).andSetDescription("Whenever you assign an agent to Blackstaff Tower, you can take another " + ResourceConstants.MASK_WIZARD_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " and place them in your Tavern."));
        addQuest(new PlotQuest(Quests.UncoverForbiddenLore, new QuestCost(Res.rogue(2), Res.wizard(3)), 
                new QuestReward()).andSetDescription("Whenever you assign an agent to Waterdeep Harbor, you can play up to another " + ResourceConstants.MASK_INTRIGUE_DATA + ResourceConstants.MASK_INTRIGUE_DATA + ". If you do so, take " + ResourceConstants.MASK_CORRUPTION_DATA + " from the Corruption track and place it in your Tavern."));
        addQuest(new PlotQuest(Quests.DefameRivalBusiness, new QuestCost(Res.cleric(1), Res.fighter(2), Res.rogue(2), Res.gold(4)), 
                new QuestReward()).andSetDescription("Whenever you purchase a Building, you can remove " + ResourceConstants.MASK_CORRUPTION_DATA + " from your Tavern and place it on any action space."));
        addQuest(new PlotQuest(Quests.ExtortAurora, new QuestCost(Res.rogue(2), Res.gold(4)), 
                new QuestReward()).andSetDescription("Whenever you assign an agent to Aurora's Realms Shop, you can take another " + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_GOLD_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " and place them in your Tavern."));
        addQuest(new PlotQuest(Quests.GiveHonorToMask, new QuestCost(Res.cleric(1), Res.rogue(1), Res.gold(2)), 
                new QuestReward()).andSetDescription("Whenever you assign an agent to the Plinth, you can take another " + ResourceConstants.MASK_CLERIC_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " and place them in your Tavern."));
        addQuest(new PlotQuest(Quests.ProtectConvertsToEilistraee, new QuestCost(Res.cleric(3), Res.fighter(2), Res.wizard(1)), 
                new QuestReward()).andSetDescription("Once on your turn, whenever you return any " + ResourceConstants.MASK_CORRUPTION_DATA + " from your Tavern to the Corruption track, you can return another "+ ResourceConstants.MASK_CORRUPTION_DATA + "."));
        addQuest(new PlotQuest(Quests.ExpandGuildActivities, new QuestCost(Res.fighter(1), Res.rogue(2), Res.gold(2)), 
                new QuestReward()).andSetDescription("Whenever you assign an agent to the Grinning Lion Tavern, you can take another " + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_ROGUE_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " and place them in your Tavern."));
        addQuest(new PlotQuest(Quests.ShelterZhentarimAgents, new QuestCost(Res.cleric(1), Res.fighter(3), Res.wizard(2)), 
                new QuestReward(Res.corruption(1))).andSetDescription(ResourceConstants.MASK_CORRUPTION_DATA + "<br>Once on your turn, whenever you gain any " + ResourceConstants.MASK_CORRUPTION_DATA + ", draw " + ResourceConstants.MASK_INTRIGUE_DATA));
        addQuest(new PlotQuest(Quests.FixTheChampionsGames, new QuestCost(Res.fighter(3), Res.gold(2)), 
                new QuestReward()).andSetDescription("Whenever you assign an agent to the Field of Triumph, you can take another " + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_FIGHTER_DATA + ResourceConstants.MASK_CORRUPTION_DATA + " and place them in your Tavern."));
        addQuest(new PlotQuest(Quests.TrainCastleguards, new QuestCost(Res.fighter(2), Res.wizard(1), Res.gold(5)), 
                new QuestReward()).andSetDescription("Whenever any player takes the first turn marker, you and that "));

        addMandatoryQuest(new MandatoryQuest(Quests.FendOffBandits, new QuestCost(Res.fighter(2), Res.wizard(1)), new QuestReward()));
        addMandatoryQuest(new MandatoryQuest(Quests.FoilTheZhentarim, new QuestCost(Res.fighter(1), Res.rogue(1), Res.wizard(1)), new QuestReward()));
        addMandatoryQuest(new MandatoryQuest(Quests.PlacateAngryMerchants, new QuestCost(Res.cleric(1), Res.fighter(1), Res.wizard(1)), new QuestReward()));
        addMandatoryQuest(new MandatoryQuest(Quests.QuellRiots, new QuestCost(Res.cleric(2), Res.fighter(1)), new QuestReward()));
        addMandatoryQuest(new MandatoryQuest(Quests.RepelDrowInvaders, new QuestCost(Res.cleric(1), Res.rogue(2)), new QuestReward()));
        addMandatoryQuest(new MandatoryQuest(Quests.StampOutCultists, new QuestCost(Res.cleric(1), Res.fighter(1), Res.rogue(1)), new QuestReward()));
        addMandatoryQuest(new MandatoryQuest(Quests.EvadeAssassins, new QuestCost(Res.rogue(2), Res.wizard(1)), new QuestReward(Res.intrigue(1))));
        addMandatoryQuest(new MandatoryQuest(Quests.SubdueIllithidMenace, new QuestCost(Res.cleric(1), Res.fighter(2)), new QuestReward(Res.intrigue(1))));
        addMandatoryQuest(new MandatoryQuest(Quests.UnveilAbyssalAgent, new QuestCost(Res.cleric(1), Res.wizard(1)), new QuestReward(Res.intrigue(1))));
        addMandatoryQuest(new MandatoryQuest(Quests.ClearRustMonsterNest, new QuestCost(Res.fighter(1), Res.wizard(1), Res.gold(2)), new QuestReward()));
        addMandatoryQuest(new MandatoryQuest(Quests.CoverUpScandal, new QuestCost(Res.cleric(1), Res.fighter(1), Res.rogue(1), Res.wizard(1)), new QuestReward()));
        addMandatoryQuest(new MandatoryQuest(Quests.HuntHiddenGhoul, new QuestCost(Res.cleric(1), Res.rogue(1), Res.gold(2)), new QuestReward()));
        
    }
    
    private void addQuest(Quest quest) {
        if(expansions.contains(quest.getQuestInfo().getExpansion())) {
            if(StringHelper.isEmpty(quest.getDescription()) && quest.getQuestInfo().getQuestAction() != QuestActions.None) {
                throw new UnsupportedOperationException("Quest " + quest.getQuestInfo().getName() + " requires a description");
            }
            quests.add(quest);
        }
    }

    private void addMandatoryQuest(MandatoryQuest quest) {
        if(expansions.contains(quest.getQuestInfo().getExpansion())) {
            mandatoryQuests.add(quest);
        }
    }

    public List<Quest> getQuests() {
        return quests;
    }

    public List<Quest> getMandatoryQuests() {
        return mandatoryQuests;
    }
    
    
}

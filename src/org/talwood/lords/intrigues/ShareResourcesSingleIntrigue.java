package org.talwood.lords.intrigues;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.ResourceChoices;

public class ShareResourcesSingleIntrigue extends Intrigue {

    private List<ResourceChoices> playerChoices = new ArrayList<ResourceChoices>();
    private List<ResourceChoices> opponentsChoices = new ArrayList<ResourceChoices>();
    
    public ShareResourcesSingleIntrigue(Intrigues intrigueInfo, ResourceChoices playerTakes, ResourceChoices opponentTakes) {
        super(intrigueInfo);
        playerChoices.add(playerTakes);
        opponentsChoices.add(opponentTakes);
    }
    
    public ShareResourcesSingleIntrigue(Intrigues intrigueInfo, ResourceChoices[] playerTakes, ResourceChoices[] allOpponentsTake) {
        super(intrigueInfo);
        for(ResourceChoices rs : playerTakes) {
            playerChoices.add(rs);
        }
        for(ResourceChoices rs : allOpponentsTake) {
            opponentsChoices.add(rs);
        }
    }
    
    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.ShareResourcesSingle;
    }

    public List<ResourceChoices> getPlayerChoices() {
        return playerChoices;
    }

    public List<ResourceChoices> getOpponentsChoices() {
        return opponentsChoices;
    }

    
}

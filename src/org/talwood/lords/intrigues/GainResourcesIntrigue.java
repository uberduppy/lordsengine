package org.talwood.lords.intrigues;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.ResourceChoices;

public class GainResourcesIntrigue extends Intrigue {
    
    private List<ResourceChoices> choices = new ArrayList<ResourceChoices>();

    public GainResourcesIntrigue(Intrigues intrigueInfo, ResourceChoices... choices) {
        super(intrigueInfo);
        for(ResourceChoices rc : choices) {
            this.choices.add(rc);
        }
        
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.GainResources;
    }

    public List<ResourceChoices> getChoices() {
        return choices;
    }
    
    
}

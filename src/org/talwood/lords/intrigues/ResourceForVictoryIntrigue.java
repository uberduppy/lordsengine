package org.talwood.lords.intrigues;

import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.ResourceChoices;

public class ResourceForVictoryIntrigue extends Intrigue {

    private ResourceChoices ownerReward;
    private ResourceChoices opponentPayment;
    private int victoryPoints;
    
    public ResourceForVictoryIntrigue(Intrigues intrigueInfo, ResourceChoices ownerReward, ResourceChoices opponentPayment, int victoryPoints) {
        super(intrigueInfo);
        this.ownerReward = ownerReward;
        this.opponentPayment = opponentPayment;
        this.victoryPoints = victoryPoints;
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.ResourceForVP;
    }

    public ResourceChoices getOwnerReward() {
        return ownerReward;
    }

    public ResourceChoices getOpponentPayment() {
        return opponentPayment;
    }

    public int getVictoryPoints() {
        return victoryPoints;
    }
    
    
}

package org.talwood.lords.intrigues;

import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.resources.Resources;

public class OptionalResourceIntrigue extends Intrigue {

    private Resources ownerReward;
    private Resources optionalReward;
    
    public OptionalResourceIntrigue(Intrigues intrigueInfo, Resources ownerReward, Resources optionalReward) {
        super(intrigueInfo);
        this.ownerReward = ownerReward;
        this.optionalReward = optionalReward;
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.OptionalResources;
    }

    public Resources getOwnerReward() {
        return ownerReward;
    }

    public Resources getOptionalReward() {
        return optionalReward;
    }
}

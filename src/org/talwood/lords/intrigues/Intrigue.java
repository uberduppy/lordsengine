package org.talwood.lords.intrigues;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.IntrigueActions;
import org.talwood.lords.enums.Intrigues;

public abstract class Intrigue {

    private List<IntrigueActions> intrigueActions = new ArrayList<IntrigueActions>();
    private Intrigues intrigueInfo;

    public abstract IntrigueActionTypes gimmeDefinedType();
    
    private Intrigue() {}
    // This will be the new basis for intrigue cards
    protected Intrigue(Intrigues intrigueInfo) {
        this.intrigueInfo = intrigueInfo;
        if(intrigueInfo.getActionType() != gimmeDefinedType()) {
            throw new UnsupportedOperationException("Action type mismatch for " + intrigueInfo.getName());
        }
    }
    protected Intrigue(Intrigues intrigueInfo, IntrigueActions... actions) {
        this.intrigueInfo = intrigueInfo;
        for(IntrigueActions a : actions) {
            this.intrigueActions.add(a);
        }
        if(intrigueInfo.getActionType() != gimmeDefinedType()) {
            throw new UnsupportedOperationException("Action type mismatch for " + intrigueInfo.getName());
        }
    }

    public List<IntrigueActions> getIntrigueActions() {
        return intrigueActions;
    }

    public Intrigues getIntrigueInfo() {
        return intrigueInfo;
    }
}

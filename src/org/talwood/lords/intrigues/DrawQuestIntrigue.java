package org.talwood.lords.intrigues;

import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;

public class DrawQuestIntrigue extends Intrigue {

    public DrawQuestIntrigue(Intrigues intrigueInfo) {
        super(intrigueInfo);
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.DrawQuests;
    }
}

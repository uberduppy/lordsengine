package org.talwood.lords.intrigues;

import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.resources.Resources;

public class GiveResourceForVPIntrigue extends Intrigue {
    
    private int victoryPoints;
    private Resources resGiven;

    public GiveResourceForVPIntrigue(Intrigues intrigueInfo, Resources resGiven, int victoryPoints) {
        super(intrigueInfo);
        this.resGiven = resGiven;
        this.victoryPoints = victoryPoints;
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.GiveResourceForVP;
    }

    public int getVictoryPoints() {
        return victoryPoints;
    }

    public Resources getResGiven() {
        return resGiven;
    }
}

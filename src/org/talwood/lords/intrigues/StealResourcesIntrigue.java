package org.talwood.lords.intrigues;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.IntrigueActions;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.ResourceChoices;

public class StealResourcesIntrigue extends Intrigue {
    
    List<ResourceChoices> choices = new ArrayList<ResourceChoices>();

    public StealResourcesIntrigue(Intrigues intrigueInfo, ResourceChoices... choices) {
        super(intrigueInfo);
        for(ResourceChoices rc : choices) {
            this.choices.add(rc);
        }
    }
    
    public StealResourcesIntrigue(Intrigues intrigueInfo, IntrigueActions... actions) {
        super(intrigueInfo, actions);
    }
    
    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.StealResources;
    }

    public List<ResourceChoices> getChoices() {
        return choices;
    }
    
}

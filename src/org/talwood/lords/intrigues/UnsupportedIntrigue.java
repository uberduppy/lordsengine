package org.talwood.lords.intrigues;

import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;

public class UnsupportedIntrigue extends Intrigue {

    public UnsupportedIntrigue(Intrigues intrigueInfo) {
        super(intrigueInfo);
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        // For now, return the one handed in
        return getIntrigueInfo().getActionType();
    }
}

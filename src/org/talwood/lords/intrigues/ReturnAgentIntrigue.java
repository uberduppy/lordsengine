package org.talwood.lords.intrigues;

import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;

public class ReturnAgentIntrigue extends Intrigue {
    
    public ReturnAgentIntrigue(Intrigues intrigueInfo) {
        super(intrigueInfo);
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.ReturnAgent;
    }

}

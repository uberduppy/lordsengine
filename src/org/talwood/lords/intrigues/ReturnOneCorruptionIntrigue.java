package org.talwood.lords.intrigues;

import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;

public class ReturnOneCorruptionIntrigue extends Intrigue {

    public ReturnOneCorruptionIntrigue(Intrigues intrigueInfo) {
        super(intrigueInfo);
    }
    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.ReturnOneCorruption; 
    }

}

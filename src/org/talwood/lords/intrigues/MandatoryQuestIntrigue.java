package org.talwood.lords.intrigues;

import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.resources.MandatoryQuestCost;
import org.talwood.lords.resources.MandatoryQuestReward;

public class MandatoryQuestIntrigue extends Intrigue {

    private MandatoryQuestCost questCost;
    private MandatoryQuestReward questReward;
    private int victoryPoints;
    public MandatoryQuestIntrigue(Intrigues intrigueInfo, MandatoryQuestCost questCost, int victoryPoints) {
        super(intrigueInfo);
        this.questCost = questCost;
        this.victoryPoints = victoryPoints;
    }

    public MandatoryQuestIntrigue(Intrigues intrigueInfo, MandatoryQuestCost questCost, MandatoryQuestReward questReward) {
        super(intrigueInfo);
        this.questCost = questCost;
        this.questReward = questReward;
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.MandatoryQuest;
    }

    public MandatoryQuestCost getQuestCost() {
        return questCost;
    }

    public int getVictoryPoints() {
        return victoryPoints;
    }
}

package org.talwood.lords.intrigues;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.ResourceChoices;

public class ReallocateResourcesIntrigue extends Intrigue {

    private ResourceChoices reallocated = null;
    
    public ReallocateResourcesIntrigue(Intrigues intrigueInfo, ResourceChoices resReallocated) {
        super(intrigueInfo);
        reallocated = resReallocated;
    }

    public ResourceChoices getReallocated() {
        return reallocated;
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.ReallocateResources;
    }
}

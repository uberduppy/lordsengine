package org.talwood.lords.intrigues;

import java.util.ArrayList;
import java.util.List;
import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.enums.ResourceChoices;

public class ReturnTwoCorruptionAndResourceIntrigue extends Intrigue {
    
    private List<ResourceChoices> options = new ArrayList<ResourceChoices>();
    public ReturnTwoCorruptionAndResourceIntrigue(Intrigues intrigueInfo) {
        super(intrigueInfo);
        options.add(ResourceChoices.OneClericTwoCorruption);
        options.add(ResourceChoices.OneFighterTwoCorruption);
        options.add(ResourceChoices.OneRogueTwoCorruption);
        options.add(ResourceChoices.OneWizardTwoCorruption);
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.TwoCorruptionAndResource;
    }

    public List<ResourceChoices> getOptions() {
        return options;
    }
}

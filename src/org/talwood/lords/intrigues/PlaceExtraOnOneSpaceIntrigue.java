package org.talwood.lords.intrigues;

import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.resources.Resources;

public class PlaceExtraOnOneSpaceIntrigue extends Intrigue {

    private Resources reward;
    private Resources extra;
    
    public PlaceExtraOnOneSpaceIntrigue(Intrigues intrigueInfo, Resources reward, Resources extra) {
        super(intrigueInfo);
        this.reward = reward;
        this.extra = extra;
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.PlaceExtraOnOneSpace;
    }

    public Resources getReward() {
        return reward;
    }

    public Resources getExtra() {
        return extra;
    }
}

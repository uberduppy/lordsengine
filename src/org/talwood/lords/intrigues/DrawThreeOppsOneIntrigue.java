package org.talwood.lords.intrigues;

import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;

public class DrawThreeOppsOneIntrigue extends Intrigue {

    public DrawThreeOppsOneIntrigue(Intrigues intrigueInfo) {
        super(intrigueInfo);
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.DrawThreeIntrigueOppsDrawOne;
    }
}

package org.talwood.lords.intrigues;

import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;
import org.talwood.lords.resources.Resources;

public class BuyBuildingIntrigue extends Intrigue {
    private Resources reward;
    public BuyBuildingIntrigue(Intrigues intrigueInfo, Resources reward) {
        super(intrigueInfo);
        this.reward = reward;
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.BuyBuilding;
    }

    public Resources getReward() {
        return reward;
    }
}

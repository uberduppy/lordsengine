package org.talwood.lords.intrigues;

import org.talwood.lords.enums.IntrigueActionTypes;
import org.talwood.lords.enums.Intrigues;

public class VictoryForCorruptionIntrigue extends Intrigue {

    public VictoryForCorruptionIntrigue(Intrigues intrigueInfo) {
        super(intrigueInfo);
    }

    @Override
    public IntrigueActionTypes gimmeDefinedType() {
        return IntrigueActionTypes.VictoryForCorruption;
    }
}
